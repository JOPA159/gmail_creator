# %%
####!!!!FINAL!!!!!
import ctypes,  json, time, win32api, win32gui, win32process, win32con
from ctypes import wintypes
from win32con import (GWL_EXSTYLE, GWL_STYLE, HWND_TOPMOST, SW_HIDE,
					  SW_MAXIMIZE, SW_NORMAL, SW_SHOW, SWP_NOMOVE, SWP_NOSIZE,
					  WS_EX_APPWINDOW, WS_EX_TOOLWINDOW, WS_VISIBLE, WM_CLOSE)

EnumWindows = ctypes.windll.user32.EnumWindows
EnumWindowsProc = ctypes.WINFUNCTYPE(ctypes.c_bool, ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int))
GetWindowText = ctypes.windll.user32.GetWindowTextW
GetWindowTextLength = ctypes.windll.user32.GetWindowTextLengthW
IsWindowVisible = ctypes.windll.user32.IsWindowVisible

def find_window(name):
	try:
		return win32gui.FindWindow(None, name)
	except win32gui.error:
		print("Error while finding the window")
		return None

'''def get_window_id(self,name):
	titles = []
	special = name
	try:
		def foreach_window(hwnd):
			if IsWindowVisible(hwnd):
				length = GetWindowTextLength(hwnd)
				
				buff = ctypes.create_unicode_buffer(length + 1)
				
				GetWindowText(hwnd, buff, length + 1)
				print(buff.value)
				if special in buff.value:
					data = f"{str(self.find_window(buff.value))}###{buff.value}"
					print(data)
					titles.append(data)
					
					print(title)
					#titles.append(buff.value)
			
		EnumWindows(EnumWindowsProc(foreach_window), 0)
		return titles
	except:
		print("Error while finding the window")
		return None'''

def get_window_id(name):
	try:
		titles = []
		def callback(hwnd, _):
			if IsWindowVisible(hwnd):
				nonlocal titles
				length = GetWindowTextLength(hwnd)     
				buff = ctypes.create_unicode_buffer(length + 1)
				GetWindowText(hwnd, buff, length + 1)

				if name in buff.value:
					data = f"{str(find_window(buff.value))}###{buff.value}"
					titles.append(data)

					titles.append(buff.value)

		win32gui.EnumWindows(callback, None)
		return titles
	except:
		print("Error while hiding the window")
		return None 

def check_if_window_exists(hw):
	try:
		def callback(hwnd, _):
			length = GetWindowTextLength(hwnd)     
			buff = ctypes.create_unicode_buffer(length + 1)
			GetWindowText(hwnd, buff, length + 1)

			if int(hw) == find_window(buff.value):
				return True
				
		win32gui.EnumWindows(callback, None)
	except:
		print("Error while checking the window")
		return None 

def hide_from_taskbar(hw):
	try:
		def callback():
			win32gui.ShowWindow(hw, 0)

			win32gui.SetWindowLong(hw, GWL_EXSTYLE,win32gui.GetWindowLong(hw, GWL_EXSTYLE)| WS_EX_TOOLWINDOW)
					  
			#win32gui.ShowWindow(hw, 5)
		 
		callback() 
		return True
	except:
		print("Error while hiding the window")
		return None   
	

def maximize_from_taskbar(hw):
	try:
		def callback():
			win32gui.ShowWindow(hw, 5)
			win32gui.ShowWindow(hw, SW_MAXIMIZE)
			win32gui.SetForegroundWindow(hw)
		callback() 
		return True
	except:
		print("Error while hiding the window")
		return None   

def check_if_visible(hw):
	try:
		def callback():
			win32gui.IsWindowVisible(hw)
		callback() 
		return True
	except:
		print("Error while hiding the window")
		return None   

def kill_window(hw):
	try:
		if check_if_window_exists(hw):
			def callback():
				win32gui.PostMessage(int(hw), WM_CLOSE,0,0)
			
			callback() 
			return True
	except:
		print("Error while killing the window")
		return None          
	

# %%

#win32gui.ShowWindow(3539124, 5)
#win32gui.ShowWindow(3539124, SW_MAXIMIZE)
# %%

#WindowManager.get_window_id(" ")
#window.find_window_for_pid("Chrome")

# %%
#kill_window(984910)
#win32gui.PostMessage(1969282,win32con.WM_CLOSE,0,0)

# %%

# %%
