#%%
import datetime, json, os, sqlite3, sys, requests, os.path

path_to_myproject1 = os.path.abspath(__file__+"/../../win_lib")
sys.path.insert(0, path_to_myproject1)

import gsmc

pathToDb = os.getenv('APPDATA')+"/KOALABOTsystem/DBemails.db"

conn = sqlite3.connect(pathToDb, timeout=300)  # You can create a new database by changing the name within the quotes
c = conn.cursor()

resultQuery = c.execute(f'''select key,email_secret from PROVIDERS where provider='GETSMSCODE' ''')	
credentials = resultQuery.fetchall()
apiKey = credentials[0][0]
email = credentials[0][1]

#apiKey = 'c2474b22db0cb13091d17f58648b780b'
#email = 'tomas.prokop95@gmail.com'
api = gsmc.getsmscode(email, apiKey)

c.execute(f'''update PROVIDERS set balance = {api.get_balance()}, updated_at = datetime('now') where provider='GETSMSCODE' ''')
conn.commit()

conn.close()

print(api.get_balance())
# %%
