
#%%
import datetime, json, os, sqlite3, sys, requests
pathToDb = os.getenv('APPDATA')+"/KOALABOTsystem/DBemails.db"

conn = sqlite3.connect(pathToDb, timeout=300)  # You can create a new database by changing the name within the quotes
c = conn.cursor()

resultQuery = c.execute(f'''select key,email_secret from PROVIDERS where provider='LUCKYSMS' ''')	
credentials = resultQuery.fetchall()
apiKey = credentials[0][0]
secret = credentials[0][1]

#potiahne za z db
#apiKey = 'ea30b1a5bc00b08f78dbae89'

url = 'https://lucky-sms.com:4008/api/v1/profile/getBalance'
headers = {'Authorization': apiKey}

response = requests.post(url, headers=headers, verify=False)

balance_str = response.json()
data = balance_str

json_str = json.dumps(data)

resp = json.loads(json_str)

c.execute(f'''update PROVIDERS set balance = {resp['balance']}, updated_at = datetime('now') where provider='LUCKYSMS' ''')
conn.commit()

conn.close()
print(resp['balance'])

# %%
