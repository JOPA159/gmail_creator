import datetime, json, os, sqlite3, sys, requests

pathToDb = os.getenv('APPDATA')+"/KOALABOTsystem/DBemails.db"

conn = sqlite3.connect(pathToDb, timeout=300)  # You can create a new database by changing the name within the quotes
c = conn.cursor()

resultQuery = c.execute(f'''select key,email_secret from PROVIDERS where provider='PVACODES' ''')	
credentials = resultQuery.fetchall()
apiKey = credentials[0][0]
secret = credentials[0][1]

#potiahne za z db
#apiKey = '2215e14f7ece529a6819'

balance = requests.get('https://pvacodes.com/user/api/get_balance.php?customer={}'.format(apiKey))

balance_str = balance.json()
data = balance_str

json_str = json.dumps(data)

resp = json.loads(json_str)

c.execute(f'''update PROVIDERS set balance = {resp['balance']}, updated_at = datetime('now') where provider='PVACODES' ''')
conn.commit()

conn.close()

print(resp['balance'])