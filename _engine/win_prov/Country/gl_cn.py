#%%
import requests
import json
import sys
import datetime
import os.path
import sys

path_to_myproject1 = os.path.abspath(__file__+"/../../../")
sys.path.insert(0, path_to_myproject1)

from litedbf import DBfunctions

check_if_table_exist = DBfunctions.check_if_table_exist("COUNTRY")

if not check_if_table_exist:
	DBfunctions.create_table('COUNTRY', '([id] INTEGER PRIMARY KEY,[created] date, [updated] date, [name] text, [fullname] text, [prefix] text )')
else:
	DBfunctions.delete_table('COUNTRY')    
	DBfunctions.create_table('COUNTRY', '([id] INTEGER PRIMARY KEY,[created] date, [updated] date, [name] text, [fullname] text, [prefix] text )')

url = 'http://country.io/names.json'

response = requests.get(url)

balance_str = response.json()
data = balance_str

json_str = json.dumps(data)

resp = json.loads(json_str)

for name, fullname in resp.items():
	today = datetime.datetime.now()
	values = ("'{}','{}', '{}'").format(today, fullname, name)
	columns = "created, fullname, name"
	DBfunctions.insert_into_table('COUNTRY', columns, values)

url2 = 'http://country.io/phone.json'
response2 = requests.get(url2)

balance_str2 = response2.json()
data2 = balance_str2

json_str2 = json.dumps(data2)

resp2 = json.loads(json_str2)

for name, prefix in resp2.items():
	prefix = prefix.replace("+", "")
	setPrefix = "prefix = '{}'".format(prefix)
	where = "name = '{}'".format(name)
	DBfunctions.update_table('COUNTRY', setPrefix, where)

#resultQuery = DBfunctions.select_from_table("COUNTRY","name,fullname","1=1")	
#print(resultQuery)
print("Updating countries done!")

# %%
