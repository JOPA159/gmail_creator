#%%
from litedbf import DBfunctions
import datetime
import sys

recovery_domain = str(sys.argv[1])

check_if_table_exist = DBfunctions.check_if_table_exist('RECOVERY_DOMAIN')
if not check_if_table_exist:
	DBfunctions.create_table('RECOVERY_DOMAIN', '([id] INTEGER PRIMARY KEY,[created] date, [recovery_domain] text)')

#premenna z inputu
#recovery_domain = '@procky.com'
columns = "recovery_domain, created"
today = datetime.datetime.now()
values = ("'{}','{}'").format(recovery_domain, today)
DBfunctions.insert_into_table('RECOVERY_DOMAIN', columns, values)

print('Success')
# %%
