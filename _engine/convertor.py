from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import os,shutil,glob,re
build_mode = ''

names = [
        'zamikanie',
        'vyplnucet',
        'vyt_rec_domem',
        'litedbf',
		'krajnahod',
        'ulohagen',
        'cachegcgs',
        'cachegcls',
        'cachegcpc',
        'cachegcsp',
        'sv2',
        'sv3',
        'logger',
        'setpx',
        'FM/efmx',
        'FXW/efwx',
        'HG/CTMP',
        'HG/Ehdr',
        'HG/Kwdw',
        'HG/Mzrx',
        'HG/maxmin',
        'PSK/poskyt',
        'PXY/ipmpxy',
        'PXY/pxxy',
        'win_prov/Country/gl_cn',
        'win_prov/Country/l_s_cntr',
        'win_prov/win_cr/gsmc_c',
        'win_prov/win_cr/l_s_c',
        'win_prov/win_cr/pv_c',
        'win_prov/win_cr/spv_c',
        'win_prov/win_lib/gsmc',
        'win_prov/win_lib/smspva/SMSpva',
        'win_prov/win_lib/smspva/countries',
        'win_prov/win_lib/smspva/services',
        'win_prov/win_lib/smspva/smsrequest',
       ]

ext_modules = [
    Extension('zamikanie',     ["zamikanie.py"]),
    Extension('vyplnucet',     ["vyplnucet.py"]),
    Extension('vyt_rec_domem',     ["vyt_rec_domem.py"]),
    Extension('ulohagen',     ["ulohagen.py"]),
    Extension('cachegcgs',     ["cachegcgs.py"]),
    Extension('cachegcls',     ["cachegcls.py"]),
    Extension('cachegcpc',     ["cachegcpc.py"]),
    Extension('cachegcsp',     ["cachegcsp.py"]),
    Extension('sv2',     ["sv2.py"]),
    Extension('sv3',     ["sv3.py"]),
    Extension('litedbf',  ["litedbf.py"]),
	Extension('krajnahod',  ["krajnahod.py"]),
    Extension('logger',       ["logger.py"]),
    Extension('setpx',       ["setpx.py"]),
    Extension('FM.efmx',  ["FM/efmx.py"]),
    Extension('FXW.efwx',  ["FXW/efwx.py"]),
    Extension('HG.CTMP',  ["HG/CTMP.py"]),
    Extension('HG.Ehdr',  ["HG/Ehdr.py"]),
    Extension('HG.Kwdw',  ["HG/Kwdw.py"]),
    Extension('HG.Mzrx',  ["HG/Mzrx.py"]),
    Extension('HG.maxmin',  ["HG/maxmin.py"]),
    Extension('PSK.poskyt',  ["PSK/poskyt.py"]),
    Extension('PXY.ipmpxy',  ["PXY/ipmpxy.py"]),
    Extension('pxy.pxxy',  ["PXY/pxxy.py"]),
    Extension('win_prov.Country.gl_cn',  ["win_prov/Country/gl_cn.py"]),
    Extension('win_prov.Country.l_s_cntr',  ["win_prov/Country/l_s_cntr.py"]),
    Extension('win_prov.win_cr.gsmc_c',  ["win_prov/win_cr/gsmc_c.py"]),
    Extension('win_prov.win_cr.l_s_c',  ["win_prov/win_cr/l_s_c.py"]),
    Extension('win_prov.win_cr.pv_c',  ["win_prov/win_cr/pv_c.py"]),
    Extension('win_prov.win_cr.spv_c',  ["win_prov/win_cr/spv_c.py"]),
    Extension('win_prov.win_lib.gsmc',  ["win_prov/win_lib/gsmc.py"]),
    Extension('win_prov.win_lib.smspva.SMSpva',  ["win_prov/win_lib/smspva/SMSpva.py"]),
    Extension('win_prov.win_lib.smspva.countries',  ["win_prov/win_lib/smspva/countries.py"]),
    Extension('win_prov.win_lib.smspva.services',  ["win_prov/win_lib/smspva/services.py"]),
    Extension('win_prov.win_lib.smspva.smsrequest',  ["win_prov/win_lib/smspva/smsrequest.py"]),
]

for e in ext_modules:
    e.cython_directives = {'language_level': "3"}

setup(
    name = 'Koala Bot systems',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)

if os.path.isdir(os.path.abspath(__file__+"/../build")):
    shutil.rmtree(os.path.abspath(__file__+"/../build"))

for name in names:
    if os.path.isfile(os.path.abspath(__file__+"/../"+name+".c")):
        os.remove(os.path.abspath(__file__+"/../"+name+".c"))

    # if os.path.isfile(os.path.abspath(__file__+"/../"+name+".pyd")):
    #     os.remove(os.path.abspath(__file__+"/../"+name+".pyd"))

    if os.path.isfile(os.path.abspath(__file__+"/../"+name+build_mode+".py")):
        os.remove(os.path.abspath(__file__+"/../"+name+build_mode+".py"))

    if '/' in name:
        totalFolders = name.count('/')
        folder_name = name.replace(name.split('/')[totalFolders],'')[:-1]
        file_name = name.split('/')[totalFolders]

        file_new = open(os.path.abspath(__file__+"/../"+folder_name+'/'+file_name+build_mode+".py"), "w") 
        
        file_new.write("import "+file_name+'') 
        file_new.close() 

        for file in os.listdir(os.path.abspath(__file__+"/../"+folder_name)):
            if file.endswith(".pyd"):
                if re.match(file_name, file):
                    if os.path.isfile(os.path.join(os.path.abspath(__file__+"/../"+folder_name+'/'+file_name+".pyd"))) == False:
                        os.rename(os.path.join(os.path.abspath(__file__+"/../"+folder_name), file),os.path.join(os.path.abspath(__file__+"/../"+folder_name+'/'+file_name+".pyd")))
    else:  
        file_new = open(os.path.abspath(__file__+"/../"+name+build_mode+".py"), "w") 
        file_new.write("import "+name) 
        file_new.close()   
        for file in os.listdir(os.path.abspath(__file__+"/../")):
            if file.endswith(".pyd"):
                if re.match(name, file):
                    if os.path.isfile(os.path.join(os.path.abspath(__file__+"/../"+name+".pyd"))) == False:
                        os.rename(os.path.join(os.path.abspath(__file__+"/../"), file),os.path.join(os.path.abspath(__file__+"/../"+name+".pyd")))


#%%
# import os,shutil,glob,re
# build_mode = ''
# names = [
#         'vyt_rec_domem',
#         'litedbf',
#         'ulohagen',
#         'cachegcgs',
#         'cachegcls',
#         'cachegcpc',
#         'cachegcsp',
#         'sv2',
#         'sv3',
#         'logger',
#         'setpx',
#         'FM/efmx',
#         'FXW/efwx',
#         'HG/CTMP',
#         'HG/Ehdr',
#         'HG/Kwdw',
#         'HG/Mzrx',
#         'HG/maxmin',
#         'PSK/poskyt',
#         'PXY/ipmpxy',
#         'PXY/pxxy',
#         'win_prov/Country/gl_cn',
#         'win_prov/Country/l_s_cntr',
#         'win_prov/win_cr/gsmc_c',
#         'win_prov/win_cr/l_s_c',
#         'win_prov/win_cr/pv_c',
#         'win_prov/win_cr/spv_c',
#         'win_prov/win_lib/gsmc',
#         # 'win_prov/win_lib/smspva/SMSpva',
#         # 'win_prov/win_lib/smspva/countries',
#         # 'win_prov/win_lib/smspva/services',
#         # 'win_prov/win_lib/smspva/smsrequest',
#        ]

# # shutil.rmtree(os.path.abspath(__file__+"/../build"))

# if os.path.isfile(os.path.abspath(__file__+"/../build")):
#     shutil.rmtree(os.path.abspath(__file__+"/../build"))

# for name in names:
#     if os.path.isfile(os.path.abspath(__file__+"/../"+name+".c")):
#         os.remove(os.path.abspath(__file__+"/../"+name+".c"))

#     # if os.path.isfile(os.path.abspath(__file__+"/../"+name+".pyd")):
#     #     os.remove(os.path.abspath(__file__+"/../"+name+".pyd"))

#     # if os.path.isfile(os.path.abspath(__file__+"/../"+name+build_mode+".py")):
#     #     os.remove(os.path.abspath(__file__+"/../"+name+build_mode+".py"))

#     if '/' in name:
#         totalFolders = name.count('/')
#         folder_name = name.replace(name.split('/')[totalFolders],'')[:-1]
#         file_name = name.split('/')[totalFolders]

#         file_new = open(os.path.abspath(__file__+"/../"+folder_name+'/'+file_name+build_mode+".py"), "w") 
        
#         file_new.write("import "+file_name+'') 
#         file_new.close() 

#         for file in os.listdir(os.path.abspath(__file__+"/../"+folder_name)):
#             if file.endswith(".pyd"):
#                 if re.match(file_name, file):
#                     if os.path.isfile(os.path.join(os.path.abspath(__file__+"/../"+folder_name+'/'+file_name+".pyd"))) == False:
#                         os.rename(os.path.join(os.path.abspath(__file__+"/../"+folder_name), file),os.path.join(os.path.abspath(__file__+"/../"+folder_name+'/'+file_name+".pyd")))
#     else:  
#         file_new = open(os.path.abspath(__file__+"/../"+name+build_mode+".py"), "w") 
#         file_new.write("import "+name) 
#         file_new.close()   
#         for file in os.listdir(os.path.abspath(__file__+"/../")):
#             if file.endswith(".pyd"):
#                 if re.match(name, file):
#                     if os.path.isfile(os.path.join(os.path.abspath(__file__+"/../"+name+".pyd"))) == False:
#                         os.rename(os.path.join(os.path.abspath(__file__+"/../"), file),os.path.join(os.path.abspath(__file__+"/../"+name+".pyd")))




# %%
