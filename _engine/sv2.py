
#%%
try:
	from selenium import webdriver
	from selenium.webdriver .chrome.options import Options
	from selenium.webdriver.common.by import By
	from selenium.webdriver.support.ui import WebDriverWait
	from selenium.webdriver.support import expected_conditions as EC
	from selenium.webdriver.support import expected_conditions as EC
	from selenium.webdriver.common.action_chains import ActionChains
	import time, os, random, re, requests
	from threading import Thread
	import pyautogui
	import sqlite3
	from litedbf import DBfunctions
	from setpx import SetProxy
	import zipfile
	import sys
	from logger import Logger
	import HG.maxmin as maxmin 
	import datetime
	from krajnahod import GENWEB
	from zamikanie import ZAMIKANIE
	from vyplnucet import VYTVORUCET

	stringKluc = str(sys.argv[2])
	odzamknute = ZAMIKANIE.decrypt('SecretKey', stringKluc)
	if odzamknute == 'false':
		sys.exit()


	#id emailu z db
	idEmail = sys.argv[1]
	data = DBfunctions.select_from_table('EMAILS', '*', 'id = {}'.format(idEmail))
	for row in data:
		(id, created, emailId, password, firstName, lastName, day, month, year, gender, telephoneNumber, country, providerId, gmailCode, proxyId,status,scoreV3, scoreV2, recoveryId, registeredAt) = tuple(row)

	dataProxies = DBfunctions.select_from_table('PROXIES', 'hostname, port, username, password', 'id = {}'.format(proxyId))
	for rowProxy in dataProxies:
		(hostnameProxy, portProxy, usernameProxy, passwordProxy) = tuple(rowProxy)

	#DEFINE LOGGER:
	logger_error = Logger.setup_logger('first logger', 'error.log','error',emailId,'RECAPTCHA','RECAPTCHA-V2')
	logger_process = Logger.setup_logger('second logger', 'process.log','process',emailId,'RECAPTCHA','RECAPTCHA-V2')

	delay = 45
	email = emailId
	password = password
	session_type = 'RECAPTCHA'

	if status != 'registered':
		logger_error.error('Email must be registered')
		print('Email must be registered')
		sys.exit()

	pages = [
				"https://www.ebay.com/signin/ggl/init?ru=https%3A%2F%2Fwww.ebay.com%2F&sclSignin=1",
				'https://stackoverflow.com/users/signup?ssrc=head&returnurl=%2fusers%2fstory%2fcurrent',
				'https://www.ebay.com/signin/ggl/init?ru=https%3A%2F%2Fwww.ebay.com%2F&sclSignin=1',
				'https://accounts.google.com/ServiceLogin/signinchooser?hl=sk&passive=true&continue=https%3A%2F%2Fwww.google.com%2F&ec=GAZAmgQ&flowName=GlifWebSignIn&flowEntry=ServiceLogin'
			]

	random_value = random.randrange(3)
	selected_page = pages[random_value]


	hostname = hostnameProxy  # rotating proxy or host
	port = portProxy # port
	proxy_username = usernameProxy # username
	proxy_password = passwordProxy # password 

	manifestJson = SetProxy.getManifest()
	backgroundJs = SetProxy.setBackgroundJs(hostname, port, proxy_username, proxy_password)


	timestr = time.strftime("%Y%m%d-%H%M%S")
	chrome_options = webdriver.ChromeOptions()
	pluginfile = os.path.abspath(os.getenv('APPDATA')+"/KOALABOTsystem/tmp/{}.zip").format(timestr)
	with zipfile.ZipFile(pluginfile, 'w') as zp:
		zp.writestr("manifest.json", manifestJson)
		zp.writestr("background.js", backgroundJs)

	################ OK ###################################################
	if sys.argv[3] == "1":
		#chrome_options.add_extension(pluginfile)
		pass
	else:
		chrome_options.add_extension(pluginfile)

	chrome_options.add_experimental_option('prefs', {
		'credentials_enable_service': False,
		'profile': {
			'password_manager_enabled': False
		}
	})


	# For older ChromeDriver under version 79.0.3945.16
	chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
	chrome_options.add_experimental_option('useAutomationExtension', False)

	#For ChromeDriver version 79.0.3945.16 or over
	chrome_options.add_argument("--disable-blink-features")
	chrome_options.add_argument('--disable-blink-features=AutomationControlled')
	chrome_options.add_argument("--disable-infobars")

	chrome_options.add_argument ("window-size = 1280 800")
	#user_data_file_path = app_data_local+"/Google/Chrome/"+nazovSuboru
	#chrome_options.add_argument('user-data-dir='+user_data_file_path)

	settings_arr = [
					"disable-background-networking",
					"enable-features=NetworkService,NetworkServiceInProcess"
					"disable-background-timer-throttling",
					"disable-backgrounding-occluded-windows",
					"disable-breakpad",
					"disable-client-side-phishing-detection",
					"disable-component-extensions-with-background-pages",
					"disable-default-apps",
					"disable-dev-shm-usage",
					#"disable-extensions",
					"disable-features=Translate",
					"disable-hang-monitor",
					"disable-ipc-flooding-protection",
					"disable-popup-blocking",
					"disable-prompt-on-repost",
					"disable-renderer-backgrounding",
					"disable-sync",
					"start-maximized",
					"force-color-profile=srgb",
					"metrics-recording-only",
					"no-first-run",
					"enable-automation",
					"password-store=basic",
					"use-mock-keychain",
					"enable-blink-features=IdleDetection",
					"disable-infobars",
					"no-sandbox",
					"disable-setuid-sandbox",
					"single-process",
					"no-zygote",
					"disable-features=site-per-process",
					"disable-blink-features=AutomationControlled",
					"remote-debugging-port=0",
					"flag-switches-begin",
					"flag-switches-end",
					"lang=en-US",
					"num-raster-threads=4",
					"enable-main-frame-before-activation",
					"use-gl=swiftshader-webgl"
					]

	for row in settings_arr:
		chrome_options.add_argument('--'+row)
	#chrome_options.add_argument ("user-agent = Mozilla / 5.0 (Windows NT 10.0; Win64; x64) AppleWebKit / 537,36 (KHTML, ako Gecko) Chrome / 74.0.3729.169 Safari / 537,36")
	################END OK ########################

	# chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
	# chrome_options.add_experimental_option('useAutomationExtension', False)
	# chrome_options.add_argument("--disable-blink-features")
	# chrome_options.add_argument("--disable-blink-features=AutomationControlled")

	pathToChrome = os.path.abspath(__file__+"/../chromedriver.exe")
	chrome_options.add_argument("--window-position=-32000,-32000")
	chrome_options.binary_location = os.path.abspath(__file__+"../../application/Chromium/chrome.exe")
	driver = webdriver.Chrome(executable_path=pathToChrome, options=chrome_options)

except Exception as e:
	exc_type, exc_obj, exc_tb = sys.exc_info()
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(e))

try:
	#Hide window
	#DATABASE SUBSESSION
	if(DBfunctions.check_if_table_exist("WINDOWS") == False):
		DBfunctions.create_table("Windows","([id] INTEGER PRIMARY KEY,[date_created] date, [id_window] text,[id_email] text, [sessions_type] text, [status_id] text)")
	tabs = driver.window_handles
	counterTabs = 0
	time.sleep(2)
	if len(tabs) > 1:
		for tab in tabs:
			if counterTabs == 0:
				window_name = driver.window_handles[counterTabs]
				driver.switch_to.window(window_name=window_name)
				driver.close()
				window_name = driver.window_handles[0]
				driver.switch_to.window(window_name=window_name)
			counterTabs = counterTabs + 1
	time.sleep(2)			
	driver.execute_script(f"document.title = '{emailId}#{idEmail}'")
	time.sleep(1)
	#print(emailId+'#'+id_email)
	#print(maxmin.get_window_id(emailId+'#'+id_email))
	outputWin = maxmin.get_window_id(emailId+'#'+idEmail)
	ID_window_MAIN_driver = outputWin[0].split("-")[0]
	ID_window_NUMB_driver = outputWin[0].split("###")[0]
	#Hide window function
	maxmin.hide_from_taskbar(int(ID_window_NUMB_driver))
	status = "1"
	#values = (f"'{datetime.datetime.now()}','{ID_window}', '{idEmail}', '{session_type}'")
	values = ("'{}','{}', '{}', '{}','{}'").format(datetime.datetime.now(),ID_window_NUMB_driver,idEmail,session_type,status)
	columns = "date_created, id_window, id_email, sessions_type, status_id"
	DBfunctions.insert_into_table('Windows', columns, values)

	#driver.get('https://www.whatismyip-address.com/?check')
	os.remove(pluginfile)
	time.sleep(2)
	for webpage in GENWEB.select_page_generate(random.randrange(1,2)):
		driver.get(webpage)
		time.sleep(random.randrange(1,4))
	time.sleep(random.randrange(1,4))
	time.sleep(2)
	driver.get(selected_page)
	#driver.get('https://accounts.google.com/o/oauth2/v2/auth/oauthchooseaccount?redirect_uri=https%3A%2F%2Fdevelopers.google.com%2Foauthplayground&prompt=consent&response_type=code&client_id=407408718192.apps.googleusercontent.com&scope=email&access_type=offline&flowName=GeneralOAuthFlow')
	time.sleep(2)
	if random_value == 1: 
			driver.find_element_by_xpath('//*[@id="openid-buttons"]/button[1]').click()
	time.sleep(random.randrange(1,4))
	login = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "identifier")))
	time.sleep(2)
	login.send_keys(email)
	WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='identifierNext']/div/button"))).click()
	driver.implicitly_wait(2)

	time.sleep(3)
	if driver.find_elements_by_xpath('//*[@id="view_container"]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div/div[2]/div[2]/div'):
		logger_error.error('Your Google account was not found.')
		driver.quit()
		sys.exit()

	time.sleep(5)
	logpass = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='password']/div[1]/div/div[1]/input")))  
	driver.execute_script(f"document.getElementsByName('password')[0].value = '{password}';")
	#logpass.send_keys(password_id)
	time.sleep(2)
	driver.execute_script(f"document.getElementById('passwordNext').click();")
	driver.implicitly_wait(3)

	time.sleep(2)
	googleAllowMobile = driver.find_elements_by_xpath('//*[@id="yDmH0d"]/c-wiz[2]/c-wiz/div/div[1]/div/div/div/div[2]/div[3]/div/div[2]/div')
	if len(googleAllowMobile)>0:
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="yDmH0d"]/c-wiz[2]/c-wiz/div/div[1]/div/div/div/div[2]/div[3]/div/div[2]/div'))).click()
		time.sleep(2)

	driver.implicitly_wait(3)
	if driver.find_elements_by_xpath('//*[@id="phoneNumberId"]'):
		time.sleep(2)
		if providerId == "LUCKYSMS":
			countryArray = country.split("_")
			countryLuckySMS = countryArray[1]
		else:
			countryLuckySMS = country
		
		countryName = countryLuckySMS
		countryNameForGmail = countryName.lower()

		dataCountryPrefix = DBfunctions.select_from_table('COUNTRY', 'prefix', "name = '{}'".format(countryNameForGmail.upper()))
		for rowCountryPrefix in dataCountryPrefix:
			countryPrefix = rowCountryPrefix[0] 
		countryPrefix = len(countryPrefix) 

		telephoneNumber = int(str(telephoneNumber)[countryPrefix:])
		
		time.sleep(3)
		driver.implicitly_wait(2)
		button = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="countryList"]/div[1]/div[1]/div[1]')))
		time.sleep(2)
		driver.execute_script("arguments[0].click();", button)
		time.sleep(2)
		buttonClick = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="countryList"]/div[2]/div[@data-value="{}"]'.format(countryNameForGmail))))
		hover = ActionChains(driver).move_to_element(buttonClick)
		time.sleep(3)
		hover.perform()
		driver.execute_script("arguments[0].click();", buttonClick)
		gmailNumber = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, "phoneNumberId")))
		gmailNumber.clear()
		gmailNumber.send_keys(telephoneNumber)
		time.sleep(2)
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="view_container"]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button/div[2]'))).click()
		
	driver.implicitly_wait(3)
	driver.get('https://www.google.com/')
	driver.implicitly_wait(2)
	time.sleep(2)
	driver.get('https://mail.google.com/')
	driver.implicitly_wait(2)
	time.sleep(3)

	googleAllowButton = driver.find_elements_by_xpath('//*[@id=":4u.contentEl"]/div/div[2]/div[2]/label/span')
	if len(googleAllowButton)>0:
		time.sleep(3)
		Button1 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[2]/div[2]/label/span')))
		VYTVORUCET.clickOnButton(driver, Button1)
		time.sleep(3)
		Button2 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, 'data_consent_dialog_next')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button2)
		time.sleep(3)
		Button3 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[3]/div[2]/label/span')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button3)
		time.sleep(3)
		Button4 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, 'data_consent_dialog_done')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button4)
		time.sleep(3)
		#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[2]/div[2]/label/span'))).click()
		# time.sleep(3)
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[22]/div[3]/button'))).click()
		# time.sleep(3)
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[3]/div[2]/label/span'))).click()
		# time.sleep(3)
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[22]/div[3]/button[1]'))).click()
		# time.sleep(3)

	time.sleep(1)
	googleMeetButton = driver.find_elements_by_xpath("//div[@role='alertdialog']")
	if len(googleMeetButton)>0:
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[@role='alertdialog']/div[1]/div/div[2]/div[2]"))).click()
		time.sleep(1)
	googleRefreshButton= driver.find_elements_by_xpath('/html/body/div[32]/div[3]/button')
	if len(googleRefreshButton)>0:	
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[32]/div[3]/button'))).click()
		driver.implicitly_wait(2)
		time.sleep(2)
	time.sleep(2)
	driver.get('https://mail.google.com/')
	driver.implicitly_wait(2)
	time.sleep(2)
	driver.get('https://mail.google.com/mail/u/0/#settings/fwdandpop')
	driver.implicitly_wait(2)
	time.sleep(2)
	forward_btn = driver.find_elements_by_xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[2]/input")
	driver.implicitly_wait(2)
	time.sleep(2)
	timeout = time.time() + 120
	while len(forward_btn)<=0:
		time.sleep(2)
		if time.time() > timeout:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			logger_error.error('Not logged to the email.')
			driver.quit() 
			sys.exit()
			break
		forward_btn = driver.find_elements_by_xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[2]/input")
		time.sleep(1)
		if len(forward_btn)>0:
			break

	# if len(forward_btn)<=0:
	# 	exc_type, exc_obj, exc_tb = sys.exc_info()
	# 	logger_error.error('Not logged to the email.')
	# 	driver.quit() 
	# 	sys.exit()


	time.sleep(5)

	driver.get("https://patrickhlauke.github.io/recaptcha/")
	logger_process.info("Checking the one click")

	time.sleep(5)
	frame = driver.find_element_by_xpath('/html/body/div[1]/div/div/iframe')
	driver.switch_to.frame(frame)
	WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//span[@id='recaptcha-anchor']"))).click()
	time.sleep(5)
	aria_label = driver.find_element_by_xpath("//span[@id='recaptcha-anchor']").get_attribute("aria-checked")
	setScoreInEmail = "scoreV2 = '{}'".format(aria_label)
	where = "id = '{}'".format(idEmail)
	DBfunctions.update_table('EMAILS', setScoreInEmail, where)
	logger_process.info("One click is:{}".format(aria_label))
	print(aria_label)
	DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
	time.sleep(5)
	driver.quit()
except Exception as e:
	exc_type, exc_obj, exc_tb = sys.exc_info()
	DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(e))
	driver.quit()    

# %%
