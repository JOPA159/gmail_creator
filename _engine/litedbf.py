#%%
import sqlite3,os

conn = sqlite3.connect(os.getenv('APPDATA')+"/KOALABOTsystem/DBemails.db", timeout=60) # You can create a new database by changing the name within the quotes
c = conn.cursor() # The database will be saved in the location where your 'py' file is saved

class DBfunctions():

	global  conn 
	global  c 

	def send(name):
		print(name + 'kk')
		print(DBfunctions.get_last_id(name))

	def check_if_table_exist(name_of_table):
		c.execute(f'''SELECT count(*) FROM sqlite_master WHERE type='table' AND name='{name_of_table.upper()}' ''')
		table_check = c.fetchone()[0]
		if table_check == 0:
			return False
		else:
			return True

	#name_of_table = "EMAILS"
	#columns = "([id] INTEGER PRIMARY KEY,[created] date, [email] text, [password] text, [country] text, [proxy] text )"
	#DBfunctions.create_table('EMAILS', '([id] INTEGER PRIMARY KEY,[created] date, [email] text, [password] text, [firstName] text, [lastName] text, [day] text, [month] text, [year] text, [gender] text, [telephoneNumber] text, [gmailCode] text,[scoreId] integer, [proxyId] integer )')
	def create_table(name_of_table, columns):
		sql = '''CREATE TABLE {} {}'''.format(
			name_of_table, columns)
		c.execute(sql)
		conn.commit()

	#name_of_table = "EMAILS"
	#columns = datetime('now'),'testovanie@gmail.com', 'Lol123456*','FR'
	def insert_into_table(name_of_table, columns, values):
		new_id = DBfunctions.get_last_id(name_of_table)    
		#c.execute(f'''insert into {name_of_table} select '{new_id}', {columns}  ''')
		#sql = "INSERT INTO {name_of_table} ({new_id},{columns}) VALUES ({values})"
		sql = '''INSERT INTO  {} (id,{}) VALUES ({}, {})'''.format(
			name_of_table, columns,  new_id, values)

		c.execute(sql)

		conn.commit()   
		#print('done') 

	#name_of_table = "EMAILS"
	#columns = *
	def select_from_table(name_of_table, columns, where): 
		sql = '''select {} from {} WHERE {}'''.format(
			columns, name_of_table, where)
		c.execute(sql)
		return c.fetchall()
		#conn.commit()
		#conn.close()     

	def delete_table(name_of_table):   
		sql = '''DROP TABLE {}'''.format(
			name_of_table)
		c.execute(sql) 

	def get_last_id(name_of_table): 
		sql = '''select max(id) from {}'''.format(
			name_of_table)
		last_id = c.execute(sql)
		last_id = last_id.fetchone()[0] 

		if last_id != None:
			new_id = last_id+1
		else:
			new_id = '1' 
		return    new_id

	#name_of_table = "EMAILS"
	#column = "author float"
	def add_new_column(name_of_table, column): 
		sql = '''alter table {} add column {}'''.format(
			name_of_table, column)
		c.execute(sql) 

	#name_of_table = "EMAILS"
	#set = "salary = 10000"
	#where =  id = 4
	def update_table(name_of_table, set, where): 
		#conn = sqlite3.connect('DBemails.db', timeout=10)  # You can create a new database by changing the name within the quotes
		#c = conn.cursor()
		sql = '''Update {} set {} where {}'''.format(
			name_of_table, set, where)
		#print(sql)
		c.execute(sql) 
		conn.commit()
		#print("Record Updated successfully ")

	#name_of_table = "EMAILS"
	#columns = *
	def select_from_table_random(name_of_table, columns): 
		sql = '''select {} from {} ORDER BY RANDOM() LIMIT 1'''.format(
			columns, name_of_table)
		c.execute(sql)
		return c.fetchall()
		#conn.commit()
		#conn.close()    
	
	def delete_from_table(columns,name_of_table): 
		sql = '''Delete from {} where {}'''.format(
			columns, name_of_table)
		c.execute(sql) 
		conn.commit()
		#conn.close()    
# %%
