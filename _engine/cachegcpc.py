
try:
	import traceback
	from selenium import webdriver
	from selenium.webdriver.chrome.options import Options
	from selenium.webdriver.common.by import By
	from selenium.webdriver.support.ui import WebDriverWait
	from selenium.webdriver.support import expected_conditions as EC
	import time
	import os
	import random

	from urllib.parse import urlencode
	import requests
	from selenium.webdriver.support import expected_conditions as EC
	from selenium.webdriver.common.action_chains import ActionChains
	from threading import Thread
	import pyautogui
	from litedbf import DBfunctions
	import requests
	import json
	import sys
	import datetime
	import logging
	from setpx import SetProxy
	import zipfile
	import HG.maxmin as maxmin 
	from logger import Logger
	import random
	import string
	from krajnahod import GENWEB
	from vyplnucet import VYTVORUCET
	from zamikanie import ZAMIKANIE
	import shutil
	from selenium.webdriver.common.keys import Keys
	from selenium.webdriver.support.select import Select

	stringKluc = str(sys.argv[3])
	odzamknute = ZAMIKANIE.decrypt('SecretKey', stringKluc)
	if odzamknute == 'false':
		sys.exit()

	path_to_myproject1 = os.path.abspath(__file__+"/../win_prov/win_lib/")
	sys.path.insert(0, path_to_myproject1)

	# from smspva import SMSpva
	# from smspva.countries import Country
	# from smspva.services import Service

	def num_there(s):
		return any(i.isdigit() for i in s) 

	if sys.argv[2] != '81309b12-6ddf-4333-b5c3-7bc65fe7f2c2':
		sys.exit()

	#id emailu z db
	idEmail = str(sys.argv[1])
	#idEmail = 24
	session_type = 'REGISTERING'
	data = DBfunctions.select_from_table('EMAILS', '*', 'id = {}'.format(idEmail))
	DBfunctions.update_table('EMAILS', "status = 'initializing process'", f'id = {idEmail}')

	for row in data:
		(id, created, emailId, password, firstName, lastName, day, month, year, gender, telephoneNumber, country, providerId, gmailCode, proxyId,status,scoreV3, scoreV2, recoveryId, registeredAt) = tuple(row)
	recovery_domain = recoveryId

	#DEFINE LOGGER:
	logger_error = Logger.setup_logger('first logger', 'error.log','error',emailId,'REGISTERING','CREATING-PVACODES')
	logger_process = Logger.setup_logger('second logger', 'process.log','process',emailId,'REGISTERING','CREATING-PVACODES')

	dataProxies = DBfunctions.select_from_table('PROXIES', 'hostname, port, username, password', 'id = {}'.format(proxyId))
	for rowProxy in dataProxies:
		(hostnameProxy, portProxy, usernameProxy, passwordProxy) = tuple(rowProxy)

	dataProvider = DBfunctions.select_from_table('PROVIDERS', 'provider, key, timeout', 'provider = "{}"'.format(providerId))

	for rowProvider in dataProvider:
		(providerName, keyProvider, timeoutProvider) = tuple(rowProvider)    

	if country == "RAND":
		countryRandLuckys = DBfunctions.select_from_table_random('COUNTRY', 'name, fullname')
		for countryRandLucky in countryRandLuckys:
			(countryForSMS, fullnameCountry) = tuple(countryRandLucky)
	else:
		countryRandLuckys = DBfunctions.select_from_table('COUNTRY', 'name, fullname', 'name = "{}"'.format(country))
		for countryRandLucky in countryRandLuckys:
			(countryForSMS, fullnameCountry) = tuple(countryRandLucky)


	if fullnameCountry == "United States":
		fullnameCountry = "USA"
	if fullnameCountry == "United Kingdom":
		fullnameCountry = "UK"  
	if fullnameCountry == "Bosnia and Herzegovina":
		fullnameCountry = "Bosnia"   
	if fullnameCountry == "Colombia":
		fullnameCountry = "Columbia"   
	if fullnameCountry == "Republic of the Congo":
		fullnameCountry = "Congo"
	if fullnameCountry == "Democratic Republic of the Congo":
		fullnameCountry = "Congo"
	if fullnameCountry == "Papua New Guinea":
		fullnameCountry = "New Guinea"   
	if fullnameCountry == "Dominican Republic":
		fullnameCountry = "Dominican Rep"  
	if fullnameCountry == "Vietnam":
		fullnameCountry = "Vietnum" 
	if fullnameCountry == "Saint Vincent and the Grenadines":
		fullnameCountry = "Saint Vincent" 
	if fullnameCountry == "United Arab Emirates":
		fullnameCountry = "UAE"                                         
	fullnameCountry = fullnameCountry.lower() 

	codeMessageCountry = "Country is:{}, {}".format(countryForSMS, fullnameCountry)
	logger_process.info(codeMessageCountry)
	# if country == "RAND":
	#     countryForSMS = random.choice(list(Country))
	# else:
	#     countryForSMS = country

	countryNameForGmail = countryForSMS.lower()

	if countryNameForGmail == 'uk':
		dataCountryPrefix = DBfunctions.select_from_table('COUNTRY', 'prefix', 'name = "{}"'.format('gb'))
	else:
		dataCountryPrefix = DBfunctions.select_from_table('COUNTRY', 'prefix', 'name = "{}"'.format(countryForSMS))

	# print(countryNameForGmail)
	# if not Country.has_value(countryNameForGmail):
	#     print('Taku krajinu nepodporuju')
	#     sys.exit()


	for rowCountryPrefix in dataCountryPrefix:
		countryPrefix = rowCountryPrefix[0] 
	countryPrefix = len(countryPrefix) 

	monthForGmail = int(month) 

	delay = 45

	pages = [
				"https://www.ebay.com/signin/ggl/init?ru=https%3A%2F%2Fwww.ebay.com%2F&sclSignin=1",
				'https://stackoverflow.com/users/signup?ssrc=head&returnurl=%2fusers%2fstory%2fcurrent',
				'https://www.ebay.com/signin/ggl/init?ru=https%3A%2F%2Fwww.ebay.com%2F&sclSignin=1'

			]

	random_value = random.randrange(3)
	selected_page = pages[1]

	hostname = hostnameProxy  # rotating proxy or host
	port = portProxy # port
	proxy_username = usernameProxy # username
	proxy_password = passwordProxy # password


	manifestJson = SetProxy.getManifest()
	backgroundJs = SetProxy.setBackgroundJs(hostname, port, proxy_username, proxy_password)

	timestr = time.strftime("%Y%m%d-%H%M%S")
	chrome_options = webdriver.ChromeOptions()
	pluginfile = os.path.abspath(os.getenv('APPDATA')+"/KOALABOTsystem/tmp/{}.zip").format(timestr)
	with zipfile.ZipFile(pluginfile, 'w') as zp:
			zp.writestr("manifest.json", manifestJson)
			zp.writestr("background.js", backgroundJs)

	################ OK ###################################################
	if sys.argv[4] == "1":
		#chrome_options.add_extension(pluginfile)
		pass
	else:
		chrome_options.add_extension(pluginfile)	

	chrome_options.add_experimental_option('prefs', {
		'credentials_enable_service': False,
		'profile': {
			'password_manager_enabled': False
		}
	})

	chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
	chrome_options.add_experimental_option('useAutomationExtension', False)

	#For ChromeDriver version 79.0.3945.16 or over
	chrome_options.add_argument("--disable-blink-features")
	chrome_options.add_argument('--disable-blink-features=AutomationControlled')
	chrome_options.add_argument("--disable-infobars")

	chrome_options.add_argument ("window-size = 1280 800")
	#user_data_file_path = app_data_local+"/Google/Chrome/"+nazovSuboru
	#chrome_options.add_argument('user-data-dir='+user_data_file_path)

	settings_arr = [
					"disable-background-networking",
					"enable-features=NetworkService,NetworkServiceInProcess"
					"disable-background-timer-throttling",
					"disable-backgrounding-occluded-windows",
					"disable-breakpad",
					"disable-client-side-phishing-detection",
					"disable-component-extensions-with-background-pages",
					"disable-default-apps",
					"disable-dev-shm-usage",
					#"disable-extensions",
					"disable-features=Translate",
					"disable-hang-monitor",
					"disable-ipc-flooding-protection",
					"disable-popup-blocking",
					"disable-prompt-on-repost",
					"disable-renderer-backgrounding",
					"disable-sync",
					"start-maximized",
					"force-color-profile=srgb",
					"metrics-recording-only",
					"no-first-run",
					"enable-automation",
					"password-store=basic",
					"use-mock-keychain",
					"enable-blink-features=IdleDetection",
					"disable-infobars",
					"no-sandbox",
					"disable-setuid-sandbox",
					"single-process",
					"no-zygote",
					"disable-features=site-per-process",
					"disable-blink-features=AutomationControlled",
					"remote-debugging-port=0",
					"flag-switches-begin",
					"flag-switches-end",
					"lang=en-US",
					"num-raster-threads=4",
					"enable-main-frame-before-activation",
					"use-gl=swiftshader-webgl"
					]

	for row in settings_arr:
		chrome_options.add_argument('--'+row)
	chrome_options.add_argument("--mute-audio")	
	#chrome_options.add_argument ("user-agent = Mozilla / 5.0 (Windows NT 10.0; Win64; x64) AppleWebKit / 537,36 (KHTML, ako Gecko) Chrome / 74.0.3729.169 Safari / 537,36")
	################END OK ########################

	countMistake = random.randint(1, 5)

except Exception as e:
	exc_type, exc_obj, exc_tb = sys.exc_info()
	#os.remove(pluginfile)
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(e))
	dataCheck = DBfunctions.select_from_table('EMAILS', 'status', 'id = {}'.format(idEmail))
	if dataCheck[0][0] != 'canceled' and dataCheck[0][0] != 'registered':
		DBfunctions.update_table('EMAILS', "status = 'failed_initializing'", f'id = {idEmail}')
	sys.exit()	

try:
	pathToChrome = os.path.abspath(__file__+"/../chromedriver.exe")
	chrome_options.add_argument("--window-position=-32000,-32000")
	chrome_options.binary_location = os.path.abspath(__file__+"../../application/Chromium/chrome.exe")
	#chrome_options.binary_location = os.path.abspath(__file__+"../../application/chrome.exe")
	driver = webdriver.Chrome(executable_path=pathToChrome, options=chrome_options)
	#Hide window
	#DATABASE SUBSESSION
	if(DBfunctions.check_if_table_exist("WINDOWS") == False):
		DBfunctions.create_table("Windows","([id] INTEGER PRIMARY KEY,[date_created] date, [id_window] text,[id_email] text, [sessions_type] text, [status_id] text)")
	time.sleep(2)
	tabs = driver.window_handles
	counterTabs = 0
	
	if len(tabs) > 1:
		for tab in tabs:
			if counterTabs == 0:
				window_name = driver.window_handles[counterTabs]
				driver.switch_to.window(window_name=window_name)
				driver.close()
				window_name = driver.window_handles[0]
				driver.switch_to.window(window_name=window_name)
			counterTabs = counterTabs + 1
	time.sleep(2)	
	driver.execute_script(f"document.title = '{emailId}#{id}'")
	time.sleep(1)
	outputWin = maxmin.get_window_id(emailId+'#'+str(id))
	ID_window_MAIN_driver = outputWin[0].split("-")[0]
	ID_window_NUMB_driver = outputWin[0].split("###")[0]

	#Hide window function
	maxmin.hide_from_taskbar(int(ID_window_NUMB_driver))
	status = "1"
	#values = (f"'{datetime.datetime.now()}','{ID_window}', '{id_email}', '{session_type}'")
	values = ("'{}','{}', '{}', '{}','{}'").format(datetime.datetime.now(),ID_window_NUMB_driver,id,session_type,status)
	columns = "date_created, id_window, id_email, sessions_type, status_id"
	DBfunctions.insert_into_table('Windows', columns, values)

	for webpage in GENWEB.select_page_generate(random.randrange(2,4)):
		driver.get(webpage)
		time.sleep(random.randrange(1,4))

	#driver.get('https://www.whatismyip-address.com/?check')
	time.sleep(3)    
	DBfunctions.update_table('EMAILS', "status = 'registering'", f'id = {idEmail}')
	

	driver.get("https://www.google.com/gmail/about/#")
	driver.implicitly_wait(3)
	time.sleep(2)
	skuskaInternetu1 = driver.find_elements_by_xpath('/html/body/div[3]/div[1]/div[4]/ul[1]/li[3]/a')
	skuskaInternetu2 = driver.find_elements_by_xpath('/html/body/div[4]/div/div/div/div[1]/div/div[3]/a[1]')
	skuskaInternetu3 = driver.find_elements_by_xpath('/html/body/div[2]/div[1]/div[4]/ul[1]/li[3]/a')
	skuskaInternetu4 = driver.find_elements_by_xpath('/html/body/div/header/div/div/div/div/a')
	skuskaInternetu5 = driver.find_elements_by_xpath("//a[contains(@href,'accounts.google.com')]")
	skuskaInternetu6 = driver.find_elements_by_xpath("/html/body/header/div/div/div/a[3]")

	if len(skuskaInternetu1)<=0 and len(skuskaInternetu2)<=0 and len(skuskaInternetu3)<=0 and len(skuskaInternetu4)<=0 and len(skuskaInternetu5)<=0 and len(skuskaInternetu6)<=0:
		for i in range(3):
			driver.get("https://www.google.com/gmail/about/#")
			driver.implicitly_wait(3)
			time.sleep(2)
			skuskaInternetu1 = driver.find_elements_by_xpath('/html/body/div[3]/div[1]/div[4]/ul[1]/li[3]/a')
			skuskaInternetu2 = driver.find_elements_by_xpath('/html/body/div[4]/div/div/div/div[1]/div/div[3]/a[1]')
			skuskaInternetu3 = driver.find_elements_by_xpath('/html/body/div[2]/div[1]/div[4]/ul[1]/li[3]/a')
			skuskaInternetu4 = driver.find_elements_by_xpath('/html/body/div/header/div/div/div/div/a')
			skuskaInternetu5 = driver.find_elements_by_xpath("//a[contains(@href,'accounts.google.com')]")
			skuskaInternetu6 = driver.find_elements_by_xpath("/html/body/header/div/div/div/a[3]")
			if len(skuskaInternetu1)>0 or len(skuskaInternetu2)>0 or len(skuskaInternetu3)>0 or len(skuskaInternetu4)>0 or len(skuskaInternetu5)>0 or len(skuskaInternetu6)>0:
				break
	# skuskaInternetu = driver.find_elements_by_xpath('/html/body/div[2]/div[1]/div[4]/ul[1]/li[3]/a')
	# if len(skuskaInternetu)<=0:
	# 	for i in range(3):
	# 		driver.get("https://www.google.com/gmail/about/#")
	# 		driver.implicitly_wait(3)
	# 		time.sleep(2)
	# 		skuskaInternetu = driver.find_elements_by_xpath('/html/body/div[2]/div[1]/div[4]/ul[1]/li[3]/a')
	# 		if len(skuskaInternetu)>0:
	# 			break
	time.sleep(2)			
	VYTVORUCET.firstStepAndChangeLangueageRegisterForm(driver)
	time.sleep(2)

	logger_process.info("Filling credentials")
	DBfunctions.update_table('EMAILS', "status = 'filling_form'", f'id = {idEmail}')
	# time.sleep(random.randint(2, 5))
	# if countMistake > 0:
	# 	lettersRandom = string.ascii_lowercase
	# 	randomFirstName = ''.join(random.choice(lettersRandom) for i in range(random.randint(3, 8))) 	
	# 	username = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "firstName")))
	# 	username.send_keys(firstName+randomFirstName)
	# 	countMistake -= 1


	driver.implicitly_wait(2)
	time.sleep(10)
	username = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "firstName")))
	time.sleep(1)
	VYTVORUCET.fillInputInRegistrationStep(driver, username, firstName)
	time.sleep(1)


	# if countMistake > 0:
	# 	lettersRandom = string.ascii_lowercase
	# 	randomLastName = ''.join(random.choice(lettersRandom) for i in range(random.randint(3, 8))) 
	# 	lastname = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "lastName")))
	# 	lastname.send_keys(lastName+randomLastName)
	# 	countMistake -= 1


	lastNameInput = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "lastName")))
	lettersRandom = string.ascii_lowercase
	randomLastName = ''.join(random.choice(lettersRandom) for i in range(random.randint(3, 8)))
	mistakeLastName = lastName+randomLastName
	time.sleep(1)
	VYTVORUCET.fillInputInRegistrationStep(driver, lastNameInput, mistakeLastName)	
	time.sleep(2)	
	VYTVORUCET.clearInput(lastNameInput)
	time.sleep(2)
	VYTVORUCET.fillInputInRegistrationStep(driver, lastNameInput, lastName)
	time.sleep(1)



	#badEmail = emailId + 'LP'
	#email = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "Username")))
	#email.send_keys(badEmail)
	# if countMistake > 0:
	# 	lettersRandom = string.ascii_lowercase
	# 	randomEmail= ''.join(random.choice(lettersRandom) for i in range(random.randint(3, 8))) 
	# 	email = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "Username")))
	# 	email.send_keys(randomEmail+emailId)
	# 	countMistake -= 1


	time.sleep(2)
	email = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "Username")))
	time.sleep(1)
	VYTVORUCET.clearInput(email)
	time.sleep(2)
	VYTVORUCET.fillInputInRegistrationStep(driver, email, emailId)
	time.sleep(2)

	# if countMistake > 0:
	# 	lettersRandomPasswd = string.punctuation
	# 	RandomPasswd = ''.join(random.choice(lettersRandomPasswd) for i in range(10)) 
	# 	pass1 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "Passwd")))
	# 	pass1.send_keys(RandomPasswd+password)

	pass1 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "Passwd")))
	time.sleep(1)
	VYTVORUCET.fillInputInRegistrationStep(driver, pass1, password)
	time.sleep(1)

	pass2 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "ConfirmPasswd")))
	time.sleep(1)
	VYTVORUCET.fillInputInRegistrationStep(driver, pass2, password)
	time.sleep(1)


	# if countMistake > 0:
	# 	lettersRandomPasswd = string.punctuation
	# 	RandomPasswd = ''.join(random.choice(lettersRandomPasswd) for i in range(10)) 
	# 	pass2 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "ConfirmPasswd")))
	# 	pass2.send_keys(RandomPasswd+password)

	# time.sleep(random.randint(2, 5))
	# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "ConfirmPasswd"))).clear()	
	# time.sleep(random.randint(2, 5))
	
	# pass2 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "ConfirmPasswd")))
	# pass2.send_keys(password)
	# time.sleep(random.randint(2, 5))

	time.sleep(1)
	accountDetailsNext = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="accountDetailsNext"]')))
	VYTVORUCET.clickOnButton(driver, accountDetailsNext)
	time.sleep(1)
	driver.implicitly_wait(1)
	time.sleep(2)

	if driver.find_elements_by_xpath("//*[@id='view_container']/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[2]/div[1]/div/div[2]/div[2]/div/span"):
		logger_error.error('This username is already in use. Try another.')
		driver.quit()
		DBfunctions.update_table('EMAILS', f"status = 'email_is_already_use'", f'id = {idEmail}')
		DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
		sys.exit()
		
	time.sleep(3)
	accountCountry = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="countryList"]/div/div[1]')))
	VYTVORUCET.clickOnButton(driver, accountCountry)
	time.sleep(1)
	logger_process.info("Waiting for number")
	#maxmin.maximize_from_taskbar(int(ID_window_NUMB_driver))
	time.sleep(3)
	#os.remove(pluginfile)

	urlGetNumber = 'http://pvacodes.com/user/api/get_number.php?customer={}&app=gmail&country={}'.format(keyProvider, fullnameCountry)

	response = requests.get(urlGetNumber)
	if response.status_code == 500:
		logger_error.error('Something is wrong with API for provider.')
		driver.quit()
		DBfunctions.update_table('EMAILS', "status = 'failed_register'", f'id = {idEmail}')
		DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
		sys.exit()
	else:
		if response.text:
			if not num_there(response.text):
				logger_error.error(response.text)  
				driver.quit()
				DBfunctions.update_table('EMAILS', "status = 'failed_register'", f'id = {idEmail}')
				DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
				sys.exit() 
		else: 
			logger_error.error("Something is wrong")
			driver.quit()
			DBfunctions.update_table('EMAILS', "status = 'failed_register'", f'id = {idEmail}')
			DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
			sys.exit() 

	telepehoneNumber = response.json()
	logger_process.info("Telephone number: {}".format(telepehoneNumber))

	telepehoneNumber = int(str(telepehoneNumber)[countryPrefix:])
	telepehoneNumberString = str(telepehoneNumber)[countryPrefix:]
	time.sleep(3)

	#buttonClick = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="countryList"]/div[2]/div[@data-value="{}"]'.format(countryNameForGmail))))
	time.sleep(5)
	accountCountryConcrete = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="countryList"]/div/div[2]/ul/li[@data-value="{}"]'.format(countryNameForGmail))))
	VYTVORUCET.clickOnButton(driver, accountCountryConcrete)
	time.sleep(1)
	driver.implicitly_wait(1)
	#hover = ActionChains(driver).move_to_element(buttonClick)
	#hover.perform()
	#driver.execute_script("arguments[0].click();", buttonClick)

	gmailPhoneNumber = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, "phoneNumberId")))
	#gmailNumber.clear()
	#gmailNumber.send_keys(telepehoneNumber)
	VYTVORUCET.clearInput(gmailPhoneNumber)
	time.sleep(2)
	VYTVORUCET.fillInputInRegistrationStep(driver, gmailPhoneNumber, telepehoneNumberString)
	time.sleep(2)


	#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="view_container"]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button'))).click()
	time.sleep(1)
	numberNextButton = WebDriverWait(driver, delay).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="view_container"]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button')))
	time.sleep(1)
	VYTVORUCET.clickOnButton(driver, numberNextButton)
	driver.implicitly_wait(1)
	time.sleep(3)
	
	if driver.find_elements_by_xpath("//*[@id='view_container']/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[2]/div/div[2]/div[2]/div[2]/div/span"):
		logger_error.error('This phone number cannot be used for verification.')
		driver.quit()
		DBfunctions.update_table('EMAILS', "status = 'failed_problem_with_number'", f'id = {idEmail}')
		DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
		sys.exit() 

	urlForGetCodeSMS = 'http://pvacodes.com/user/api/get_sms.php?customer={}&number={}&country={}&app=gmail'.format(keyProvider, response.json(), fullnameCountry)

	codeFromSms = requests.get(urlForGetCodeSMS)
	codeFromSms = codeFromSms.text
	codeFromSms = codeFromSms.split(' ')
	timeout = time.time() + 60*int(timeoutProvider)
	logger_process.info("Waiting for SMS")
	DBfunctions.update_table('EMAILS', "status = 'waiting_for_sms'", f'id = {idEmail}')
	while "You" == codeFromSms[0] and "Number" != codeFromSms[0]:
		if time.time() > timeout:
			logger_error.error('Timeout for get sms code')
			driver.quit()
			DBfunctions.update_table('EMAILS', "status = 'failed_register'", f'id = {idEmail}')
			DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
			sys.exit() 
			break

		codeFromSms = requests.get(urlForGetCodeSMS)
		codeFromSms = codeFromSms.text
		codeFromSms = codeFromSms.split(' ')
		if  "You" != codeFromSms[0] and "Number" != codeFromSms[0]:
			break
		
	codeFromSms = codeFromSms[0]
	codeFromSmsString = str(codeFromSms)
	codeMessage = "Gmail code is:{}".format(codeFromSms)
	logger_process.info(codeMessage)

	codeFromSms = codeFromSms.replace("G-", "")

	time.sleep(2)

	gmailSmsCode = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, "code")))
	time.sleep(2)
	VYTVORUCET.fillInputInRegistrationStep(driver, gmailSmsCode, codeFromSmsString)
	#time.sleep(2)	
	#gmailSmsCode.send_keys(codeFromSms)
	time.sleep(2)

	setGmailCodeAndTelephoneNumber = "gmailCode = '{}', telephoneNumber = '{}'".format(codeFromSms, codeFromSms)
	DBfunctions.update_table('EMAILS', setGmailCodeAndTelephoneNumber, f'id = {idEmail}')

	time.sleep(1)
	codeNextButton = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="view_container"]/div/div/div[2]/div/div[2]/div[2]/div[1]/div/div/button')))
	VYTVORUCET.clickOnButton(driver, codeNextButton)
	driver.implicitly_wait(1)
	time.sleep(2)
	DBfunctions.update_table('EMAILS', "status = 'finishing_registration'", f'id = {idEmail}')
	logger_process.info("Continue fill the form")
	if len(recovery_domain) > 0:    
		# gmailRecoveryEmail = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "recoveryEmail")))
		# gmailRecoveryEmail.send_keys(recovery_domain)
		# time.sleep(2)
		gmailRecoveryEmail = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "recoveryEmail")))
		time.sleep(1)	
		VYTVORUCET.fillInputInRegistrationStep(driver, gmailRecoveryEmail, recovery_domain)
		time.sleep(2)

	# gmailNumber = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, "day")))
	# gmailNumber.send_keys(day)
	# time.sleep(2)

	gmailDay = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, "day")))
	time.sleep(1)	
	VYTVORUCET.fillInputInRegistrationStep(driver, gmailDay, day)
	time.sleep(2)

	gmailYear = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, "year")))
	time.sleep(1)	
	VYTVORUCET.fillInputInRegistrationStep(driver, gmailYear, year)
	time.sleep(2)

	# gmailNumber = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, "year")))
	# gmailNumber.send_keys(year)
	# time.sleep(2)


	# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'month'))).click()
	# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="month"]/option[{}]'.format(monthForGmail)))).click()
	# time.sleep(2)
	time.sleep(1)
	dropdownMonth = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'month')))
	time.sleep(1)
	time.sleep(2)
	select_box_month = Select(dropdownMonth)
	select_box_month.select_by_value(str(monthForGmail))
	time.sleep(2)

	if gender == 'male':
		genderButton =  WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'gender')))
		time.sleep(1)
		select_box_month = Select(genderButton)
		select_box_month.select_by_value('1')
		time.sleep(1)
		#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'gender'))).click()
		#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="gender"]/option[3]'))).click()
	else:
		genderButton =  WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'gender')))
		time.sleep(1)
		select_box_month = Select(genderButton)
		select_box_month.select_by_value('2')
		time.sleep(1)
		#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'gender'))).click()
		#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="gender"]/option[2]'))).click()

	time.sleep(2)
	viewContainernextButton = WebDriverWait(driver, delay).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="view_container"]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button')))
	time.sleep(1)
	VYTVORUCET.clickOnButton(driver, viewContainernextButton)
	driver.implicitly_wait(1)
	time.sleep(1)

	time.sleep(2)
	viewContainernextButton1 = WebDriverWait(driver, delay).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='view_container']/div/div/div[2]/div/div[2]/div[2]/div[1]/div/div/button")))
	time.sleep(1)
	VYTVORUCET.clickOnButton(driver, viewContainernextButton1)
	driver.implicitly_wait(1)
	time.sleep(2)
	#time.sleep(3)                          
	#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='termsofserviceNext']"))).click()
	
	time.sleep(4)
	if driver.find_elements_by_xpath("//*[@id='view_container']/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/div/div[1]/div/span/div[1]/div/div[1]/div/div[3]/div"):
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='view_container']/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/div/div[1]/div/span/div[1]/div/div[1]/div/div[3]/div"))).click()
		# time.sleep(3)
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='view_container']/div/div/div[2]/div/div[2]/div/div[1]/div/div/button"))).click()
		# time.sleep(3)
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='view_container']/div/div/div[2]/div/div[2]/div/div[1]/div[2]/div/button"))).click()
		time.sleep(3)
		viewContainernextButton2 = WebDriverWait(driver, delay).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='view_container']/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/div/div[1]/div/span/div[1]/div/div[1]/div/div[3]/div")))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, viewContainernextButton2)
		driver.implicitly_wait(1)
		time.sleep(3)
		viewContainernextButton3 = WebDriverWait(driver, delay).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='view_container']/div/div/div[2]/div/div[2]/div/div[1]/div/div/button")))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, viewContainernextButton3)
		driver.implicitly_wait(1)
		time.sleep(3)
		viewContainernextButton4 = WebDriverWait(driver, delay).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='view_container']/div/div/div[2]/div/div[2]/div/div[1]/div[2]/div/button")))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, viewContainernextButton4)
		driver.implicitly_wait(1)
		time.sleep(3)

	#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="view_container"]/form/div[2]/div/div/div/div[1]/span/div/div[3]/div[1]/div/div[1]/div/div/div[1]/div'))).click()
	#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="view_container"]/form/div[2]/div/div/div/div[1]/span/div/div[3]/div[2]/div/div[1]/div/div/div[1]/div'))).click()
	driver.implicitly_wait(3)
	time.sleep(2)
	number_of_scrolls = 6
	for index,i in enumerate(range(1,number_of_scrolls+1),1):
		page_scroll_del = 6
		page_height = driver.execute_script("return document.body.scrollHeight")

		scroll_pos = "+"
		time.sleep(random.randrange(1,2)) # Kolko pocka na jednej pozicii kym scrolluje
		driver.execute_script("window.scrollBy(0,"+ f'{scroll_pos}{page_height/page_scroll_del}'+");")
	time.sleep(3)
	termsofserviceNextButton1 = WebDriverWait(driver, delay).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="view_container"]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button')))
	#termsofserviceNextButton1 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'termsofserviceNext')))
	time.sleep(1)
	VYTVORUCET.clickOnButton(driver, termsofserviceNextButton1)
	driver.implicitly_wait(1)
	time.sleep(2)
	if driver.find_elements_by_xpath('//*[@id="confirmdialog-confirm"]'):
		#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="confirmdialog-confirm"]'))).click()
		time.sleep(3)
		#time.sleep(2)
		confirmdialogButton1 = WebDriverWait(driver, delay).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="confirmdialog-confirm"]')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, confirmdialogButton1)
		driver.implicitly_wait(1)
	time.sleep(5)
	# for webpage in GENWEB.select_page_generate(random.randrange(2,4)):
	# 	driver.get(webpage)
	# 	time.sleep(random.randrange(1,4))
		
	logger_process.info("Successfull finished")
	DBfunctions.update_table('EMAILS', "status = 'registered'", f'id = {idEmail}')
	DBfunctions.update_table('EMAILS', f"registered_at = '{datetime.datetime.now()}'", f'id = {idEmail}')
	#os.remove(pluginfile)
	DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
	time.sleep(10)
	driver.quit()
	
except Exception as e:
	exc_type, exc_obj, exc_tb = sys.exc_info()
	#os.remove(pluginfile)
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(traceback.format_exc()))
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(e))
	dataCheck = DBfunctions.select_from_table('EMAILS', 'status', 'id = {}'.format(idEmail))
	if dataCheck[0][0] != 'canceled' and dataCheck[0][0] != 'registered':
		DBfunctions.update_table('EMAILS', "status = 'failed_register'", f'id = {idEmail}')
	DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
	driver.quit()
	sys.exit()
# %%
