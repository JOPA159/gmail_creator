#%%
import logging, re, os

#Example of loggins
#logger_process.info("Something is going on")
#logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(e))

#######LOGGER SETUP#######:
class Logger():
	def setup_logger(filename, log_file, type_log,email_name='',session_type='', filename_real=''):
		if email_name != '':
			if '@gmail.com' in email_name:
				email_flag = ''
			else:
				email_flag = '@gmail.com'
		else:
			email_flag = ''
		if type_log == 'error':
			level = logging.ERROR

			format = logging.Formatter('%(asctime)s#~#! '+'['+email_name+ email_flag +']'+'#~#! %(levelname)s#~#! %(message)s#~#! FILE:{'+ filename_real +'}#~~#')
		else:
			level = logging.INFO
			format = logging.Formatter('%(asctime)s#~#! '+'['+email_name+ email_flag +']'+'#~#! PROCESS#~#! '+session_type+'#~#! Message:%(message)s#~~#')

		#logger_path = __name__   
		file_path_final = os.getenv('APPDATA')+"/KOALABOTsystem/"+log_file
		handler = logging.FileHandler(file_path_final)        
		handler.setFormatter(format)

		logger = logging.getLogger(filename)
		logger.setLevel(level)
		logger.addHandler(handler)

		return logger
# %%
