try:
	import os, random, re, time, pyautogui, requests, datetime, sys, shutil, traceback
	from threading import Thread
	from selenium import webdriver
	from selenium.webdriver.chrome.options import Options
	from selenium.webdriver.common.by import By
	from selenium.webdriver.support import expected_conditions as EC
	from selenium.webdriver.support.ui import WebDriverWait
	from selenium.webdriver.common.action_chains import ActionChains
	from win32com.shell import shell, shellcon
	import zipfile

	path_to_myproject1 = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	sys.path.insert(0, path_to_myproject1)

	app_data_local = os.getenv('APPDATA')+"/KOALABOTsystem/"

	from litedbf import DBfunctions
	import HG.maxmin as maxmin 
	from setpx import SetProxy
	from logger import Logger
	from krajnahod import GENWEB
	from vyplnucet import VYTVORUCET
	from zamikanie import ZAMIKANIE

	stringKluc = str(sys.argv[3])
	odzamknute = ZAMIKANIE.decrypt('SecretKey', stringKluc)
	if odzamknute == 'false':
		sys.exit()

	start_time = time.time()

	def enter_proxy_auth(proxy_username, proxy_password):
		time.sleep(1)
		pyautogui.typewrite(proxy_username)
		pyautogui.press('tab')
		pyautogui.typewrite(proxy_password)
		pyautogui.press('enter')


	def open_a_page(driver, url):
		driver.get(url)

	#SYSTEM VAR
	delay = 45
	session_type = 'FARMING'
	pathToChrome = os.path.abspath(__file__+"../../../chromedriver.exe")

	if sys.argv[2] != '0ced6612-79b9-41f9-bb2c-d75b64aae37a':
		sys.exit()

	id_email = sys.argv[1]

	#################STATUS SET#######################
	farmingData = DBfunctions.select_from_table('FARMING', 'email_id, total_farming', 'email_id = {}'.format(id_email))

	farmDataRow = False
	if len(farmingData):
		farmDataRow = True
		setNewFarmingData = "status = '{}'".format("INITIALIZING PROCESS")
		DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))
	else:
		values = ("'{}','{}'").format(id_email,"INITIALIZING PROCESS")
		columns = "email_id,status"
		DBfunctions.insert_into_table('FARMING', columns, values)
	###################################################


	data = DBfunctions.select_from_table('EMAILS', '*', 'id = {}'.format(id_email))

	for row in data:
		(id, created, email_id, password_id, firstName, lastName, day, month, year, gender, telephoneNumber, country, providerId, gmailCode, proxyId,status,scoreV3, scoreV2, recoveryId, registeredAt) = tuple(row)

	dataProxies = DBfunctions.select_from_table('PROXIES', 'hostname, port, username, password', 'id = {}'.format(proxyId))
	for rowProxy in dataProxies:
		(hostnameProxy, portProxy, usernameProxy, passwordProxy) = tuple(rowProxy)

	dataSettings = DBfunctions.select_from_table('SETTINGS', 'videos_val,min_videos,max_videos,news_val,min_news,max_news,items_val,items_category_val,search_val', 'category="FARMING"')
	for rowSetting in dataSettings:
		(videosVal,minVideos,maxVideos,newsVal,minNews,maxNews,itemsVal,items_category_val,searchVal) = tuple(rowSetting)

	#FARMING DATA UPDATE
	if farmDataRow:
		farmingData = DBfunctions.select_from_table('FARMING', 'email_id, total_farming', 'email_id = {} and total_farming is not null'.format(id_email))
		if len(farmingData):
			for rowFarm in farmingData:
				(email_id_farm, total_farm_time) = tuple(rowFarm)    

				setNewFarmingData = "last_farming_date = '{}', total_farming = '{}', last_farming = '{}', status = '{}'".format(datetime.datetime.now(),total_farm_time+(time.time() - start_time),time.time() - start_time,'INITIALIZING PROCESS')
				DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))
		else:
			setNewFarmingData = "last_farming_date = '{}', total_farming = '{}', last_farming = '{}', status = '{}'".format(datetime.datetime.now(),time.time() - start_time,time.time() - start_time,'INITIALIZING PROCESS')
			DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))            

	else:
		setNewFarmingData = "last_farming_date = '{}', total_farming = '{}', last_farming = '{}', status = '{}'".format(datetime.datetime.now(),time.time() - start_time,time.time() - start_time,'INITIALIZING PROCESS')
		DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))


	hostname = hostnameProxy  # rotating proxy or host
	port = portProxy # port
	proxy_username = usernameProxy # username
	proxy_password = passwordProxy # password


	#Zaciatok - Pridanie historie a prihl(treba to davat pod vyber z databazy aby si vedel dat nazov mailu)
	#Funkciu treba dat vsade kde sa môže skončiť 
	nazovSuboru = 'a1a88x'
	# def archivovanie_historie():
	# 	try:
	# 		if os.path.isfile(os.path.abspath(app_data_local+"/Google/Chrome/"+nazovSuboru+".zip")):
	# 			os.remove(os.path.abspath(app_data_local+"/Google/Chrome/"+nazovSuboru+".zip"))
	# 		#Vymazanie nepotrebneho cahce co zaberal vela
	# 		#if os.path.isdir(os.path.abspath(app_data_local+"/Google/Chrome/"+nazovSuboru+"/Default/Cache")):
	# 		#	shutil.rmtree(os.path.abspath(app_data_local+"/Google/Chrome/"+nazovSuboru+"/Default/Cache"))

	# 		if os.path.isdir(os.path.abspath(app_data_local+"/Google/Chrome/"+nazovSuboru)):	
	# 			shutil.make_archive(app_data_local+"\\Google\\Chrome\\"+nazovSuboru, 'zip', app_data_local+"\\Google\\Chrome\\"+nazovSuboru)
	# 			shutil.rmtree(os.path.abspath(app_data_local+"/Google/Chrome/"+nazovSuboru))
	# 	except OSError:
	# 		pass

	# try:
	# 	if os.path.isdir(os.path.abspath(app_data_local+"/Google/Chrome/"+nazovSuboru)) == False:
	# 		#vymazanie suboru ak by bolo treba niekde
	# 		#shutil.rmtree(os.path.abspath(app_data_local+"/Google/Chrome/"+nazovSuboru))
	# 		if os.path.isfile(os.path.abspath(app_data_local+"/Google/Chrome/"+nazovSuboru+".zip")):
	# 			shutil.unpack_archive(app_data_local+"\\Google\\Chrome\\"+nazovSuboru+".zip", app_data_local+"\\Google\\Chrome\\"+nazovSuboru,'zip', )
	# 			os.remove(os.path.abspath(app_data_local+"/Google/Chrome/"+nazovSuboru+".zip"))
	# except OSError:
	# 	pass
	time.sleep(1)
	#Koniec - pridanie historie a prihl


	'''
	SettingData = DBfunctions.select_from_table('SETTINGS', 'videos_val, min_videos, max_videos, news_val, min_news, max_news, items_val, min_items, max_items, search_val', 'category = {}'.format(SESSTION_TYPE))

	if len(SettingData):
		for rowFarm in SettingData:
			(videos_val, min_videos, max_videos, news_val, min_news, max_news, items_val, min_items, max_items, search_val) = tuple(rowFarm)   
	'''

	#youtube var
	farm_number_of_videos = videosVal
	farm_min_seconds_on_videos = minVideos
	farm_max_seconds_on_video = maxVideos
	#news var
	farm_number_of_news = newsVal
	farm_min_seconds_on_news = minNews
	farm_max_seconds_on_news = maxNews
	farm_number_of_scrolls_on_news = 4
	farm_max_sleep_on_one_position_news = 5
	#shopping var
	farm_number_of_items_in_categ = itemsVal
	farm_max_sleep_on_one_position_shop = 4
	farm_number_of_shop_categ = items_category_val
	shopping_categories = [
							"Shoes","Accessories","Clothes",
							"Bags","Jackets","Socks",
							"Underwear","Bikini","Belt"
						]
	#gmail search var
	farm_number_of_searching_items = searchVal

	logger_error = Logger.setup_logger('first logger', 'error.log','error',email_id,'FARMING','FARMING-EMAIL')
	logger_process = Logger.setup_logger('second logger', 'process.log','process',email_id,'FARMING','FARMING-EMAIL')

	logger_process.info("Setting things up")

	pages = [
				"https://www.ebay.com/signin/ggl/init?ru=https%3A%2F%2Fwww.ebay.com%2F&sclSignin=1",
				'https://stackoverflow.com/users/signup?ssrc=head&returnurl=%2fusers%2fstory%2fcurrent',
				'https://www.ebay.com/signin/ggl/init?ru=https%3A%2F%2Fwww.ebay.com%2F&sclSignin=1',
				'https://accounts.google.com/ServiceLogin/signinchooser?hl=sk&passive=true&continue=https%3A%2F%2Fwww.google.com%2F&ec=GAZAmgQ&flowName=GlifWebSignIn&flowEntry=ServiceLogin'
			]

	random_value = random.randrange(4)
	selected_page = pages[random_value]

	hostname = hostnameProxy  # rotating proxy or host
	port = portProxy # port
	proxy_username = usernameProxy # username
	proxy_password = passwordProxy # password


	manifestJson = SetProxy.getManifest()
	backgroundJs = SetProxy.setBackgroundJs(hostname, port, proxy_username, proxy_password)

	timestr = time.strftime("%Y%m%d-%H%M%S")
	chrome_options = webdriver.ChromeOptions()
	pluginfile = os.path.abspath(os.getenv('APPDATA')+"/KOALABOTsystem/tmp/{}.zip").format(timestr)

	with zipfile.ZipFile(pluginfile, 'w') as zp:
			zp.writestr("manifest.json", manifestJson)
			zp.writestr("background.js", backgroundJs)


	################ OK ###################################################
	if sys.argv[4] == "1":
		#chrome_options.add_extension(pluginfile)
		pass
	else:
		chrome_options.add_extension(pluginfile)	

	chrome_options.add_experimental_option('prefs', {
		'credentials_enable_service': False,
		'profile': {
			'password_manager_enabled': False
		}
	})

	# For older ChromeDriver under version 79.0.3945.16
	chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
	chrome_options.add_experimental_option('useAutomationExtension', False)

	#For ChromeDriver version 79.0.3945.16 or over
	chrome_options.add_argument("--disable-blink-features")
	chrome_options.add_argument('--disable-blink-features=AutomationControlled')
	chrome_options.add_argument("--disable-infobars")

	chrome_options.add_argument ("window-size = 1280 800")
	#user_data_file_path = app_data_local+"/Google/Chrome/"+nazovSuboru
	#chrome_options.add_argument('user-data-dir='+user_data_file_path)

	settings_arr = [
					"disable-background-networking",
					"enable-features=NetworkService,NetworkServiceInProcess"
					"disable-background-timer-throttling",
					"disable-backgrounding-occluded-windows",
					"disable-breakpad",
					"disable-client-side-phishing-detection",
					"disable-component-extensions-with-background-pages",
					"disable-default-apps",
					"disable-dev-shm-usage",
					#"disable-extensions",
					"disable-features=Translate",
					"disable-hang-monitor",
					"disable-ipc-flooding-protection",
					"disable-popup-blocking",
					"disable-prompt-on-repost",
					"disable-renderer-backgrounding",
					"disable-sync",
					"start-maximized",
					"force-color-profile=srgb",
					"metrics-recording-only",
					"no-first-run",
					"enable-automation",
					"password-store=basic",
					"use-mock-keychain",
					"enable-blink-features=IdleDetection",
					"disable-infobars",
					"no-sandbox",
					"disable-setuid-sandbox",
					"single-process",
					"no-zygote",
					"disable-features=site-per-process",
					"disable-blink-features=AutomationControlled",
					"remote-debugging-port=0",
					"flag-switches-begin",
					"flag-switches-end",
					"lang=en-US",
					"num-raster-threads=4",
					"enable-main-frame-before-activation",
					"use-gl=swiftshader-webgl"
					]

	for row in settings_arr:
		chrome_options.add_argument('--'+row)

	################END OK ########################

	chrome_options.add_argument("--mute-audio")
	chrome_options.binary_location = os.path.abspath(__file__+"../../../application/Chromium/chrome.exe")
	#options.binary_location = os.path.abspath(__file__+"../../../application/Chromium/chrome.exe")
	driver = None

except Exception as e:
	exc_type, exc_obj, exc_tb = sys.exc_info()
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(traceback.format_exc()))
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(e))
	print("Error_log")
	
	farmingData = DBfunctions.select_from_table('FARMING', 'email_id, total_farming', 'email_id = {} and total_farming is not null '.format(id_email))
	if len(farmingData):
		dataCheck = DBfunctions.select_from_table('FARMING', 'status', 'email_id = {}'.format(id_email))
		if dataCheck[0][0] != 'CANCELED':
			setNewFarmingData = "status = '{}'".format("FAILED")
			#setNewFarmingData = "status = '{}'".format("FARMED")
			DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))
			setNewFarmingData = "last_farming_date = '{}'".format(datetime.datetime.now())
			DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))
	else:
		dataCheck = DBfunctions.select_from_table('FARMING', 'status', 'email_id = {}'.format(id_email))
		if dataCheck[0][0] != 'CANCELED':
			DBfunctions.update_table('FARMING', "status = 'FAILED'", f"email_id = {id_email}")
			setNewFarmingData = "last_farming_date = '{}'".format(datetime.datetime.now())
			DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))
		#DBfunctions.delete_from_table('FARMING',f"email_id = {id_email}")
	#archivovanie_historie()
	#os.remove(pluginfile)
	sys.exit()

try:
	chrome_options.add_argument("--window-position=-32000,-32000")
	driver = webdriver.Chrome(executable_path=pathToChrome,options=chrome_options)
	logger_process.info("Opening browser")
	time.sleep(3)	
	driver.execute_script("Object.defineProperty(navigator, 'webdriver', {get: () => undefined})")
	#Hide window
	#DATABASE SUBSESSION
	if(DBfunctions.check_if_table_exist("WINDOWS") == False):
		DBfunctions.create_table("WINDOWS","([id] INTEGER PRIMARY KEY,[date_created] date, [id_window] text,[id_email] text, [sessions_type] text, [status_id] text)")
	tabs = driver.window_handles
	counterTabs = 0
	time.sleep(2)
	if len(tabs) > 1:
		for tab in tabs:
			if counterTabs == 0:
				window_name = driver.window_handles[counterTabs]
				driver.switch_to.window(window_name=window_name)
				driver.close()
				window_name = driver.window_handles[0]
				driver.switch_to.window(window_name=window_name)
			counterTabs = counterTabs + 1
	time.sleep(2)
	driver.execute_script(f"document.title = '{email_id}#{id_email}'")
	time.sleep(3)
	#print(email_id+'#'+id_email)
	#print(maxmin.get_window_id(email_id+'#'+id_email))
	outputWin = maxmin.get_window_id(email_id+'#'+id_email)
	ID_window_MAIN_driver = outputWin[0].split("-")[0]
	ID_window_NUMB_driver = outputWin[0].split("###")[0]
	#Hide window function
	maxmin.hide_from_taskbar(int(ID_window_NUMB_driver))
	#maxmin.maximize_from_taskbar(int(ID_window_NUMB_driver))
	status = "1"
	#values = (f"'{datetime.datetime.now()}','{ID_window}', '{id_email}', '{session_type}'")
	values = ("'{}','{}', '{}', '{}','{}'").format(datetime.datetime.now(),ID_window_NUMB_driver,id_email,session_type,status)
	columns = "date_created, id_window, id_email, sessions_type, status_id"
	DBfunctions.insert_into_table('Windows', columns, values)

	#FARMING
	for webpage in GENWEB.select_page_generate(random.randrange(2,4)):
		driver.get(webpage)
		time.sleep(random.randrange(1,4))

	#STATUS SET
	
	farmingData = DBfunctions.select_from_table('FARMING', 'email_id, total_farming', 'email_id = {}'.format(id_email))
	farmDataRow = False
	if len(farmingData):
		farmDataRow = True
		setNewFarmingData = "status = '{}'".format("PROCESSING FARMING")
		DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))
	else:
		values = ("'{}','{}'").format(id_email,"PROCESSING FARMING")
		columns = "email_id,status"
		DBfunctions.insert_into_table('FARMING', columns, values)

	
	driver.get(selected_page)
	logger_process.info("Logging into email")
	time.sleep(5)
	if random_value == 1: 
		driver.find_element_by_xpath('//*[@id="openid-buttons"]/button[1]').click()

	#login = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "identifier")))
	# time.sleep(5)
	# login.send_keys(email_id)
	# time.sleep(2)
	# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='identifierNext']/div/button"))).click()
	# time.sleep(5)
	# logpass = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='password']/div[1]/div/div[1]/input")))  
	# driver.execute_script(f"document.getElementsByName('password')[0].value = '{password_id}';")
	# time.sleep(2)
	# driver.execute_script(f"document.getElementById('passwordNext').click();")

	driver.implicitly_wait(3)
	time.sleep(2)
	login = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "identifier")))
	time.sleep(1)
	VYTVORUCET.fillInputInRegistrationStep(driver, login, email_id)
	time.sleep(2)

	LoginNextButton = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='identifierNext']/div/button")))
	VYTVORUCET.clickOnButton(driver, LoginNextButton)
	time.sleep(1)

	
	# login = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "identifier")))
	# driver.implicitly_wait(3)
	# login.send_keys(email_id)
	# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='identifierNext']/div/button"))).click()
	# driver.implicitly_wait(3)

	if driver.find_elements_by_xpath('//*[@id="view_container"]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div/div[2]/div[2]/div'):
		logger_error.error('Your Google account was not found.')
		DBfunctions.update_table('FARMING', "status = 'FAILED'", f"email_id = {id_email}")
		DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
		sys.exit()

	driver.implicitly_wait(3)
	time.sleep(2)
	logpass = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='password']/div[1]/div/div[1]/input"))) 
	time.sleep(3)
	VYTVORUCET.fillInputInRegistrationStep(driver, logpass, password_id)
	time.sleep(2)

	LoginNextButton2 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, "passwordNext")))
	VYTVORUCET.clickOnButton(driver, LoginNextButton2)
	time.sleep(2)
	# logpass = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='password']/div[1]/div/div[1]/input")))  
	# driver.execute_script(f"document.getElementsByName('password')[0].value = '{password_id}';")
	# driver.implicitly_wait(3)
	# #logpass.send_keys(password_id)
	# driver.execute_script(f"document.getElementById('passwordNext').click();")
	# driver.implicitly_wait(3)
	# time.sleep(2)
	googleAllowMobile = driver.find_elements_by_xpath('//*[@id="yDmH0d"]/c-wiz[2]/c-wiz/div/div[1]/div/div/div/div[2]/div[3]/div/div[2]/div')
	if len(googleAllowMobile)>0:
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="yDmH0d"]/c-wiz[2]/c-wiz/div/div[1]/div/div/div/div[2]/div[3]/div/div[2]/div'))).click()
		time.sleep(2)
	
	googleMobileCheck = driver.find_elements_by_xpath('//*[@id="view_container"]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/ul/li[3]/div')
	if len(googleMobileCheck)>0:
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="view_container"]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/ul/li[3]/div'))).click()
		time.sleep(2)

	driver.implicitly_wait(3)
	if driver.find_elements_by_xpath('//*[@id="phoneNumberId"]'):
		time.sleep(2)
		if providerId == "LUCKYSMS":
			countryArray = country.split("_")
			countryLuckySMS = countryArray[1]
			countryName = countryLuckySMS
			countryNameForGmail = countryName.lower()
		else:
			countryLuckySMS = country
			countryNameForGmail = countryName
		

		dataCountryPrefix = DBfunctions.select_from_table('COUNTRY', 'prefix', "name = '{}'".format(countryNameForGmail.upper()))
		for rowCountryPrefix in dataCountryPrefix:
			countryPrefix = rowCountryPrefix[0] 
		countryPrefix = len(countryPrefix) 

		telephoneNumber = int(str(telephoneNumber)[countryPrefix:])
		
		time.sleep(3)
		driver.implicitly_wait(2)
		button = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="countryList"]/div[1]/div[1]/div[1]')))
		time.sleep(2)
		driver.execute_script("arguments[0].click();", button)
		time.sleep(2)
		buttonClick = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="countryList"]/div[2]/div[@data-value="{}"]'.format(countryNameForGmail))))
		hover = ActionChains(driver).move_to_element(buttonClick)
		time.sleep(3)
		hover.perform()
		driver.execute_script("arguments[0].click();", buttonClick)
		gmailNumber = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, "phoneNumberId")))
		gmailNumber.clear()
		gmailNumber.send_keys(telephoneNumber)
		time.sleep(2)
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="view_container"]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button/div[2]'))).click()

	time.sleep(2)
	driver.implicitly_wait(3)
	googleSetSkip = driver.find_elements_by_xpath('//*[@id="yDmH0d"]/c-wiz/div/div/div/div[2]/div[4]/div[1]')
	if len(googleSetSkip)>0:
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="yDmH0d"]/c-wiz/div/div/div/div[2]/div[4]/div[1]'))).click()
		time.sleep(2)
	allowSmartFeatures = driver.find_elements_by_xpath("//div[@role='dialog']")
	if len(allowSmartFeatures)>0:
		time.sleep(3)
		Button1allowSmartFeatures = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//div[@role="dialog"]/div[2]/div/div[2]/div[2]/label/span')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button1allowSmartFeatures)
		time.sleep(3)	
		Button2allowSmartFeatures = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, 'data_consent_dialog_next')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button2allowSmartFeatures)
		time.sleep(2)
		Button3allowSmartFeatures = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//div[@role="dialog"]/div[2]/div/div[3]/div[2]/label/span')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button3allowSmartFeatures)
		time.sleep(1)
		Button4allowSmartFeatures = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, 'data_consent_dialog_done')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button4allowSmartFeatures)
		time.sleep(2)

	googleMeetButton1 = driver.find_elements_by_xpath("//div[@role='alertdialog']")
	if len(googleMeetButton1)>0:
		time.sleep(2)	
		Button1googleMeetButton1 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[@role='alertdialog']/div[1]/div/div[2]/div[2]")))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button1googleMeetButton1)
		#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[@role='alertdialog']/div[1]/div/div[2]/div[2]"))).click()
		time.sleep(1)		

	driver.implicitly_wait(3)
	driver.get('https://www.google.com/')
	driver.implicitly_wait(2)
	time.sleep(2)
	driver.get('https://mail.google.com/')
	driver.implicitly_wait(2)
	time.sleep(3)

	googleAllowButton = driver.find_elements_by_xpath('//*[@id=":4u.contentEl"]/div/div[2]/div[2]/label/span')
	if len(googleAllowButton)>0:
		
		time.sleep(3)
		Button1 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[2]/div[2]/label/span')))
		VYTVORUCET.clickOnButton(driver, Button1)
		time.sleep(3)
		Button2 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, 'data_consent_dialog_next')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button2)
		time.sleep(3)
		Button3 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[3]/div[2]/label/span')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button3)
		time.sleep(3)
		Button4 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, 'data_consent_dialog_done')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button4)
		time.sleep(3)
		#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[2]/div[2]/label/span'))).click()
		# time.sleep(3)
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[22]/div[3]/button'))).click()
		# time.sleep(3)
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[3]/div[2]/label/span'))).click()
		# time.sleep(3)
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[22]/div[3]/button[1]'))).click()
		# time.sleep(3)
	time.sleep(1)
	googleMeetButton = driver.find_elements_by_xpath("//div[@role='alertdialog']")
	if len(googleMeetButton)>0:
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[@role='alertdialog']/div[1]/div/div[2]/div[2]"))).click()
		time.sleep(1)
	googleRefreshButton= driver.find_elements_by_xpath('/html/body/div[32]/div[3]/button')
	if len(googleRefreshButton)>0:	
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[32]/div[3]/button'))).click()
		driver.implicitly_wait(2)
		time.sleep(2)
	
	time.sleep(2)
	driver.get('https://mail.google.com/')
	driver.implicitly_wait(2)
	time.sleep(2)
	driver.get('https://mail.google.com/mail/u/0/#settings/fwdandpop')
	driver.implicitly_wait(2)
	time.sleep(2)
	forward_btn = driver.find_elements_by_xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[2]/input")
	driver.implicitly_wait(2)
	time.sleep(2)
	timeout = time.time() + 120
	while len(forward_btn)<=0:
		time.sleep(2)
		if time.time() > timeout:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			logger_error.error('Not logged to the email.')
			if driver != None:
				driver.quit()
				DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
				farmingData = DBfunctions.select_from_table('FARMING', 'email_id, total_farming', 'email_id = {} and total_farming is not null '.format(id_email))
				if len(farmingData):
					#setNewFarmingData = "status = '{}'".format("FARMED")
					dataCheck = DBfunctions.select_from_table('FARMING', 'status', 'email_id = {}'.format(id_email))
					if dataCheck[0][0] != 'CANCELED':
						setNewFarmingData = "status = '{}'".format("FAILED")
						DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))
				else:
					dataCheck = DBfunctions.select_from_table('FARMING', 'status', 'email_id = {}'.format(id_email))
					if dataCheck[0][0] != 'CANCELED':
						DBfunctions.update_table('FARMING', "status = 'FAILED'", f"email_id = {id_email}")
					#DBfunctions.delete_from_table('FARMING',f"email_id = {id_email}")
				#os.remove(pluginfile)
				sys.exit()
				break
		forward_btn = driver.find_elements_by_xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[2]/input")
		time.sleep(1)
		if len(forward_btn)>0:
			break

	# if len(forward_btn)<=0:
	# 	exc_type, exc_obj, exc_tb = sys.exc_info()
	# 	logger_error.error('Not logged to the email.')
	# 	if driver != None:
	# 		driver.quit()
	# 		DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
	# 		farmingData = DBfunctions.select_from_table('FARMING', 'email_id, total_farming', 'email_id = {} and total_farming is not null '.format(id_email))
	# 		if len(farmingData):
	# 			#setNewFarmingData = "status = '{}'".format("FARMED")
	# 			dataCheck = DBfunctions.select_from_table('FARMING', 'status', 'email_id = {}'.format(id_email))
	# 			if dataCheck[0][0] != 'CANCELED':
	# 				setNewFarmingData = "status = '{}'".format("FAILED")
	# 				DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))
	# 		else:
	# 			dataCheck = DBfunctions.select_from_table('FARMING', 'status', 'email_id = {}'.format(id_email))
	# 			if dataCheck[0][0] != 'CANCELED':
	# 				DBfunctions.update_table('FARMING', "status = 'FAILED'", f"email_id = {id_email}")
	# 			#DBfunctions.delete_from_table('FARMING',f"email_id = {id_email}")
	# 		#os.remove(pluginfile)
	# 		sys.exit()

	
except Exception as e:
	exc_type, exc_obj, exc_tb = sys.exc_info()
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(traceback.format_exc()))
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(e))
	print("Error_log")
	if driver != None:
		driver.quit()

	DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
	
	farmingData = DBfunctions.select_from_table('FARMING', 'email_id, total_farming', 'email_id = {} and total_farming is not null '.format(id_email))
	if len(farmingData):
		dataCheck = DBfunctions.select_from_table('FARMING', 'status', 'email_id = {}'.format(id_email))
		if dataCheck[0][0] != 'CANCELED':
			setNewFarmingData = "status = '{}'".format("FAILED")
			#setNewFarmingData = "status = '{}'".format("FARMED")
			DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))
	else:
		dataCheck = DBfunctions.select_from_table('FARMING', 'status', 'email_id = {}'.format(id_email))
		if dataCheck[0][0] != 'CANCELED':
			DBfunctions.update_table('FARMING', "status = 'FAILED'", f"email_id = {id_email}")

	if farmDataRow:
		farmingData = DBfunctions.select_from_table('FARMING', 'email_id, total_farming', 'email_id = {} and total_farming is not null'.format(id_email))
		if len(farmingData):
			for rowFarm in farmingData:
				(email_id_farm, total_farm_time) = tuple(rowFarm)    

				setNewFarmingData = "last_farming_date = '{}', total_farming = '{}', last_farming = '{}', status = '{}'".format(datetime.datetime.now(),total_farm_time+(time.time() - start_time),time.time() - start_time,'FARMED')
				DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))
		else:
			setNewFarmingData = "last_farming_date = '{}', total_farming = '{}', last_farming = '{}', status = '{}'".format(datetime.datetime.now(),time.time() - start_time,time.time() - start_time,'FARMED')
			DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))            

	else:
		setNewFarmingData = "last_farming_date = '{}', total_farming = '{}', last_farming = '{}', status = '{}'".format(datetime.datetime.now(),time.time() - start_time,time.time() - start_time,'FARMED')
		DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))
		#DBfunctions.delete_from_table('FARMING',f"email_id = {id_email}")
	#archivovanie_historie()
	#os.remove(pluginfile)
	sys.exit()


if driver == None:
	sys.exit()
######### SAVE ACTUAL TRENDS ##########
try:
		#DATABASE SUBSESSION
	#DBfunctions.delete_table("Trends")

	if(DBfunctions.check_if_table_exist("TRENDS") == False):
		DBfunctions.create_table("TRENDS","([id] INTEGER PRIMARY KEY,[date_created] date, [trend] text)")

	newest_trend = DBfunctions.select_from_table('TRENDS', 'max(date_created)','1=1')

	if newest_trend[0][0] == None:
		last_date_trend = datetime.date.today() - datetime.timedelta(days=90)
		last_date_trend_final =  last_date_trend
	else:
		last_date_trend = newest_trend[0][0]
		last_date_trend_final =  datetime.datetime.strptime(last_date_trend,'%Y-%m-%d %H:%M:%S.%f').date()

	delta = datetime.date.today() - last_date_trend_final

	if delta.days >= 7:
		driver.get("https://trends.google.com/trends/trendingsearches/daily?geo=US")
		trends = WebDriverWait(driver, delay).until(EC.presence_of_all_elements_located((By.XPATH, "//*[@class='details-bottom']/div[1]/div[1]/a")))

		for trend in trends:
			draft = trend.text
			outputTexta = draft.replace('...','')
			outputText = outputTexta.replace("'",'')
			values = ("'{}','{}'").format(datetime.datetime.now(),outputText)
			columns = "date_created, trend"
			DBfunctions.insert_into_table('TRENDS',columns,values)
		
except:
	driver.get("https://www.google.com/")
	logger_process.info("Something went wrong when trying to get actual trends.")

########GOOGLE FARMING#########
if farm_number_of_searching_items != 0:
	try:
		logger_process.info("Searching for new trends.")
		driver.get("https://www.google.com/")
		DBfunctions.update_table('FARMING', "status = 'BROWSING'", f"email_id = {id_email}")
		#Cookies agreement
		iframe = driver.find_elements_by_tag_name('iframe')[0]

		driver.switch_to_frame(iframe)

		cookies = driver.find_elements_by_id("introAgreeButton")

		if len(cookies) > 0:
			cookies[0].click()

		driver.switch_to.default_content()

		total_trends = DBfunctions.select_from_table('TRENDS', 'count(*)','1=1')[0][0]

		for i in range(1,farm_number_of_searching_items+1):
			try:
				time.sleep(4)

				random_trend_num = random.randrange(1,total_trends)
				newest_trend = DBfunctions.select_from_table('TRENDS', 'trend','1=1')[random_trend_num][0]
				trend_output = newest_trend.replace('...','')
				time.sleep(2)
				main_input = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "q")))
				main_input.send_keys(trend_output)
				time.sleep(3)
				form = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "btnK")))
				form.submit()
				
				time.sleep(4)
				total_items = driver.find_elements_by_xpath("//*[@class='g']/div/div[1]/a")
				total_items_rand_num = random.randrange(1,len(total_items))

				link_open = total_items[total_items_rand_num].get_attribute('href')
				driver.get(link_open)
				driver.execute_script("window.scrollBy(0,document.body.scrollHeight/8);")
				time.sleep(random.randrange(2,farm_max_sleep_on_one_position_shop))

				driver.back()
				driver.get("https://www.google.com/")
			except:
				driver.get("https://www.google.com/")
				logger_process.info("Something went wrong while searching on google. Trying again..")
				continue

	except:
		logger_process.info("Something went wrong while strating search on google. Going to another..")


######### YOUTUBE FARMING#######
if farm_number_of_videos != 0:
	driver.get("https://www.youtube.com/")
	logger_process.info("Watching videos on Youtube.")
	try:
		DBfunctions.update_table('FARMING', "status = 'YOUTUBE'", f"email_id = {id_email}")
		for i in range(1,farm_number_of_videos+1):
			try:

				videos = WebDriverWait(driver, delay).until(EC.presence_of_all_elements_located((By.XPATH, "//*[@id='thumbnail']")))

				video_number_rand = random.randrange(1,len(videos)) #vyber videa
				video_random_seconds = random.randrange(farm_min_seconds_on_videos,farm_max_seconds_on_video+1) #pocet sekund
				videos[video_number_rand].click()
				time.sleep(video_random_seconds)
				driver.get("https://www.youtube.com/")
			except:
				driver.get("https://www.youtube.com/")
				logger_process.info("Something went wrong while wathing Youtube. Trying again..")
				continue
	except:
		logger_process.info("Something went wrong while wathing Youtube. Going to another..")

########## GOOGLE NEWS ###########
if farm_number_of_news != 0:
	driver.get("https://news.google.com/")
	logger_process.info("Reading new articles.")
	try:
		DBfunctions.update_table('FARMING', "status = 'NEWS'", f"email_id = {id_email}")
		for i in range(1,farm_number_of_news+1):
			try:
				time.sleep(2)
				news_article = WebDriverWait(driver, delay).until(EC.presence_of_all_elements_located((By.TAG_NAME, "article")))
				article_number_rand = random.randrange(1,len(news_article))
				article_random_seconds = random.randrange(farm_min_seconds_on_news,farm_max_seconds_on_news+1) 
				driver.get(news_article[article_number_rand].find_element_by_xpath('.//a').get_attribute("href"))

				time.sleep(15)
				operators = ["+","-"]
				page_height_actual = 0

				for index,i in enumerate(range(1,farm_number_of_scrolls_on_news+1),1):
					page_scroll_del = 6
					page_height = driver.execute_script("return document.body.scrollHeight")
					
					if index < 3:
						scroll_pos_num = random.randrange(0,1)
					else:
						scroll_pos_num = random.randrange(0,2)

					scroll_pos = operators[scroll_pos_num]
					time.sleep(random.randrange(2,farm_max_sleep_on_one_position_news+1)) # Kolko pocka na jednej pozicii kym scrolluje
					driver.execute_script("window.scrollBy(0,"+ f'{scroll_pos}{page_height/page_scroll_del}'+");")


				time.sleep(article_random_seconds)

				#Cookies submit
				if len(driver.window_handles) > 1:
					window_after = driver.window_handles[1]
					driver.switch_to_window(window_after)
					driver.close()
					window_main = driver.window_handles[0]
					driver.switch_to_window(window_main)

				driver.back()
			except:
				driver.get("https://news.google.com/")
				logger_process.info("Something went wrong while reading Google NEWS. Trying again..")
				continue
	except:
		logger_process.info("Something went wrong while reading Google NEWS. Going to another..")

########GOOGLE SHOPPING#########
if farm_number_of_shop_categ != 0:
	logger_process.info("Shopping on internet.")
	try:
		DBfunctions.update_table('FARMING', "status = 'SHOPPING'", f"email_id = {id_email}")
		for i in range(1,farm_number_of_shop_categ+1):
			try:
				time.sleep(2)

				driver.get("https://www.google.com/shopping")

				time.sleep(15)

				#Cookies agreement
				iframe = driver.find_elements_by_tag_name('iframe')[0]

				driver.switch_to_frame(iframe)

				cookies = driver.find_elements_by_id("introAgreeButton")

				if len(cookies) > 0:
					cookies[0].click()

				driver.switch_to.default_content()

				search_box = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "q")))

				search_box.send_keys(f'{shopping_categories[random.randrange(0,len(shopping_categories))]}')

				#driver.execute_script("document.getElementsByName('q').addEventListener('keyup', function(event) { if (event.keyCode === 13) { document.getElementById('myFormID').submit();return false;}});")
				#form = driver.find_element_by_id('tsf')
				form = driver.find_element_by_name('btnK')
				form.submit()

				total_items = driver.find_elements_by_xpath("//*[@id='rso']/div/div/div/div")

				for i in range(1,farm_number_of_items_in_categ):
					total_items_rand_num = random.randrange(1,len(total_items))
					driver.get(WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, f"//*[@id='rso']/div/div/div/div[{total_items_rand_num}]/div/div/div[2]/div[1]/div/a"))).get_attribute("href"))
					driver.execute_script("window.scrollBy(0,document.body.scrollHeight/8);")
					time.sleep(random.randrange(2,farm_max_sleep_on_one_position_shop))
					driver.back()
			except:
				driver.get("https://www.google.com/shopping")
				logger_process.info("Something went wrong when trying to shopping. Trying again..")
				continue
	except:
		logger_process.info("Something went wrong when trying to shopping. Going to another..")

try:
	#End 
	logger_process.info("Completing the farming process.")
	time.sleep(2)
	driver.quit()
	DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')

	#os.remove(pluginfile)

	

	if farmDataRow:
		farmingData = DBfunctions.select_from_table('FARMING', 'email_id, total_farming', 'email_id = {} and total_farming is not null'.format(id_email))
		if len(farmingData):
			for rowFarm in farmingData:
				(email_id_farm, total_farm_time) = tuple(rowFarm)    

				setNewFarmingData = "last_farming_date = '{}', total_farming = '{}', last_farming = '{}', status = '{}'".format(datetime.datetime.now(),total_farm_time+(time.time() - start_time),time.time() - start_time,'FARMED')
				DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))
		else:
			setNewFarmingData = "last_farming_date = '{}', total_farming = '{}', last_farming = '{}', status = '{}'".format(datetime.datetime.now(),time.time() - start_time,time.time() - start_time,'FARMED')
			DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))            

	else:
		setNewFarmingData = "last_farming_date = '{}', total_farming = '{}', last_farming = '{}', status = '{}'".format(datetime.datetime.now(),time.time() - start_time,time.time() - start_time,'FARMED')
		DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))

	logger_process.info("Email farmed successfully")
	#archivovanie_historie()

except Exception as e:
	exc_type, exc_obj, exc_tb = sys.exc_info()
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(traceback.format_exc()))
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(e))
	print("Error_log")
	driver.quit()
	DBfunctions.update_table('Windows', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
	
	farmingData = DBfunctions.select_from_table('FARMING', 'email_id, total_farming', 'email_id = {} and total_farming is not null '.format(id_email))
	if len(farmingData):
		dataCheck = DBfunctions.select_from_table('FARMING', 'status', 'email_id = {}'.format(id_email))
		if dataCheck[0][0] != 'CANCELED':
			setNewFarmingData = "status = '{}'".format("FAILED")
			#setNewFarmingData = "status = '{}'".format("FARMED")
			DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))
	else:
		dataCheck = DBfunctions.select_from_table('FARMING', 'status', 'email_id = {}'.format(id_email))
		if dataCheck[0][0] != 'CANCELED':
			DBfunctions.update_table('FARMING', "status = 'FAILED'", f"email_id = {id_email}")

	if farmDataRow:
		farmingData = DBfunctions.select_from_table('FARMING', 'email_id, total_farming', 'email_id = {} and total_farming is not null'.format(id_email))
		if len(farmingData):
			for rowFarm in farmingData:
				(email_id_farm, total_farm_time) = tuple(rowFarm)    

				setNewFarmingData = "last_farming_date = '{}', total_farming = '{}', last_farming = '{}', status = '{}'".format(datetime.datetime.now(),total_farm_time+(time.time() - start_time),time.time() - start_time,'FARMED')
				DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))
		else:
			setNewFarmingData = "last_farming_date = '{}', total_farming = '{}', last_farming = '{}', status = '{}'".format(datetime.datetime.now(),time.time() - start_time,time.time() - start_time,'FARMED')
			DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))            

	else:
		setNewFarmingData = "last_farming_date = '{}', total_farming = '{}', last_farming = '{}', status = '{}'".format(datetime.datetime.now(),time.time() - start_time,time.time() - start_time,'FARMED')
		DBfunctions.update_table('FARMING', setNewFarmingData, 'email_id = {}'.format(id_email))
		#DBfunctions.delete_from_table('FARMING',f"email_id = {id_email}")
	#archivovanie_historie()
	#os.remove(pluginfile)
	sys.exit()
