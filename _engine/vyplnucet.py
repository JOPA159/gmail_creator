import random
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time

delay = 45
class VYTVORUCET():
	global delay
	def firstStepAndChangeLangueageRegisterForm(driver):
		time.sleep(2)
		
		elemRegButt1 = driver.find_elements_by_xpath('/html/body/div[3]/div[1]/div[4]/ul[1]/li[3]/a')
		elemRegButt2 = driver.find_elements_by_xpath('/html/body/div[4]/div/div/div/div[1]/div/div[3]/a[1]')
		elemRegButt3 = driver.find_elements_by_xpath('/html/body/div[2]/div[1]/div[4]/ul[1]/li[3]/a')
		elemRegButt4 = driver.find_elements_by_xpath('/html/body/div/header/div/div/div/div/a')
		elemRegButt5 = driver.find_elements_by_xpath('/html/body/header/div/div/div/a[3]')
		
		#WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[1]/div[4]/ul[1]/li[3]/a'))).click()
		if(len(elemRegButt1)>0):
			createAccountElement = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[3]/div[1]/div[4]/ul[1]/li[3]/a')))
		elif(len(elemRegButt2)>0):
			createAccountElement = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[4]/div/div/div/div[1]/div/div[3]/a[1]')))
		elif(len(elemRegButt3)>0):
			createAccountElement = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div[1]/div[4]/ul[1]/li[3]/a')))
		elif(len(elemRegButt4)>0):
			createAccountElement = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div/header/div/div/div/div/a')))
		elif(len(elemRegButt5)>0):
			createAccountElement = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.XPATH, '/html/body/header/div/div/div/a[3]')))
		else:
			createAccountElement = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.PARTIAL_LINK_TEXT, 'accounts.google.com')))
		#driver.find_element_by_xpath("//a[contains(@href,'accounts.google.com')]").click()
		
		time.sleep(1)
		#createAccountElement = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div[1]/div[4]/ul[1]/li[3]/a')))
		ActionChains(driver).move_to_element(createAccountElement).click(createAccountElement).perform()
		tabs = driver.window_handles
		counterTabs = 0
		time.sleep(2)
		if len(tabs) > 1:
			for tab in tabs:
				if counterTabs == 0:
					window_name = driver.window_handles[counterTabs]
					driver.switch_to.window(window_name=window_name)
					driver.close()
					window_name = driver.window_handles[0]
					driver.switch_to.window(window_name=window_name)
				counterTabs = counterTabs + 1
		driver.implicitly_wait(3)
		time.sleep(2)
		skuskaInternetu = driver.find_elements_by_xpath("//div[@aria-selected='true']")
		if len(skuskaInternetu)<=0:
			for i in range(3):
				driver.get("https://accounts.google.com/signup/v2/webcreateaccount?service=mail&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F%3Fpc%3Dtopnav-about-n-en&hl=en&flowName=GlifWebSignIn&flowEntry=SignUp")
				driver.implicitly_wait(3)
				time.sleep(2)
				skuskaInternetu = driver.find_elements_by_xpath("//div[@aria-selected='true']")
				if len(skuskaInternetu)>0:
					break
		#langChooser = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[@aria-selected='true']")))		
		langChooser = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.XPATH, "//div[@aria-selected='true']")))
		time.sleep(1)
		ActionChains(driver).move_to_element(langChooser).click(langChooser).perform()
		time.sleep(3)

		langChooser1 = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="lang-chooser"]/div[2]/div[@data-value="en"]')))
		#langChooser1 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="lang-chooser"]/div[2]/div[@data-value="en"]')))
		time.sleep(1)
		ActionChains(driver).move_to_element(langChooser1).perform()
		time.sleep(1)
		ActionChains(driver).click(langChooser1).perform()		
		time.sleep(1)

	def fillInputInRegistrationStep(driver, element, string):
		ActionChains(driver).move_to_element(element).perform()
		time.sleep(0.1)
		ActionChains(driver).click(element).perform()	
		time.sleep(0.3)
		for char in range(0, len(string)):
			time.sleep(random.uniform(0.1, 0.2))
			ActionChains(driver).key_down(string[char]).key_up(string[char]).perform()	

	def clickOnButton(driver, element):
		ActionChains(driver).move_to_element(element).perform()
		time.sleep(0.1)
		ActionChains(driver).click(element).perform()	
		time.sleep(0.3)	

	def clearInput(element):
		while len(element.get_attribute('value')) != 0:
			element.send_keys(Keys.BACKSPACE)	

	def cakanieNaElementXPATH(driver, XPATH):	
		time.sleep(2)
		timeout = time.time() + 120	
		button = driver.find_elements_by_xpath(XPATH)
		while len(button)<=0:	
			if time.time() > timeout:
				break
			button = driver.find_elements_by_xpath(XPATH)
			time.sleep(1)
			if len(button)>0:
				break

	def cakanieNaElementID(driver, ID):	
		time.sleep(2)
		timeout = time.time() + 120	
		button = driver.find_elements_by_id(ID)
		while len(button)<=0:	
			if time.time() > timeout:
				break
			button = driver.find_elements_by_id(ID)
			time.sleep(1)
			if len(button)>0:
				break		
