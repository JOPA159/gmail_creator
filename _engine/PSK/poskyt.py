import sys
import json
import sqlite3
import os

pathToDb = os.getenv('APPDATA')+"/KOALABOTsystem/DBemails.db"
# You can create a new database by changing the name within the quotes
conn = sqlite3.connect(pathToDb, timeout=60)
c = conn.cursor()  # The database will be saved in the location where your 'py' file is saved

# Create table - CLIENTS
table_name = 'PROVIDERS'
#c.execute('''DELETE FROM PROXIES''')
c.execute(
	f'''SELECT count(*) FROM sqlite_master WHERE type='table' AND name='{table_name}' ''')
table_check = c.fetchone()[0]

if table_check == 0:
	print("Creating new table")
	c.execute('''CREATE TABLE PROVIDERS
				([id] INTEGER PRIMARY KEY,[provider] text, [key] text, [email_secret] text, [timeout] text, [balance] text, [created_at] date,[updated_at] date  )''')

conn.commit()

output = json.load(sys.stdin)

#c.execute('''DELETE FROM EMAILS''')

for line in output:
	last_id = c.execute('''select max(id) from PROVIDERS''')

	#Toto je script na unikátne ID aby to vytvýralo v databaze
	last_id = last_id.fetchone()[0]
	if last_id != None:
		new_id = last_id+1
	else:
		new_id = '1'
	#print(last_id)
	c.execute(
		f'''insert into PROVIDERS select '{new_id}','{line['provider']}','{line['key']}','{line['secret']}','{line['timeout']}','',datetime('now'),datetime('now')''')
	#line['username']
	#print(line['hostname'])
conn.commit()
resultQuery = c.execute('''select * from PROVIDERS''')
print(json.dumps(resultQuery.fetchall()))

#print(output)


#result = testovanie.json()

#print(result['hostname'])

