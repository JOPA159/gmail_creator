# coding: iso-8859-1
#%%
import sys, json, sqlite3, os, re, datetime

recoveryEmailRegex = r'(?:[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])'

pathToDb = os.getenv('APPDATA')+"/KOALABOTsystem/DBemails.db"
conn = sqlite3.connect(pathToDb, timeout=60)  # You can create a new database by changing the name within the quotes
c = conn.cursor() # The database will be saved in the location where your 'py' file is saved

# Create table - CLIENTS
table_name = 'PROXIES'
#c.execute('''DELETE FROM PROXIES''')
c.execute(f'''SELECT count(*) FROM sqlite_master WHERE type='table' AND name='{table_name}' ''')
table_check = c.fetchone()[0]

if table_check == 0:
	print("Creating new table")
	c.execute('''CREATE TABLE PROXIES
				([id] INTEGER PRIMARY KEY,[date_created] date, [ipaddress] text, [port] text, [username] text, [password] text, [id_email] text,[date_email] date  )''')

conn.commit()

output = json.load(sys.stdin)
max_proxy_id = c.execute('''select max(id) from PROXIES''').fetchone()[0]
max_email_id = c.execute('''select max(id) from EMAILS''').fetchone()[0]
provider_list = c.execute('''select provider from PROVIDERS''').fetchall()
country_list = c.execute('''select name from COUNTRY union select system||'_'||name from COUNTRY_LUCKY''').fetchall()
today = datetime.datetime.now()

#print(len([item for item in country_list if item[0].upper() == 'SYSTEM4_BD']))
try:
	for i,line in enumerate(output,1):	
		print('PROGRESSNUM:'+str(i)) 
		#gmail kontr
		if len(line['proxyGroup']) > 25:
			raise Exception('Your proxy group name is too long... Max proxy group length is 25.')
		else:
			if len(line['proxyGroup']) == 0:
				proxy_group_val = 'DEFAULT GROUP'
			else:
				proxy_group_val = line['proxyGroup']

		if(bool(re.match(r'^([A-Za-z0-9_\-\.])+\@([gmail|GMAIL])+\.(com)$',line['email'])) == False):
			raise Exception('Wrong email format(must be: testemail@gmail.com). Only gmail accounts are accepted.')
		
		if('@' in line['email']):
			email_name = line['email'].split("@",1)[0]
		else:
			email_name = line['email']

		c.execute(f'''INSERT INTO PROXIES (date_created,hostname,port,username,password,proxy_group) VALUES(datetime('now'),'{line['ipaddress']}','{line['port']}','{line['username']}','{line['password']}','{proxy_group_val}')''')
		new_proxy_id = c.execute('''select max(id) from PROXIES''').fetchone()[0]

		if line['status'] == 'registered':
			try:
				if 'phone' in line:
					if(bool(re.match(r'^([\s\d]+)$',str(line['phone']))) == False):
						raise Exception('Wrong phone format(must be: 155688888944). Only telephone prefix and telephone number without any + or other special characters.')
			except Exception as e:
				print(e.args)
				sys.exit()

			#c.execute(f'''INSERT INTO EMAILS (created,email,password,proxyId,telephoneNumber,country,smsProviderId,status,registered_at) VALUES('{today}','{email_name}','{line['email_password']}','{new_proxy_id}','{line['phone']}','{line['country']}','{line['smsProviderId']}','{line['status']}','{today}')''')
			if 'phone' in line:
				c.execute(f'''INSERT INTO EMAILS (created,email,password,proxyId,telephoneNumber,status) VALUES('{today}','{email_name}','{line['email_password']}','{new_proxy_id}','{line['phone']}','{line['status']}')''')
			else:
				c.execute(f'''INSERT INTO EMAILS (created,email,password,proxyId,status) VALUES('{today}','{email_name}','{line['email_password']}','{new_proxy_id}','{line['status']}')''')
			
			new_email_id = c.execute('''select max(id) from EMAILS''').fetchone()[0]
			c.execute(f'''UPDATE PROXIES SET id_email = '{new_email_id}' WHERE id = {new_proxy_id}''')

		elif line['status'] == 'unregistered':
			try:
				if 'recoveryId' in line:
					if(bool(re.match(recoveryEmailRegex,line['recoveryId'].lower())) == False):
						raise Exception('Wrong email format of RECOVERY EMAIL(must be: testemail@domain.com). Accepted only email formats.')
			except Exception as e:
				print(e.args)
				sys.exit()

			#kontrola providerov
			try:
				if len([item for item in provider_list if item[0] == line['smsProviderId'].upper()]) == 0:
					raise Exception('Check if the provider is supported and set it in the Providers tab. If not, change it to another we support.')
			except Exception as e:
				print(e.args)
				sys.exit()
			
			#kontrola krajín
			try:
				if len([item for item in country_list if item[0].upper() == line['country'].upper()]) == 0:

					raise Exception('Check if the provider is supported and set it in the Providers tab. If not, change it to another we support.')
			except Exception as e:
				print(e.args)
				sys.exit()
			
			if line['smsProviderId'].upper() == 'LUCKYSMS':
				lower_country = line['country'].lower()
				if 'system' in lower_country and '_' in lower_country and bool(re.match(r'^([^\_]+)([_][a-zA-Z]*)',line['country'])):
					country_val = line['country'].split("_",1)[0].lower() + "_" + line['country'].split("_",1)[1].upper()
				else:
					raise Exception('Bad country value. LUCKYSMS provider must contain the country code of the system. For example, system4_US.')
			
			if('recoveryId' in line and len(line['recoveryId']) > 0):
				c.execute(f'''INSERT INTO EMAILS (created,email,password,firstName,lastName,day,month,year,gender,country,smsProviderId,proxyId,status,recoveryId) VALUES('{today}','{email_name}','{line['email_password']}','{line['firstName']}','{line['lastName']}','{line['day']}','{line['month']}','{line['year']}','{line['gender']}','{country_val}','{line['smsProviderId']}','{new_proxy_id}','{line['status']}','{line['recoveryId']}')''')
			else:
				c.execute(f'''INSERT INTO EMAILS (created,email,password,firstName,lastName,day,month,year,gender,country,smsProviderId,proxyId,status) VALUES('{today}','{email_name}','{line['email_password']}','{line['firstName']}','{line['lastName']}','{line['day']}','{line['month']}','{line['year']}','{line['gender']}','{country_val}','{line['smsProviderId']}','{new_proxy_id}','{line['status']}')''')
			new_email_id = c.execute('''select max(id) from EMAILS''').fetchone()[0]
			c.execute(f'''UPDATE PROXIES SET id_email = '{new_email_id}' WHERE id = {new_proxy_id}''')
	conn.commit()
		#resultQuery = c.execute('''select * from PROXIES''')
		
except Exception as e:
	c.execute(f'''DELETE FROM PROXIES WHERE id > {max_email_id}''')
	c.execute(f'''DELETE FROM EMAILS WHERE id > {max_proxy_id}''')
	print('ERROR REASON - '+ str(e))

print("IMPORT_DONE")
