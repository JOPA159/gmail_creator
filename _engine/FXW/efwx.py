try:
	from selenium import webdriver
	from selenium.webdriver .chrome.options import Options
	from selenium.webdriver.common.by import By
	from selenium.webdriver.support.ui import WebDriverWait
	from selenium.webdriver.support import expected_conditions as EC
	from selenium.webdriver.common.action_chains import ActionChains
	import time
	import os.path
	import random
	import re
	import sys
	import datetime
	import sqlite3
	import pyautogui
	import requests
	import zipfile
	import traceback
	from selenium.webdriver.support.select import Select

	path_to_myproject1 = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	sys.path.insert(0, path_to_myproject1)

	from litedbf import DBfunctions
	import HG.maxmin as maxmin 
	from setpx import SetProxy
	from logger import Logger
	from krajnahod import GENWEB
	from vyplnucet import VYTVORUCET
	from zamikanie import ZAMIKANIE

	stringKluc = str(sys.argv[5])
	odzamknute = ZAMIKANIE.decrypt('SecretKey', stringKluc)
	if odzamknute == 'false':
		sys.exit()

	#SYSTEM VAR
	delay = 45
	session_type = 'FORWARDING'
	pathToChrome = os.path.abspath(__file__+"../../../chromedriver.exe")

	if sys.argv[4] != '0ced6612-79b9-41f9-bb2c-d75b64aae37a':
		sys.exit()

	id_email = str(sys.argv[1])
	proxyCustomForward = str(sys.argv[6])
	#email_id = 'mpav406@gmail.com'
	#password_id = 'Idemsem-123'
	#email na ktory sa budu odosielat emaily(HLAVNY EMAIL)
	id_email_forward = str(random.randrange(20,99999))
	email_id_forward = str(sys.argv[2])
	password_id_forward = str(sys.argv[3])

	#DELETE ALL FORWARD HISTORY
	DBfunctions.delete_from_table('FORWARDING',f"email_id = {id_email}")

	values = ("'{}','{}','{}','{}','{}'").format("",id_email,email_id_forward,"INITIALIZING PROCESS",proxyCustomForward)
	columns = "forwarding_date, email_id, forward_email, status, forward_proxy"
	DBfunctions.insert_into_table('FORWARDING', columns, values)

	data = DBfunctions.select_from_table('EMAILS', '*', 'id = {}'.format(id_email))
	for row in data:
		(id, created, email_id, password_id, firstName, lastName, day, month, year, gender, telephoneNumber, country, providerId, gmailCode, proxyId,status,scoreV3, scoreV2, recoveryId, registeredAt) = tuple(row)

	dataProxies = DBfunctions.select_from_table('PROXIES', 'hostname, port, username, password', 'id = {}'.format(proxyId))
	for rowProxy in dataProxies:
		(hostnameProxy, portProxy, usernameProxy, passwordProxy) = tuple(rowProxy)

	logger_error = Logger.setup_logger('first logger', 'error.log','error',email_id,'FORWARDING','FORWARDING-EMAIL')
	logger_process = Logger.setup_logger('second logger', 'process.log','process',email_id,'FORWARDING','FORWARDING-EMAIL')

	logger_process.info("Setting things up")

	pages = [
				"https://www.ebay.com/signin/ggl/init?ru=https%3A%2F%2Fwww.ebay.com%2F&sclSignin=1",
				'https://stackoverflow.com/users/signup?ssrc=head&returnurl=%2fusers%2fstory%2fcurrent',
				'https://www.ebay.com/signin/ggl/init?ru=https%3A%2F%2Fwww.ebay.com%2F&sclSignin=1',
				'https://accounts.google.com/ServiceLogin/signinchooser?hl=sk&passive=true&continue=https%3A%2F%2Fwww.google.com%2F&ec=GAZAmgQ&flowName=GlifWebSignIn&flowEntry=ServiceLogin'
			]

	random_value = random.randrange(4)
	selected_page = pages[random_value]

	hostname = hostnameProxy  # rotating proxy or host
	port = portProxy # port
	proxy_username = usernameProxy # username
	proxy_password = passwordProxy # password


	manifestJson = SetProxy.getManifest()
	backgroundJs = SetProxy.setBackgroundJs(hostname, port, proxy_username, proxy_password)

	timestr = time.strftime("%Y%m%d-%H%M%S")
	chrome_options = webdriver.ChromeOptions()
	pluginfile = os.path.abspath(os.getenv('APPDATA')+"/KOALABOTsystem/tmp/{}.zip").format(timestr)
	with zipfile.ZipFile(pluginfile, 'w') as zp:
			zp.writestr("manifest.json", manifestJson)
			zp.writestr("background.js", backgroundJs)

	################ OK ###################################################
	if sys.argv[7] == "1":
		#chrome_options.add_extension(pluginfile)
		pass
	else:
		chrome_options.add_extension(pluginfile)	

	chrome_options.add_experimental_option('prefs', {
		'credentials_enable_service': False,
		'profile': {
			'password_manager_enabled': False
		}
	})

	# For older ChromeDriver under version 79.0.3945.16
	chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
	chrome_options.add_experimental_option('useAutomationExtension', False)

	#For ChromeDriver version 79.0.3945.16 or over
	chrome_options.add_argument("--disable-blink-features")
	chrome_options.add_argument('--disable-blink-features=AutomationControlled')
	chrome_options.add_argument("--disable-infobars")

	chrome_options.add_argument ("window-size = 1280 800")
	#user_data_file_path = app_data_local+"/Google/Chrome/"+nazovSuboru
	#chrome_options.add_argument('user-data-dir='+user_data_file_path)

	settings_arr = [
					"disable-background-networking",
					"enable-features=NetworkService,NetworkServiceInProcess"
					"disable-background-timer-throttling",
					"disable-backgrounding-occluded-windows",
					"disable-breakpad",
					"disable-client-side-phishing-detection",
					"disable-component-extensions-with-background-pages",
					"disable-default-apps",
					"disable-dev-shm-usage",
					#"disable-extensions",
					"disable-features=Translate",
					"disable-hang-monitor",
					"disable-ipc-flooding-protection",
					"disable-popup-blocking",
					"disable-prompt-on-repost",
					"disable-renderer-backgrounding",
					"disable-sync",
					"start-maximized",
					"force-color-profile=srgb",
					"metrics-recording-only",
					"no-first-run",
					"enable-automation",
					"password-store=basic",
					"use-mock-keychain",
					"enable-blink-features=IdleDetection",
					"disable-infobars",
					"no-sandbox",
					"disable-setuid-sandbox",
					"single-process",
					"no-zygote",
					"disable-features=site-per-process",
					"disable-blink-features=AutomationControlled",
					"remote-debugging-port=0",
					"flag-switches-begin",
					"flag-switches-end",
					"lang=en-US",
					"num-raster-threads=4",
					"enable-main-frame-before-activation",
					"use-gl=swiftshader-webgl"
					]

	for row in settings_arr:
		chrome_options.add_argument('--'+row)

	################END OK ########################

	chrome_options.add_argument("--mute-audio")
except Exception as e:
	exc_type, exc_obj, exc_tb = sys.exc_info()
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(traceback.format_exc()))
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(e))
	print("Error_log")
	#os.remove(pluginfile)

	forwardData = DBfunctions.select_from_table('FORWARDING', 'email_id', 'email_id = {}'.format(id_email))
	if len(forwardData):
		dataCheck = DBfunctions.select_from_table('FORWARDING', 'status', 'email_id = {}'.format(id_email))
		if dataCheck[0][0] != 'CANCELED':
			DBfunctions.update_table('FORWARDING', "status = 'FAILED'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")	
	else:
		DBfunctions.update_table('FORWARDING', "status = 'FAILED'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")	

	#DBfunctions.delete_from_table('FORWARDING',f"email_id = {id_email} and forward_email = '{email_id_forward}'")
	sys.exit()

try:
	chrome_options.add_argument("--window-position=-32000,-32000")
	chrome_options.binary_location = os.path.abspath(__file__+"../../../application/Chromium/chrome.exe")
	#chrome_options.binary_location = os.path.abspath(__file__+"../../../application/chrome.exe")
	driver = webdriver.Chrome(executable_path=pathToChrome,options=chrome_options)
	logger_process.info("Opening browser")
	#Hide window
	#DATABASE SUBSESSION
	if(DBfunctions.check_if_table_exist("WINDOWS") == False):
		DBfunctions.create_table("WINDOWS","([id] INTEGER PRIMARY KEY,[date_created] date, [id_window] text,[id_email] text, [sessions_type] text, [status_id] text)")
	tabs = driver.window_handles
	counterTabs = 0
	time.sleep(2)
	if len(tabs) > 1:
		for tab in tabs:
			if counterTabs == 0:
				window_name = driver.window_handles[counterTabs]
				driver.switch_to.window(window_name=window_name)
				driver.close()
				window_name = driver.window_handles[0]
				driver.switch_to.window(window_name=window_name)
			counterTabs = counterTabs + 1
	time.sleep(2)
	driver.execute_script(f"document.title = '{email_id}#{id_email}'")
	time.sleep(1)
	#print(email_id+'#'+id_email)
	#print(maxmin.get_window_id(email_id+'#'+id_email))
	outputWin = maxmin.get_window_id(email_id+'#'+id_email)
	ID_window_MAIN_driver = outputWin[0].split("-")[0]
	ID_window_NUMB_driver = outputWin[0].split("###")[0]
	#Hide window function
	maxmin.hide_from_taskbar(int(ID_window_NUMB_driver))
	status = "1"
	#values = (f"'{datetime.datetime.now()}','{ID_window}', '{id_email}', '{session_type}'")
	values = ("'{}','{}', '{}', '{}','{}'").format(datetime.datetime.now(),ID_window_NUMB_driver,id_email,session_type,status)
	columns = "date_created, id_window, id_email, sessions_type, status_id"
	DBfunctions.insert_into_table('WINDOWS', columns, values)

	for webpage in GENWEB.select_page_generate(random.randrange(2,4)):
		driver.get(webpage)
		time.sleep(random.randrange(1,4))

	DBfunctions.update_table('FORWARDING', 'status = "PROCESSING FORWARD"', 'email_id = {}'.format(id_email))
	
	driver.get(selected_page)
	time.sleep(random.randrange(1,4))
	logger_process.info("Logging into email")
	if random_value == 1: 
		driver.find_element_by_xpath('//*[@id="openid-buttons"]/button[1]').click()
	#TREBA SEM POTOM DAT RANDOM_NAME namiesto EMAIL_ID !!!!!!!
	# login = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "identifier")))
	# time.sleep(2)
	# login.send_keys(email_id)
	# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='identifierNext']/div/button"))).click()
	# driver.implicitly_wait(2)
	# time.sleep(2)
	driver.implicitly_wait(3)
	time.sleep(2)
	login = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "identifier")))
	time.sleep(1)
	VYTVORUCET.fillInputInRegistrationStep(driver, login, email_id)
	time.sleep(2)

	LoginNextButton = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='identifierNext']/div/button")))
	VYTVORUCET.clickOnButton(driver, LoginNextButton)
	time.sleep(1)


	if driver.find_elements_by_xpath('//*[@id="view_container"]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div/div[2]/div[2]/div'):
		logger_error.error('Your Google account was not found.')
		DBfunctions.update_table('FORWARDING', "status = 'FAILED'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")	
		DBfunctions.update_table('WINDOWS', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
		driver.quit()
		sys.exit()
	
	driver.implicitly_wait(3)
	time.sleep(2)
	logpass = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='password']/div[1]/div/div[1]/input"))) 
	time.sleep(1)
	VYTVORUCET.fillInputInRegistrationStep(driver, logpass, password_id)
	time.sleep(2)

	LoginNextButton2 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, "passwordNext")))
	VYTVORUCET.clickOnButton(driver, LoginNextButton2)
	time.sleep(1)
	
	# time.sleep(5)
	# logpass = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='password']/div[1]/div/div[1]/input")))  
	# driver.execute_script(f"document.getElementsByName('password')[0].value = '{password_id}';")
	# #logpass.send_keys(password_id)
	# time.sleep(2)
	# driver.execute_script(f"document.getElementById('passwordNext').click();")
	#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='passwordNext']/div/button"))).click()
	
	googleAllowMobile = driver.find_elements_by_xpath('//*[@id="yDmH0d"]/c-wiz[2]/c-wiz/div/div[1]/div/div/div/div[2]/div[3]/div/div[2]/div')
	if len(googleAllowMobile)>0:
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="yDmH0d"]/c-wiz[2]/c-wiz/div/div[1]/div/div/div/div[2]/div[3]/div/div[2]/div'))).click()
		time.sleep(2)

	googleMobileCheck = driver.find_elements_by_xpath('//*[@id="view_container"]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/ul/li[3]/div')
	if len(googleMobileCheck)>0:
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="view_container"]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/ul/li[3]/div'))).click()
		time.sleep(2)

	driver.implicitly_wait(3)
	if driver.find_elements_by_xpath('//*[@id="phoneNumberId"]'):
		time.sleep(2)
		if providerId == "LUCKYSMS":
			countryArray = country.split("_")
			countryLuckySMS = countryArray[1]
		else:
			countryLuckySMS = country
		
		countryName = countryLuckySMS
		countryNameForGmail = countryName.lower()

		dataCountryPrefix = DBfunctions.select_from_table('COUNTRY', 'prefix', "name = '{}'".format(countryNameForGmail.upper()))
		for rowCountryPrefix in dataCountryPrefix:
			countryPrefix = rowCountryPrefix[0] 
		countryPrefix = len(countryPrefix) 

		telephoneNumber = int(str(telephoneNumber)[countryPrefix:])
		
		time.sleep(3)
		driver.implicitly_wait(2)
		button = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="countryList"]/div[1]/div[1]/div[1]')))
		time.sleep(2)
		driver.execute_script("arguments[0].click();", button)
		time.sleep(2)
		buttonClick = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="countryList"]/div[2]/div[@data-value="{}"]'.format(countryNameForGmail))))
		hover = ActionChains(driver).move_to_element(buttonClick)
		time.sleep(3)
		hover.perform()
		driver.execute_script("arguments[0].click();", buttonClick)
		gmailNumber = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, "phoneNumberId")))
		gmailNumber.clear()
		gmailNumber.send_keys(telephoneNumber)
		time.sleep(2)
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="view_container"]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button/div[2]'))).click()

	time.sleep(2)
	driver.implicitly_wait(3)
	googleSetSkip = driver.find_elements_by_xpath('//*[@id="yDmH0d"]/c-wiz/div/div/div/div[2]/div[4]/div[1]')
	if len(googleSetSkip)>0:
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="yDmH0d"]/c-wiz/div/div/div/div[2]/div[4]/div[1]'))).click()
		time.sleep(2)

	driver.implicitly_wait(3)
	driver.get('https://www.google.com/')
	driver.implicitly_wait(2)
	time.sleep(2)
	driver.get('https://mail.google.com/')
	driver.implicitly_wait(2)
	time.sleep(2)

	allowSmartFeatures = driver.find_elements_by_xpath("//div[@role='dialog']")
	if len(allowSmartFeatures)>0:
		time.sleep(3)
		Button1allowSmartFeatures = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//div[@role="dialog"]/div[2]/div/div[2]/div[2]/label/span')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button1allowSmartFeatures)
		time.sleep(3)	
		Button2allowSmartFeatures = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, 'data_consent_dialog_next')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button2allowSmartFeatures)
		time.sleep(2)
		Button3allowSmartFeatures = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//div[@role="dialog"]/div[2]/div/div[3]/div[2]/label/span')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button3allowSmartFeatures)
		time.sleep(1)
		Button4allowSmartFeatures = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, 'data_consent_dialog_done')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button4allowSmartFeatures)
		time.sleep(2)

	googleMeetButton1 = driver.find_elements_by_xpath("//div[@role='alertdialog']")
	if len(googleMeetButton1)>0:
		time.sleep(2)	
		Button1googleMeetButton1 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[@role='alertdialog']/div[1]/div/div[2]/div[2]")))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button1googleMeetButton1)
		#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[@role='alertdialog']/div[1]/div/div[2]/div[2]"))).click()
		time.sleep(1)	

	googleAllowButton = driver.find_elements_by_xpath('//*[@id=":4u.contentEl"]/div/div[2]/div[2]/label/span')
	if len(googleAllowButton)>0:
		
		time.sleep(3)
		Button1 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[2]/div[2]/label/span')))
		VYTVORUCET.clickOnButton(driver, Button1)
		time.sleep(3)
		Button2 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, 'data_consent_dialog_next')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button2)
		time.sleep(3)
		Button3 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[3]/div[2]/label/span')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button3)
		time.sleep(3)
		Button4 = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, 'data_consent_dialog_done')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button4)
		time.sleep(3)
		#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[2]/div[2]/label/span'))).click()
		# time.sleep(3)
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[22]/div[3]/button'))).click()
		# time.sleep(3)
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[3]/div[2]/label/span'))).click()
		# time.sleep(3)
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[22]/div[3]/button[1]'))).click()
		# time.sleep(3)
		
	googleRefreshButton= driver.find_elements_by_xpath('/html/body/div[32]/div[3]/button')
	if len(googleRefreshButton)>0:	
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[32]/div[3]/button'))).click()
		driver.implicitly_wait(2)
		time.sleep(2)
	time.sleep(2)	
	googleMeetButton = driver.find_elements_by_xpath("//div[@role='alertdialog']")
	if len(googleMeetButton)>0:
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[@role='alertdialog']/div[1]/div/div[2]/div[2]"))).click()
		time.sleep(1)
	
	time.sleep(1)
	driver.implicitly_wait(2)
	driver.get('https://mail.google.com/')
	driver.implicitly_wait(2)
	driver.get('https://mail.google.com/mail/u/0/#settings/fwdandpop')
	driver.implicitly_wait(2)
	time.sleep(2)
	forward_btn = driver.find_elements_by_xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[2]/input")
	driver.implicitly_wait(2)
	time.sleep(2)
	timeout = time.time() + 120
	while len(forward_btn)<=0:
		time.sleep(2)
		if time.time() > timeout:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			logger_error.error('Not logged to the email.')
			logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(e))
			print("Error_log")
			driver.quit()
			DBfunctions.update_table('WINDOWS', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
			#os.remove(pluginfile)

			forwardData = DBfunctions.select_from_table('FORWARDING', 'email_id', 'email_id = {}'.format(id_email))
			if len(forwardData):
				dataCheck = DBfunctions.select_from_table('FORWARDING', 'status', 'email_id = {}'.format(id_email))
				if dataCheck[0][0] != 'CANCELED':
					DBfunctions.update_table('FORWARDING', "status = 'FAILED'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")	
			else:
				DBfunctions.update_table('FORWARDING', "status = 'FAILED'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")	

			#DBfunctions.delete_from_table('FORWARDING',f"email_id = {id_email} and forward_email = '{email_id_forward}'")
			sys.exit()
		forward_btn = driver.find_elements_by_xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[2]/input")
		time.sleep(1)
		if len(forward_btn)>0:
			break



	logger_process.info("Inserting main email for forwarding")
	#time.sleep(3000)
	remove_addr = driver.find_elements_by_xpath("//*[@act='removeAddr']")
	duplicite_email_forward = driver.find_elements_by_xpath(f"/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[1]/table[2]/tbody/tr/td[2]/span/select[1]/option[contains(text(),'{email_id_forward}')]")
	if len(remove_addr)>0:
		remove_addr[0].click()
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "ok"))).click()
	time.sleep(2)
	if len(duplicite_email_forward)>0:
		logger_process.info("The email exists in the forwarding email database. Choosing him again")
		genderButton =  WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[1]/table[2]/tbody/tr/td[2]/span/select[1]')))
		time.sleep(1)
		select_box = Select(genderButton)
		time.sleep(1)
		select_box.select_by_value('2')
		time.sleep(1)
		Button4allowSmartFeatures = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//button[@name='ok']")))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver, Button4allowSmartFeatures)
		time.sleep(1)
		# duplicate_option_val_opt = duplicite_email_forward[0].get_attribute("index")
		# duplicate_option_val = int(duplicate_option_val_opt)+1
		# multi_forward = driver.find_elements_by_name("sx_em")
		# multi_forward[1].click()
		# duplicite_email_click = driver.find_element_by_xpath(f"/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[1]/table[2]/tbody/tr/td[2]/span/select[1]/option[{duplicate_option_val}]")
		# duplicite_email_click.click()
		# time.sleep(1)
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//button[@guidedhelpid='save_changes_button']"))).click()
		# #End 
		# time.sleep(2)
		# #driver.quit()
		# DBfunctions.update_table('WINDOWS', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
		# DBfunctions.update_table('FORWARDING', 'status = FORWARDED', f"email_id = {id_email} and forward_email = '{email_id_forward}'")
		# sys.exit()
	time.sleep(1)	
	forward_btn = driver.find_elements_by_xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[2]/input")
	#Adjustment of email forwarding in options
	if len(forward_btn)>0:
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[2]/input"))).click()
		#driver.find_element_by_xpath("//input[@type='button' and @value='Pridať adresu na preposielanie']").click()
		#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id=':4t']/input"))).click()
		time.sleep(3)

		forward = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[@role='alertdialog']/div[2]/div/input")))
		time.sleep(1)
		forward.send_keys(email_id_forward)
		time.sleep(1)
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "next"))).click()
		time.sleep(3)
		window_after = driver.window_handles[1]
		time.sleep(1)
		driver.switch_to_window(window_after)
		time.sleep(1)
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "/html/body/form/table/tbody/tr/td/input[3]"))).click()
		time.sleep(1)
		window_main = driver.window_handles[0]
		time.sleep(1)
		driver.switch_to_window(window_main)
		time.sleep(1)
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "ok"))).click()
except Exception as e:
	exc_type, exc_obj, exc_tb = sys.exc_info()
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(traceback.format_exc()))
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(e))
	print("Error_log")
	driver.quit()
	DBfunctions.update_table('WINDOWS', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')

	forwardData = DBfunctions.select_from_table('FORWARDING', 'email_id', 'email_id = {}'.format(id_email))
	if len(forwardData):
		dataCheck = DBfunctions.select_from_table('FORWARDING', 'status', 'email_id = {}'.format(id_email))
		if dataCheck[0][0] != 'CANCELED':
			DBfunctions.update_table('FORWARDING', "status = 'FAILED'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")	
	else:
		DBfunctions.update_table('FORWARDING', "status = 'FAILED'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")

	#DBfunctions.delete_from_table('FORWARDING',f"email_id = {id_email} and forward_email = '{email_id_forward}'")
	#os.remove(pluginfile)
	sys.exit()

try:
	data = DBfunctions.select_from_table('EMAILS', '*', 'id = {}'.format(id_email))
	for row in data:
		(id, created, emailId, password, firstName, lastName, day, month, year, gender, telephoneNumber, country, providerId, gmailCode, proxyId,status,scoreV3, scoreV2, recoveryId, registeredAt) = tuple(row)

	dataProxies = DBfunctions.select_from_table('PROXIES', 'hostname, port, username, password', 'id = {}'.format(proxyId))
	for rowProxy in dataProxies:
		(hostnameProxy, portProxy, usernameProxy, passwordProxy) = tuple(rowProxy)

	logger_process.info("Preparing to obtain activation key")

	# hostname = hostnameProxy  # rotating proxy or host
	# port = portProxy # port
	# proxy_username = usernameProxy # username
	# proxy_password = passwordProxy # password

	proxyCustomForward = proxyCustomForward.split(':')
	if 0 < len(proxyCustomForward):
		if len(proxyCustomForward[0]):
			logger_process.info(proxyCustomForward[0])
			hostname = proxyCustomForward[0]
	else:
		hostname = hostnameProxy  # rotating proxy or host	
	if 1 < len(proxyCustomForward):
		if len(proxyCustomForward[1]):
			logger_process.info(proxyCustomForward[1])
			port = proxyCustomForward[1]
	else:
		port = portProxy # port
	if 2 < len(proxyCustomForward):
		if len(proxyCustomForward[2]):
			logger_process.info(proxyCustomForward[2])
			proxy_username = proxyCustomForward[2]
	else:
		proxy_username = usernameProxy # username
	if 3 < len(proxyCustomForward):
		if len(proxyCustomForward[3]):
			logger_process.info(proxyCustomForward[3])
			proxy_password = proxyCustomForward[3]
	else:
		proxy_password = passwordProxy # password


	manifestJson = SetProxy.getManifest()
	backgroundJs = SetProxy.setBackgroundJs(hostname, port, proxy_username, proxy_password)

	timestr = time.strftime("%Y%m%d-%H%M%S")
	chrome_options = webdriver.ChromeOptions()
	pluginfile = os.path.abspath(os.getenv('APPDATA')+"/KOALABOTsystem/tmp/{}.zip").format(timestr)
	with zipfile.ZipFile(pluginfile, 'w') as zp:
			zp.writestr("manifest.json", manifestJson)
			zp.writestr("background.js", backgroundJs)

	################ OK ###################################################
	chrome_options.add_extension(pluginfile)

	chrome_options.add_experimental_option('prefs', {
		'credentials_enable_service': False,
		'profile': {
			'password_manager_enabled': False
		}
	})

	# For older ChromeDriver under version 79.0.3945.16
	chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
	chrome_options.add_experimental_option('useAutomationExtension', False)

	#For ChromeDriver version 79.0.3945.16 or over
	chrome_options.add_argument("--disable-blink-features")
	chrome_options.add_argument('--disable-blink-features=AutomationControlled')
	chrome_options.add_argument("--disable-infobars")

	chrome_options.add_argument ("window-size = 1280 800")
	#user_data_file_path = app_data_local+"/Google/Chrome/"+nazovSuboru
	#chrome_options.add_argument('user-data-dir='+user_data_file_path)

	settings_arr = [
					"disable-background-networking",
					"enable-features=NetworkService,NetworkServiceInProcess"
					"disable-background-timer-throttling",
					"disable-backgrounding-occluded-windows",
					"disable-breakpad",
					"disable-client-side-phishing-detection",
					"disable-component-extensions-with-background-pages",
					"disable-default-apps",
					"disable-dev-shm-usage",
					#"disable-extensions",
					"disable-features=Translate",
					"disable-hang-monitor",
					"disable-ipc-flooding-protection",
					"disable-popup-blocking",
					"disable-prompt-on-repost",
					"disable-renderer-backgrounding",
					"disable-sync",
					"start-maximized",
					"force-color-profile=srgb",
					"metrics-recording-only",
					"no-first-run",
					"enable-automation",
					"password-store=basic",
					"use-mock-keychain",
					"enable-blink-features=IdleDetection",
					"disable-infobars",
					"no-sandbox",
					"disable-setuid-sandbox",
					"single-process",
					"no-zygote",
					"disable-features=site-per-process",
					"disable-blink-features=AutomationControlled",
					"remote-debugging-port=0",
					"flag-switches-begin",
					"flag-switches-end",
					"lang=en-US",
					"num-raster-threads=4",
					"enable-main-frame-before-activation",
					"use-gl=swiftshader-webgl"
					]

	for row in settings_arr:
		chrome_options.add_argument('--'+row)

	################END OK ########################

	chrome_options.add_argument("--mute-audio")

except Exception as e:
	exc_type, exc_obj, exc_tb = sys.exc_info()
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(traceback.format_exc()))
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(e))
	print("Error_log")
	#os.remove(pluginfile)

	forwardData = DBfunctions.select_from_table('FORWARDING', 'email_id', 'email_id = {}'.format(id_email))
	if len(forwardData):
		dataCheck = DBfunctions.select_from_table('FORWARDING', 'status', 'email_id = {}'.format(id_email))
		if dataCheck[0][0] != 'CANCELED':
			DBfunctions.update_table('FORWARDING', "status = 'FAILED'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")	
	else:
		DBfunctions.update_table('FORWARDING', "status = 'FAILED'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")	

	#DBfunctions.delete_from_table('FORWARDING',f"email_id = {id_email} and forward_email = '{email_id_forward}'")
	sys.exit()

try:
	chrome_options.add_argument("--window-position=-32000,-32000")
	#chrome_options.binary_location = os.path.abspath(__file__+"../../../application/chrome.exe")
	chrome_options.binary_location = os.path.abspath(__file__+"../../../application/Chromium/chrome.exe")
	driver2 = webdriver.Chrome(executable_path=pathToChrome,chrome_options=chrome_options)
	logger_process.info("Logging into main email for getting key")
	time.sleep(2)
	tabs = driver2.window_handles
	counterTabs = 0
	
	if len(tabs) > 1:
		for tab in tabs:
			if counterTabs == 0:
				window_name = driver2.window_handles[counterTabs]
				driver2.switch_to.window(window_name=window_name)
				driver2.close()
				window_name = driver2.window_handles[0]
				driver2.switch_to.window(window_name=window_name)
			counterTabs = counterTabs + 1
	time.sleep(2)
	driver2.execute_script(f"document.title = '{email_id_forward}#{id_email_forward}'")
	time.sleep(1)
	#print(email_id+'#'+id_email)
	#print(maxmin.get_window_id(email_id+'#'+id_email))
	outputWin = maxmin.get_window_id(email_id_forward+'#'+id_email_forward)
	ID_window_MAIN_driver2 = outputWin[0].split("-")[0]
	ID_window_NUMB_driver2 = outputWin[0].split("###")[0]
	#Hide window function
	maxmin.hide_from_taskbar(int(ID_window_NUMB_driver2))
	status = "1"
	
	#values = (f"'{datetime.datetime.now()}','{ID_window}', '{id_email}', '{session_type}'")
	values = ("'{}','{}', '{}', '{}','{}'").format(datetime.datetime.now(),ID_window_NUMB_driver2,id_email_forward,session_type,status)
	columns = "date_created, id_window, id_email, sessions_type, status_id"
	#DBfunctions.insert_into_table('WINDOWS', columns, values)
	for webpage in GENWEB.select_page_generate(random.randrange(1,2)):
		driver2.get(webpage)
		time.sleep(random.randrange(1,4))
	time.sleep(random.randrange(1,4))
	driver2.get(selected_page)
	if random_value == 1: 
		driver2.find_element_by_xpath('//*[@id="openid-buttons"]/button[1]').click()
	
	driver2.implicitly_wait(3)
	time.sleep(2)
	login = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.NAME, "identifier")))
	time.sleep(1)
	VYTVORUCET.fillInputInRegistrationStep(driver2, login, email_id_forward)
	time.sleep(3)

	LoginNextButton = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='identifierNext']/div/button")))
	time.sleep(1)
	VYTVORUCET.clickOnButton(driver2, LoginNextButton)
	time.sleep(1)
	# login = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.NAME, "identifier")))
	# login.send_keys(email_id_forward)
	# WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='identifierNext']/div/button"))).click()
	# driver2.implicitly_wait(1)
	# time.sleep(3)

	if driver2.find_elements_by_xpath('//*[@id="view_container"]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div/div[2]/div[2]/div'):
		logger_error.error('Your Google account was not found.')
		DBfunctions.update_table('FORWARDING', "status = 'FAILED'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")	
		driver.quit()
		DBfunctions.update_table('WINDOWS', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
		driver2.quit()
		DBfunctions.update_table('WINDOWS', 'status_id = 0', f'id_window = {ID_window_NUMB_driver2}')
		sys.exit()

	driver2.implicitly_wait(3)
	time.sleep(2)
	logpass = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='password']/div[1]/div/div[1]/input"))) 
	time.sleep(1)
	VYTVORUCET.fillInputInRegistrationStep(driver2, logpass, password_id_forward)
	time.sleep(2)

	LoginNextButton2 = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.ID, "passwordNext")))
	VYTVORUCET.clickOnButton(driver2, LoginNextButton2)
	time.sleep(1)
	# time.sleep(5)
	# logpass = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='password']/div[1]/div/div[1]/input")))  
	# driver2.execute_script(f"document.getElementsByName('password')[0].value = '{password_id_forward}';")
	# #logpass.send_keys(password_id)
	# time.sleep(2)
	# driver2.execute_script(f"document.getElementById('passwordNext').click();")
	# #WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id='passwordNext']/div/button"))).click()
	googleAllowMobile = driver2.find_elements_by_xpath('//*[@id="yDmH0d"]/c-wiz[2]/c-wiz/div/div[1]/div/div/div/div[2]/div[3]/div/div[2]/div')
	if len(googleAllowMobile)>0:
		WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="yDmH0d"]/c-wiz[2]/c-wiz/div/div[1]/div/div/div/div[2]/div[3]/div/div[2]/div'))).click()
		time.sleep(2)

	googleMobileCheck = driver.find_elements_by_xpath('//*[@id="view_container"]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/ul/li[3]/div')
	if len(googleMobileCheck)>0:
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="view_container"]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/ul/li[3]/div'))).click()
		time.sleep(2)

	driver2.implicitly_wait(3)
	if driver2.find_elements_by_xpath('//*[@id="phoneNumberId"]'):
		time.sleep(2)
		if providerId == "LUCKYSMS":
			countryArray = country.split("_")
			countryLuckySMS = countryArray[1]
		else:
			countryLuckySMS = country
		
		countryName = countryLuckySMS
		countryNameForGmail = countryName.lower()

		dataCountryPrefix = DBfunctions.select_from_table('COUNTRY', 'prefix', "name = '{}'".format(countryNameForGmail.upper()))
		for rowCountryPrefix in dataCountryPrefix:
			countryPrefix = rowCountryPrefix[0] 
		countryPrefix = len(countryPrefix) 

		telephoneNumber = int(str(telephoneNumber)[countryPrefix:])
		
		time.sleep(3)
		driver2.implicitly_wait(2)
		button = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="countryList"]/div[1]/div[1]/div[1]')))
		time.sleep(2)
		driver2.execute_script("arguments[0].click();", button)
		time.sleep(2)
		buttonClick = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="countryList"]/div[2]/div[@data-value="{}"]'.format(countryNameForGmail))))
		hover = ActionChains(driver2).move_to_element(buttonClick)
		time.sleep(3)
		hover.perform()
		driver2.execute_script("arguments[0].click();", buttonClick)
		gmailNumber = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.ID, "phoneNumberId")))
		gmailNumber.clear()
		gmailNumber.send_keys(telephoneNumber)
		time.sleep(2)
		WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="view_container"]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button/div[2]'))).click()

	time.sleep(2)
	driver.implicitly_wait(3)
	googleSetSkip = driver.find_elements_by_xpath('//*[@id="yDmH0d"]/c-wiz/div/div/div/div[2]/div[4]/div[1]')
	if len(googleSetSkip)>0:
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id="yDmH0d"]/c-wiz/div/div/div/div[2]/div[4]/div[1]'))).click()
		time.sleep(2)

	driver2.implicitly_wait(3)
	driver2.get('https://www.google.com/')
	driver2.implicitly_wait(2)
	time.sleep(2)
	driver2.get('https://mail.google.com/')
	driver2.implicitly_wait(2)
	time.sleep(3)

	allowSmartFeatures = driver2.find_elements_by_xpath("//div[@role='dialog']")
	if len(allowSmartFeatures)>0:
		time.sleep(3)
		Button1allowSmartFeatures = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.XPATH, '//div[@role="dialog"]/div[2]/div/div[2]/div[2]/label/span')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver2, Button1allowSmartFeatures)
		time.sleep(3)	
		Button2allowSmartFeatures = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.NAME, 'data_consent_dialog_next')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver2, Button2allowSmartFeatures)
		time.sleep(2)
		Button3allowSmartFeatures = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.XPATH, '//div[@role="dialog"]/div[2]/div/div[3]/div[2]/label/span')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver2, Button3allowSmartFeatures)
		time.sleep(1)
		Button4allowSmartFeatures = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.NAME, 'data_consent_dialog_done')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver2, Button4allowSmartFeatures)
		time.sleep(2)

	googleMeetButton1 = driver2.find_elements_by_xpath("//div[@role='alertdialog']")
	if len(googleMeetButton1)>0:
		time.sleep(2)	
		Button1googleMeetButton1 = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.XPATH, "//div[@role='alertdialog']/div[1]/div/div[2]/div[2]")))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver2, Button1googleMeetButton1)
		#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[@role='alertdialog']/div[1]/div/div[2]/div[2]"))).click()
		time.sleep(1)	
	googleAllowButton = driver2.find_elements_by_xpath('//*[@id=":4u.contentEl"]/div/div[2]/div[2]/label/span')
	if len(googleAllowButton)>0:
		
		time.sleep(3)
		Button1 = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[2]/div[2]/label/span')))
		VYTVORUCET.clickOnButton(driver2, Button1)
		time.sleep(3)
		Button2 = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.NAME, 'data_consent_dialog_next')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver2, Button2)
		time.sleep(3)
		Button3 = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[3]/div[2]/label/span')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver2, Button3)
		time.sleep(3)
		Button4 = WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.NAME, 'data_consent_dialog_done')))
		time.sleep(1)
		VYTVORUCET.clickOnButton(driver2, Button4)
		time.sleep(3)
		#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[2]/div[2]/label/span'))).click()
		# time.sleep(3)
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[22]/div[3]/button'))).click()
		# time.sleep(3)
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '//*[@id=":4u.contentEl"]/div/div[3]/div[2]/label/span'))).click()
		# time.sleep(3)
		# WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[22]/div[3]/button[1]'))).click()
		# time.sleep(3)
	time.sleep(1)
	googleMeetButton = driver.find_elements_by_xpath("//div[@role='alertdialog']")
	if len(googleMeetButton)>0:
		WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[@role='alertdialog']/div[1]/div/div[2]/div[2]"))).click()
		time.sleep(1)	
	googleRefreshButton= driver2.find_elements_by_xpath('/html/body/div[32]/div[3]/button')
	if len(googleRefreshButton)>0:	
		WebDriverWait(driver2, delay).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[32]/div[3]/button'))).click()
		driver2.implicitly_wait(2)
		time.sleep(2)
	
	time.sleep(1)
	driver2.get('https://mail.google.com/')
	driver2.implicitly_wait(2)
	time.sleep(2)
	driver2.get('https://mail.google.com/mail/u/0/#settings/fwdandpop')
	driver2.implicitly_wait(2)
	time.sleep(2)
	forward_btn = driver2.find_elements_by_xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[2]/input")
	driver2.implicitly_wait(2)
	time.sleep(2)
	timeout = time.time() + 120
	while len(forward_btn)<=0:
		time.sleep(2)
		if time.time() > timeout:
			logger_error.error('Not logged to the email.')
			exc_type, exc_obj, exc_tb = sys.exc_info()
			logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(e))
			print("Error_log")
			driver.quit()
			DBfunctions.update_table('WINDOWS', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
			driver2.quit()
			DBfunctions.update_table('WINDOWS', 'status_id = 0', f'id_window = {ID_window_NUMB_driver2}')
			#os.remove(pluginfile)

			forwardData = DBfunctions.select_from_table('FORWARDING', 'email_id', 'email_id = {}'.format(id_email))
			if len(forwardData):
				dataCheck = DBfunctions.select_from_table('FORWARDING', 'status', 'email_id = {}'.format(id_email))
				if dataCheck[0][0] != 'CANCELED':
					DBfunctions.update_table('FORWARDING', "status = 'FAILED'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")	
			else:
				DBfunctions.update_table('FORWARDING', "status = 'FAILED'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")	

			#DBfunctions.delete_from_table('FORWARDING',f"email_id = {id_email} and forward_email = '{email_id_forward}'")
			sys.exit()
		forward_btn = driver2.find_elements_by_xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[2]/input")
		time.sleep(1)
		if len(forward_btn)>0:
			break

	logger_process.info("Successfully logged in to the main email")
	driver2.implicitly_wait(2)
	driver2.get('https://mail.google.com/')
	#maxmin.maximize_from_taskbar(int(ID_window_NUMB_driver))
	logger_process.info("Getting forwarding activation key")
	driver2.implicitly_wait(2)
	#Extract verify key from emails
	#inbox = driver2.find_elements_by_xpath("//*[@id=':2l']/tbody/tr")
	#inbox = driver2.find_elements_by_xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div[7]/div/div[1]/div[3]/div/table/tbody/tr")
	allCheckboxes = driver2.find_elements_by_xpath("//div[@role='checkbox']")
	time.sleep(1)
	allEmails = []
	endeSchluss = 'false'
	maxUrovne = range(7)
	appendString = '..'
	appendStringLomito = '/..'
	for allCheckboxe in allCheckboxes:
		for uroven in maxUrovne:
			if uroven == 0:
				findTr = allCheckboxe.find_element_by_xpath('../..')
				time.sleep(1)
			else:
				stringTOfind = appendString + appendStringLomito * uroven
				time.sleep(1)
				findTr = allCheckboxe.find_element_by_xpath(stringTOfind)
				time.sleep(1)
			if findTr.tag_name == 'tr':
				allEmails.append(findTr)
				time.sleep(1)
				spec_string = ['(#','Gmail']
				#counter = counter + 1
				time.sleep(1)
				if any(x in findTr.text for x in spec_string):
					m = re.search('(?<=#)(.*)(?=\))', findTr.text)
					time.sleep(1)
					if m:
						ActivationKeyValue = m.group(1)
						endeSchluss = 'true'
				break
		if endeSchluss == 'true':
			break	
	
	# spec_string = ['(#','Gmail']
	# counter = 1
	# for item in allEmails:
	# 	time.sleep(2)
	# 	#text_val = driver2.find_element_by_xpath('/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div[7]/div/div[1]/div[3]/div/table/tbody/tr[{}]/td[5]/div/div/div[2]/span/span'.format(counter)).text
	# 	counter = counter + 1
	# 	if any(x in item.text for x in spec_string):
	# 		m = re.search('(?<=#)(.*)(?=\))', item.text)
	# 		if m:
	# 			ActivationKeyValue = m.group(1)
	# 		else:
	# 			raise ValueError("No forward key")
	# 		break
	
	DBfunctions.update_table('FORWARDING', "status = 'FINISHING FORWARD'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")

	#verify_forward = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id=':81']/table/tbody/tr[4]/td[2]/input[1]")))
	logger_process.info("Inserting the activation key and finishing")
	#time.sleep(3000)
	time.sleep(1)
	gmailCode = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[3]/table/tbody/tr[4]/td[2]/input[1]")))
	time.sleep(1)	
	VYTVORUCET.fillInputInRegistrationStep(driver, gmailCode, ActivationKeyValue)
	time.sleep(2)
	#verify_forward = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[3]/table/tbody/tr[4]/td[2]/input[1]")))
	#verify_forward.click()
	#verify_forward.send_keys(ActivationKeyValue)
	#Submit verifing key in child email  


	#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, "verify"))).click()
	verifyButton = WebDriverWait(driver, delay).until(EC.element_to_be_clickable((By.NAME, "verify")))
	time.sleep(1)
	VYTVORUCET.clickOnButton(driver, verifyButton)
	time.sleep(1)

	#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//*[@id=':7g']"))).click()
	# duplicite_email_forward = driver.find_elements_by_xpath(f"/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[1]/table[2]/tbody/tr/td[2]/span/select[1]/option[contains(text(),'{email_id_forward}')]")
	# time.sleep(1)
	# duplicate_option_val_opt = duplicite_email_forward[0].get_attribute("index")
	# time.sleep(1)
	# duplicate_option_val = int(duplicate_option_val_opt)+1


	multi_forward = driver.find_elements_by_name("sx_em")
	multi_forward[1].click()
	# time.sleep(1)
	# gmailsx_em = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.NAME, "sx_em")))
	# time.sleep(1)	
	# VYTVORUCET.clickOnButton(driver, gmailsx_em)
	# time.sleep(2)


	# #duplicite_email_click = driver.find_element_by_xpath(f"/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[1]/table[2]/tbody/tr/td[2]/span/select[1]/option[{duplicate_option_val}]")
	# #duplicite_email_click.click
	# time.sleep(1)
	# gmailNeviem = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.XPATH, f"/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[1]/table[2]/tbody/tr/td[2]/span/select[1]/option[{duplicate_option_val}]")))
	# time.sleep(1)	
	# VYTVORUCET.clickOnButton(driver, gmailNeviem)

	#time.sleep(1)
	#gmailNeviem5 = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[6]/div/table/tbody/tr[1]/td[2]/div/div[1]/table[2]/tbody/tr/td[1]/input")))
	#time.sleep(1)	
	#VYTVORUCET.clickOnButton(driver, gmailNeviem5)
	time.sleep(1)
	gmailNeviem1 = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.XPATH, "//button[@guidedhelpid='save_changes_button']")))
	time.sleep(1)	
	VYTVORUCET.clickOnButton(driver, gmailNeviem1)
	time.sleep(1)
	#WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//button[@guidedhelpid='save_changes_button']"))).click()
	#End 
	DBfunctions.update_table('FORWARDING', "status = 'FORWARDED'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")
	DBfunctions.update_table('FORWARDING', f"forwarding_date = '{datetime.datetime.now()}'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")

	#DBfunctions.update_table('FORWARDING', f"status = 'FORWARDED' and forwarding_date = '{datetime.datetime.now()}' and forward_email = '{email_id_forward}'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")

	time.sleep(4)

	driver.quit()
	DBfunctions.update_table('WINDOWS', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
	driver2.quit()
	DBfunctions.update_table('WINDOWS', 'status_id = 0', f'id_window = {ID_window_NUMB_driver2}')
	#os.remove(pluginfile)

	logger_process.info("Email forwarded successfully")
	
except Exception as e:
	exc_type, exc_obj, exc_tb = sys.exc_info()
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(traceback.format_exc()))
	logger_error.error('ON LINE:' + str(exc_tb.tb_lineno)+ "#~#! " + str(e))
	print("Error_log")
	driver.quit()
	DBfunctions.update_table('WINDOWS', 'status_id = 0', f'id_window = {ID_window_NUMB_driver}')
	driver2.quit()
	DBfunctions.update_table('WINDOWS', 'status_id = 0', f'id_window = {ID_window_NUMB_driver2}')
	#os.remove(pluginfile)

	forwardData = DBfunctions.select_from_table('FORWARDING', 'email_id', 'email_id = {}'.format(id_email))
	if len(forwardData):
		dataCheck = DBfunctions.select_from_table('FORWARDING', 'status', 'email_id = {}'.format(id_email))
		if dataCheck[0][0] != 'CANCELED':
			DBfunctions.update_table('FORWARDING', "status = 'FAILED'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")	
	else:
		DBfunctions.update_table('FORWARDING', "status = 'FAILED'", f"email_id = {id_email} and forward_email = '{email_id_forward}'")	

	#DBfunctions.delete_from_table('FORWARDING',f"email_id = {id_email} and forward_email = '{email_id_forward}'")
	sys.exit()
