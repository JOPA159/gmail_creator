#%%
import names
import random
import secrets
import string
from litedbf import DBfunctions
import datetime
import sys

if sys.argv[6] != 'a8fef5e1-f6fd-429e-bb91-f3a6dfd9bcee':
	sys.exit()

def random_with_N_digits(n):
	range_start = 10**(n-1)
	range_end = (10**n)-1
	return random.randint(range_start, range_end)

check_if_table_exist = DBfunctions.check_if_table_exist("EMAILS")

#premenna z frontendu

if not check_if_table_exist:
	DBfunctions.create_table('EMAILS', '([id] INTEGER PRIMARY KEY,[created] date, [email] text, [password] text, [firstName] text, [lastName] text, [day] text, [month] text, [year] text, [gender] text, [telephoneNumber] text, [country] text, [smsProviderId] integer,  [gmailCode] text, [proxyId] integer, [status] text,[scoreV3] text, [scoreV2] boolean, [recoveryId] integer, [registered_at] date )')

recoveryId = sys.argv[4]
maxNumsName = int(sys.argv[5])
proxyGroup = sys.argv[7]
idProvider = str(sys.argv[1])
if 'system' in str(sys.argv[2]):
	country_draft = str(sys.argv[2])
	providerCountry = country_draft.split("_",1)[1:][0]
else:
	providerCountry = str(sys.argv[2])
	country_draft = providerCountry
quantity = int(sys.argv[3])



#idProvider
#idProvider = 2
#Country
#providerCountry = "GB"

providerCountryBase = providerCountry

for x in range(quantity):
	maleOrfemale = random.randint(0,1)

	genderArray = ['female', 'male']
	gender = genderArray[maleOrfemale]

	firstName = names.get_first_name(gender=gender)
	lastName = names.get_last_name()

	randomGmailNumber = random_with_N_digits(maxNumsName)

	gmailAcoount = firstName+'.'+lastName+str(randomGmailNumber)

	if(recoveryId != '0'):
		randomRecoveryNumber = random_with_N_digits(maxNumsName)

		firstNameRecovery = names.get_first_name(gender=gender)
		lastNameRecovery = names.get_last_name()

		recoveryAccount = firstNameRecovery+'.'+lastNameRecovery+str(randomRecoveryNumber)+recoveryId
	else:
		recoveryAccount = ''

	alphabet = string.ascii_letters + string.digits
	password = ''.join(secrets.choice(alphabet) for i in range(20))

	day = random.randint(1,25)
	year = random.randint(1985,2000)
	month = random.randint(1,12)

	today = datetime.datetime.now()

	if proxyGroup == '':
		availableProxies = DBfunctions.select_from_table('PROXIES', '*', "(id_email  = '' or id_email is null) LIMIT 1")
	else:
		availableProxies = DBfunctions.select_from_table('PROXIES', '*', f"(id_email  = '' or id_email is null) and upper(proxy_group) = '{proxyGroup}' LIMIT 1")

	if len(availableProxies) == 0:
		print('Any free proxies')
		sys.exit()
	for row in availableProxies:
		idProxy = row[0]
	
	if 'RAND' in providerCountryBase:
		if 'system' in str(sys.argv[2]):
			providerCountryNumb = country_draft.split("_",1)[1].split("_",1)[0][-1:]
			availableCountries = DBfunctions.select_from_table('COUNTRY_LUCKY', 'name', f'system = "system{providerCountryNumb}"')
			if len(availableCountries) != 0:
				providerCountry = 'system'+providerCountryNumb+'_'+availableCountries[random.randrange(0,len(availableCountries))][0]
			else:
				providerCountry = country_draft.split("_",1)[1:][0]
		else:
			if str(sys.argv[1]) == 'SMSPVA':
				availableCountries = DBfunctions.select_from_table('COUNTRY', 'name', 'name in ("RU","UA","DE","KZ","HT","RO","AR","BA","BR","KH","CM","CA","CL","CY","DO","EG","EE","FI","FR","GH","IN","ID","IQ","IE","IL","KE","KG","LA","LV","LT","MY","MX","MD","MA","NL","NZ","NG","PK","PH","PL","PT","ZA","ES","SE","UK","US","VN")')
			elif str(sys.argv[1]) == 'GETSMSCODE':
				availableCountries = DBfunctions.select_from_table('COUNTRY', 'name', 'name in ("CN","US","MY","PH","KH","MN","VN","ID","BR","ZA","UK","HK","MO","MA")')
			elif str(sys.argv[1]) == 'PVACODES':
				#availableCountries = DBfunctions.select_from_table('COUNTRY', 'name', 'name in ("AF","AG")')
				availableCountries = DBfunctions.select_from_table('COUNTRY', 'name', '1=1')
			else:
				availableCountries = DBfunctions.select_from_table('COUNTRY', 'name', '1=1')
			
			if len(availableCountries) != 0:
				providerCountry = availableCountries[random.randrange(0,len(availableCountries))][0]
			else:
				providerCountry = providerCountryBase

	values = ("'{}','{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}'").format(today, gmailAcoount, password, firstName, lastName ,day, month, year, gender, idProxy, idProvider, providerCountry, recoveryAccount,'unregistered')
	columns = "created, email, password, firstName, lastName, day, month, year, gender, proxyId, smsProviderId, country, recoveryId, status"
	DBfunctions.insert_into_table('EMAILS', columns, values)

	lastEmailID = DBfunctions.get_last_id('EMAILS')
	lastEmailID = lastEmailID - 1
	setEmailIdInProxies = "id_email = '{}'".format(lastEmailID)
	where = "id = '{}'".format(idProxy)
	DBfunctions.update_table('PROXIES', setEmailIdInProxies, where)
print("Generating new emails done!")

