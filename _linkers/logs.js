const { app } = require("electron").remote;

var filterItems = (needle="", heystack) => {

  let query = needle.toLowerCase();

  return heystack.filter(item => item.toLowerCase().indexOf(query) >= 0);
}

var self_log = module.exports = {
  getOutputLogs: function() {
  //Tu sa zmení názov súboru pre errory a pre logy !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      var processCheck = false
      if ($("#textarea-process").css("display") == "block") {
        var fileNameVal = "process.log";
        processCheck = true
      } else if ($("#textarea-error").css("display") == "block") {
        var fileNameVal = "error.log";
      }
      
    var logPath = app.getPath('userData') + "\\" + fileNameVal;
    
    if($("#textarea-process").length || $("#textarea-error").length){
      $.ajax({
        url: logPath,
        dataType: "text",
        success: function (data) {
          var output = ""
          var draftArray = data.split("#~~#")

          if($("#find-val").val() != ""){
            var arrayVal = filterItems($("#find-val").val(), draftArray)
          }
          else{
            var arrayVal = draftArray.slice(0, draftArray.length - 1)
          }
          var textVal = "";
          var row = 20;

          if ($("#textarea-process").css("display") == "block") {
            
            var loadValProc = $(`#process-load-more`).data("load");
            $(`#process-load-more`).show();
            if (loadValProc >= arrayVal.length || arrayVal.length == 0 || row >= arrayVal.length) {
              $(`#process-load-more`).hide();
            }
            
            var rowSum = loadValProc;
          } else if ($("#textarea-error").css("display") == "block") {
            var loadValErr = $(`#error-load-more`).data("load");

            $(`#error-load-more`).show();
            if (loadValErr >= arrayVal.length || arrayVal.length == 0 || row >= arrayVal.length) {
              $(`#error-load-more`).hide();
            }
            
            var rowSum = loadValErr;
          }
          
          var maxOutput = arrayVal.length >= rowSum ? rowSum : arrayVal.length;

          //console.log(maxOutput)
          //console.log(arrayVal.length)
          var limitVal = arrayVal.length-maxOutput-1
          var limitValFinal = limitVal <= 0 ? 0 : limitVal
          var limitValFinalOut = limitValFinal
          if($("#find-val").val() != ""){
            var limitValFinalOut = limitValFinal
          }

          startValNumber = arrayVal.length == 1 ? arrayVal.length : arrayVal.length-1
          for (row = startValNumber; row >= limitValFinalOut; --row) {
            row = limitValFinalOut == 1 && startValNumber == 1 ? 0 : row
            var regexValText = /(<([^>]+)>)/ig
            if(arrayVal[row] != undefined){
              resultText = arrayVal[row].replace(regexValText, "");
              draftArr = resultText.split("#~#!");
              
              if (draftArr[4] != undefined) {
                
                textVal = draftArr[4].replace(
                  /\'(.*)[^[]*\'/gi,
                  "<span style='color:#FFB24A;'>'$1'</span>"
                );
                if(processCheck || draftArr[5] == undefined){
                  lastPosition = ''
                }
                else{
                  lastPosition = draftArr[5]
                }

                output += `<li><span style="color:#16AB35;">${draftArr[0]}</span><span style="color:#eb3434;">${draftArr[1]}</span> - <span style="color:#FFB24A;">${draftArr[2]}</span> - </span><span style="color:#035efc;">${draftArr[3]}</span>${textVal}${lastPosition}</li>`;
                output += `<li>------------------------------------------------------------</li>`;
              } else {
                textVal = arrayVal[row];
                if (textVal != "") {
                  output += `<li>${textVal}</li>`;
                  output += `<li>------------------------------------------------------------</li>`;
                } else {
                  output += `<li>It looks like you have nothing here right now..</li>`;
                }
              }
            }
          }

          if ($("#textarea-process").css("display") == "block") {
            $("#process-val").empty();
            $("#process-val").append(output);
            $(`#process-load-more`).text("LOAD more..");
          } else if ($("#textarea-error").css("display") == "block") {
            $("#error-val").empty();
            $("#error-val").append(output);
            $(`#error-load-more`).text("LOAD more..");
          }
        },
      });
    }
  },

  loadMore: function(logType) {
    var rowErr = $(`#error-load-more`).data("load");
    var rowProc = $(`#process-load-more`).data("load");
    if (logType == "error") {
      if ($(`#error-load-more`).text() != "Loading..") {
        $(`#error-load-more`).text("Loading..");
        var rowOutErr = rowErr + 20;
        $(`#error-load-more`).data("load", rowOutErr);
        return rowOutErr;
      }
      return null;
    } else if (logType == "process") {
      if ($(`#process-load-more`).text() != "Loading..") {
        $(`#process-load-more`).text("Loading..");
        var rowOutProc = rowProc + 20;
        $(`#process-load-more`).data("load", rowOutProc);
        return rowOutProc;
      }
      return null;
    }
    self_log.getOutputLogs();
  },

  clearErrorLogs: function() {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        const fs = require("fs");
        if ($("#textarea-process").css("display") == "block") {
          var fileNameVal = "process.log";
          processCheck = true
        } else if ($("#textarea-error").css("display") == "block") {
          var fileNameVal = "error.log";
        }
        fs.truncate(app.getPath('userData') + "\\"+ fileNameVal, 0, function () {
          //console.log("done");
        });
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
      }
    });
  },
}

document.addEventListener("newPageLoad", function(){
  $("#process").focus();
  setInterval(self_log.getOutputLogs, 1300);
});
