var dateFormat = require('dateformat');
var crypto = require('crypto');
var Moment = require('moment-timezone');
const algorithm = 'aes-256-cfb';
var Buffer = require('buffer/').Buffer


function encryptText(keyStr, text) {
  const hash = crypto.createHash('sha256');
  hash.update(keyStr);
  const keyBytes = hash.digest();

  const iv = crypto.randomBytes(16);
  const cipher = crypto.createCipheriv(algorithm, keyBytes, iv);
  let enc = [iv, cipher.update(text, 'utf8')];
  enc.push(cipher.final());

  ttt = []

  enc[0].forEach(element => {
    ttt.push(element)
  });

  enc[1].forEach(element => {
    ttt.push(element)
  });

  enc[2].forEach(element => {
    ttt.push(element)
  });

  return Buffer.from(ttt).toString('base64');
}




var self_email = module.exports = {
  sendFormMain: function(event) {
    event.preventDefault();
    if(document.getElementById("quantityVal").value < 1){
      Swal.fire({
        icon: "error",
        title: "Oops!",
        text: "The minimum number to generate emails is 1.",
      });
      return false
    }

    if(document.getElementById("quantityVal").value > 5000 ){
      Swal.fire({
        icon: "error",
        title: "Oops!",
        text: "The maximum number to generate emails is 5000. In case you want more than 5000, divide it into several generations.",
      });
      return false
    }

    if (
      document.getElementById("providers-list").value == "item0" ||
      document.getElementById("countries-list").value == "country0"
    ) {
      hideLoading();
      return false;
    }

    showLoading();

    provider = document.getElementById("providers-list").value;
    country = document.getElementById("countries-list").value;
    quantity = document.getElementById("quantityVal").value;
    recovery_email = $('#recovery-email:checked').length ? $('#recovery-email').val() : 0;
    max_numbers_email_name = $('#recovery-email').data("maxnums")
    proxy_group = document.getElementById("selectProxyGroup").value


    if($('#recovery-email:checked').length && ($('#recovery-email').val() == '' || $('#recovery-email').val() == null || $('#recovery-email').val() == undefined)){
      Swal.fire({
        icon: "error",
        title: "Oops!",
        text: "No recovery domain!",
      });
      return false
    }
    vrabec = 'a8fef5e1-f6fd-429e-bb91-f3a6dfd9bcee'
    var opt = {
      scriptPath: process.cwd()+LOCATION_UNPACK+'\\_engine\\',
      pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\python.exe',
      args: [provider, country, quantity, recovery_email,max_numbers_email_name,vrabec,proxy_group],
    };

    var pyshell = new child.PythonShell("ulohagen.py", opt);

    pyshell.on("message", (results) => {
      //pyshell.childProcess.kill()
      if (results == "Any free proxies") {
        hideLoading();
        Swal.fire({
          icon: "error",
          title: "Oops!",
          text: "Any free proxies",
          allowOutsideClick: false,
          showCancelButton: false,
          showConfirmButton: false,
        });

        setTimeout(function () {
          hideLoading()

          email_data.refreshCreateEmailsTable()
          throw "Any free proxies"
        }, 2500);
        
        email_data.getFreeProxyDataVal()
        
      }

      document.getElementById("providers-list").value = "item0"
      document.getElementById("countries-list").innerHTML = ""
      var opt = document.createElement("option")
      opt.value = "country0"
      opt.innerHTML = "Please select Country.."
      document.getElementById("countries-list").appendChild(opt)
      document.getElementById("recovery-email").checked = false
      document.getElementById("quantityVal").value = "1"
      document.getElementById("selectProxyGroup").value = ''

      setTimeout(function () {
        hideLoading()

        email_data.refreshCreateEmailsTable()
        email_data.getFreeProxyDataVal()
      }, 2500);
      
    });

    pyshell.end((err) => {
      //pyshell.childProcess.kill()
      if (err) {
        hideLoading();
        Swal.fire({
          icon: "error",
          title: "Oops!",
          text: "Error reason:" + err.message,
        });
        throw err;
      }
      //email_data.refreshCreateEmailsTable()
      document.getElementById("providers-list").value = "item0"
      document.getElementById("countries-list").innerHTML = ""
      var opt = document.createElement("option")
      opt.value = "country0";
      opt.innerHTML = "Please select Country.."
      document.getElementById("countries-list").appendChild(opt)
      document.getElementById("recovery-email").checked = false
      document.getElementById("quantityVal").value = "1"
      document.getElementById("selectProxyGroup").value = ''
      return "";
    });
  },

  deleteData: function() {
    var data = [...document.querySelectorAll(".subcheckbox:checked")].map(
      (e) => e.value
    );
    showLoading();
    data = data.join()

    if(data == ''){
      hideLoading()
      return false
    }

    var arrayVal = []
    child.db.all("SELECT id, id_window FROM WINDOWS WHERE id_email IN ("+data+")", function(err, rows) {  
      if (err) {
              Swal.fire({
                  icon: 'error',
                  title: 'Oops..',
                  text: 'Error reason: ' + err.message
              })
          }

        if(rows === undefined || rows.length == 0){
          hideLoading()
          return false
        }
      
      rows.forEach(function (row) {
          draft = Object.assign(row)
          arrayVal.push(Object.values(draft))
        });

        if (arrayVal == null) {
          return false;
        }

        backgroundAuto = false;
        modeJSON = true;

        var opt = {
          scriptPath: process.cwd()+LOCATION_UNPACK+'\\_engine\\HG\\',
          pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
          args: [backgroundAuto, modeJSON],
        };

        var pyshell = new child.PythonShell("Kwdw.py", opt);

        pyshell.send(JSON.stringify(arrayVal), { mode: 'json' });
        
        pyshell.on("message", (results) => {
        });

        pyshell.end((err) => {
          if (err) {    
            hideLoading();
            Swal.fire({
              icon: "error",
              title: "Oops!",
              text: "Error reason:" + err.message,
            });
            throw err;
        }
      });
    })

    child.db.all("SELECT id,status FROM EMAILS WHERE id IN ("+data+")", function(err, rows) {  
      if (err) {
              Swal.fire({
                  icon: 'error',
                  title: 'Oops..',
                  text: 'Error reason: ' + err.message
              })
          }

      if(rows === undefined || rows.length == 0){
          hideLoading()
          return false
      }

      rows.forEach(function(row, idx, array){
        var sql = `DELETE FROM LOGS WHERE id_email = ${row['id']}`;
    
        child.db.run(sql, function (err) {
          if (err) {
            Swal.fire({
              icon: 'error',
              title: 'Oops..',
              text: 'Error reason: ' + err.message
            })
          }
        });

        if(row['status'] == 'registered'){
          
          let sqlProx = `DELETE FROM PROXIES WHERE id_email = ${row['id']}`;
          
          child.db.run(sqlProx, function (err) {
            if (err) {
              Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'Error reason: ' + err.message
              })
            }
            else{
              let sqlFarm = `DELETE FROM FARMING WHERE email_id = ${row['id']}`;

              child.db.run(sqlFarm, function (err) {
                if (err) {
                  Swal.fire({
                    icon: 'error',
                    title: 'Oops..',
                    text: 'Error reason: ' + err.message
                  })
                } 
                else{
                  let sqlForw = `DELETE FROM FORWARDING WHERE email_id = ${row['id']}`;

                  child.db.run(sqlForw, function (err) {
                    if (err) {
                      Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: bb' + err.message
                      })
                    } else {
                      var sql = `DELETE FROM EMAILS WHERE id = ${row['id']}`;
    
                      child.db.run(sql, function (err) {
                        if (err) {
                            Swal.fire({
                              icon: 'error',
                              title: 'Oops..',
                              text: 'Error reason: ' + err.message
                            })
                        }
                      });
                    }
                  });
                }
              });

            }
          });
        }
        else{
          let sqlProx = `UPDATE PROXIES SET id_email = '' WHERE id_email = ${row['id']}`;

          child.db.run(sqlProx, function (err) {
            if (err) {
              Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'Error reason: ' + err.message
              })
            } else {
              var sql = `DELETE FROM EMAILS WHERE id = ${row['id']}`;
    
              child.db.run(sql, function (err) {
                if (err) {
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops..',
                      text: 'Error reason: ' + err.message
                    })
                } 
              });   
            }
          });
        } 
        if (idx === array.length - 1){ 
          document.getElementById('mainCheck').checked = false;
          Array.from(document.querySelectorAll('.subcheckbox:checked')).forEach(e => e.checked = false) 
          showLoading();
          setTimeout(() => {
            if($('#smsCreateEmailsTable').length){ 
              email_data.refreshCreateEmailsTable()
              email_data.getFreeProxyDataVal()
            }else{
              farm_data.refreshFarmingTable()
            }
          }, 2000);
        }    
      }); 
    }); 
  },

  getRecaptchaScore: function(event) {  
    var aktualCas = Moment().tz('Europe/Vienna').add(3, 'minutes').unix()
    var zamokkk = 'jopaIsKing/'+aktualCas
    var uvidimeVyskusmae = encryptText('SecretKey', zamokkk);
    var localProxy = $('#local-proxy:checked').length
    child.db.all(`SELECT	
                  count(*) as total
                FROM (
                  SELECT 
                    id_email,
                    sessions_type
                  FROM WINDOWS 
                  WHERE 1=1
                    and status_id = '1'
                  union
                  SELECT
                    email_id,
                    'FARMING' as session
                  FROM FARMING 
                  WHERE 1=1
                    and status in ('PROCESSING FARMING','INITIALIZING PROCESS')
                  union
                  SELECT
                    email_id,
                    'FORWARDING' as session
                  FROM FORWARDING
                  WHERE 1=1
                    and status in ('PROCESSING FORWARDING','INITIALIZING PROCESS')
                  union 
                  SELECT
                    id,
                    'REGISTERING' as session
                  FROM EMAILS
                  WHERE 1=1
                    and status in ('registering','initializing process')
                )`, function(err, rows) { 
        if(rows != undefined && rows != null && rows.length > 0){
          if (err) {
              Swal.fire({
                  icon: 'error',
                  title: 'Oops..',
                  text: 'Error reason: ' + err.message
              })
          }
          var actualWindowsExecVal = rows[0].total
        }
        else{
            var actualWindowsExecVal = 5
        }

      if(actualWindowsExecVal >= $(".page-wrapper").data("maxwindowval")){
        if($('#maxWindowsSwalError').length){
            return false
        }
        Swal.fire({
          icon: 'error',
          title: 'Maximum number of processes reached',
          html: `You have reached the maximum number of processes running at one time. 
                If you want a different maximum number of processes, you can change it in the settings or click here: <a id="maxWindowsSwalError" onclick="loader.loadNewPage(this,'nastav.html','','nastav');"href="javascript:void(0)">Settings</a>`, 
        })
        return false
      }

      var newArray = [];
      var data = [...document.querySelectorAll(".subcheckbox:checked")].map(
        (e) => e.value
      );
      data = data.join();
      arrayPrep = data.split(",");
      recaptchaType = document.getElementById("recaptcha-list").value;

      if (recaptchaType == "" || data == "") {
        return "";
      }
      
      child.countdownStartDebounce("regEmailStart")
      child.countdownStartDebounce("recaptchaEmailCheckScore")

      for (let row = 0; row < arrayPrep.length; row++) {
        var opt = {
          scriptPath: process.cwd()+LOCATION_UNPACK+'\\_engine\\',
          pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
          args: [arrayPrep[row],uvidimeVyskusmae,localProxy],
        };
         
        if(actualWindowsExecVal+1 >= $(".page-wrapper").data("maxwindowval") || row+1 >= $(".page-wrapper").data("maxwindowval")){           
          if($('#maxWindowsSwalError').length){
            return false
          }
          //if(automate == false){
          if($('#maxWindowsSwalError').length == 0){                          
            Swal.fire({
              icon: 'error',
              title: 'Maximum number of processes reached',
              html: `You have reached the maximum number of processes running at one time. The remaining processes have been added to the waiting tasks. 
                    If you want a different maximum number of processes, you can change it in the settings or click here: <a id="maxWindowsSwalError" onclick="loader.loadNewPage(this,'nastav.html','','nastav');"href="javascript:void(0)">Settings</a>`, 
            })
          }
        }

        child.db.all(`SELECT	
            count(*) as total
          FROM (
            SELECT 
              id_email
            FROM WINDOWS a
            WHERE status_id = 1
            and id_email = ${arrayPrep[row]}
            union all 
            SELECT 
              id_email
            FROM (
              SELECT	
                id_email,
                sessions_type,
                max(created) as created
              FROM LOGS
              WHERE id_email = ${arrayPrep[row]}	
              and datetime(created/1000, 'unixepoch','localtime') >= datetime(datetime(),'-10 seconds','localtime')
              GROUP BY
                id_email,
                sessions_type
            )
        )`, function(err, rows) { 
          
          if(rows != undefined && rows != null && rows.length > 0 && rows[0].total > 0){
            if($('#alreadyRecaptchaEmailErr').length){
              return false
            }

            Swal.fire({
              icon: 'error',
              title: 'The recaptcha score check is currently running on this email.',
              html: `<span id="alreadyRecaptchaEmailErr">Please, try another.</span>`, 
            })
            return false
          }

          //child.taskLogger(arrayPrep[row],"RECAPTCHA") 

          if (recaptchaType == "V3") {
              child.taskLogger(arrayPrep[row],"RECAPTCHA3")
              var pyshell = new child.PythonShell("sv3.py", opt);
          } else if (recaptchaType == "V2") {
              child.taskLogger(arrayPrep[row],"RECAPTCHA2")
              var pyshell = new child.PythonShell("sv2.py", opt);
          } else {
            return "";
          }

          pyshell.on("message", (results) => {
            if(results == 'Email must be registered'){
              if($('#alreadyRecaptchaUnregistered').length == 0){
                Swal.fire({
                  icon: 'error',
                  title: 'Email must be registered first.',
                  html: `<span id="alreadyRecaptchaUnregistered">Please, try another.</span>`, 
                })
              }
            }
          });
  
          pyshell.end((err) => {
            if (err) {
              hideLoading();
              Swal.fire({
                icon: "error",
                title: "Oops!",
                text: "Error reason:" + err.message,
              });
            }
          });
        })

        elementCheck = document.getElementById('mainCheck')
        if(elementCheck != undefined || elementCheck != null){
          elementCheck.checked = false;
        }

        Array.from(document.querySelectorAll(".subcheckbox:checked")).forEach(
          (e) => (e.checked = false)
        );
      }
    }); 
  },

  registerFromPlay: function(event, emailId) {
    event.preventDefault();

    if(window.disTimeVal > 0){
      return false
    }

    setTimeout(function () {
      return true
    }, 2000);

    if(document.querySelectorAll('.loading-play-logo').length >= $(".page-wrapper").data("maxwindowval")){
      Swal.fire({
        icon: "error",
        title: "Too much processes",
        text: "You must wait for the running processes to complete",
      });
      return false
    }

    document.getElementById(
      `email-process-${emailId}`
    ).innerHTML = `<img src="${process.cwd()+LOCATION}/cache-data/assets/images/loading.gif" id="email-process-${emailId}" class="play loading-play-logo" data-sec="0" style="height:13px"/ >`;
    
    //console.log($('img[id^="email-process-"].play').length)

    var checkExist = setInterval(function () {
      var maxVal = 0;
      $('img[id^="email-process-"].play').each(function () {
        if (
          $(this).attr("id") != `email-process-${emailId}` &&
          maxVal < $(this).data("sec")
        ) {
          maxVal = $(this).data("sec");
        }
      });

      if (
        maxVal <= $(`img[id^="email-process-${emailId}"].play`).data("sec")
      ) { 
        $(`:checkbox[value=${emailId}]`).prop("checked", "true");
        self_email.registerEmails(event);
        clearInterval(checkExist);
        setTimeout(function () {
          return true
        }, 4000);
        
      }
    }, 2000);
  
    // $(`:checkbox[value=${emailId}]`).prop("checked", "true");
    // registerEmails(event);
  },

  registerEmails: function(event) {
    event.preventDefault();
    child.countdownStartDebounce("recaptchaEmailCheckScore")
    child.countdownStartDebounce("regEmailStart")
    
    child.Slaninka()
    var data = [...document.querySelectorAll(".subcheckbox:checked")].map(
      (e) => e.value
    );

    userEvent = false

    data = data.join();
    if (data == "") {
      child.db.all(`SELECT id,smsProviderId as provider FROM EMAILS WHERE (status IS NULL or status in ('unregistered','canceled'))`, function (err, rows) {
        if (err) {
          Swal.fire({
            icon: "error",
            title: "Oops..",
            text: "Error reason: " + err.message,
          });
        }
        var data = [...rows].map((e) => e.id)
        data = data.join()
        arrayPrep = data.split(",")
        if (arrayPrep.length == 0 || arrayPrep[0] == "") {
          Swal.fire({
            icon: "error",
            title: "Oops!",
            text: "No free emails for registrating",
          });
          return "";
        }

        self_email.runRegistration(arrayPrep,false,false);
      });
    } else {
      arrayPrep = data.split(",");

      self_email.runRegistration(arrayPrep,false,true);
    }
    Array.from(document.querySelectorAll(".subcheckbox:checked")).forEach(
      (e) => (e.checked = false)
    );
  },

  runRegistration: function(arrayPrep = [],automate = false, userEvent = false) {
    
    var aktualCas = Moment().tz('Europe/Vienna').add(3, 'minutes').unix()
    var zamokkk = 'jopaIsKing/'+aktualCas
    var uvidimeVyskusmae = encryptText('SecretKey', zamokkk);
    var localProxy = $('#local-proxy:checked').length

    child.db.all(`
    SELECT	
      count(*) as total
    FROM (
      SELECT 
        id_email,
        sessions_type
      FROM WINDOWS 
      WHERE 1=1
        and status_id = '1'
      union
      SELECT
        email_id,
        'FARMING' as session
      FROM FARMING 
      WHERE 1=1
        and status in ('PROCESSING FARMING','INITIALIZING PROCESS')
      union
      SELECT
        email_id,
        'FORWARDING' as session
      FROM FORWARDING
      WHERE 1=1
        and status in ('PROCESSING FORWARDING','INITIALIZING PROCESS')
      union 
      SELECT
        id,
        'REGISTERING' as session
      FROM EMAILS
      WHERE 1=1
        and status in ('registering','initializing process')
    )
    `, function(err, rows) { 
        if(rows != undefined && rows != null && rows.length > 0){
            if (err) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops..',
                    text: 'Error reason: ' + err.message
                })
            }
            var actualWindowsExecVal = rows[0].total
        }
        else{
            var actualWindowsExecVal = 5
        } 

      if(actualWindowsExecVal > $(".page-wrapper").data("maxwindowval")){
        if($('#maxWindowsSwalError').length){
            return false
        }
        if(automate == false){
          Swal.fire({
            icon: 'error',
            title: 'Maximum number of processes reached',
            html: `You have reached the maximum number of processes running at one time. The remaining processes have been added to the waiting tasks. 
                  If you want a different maximum number of processes, you can change it in the settings or click here: <a id="maxWindowsSwalError" onclick="loader.loadNewPage(this,'nastav.html','','nastav');"href="javascript:void(0)">Settings</a>`, 
          })

          for (let rowX = 0; rowX < arrayPrep.length; rowX++) {
            child.db.all(`SELECT id FROM WINDOWS WHERE id_email = ${arrayPrep[rowX]} and sessions_type = 'REGISTERING'`, function(err, rows) { 
              if (err) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops..',
                    text: 'Error reason: ' + err.message
                })
              }
              
              if(rows == undefined || rows == null || rows.length == 0){
                  var now = new Date()

                  let data_window = [now, arrayPrep[rowX] ,"REGISTERING"]
                  let sql_window = `INSERT INTO WAITING (created, id_email, sessions_type) values(?,?,?) `

                  child.db.run(sql_window,data_window,function(err){
                    //if (err) {
                      // console.log(err.message)
                    //}
                    //email_data.refreshCreateEmailsTable()
                  })
              }
            })
          }

          return 
        }
        else{
          return
        }
      }

      child.db.all(`SELECT id_email FROM WAITING WHERE sessions_type = 'REGISTERING' order by created`, function (err, rows) {
        if (err) {
          Swal.fire({
            icon: "error",
            title: "Oops..",
            text: "Error reason: " + err.message,
          });
        }
        
        if(!userEvent){
          if(rows != undefined && rows != null && rows.length > 0){
            //arrayPrep = [...rows,...arrayPrep]
            var dataWait = [...rows].map((e) => e.id_email);
            dataWait = dataWait.join();
            arrayPrepWait = dataWait.split(",");
            arrayPrep = child.uniq_fast(arrayPrepWait.concat(arrayPrep))
          }
        }

        for (let row = 0; row < arrayPrep.length; row++) {
          if(arrayPrep[row].length == 0){ 
            Swal.fire({
              icon: "error",
              title: "Oops!",
              text: "No free emails for registering. Let's see if we have any farming tasks..",
            });
            farm.farmEmails(true,false)
            return false 
          }

          //SWALL vypisanie ci uz nie je registrovany alebo sa neregistruje prave
          child.db.all(`SELECT id from WINDOWS WHERE sessions_type = 'REGISTERING' and id_email = '${arrayPrep[row]}' and status_id = '1'
                          union all
                          SELECT id from EMAILS where status in ('registering','initializing process','registered') and id = '${arrayPrep[row]}'`, function(err, rows) { 
              if(rows.length > 0){
                if($('#alreadyRegisteringEmailErr').length){
                  return false
                }
      
                if(automate == false){
                  Swal.fire({
                    icon: 'error',
                    title: 'This email is being registered or has already been successfully registered.',
                    html: `<span id="alreadyRegisteringEmailErr">Please, try another.</span>`, 
                  })
                }
              }
          })

          //if(arrayPrep[row].length == 0){ return false }
          if(actualWindowsExecVal+1 > $(".page-wrapper").data("maxwindowval") || row+1 >= $(".page-wrapper").data("maxwindowval")){           
                if($('#maxWindowsSwalError').length){
                  return false
                }
                //if(automate == false){
                if($('#maxWindowsSwalError').length == 0 && automate == false){                          
                  Swal.fire({
                    icon: 'error',
                    title: 'Maximum number of processes reached',
                    html: `You have reached the maximum number of processes running at one time. The remaining processes have been added to the waiting tasks. 
                          If you want a different maximum number of processes, you can change it in the settings or click here: <a id="maxWindowsSwalError" onclick="loader.loadNewPage(this,'nastav.html','','nastav');"href="javascript:void(0)">Settings</a>`, 
                  })
                }
                for (let rowX = row; rowX < arrayPrep.length; rowX++) {
                  if(rowX > 101){
                    return 
                  }

                  child.db.all(`SELECT id FROM WAITING WHERE id_email = '${arrayPrep[rowX]}' and sessions_type = 'REGISTERING'`, function(err, rows) { 
                    if (err) {
                      Swal.fire({
                          icon: 'error',
                          title: 'Oops..',
                          text: 'Error reason: ' + err.message
                      })
                    }
                    
                    if(rows == undefined || rows == null || rows.length == 0){
                        var now = new Date()

                        let data_window = [now, arrayPrep[rowX] ,"REGISTERING"]
                        let sql_window = `INSERT INTO WAITING (created,id_email, sessions_type) values(?,?,?) `
                        
                        child.db.run(sql_window,data_window,function(err){
                          //if (err) {
                            // console.log(err.message)
                          //}
                          //email_data.refreshCreateEmailsTable()
                        })
                    }
                  })
                }

                return false
              
            //}
            // else{
            //   console.log("ANOOOOOO SKONCIL TUUUUUUUUU")
            //   return
            // }
          }

          actualWindowsExecVal += 1

          setTimeout(function timer() {

            let sqlWait = `DELETE FROM WAITING WHERE id_email = ${arrayPrep[row]}`;

            child.db.run(sqlWait, function (err) {
              if (err) {
                // Swal.fire({
                //   icon: 'error',
                //   title: 'Oops..',
                //   text: 'Error reason: ' + err.message
                // })
              }
            })

            child.db.all(`SELECT id from WINDOWS WHERE sessions_type = 'REGISTERING' and id_email = '${arrayPrep[row]}' and status_id = '1'
                          union all
                          SELECT id from EMAILS where status in ('registering','initializing process','registered') and id = '${arrayPrep[row]}'`, function(err, rows) { 
              if(rows == undefined || rows == null || rows.length == 0){
                
                child.db.all(`SELECT distinct smsProviderId,status FROM EMAILS WHERE id = '${arrayPrep[row]}'`, function(err, rows) {         
                    if (err) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops..',
                            text: 'Error reason: ' + err.message
                        })
                    }
                    
                    if(rows == undefined || rows[0] == null){
                      return false
                    }
                    
                    if(rows[0]['smsProviderId'] == undefined || rows[0]['smsProviderId'] == null){
                      Swal.fire({
                        icon: "error",
                        title: "Oops!",
                        text: "It looks like you want to use an unknown provider. It looks like you want to use an unknown provider or this email is already registered.",
                      });
                      return false
                    }

                    if(rows[0]['smsProviderId'].toLowerCase() == 'registered'){
                      return false
                    }

                    if (rows[0]['smsProviderId'] == "LUCKYSMS") {
                      var registrationFileName = "cachegcls.py"
                    } else if (rows[0]['smsProviderId'] == "SMSPVA") {
                      var registrationFileName = "cachegcsp.py"
                    } else if (rows[0]['smsProviderId'] == "PVACODES") {
                      var registrationFileName = "cachegcpc.py"
                    } else if (rows[0]['smsProviderId'] == "GETSMSCODE") {
                      var registrationFileName = "cachegcgs.py"
                    } else {
                      Swal.fire({
                        icon: "error",
                        title: "Oops!",
                        text: "It looks like you want to use an unknown provider",
                      });
                    }
                    vrabec = '81309b12-6ddf-4333-b5c3-7bc65fe7f2c2'
                    var opt = {
                      scriptPath: process.cwd()+LOCATION_UNPACK+'\\_engine\\',
                      pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
                      args: [arrayPrep[row],vrabec, uvidimeVyskusmae,localProxy],
                    };
                    child.taskLogger(arrayPrep[row],"REGISTERING")
                    var pyshell = new child.PythonShell(registrationFileName, opt);
                    //clearTimeout(`emailRegId${arrayPrep[row]}`)

                    pyshell.on("message", (results) => {
                      if(results == 'Country_support_error' || results == 'Error_log'){
                        document.getElementById(`email-process-${arrayPrep[row]}`).innerHTML = `<p id="email-process-${arrayPrep[row]}" class="exec-button" style="color:#f62d51;"><i id="exec-icon-${arrayPrep[row]}" class="m-r-10 mdi mdi-close-box" ></i></p>`
                      }else{
                        
                        child.db.all(`SELECT status FROM EMAILS WHERE id = '${arrayPrep[row]}'`, function(err, rows) {         
                            if (err) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops..',
                                    text: 'Error reason: ' + err.message
                                })
                            }

                            if(rows == undefined || rows == null){
                              return false
                            }

                            if(rows != undefined && rows != null && rows.length > 0){
                                if(rows[0].status != 'canceled'){
                                  child.db.all(`SELECT id_email FROM WAITING WHERE sessions_type = 'REGISTERING' order by created desc`, function (err, rows) {
                                    if (err) {
                                      Swal.fire({
                                        icon: "error",
                                        title: "Oops..",
                                        text: "Error reason: " + err.message,
                                      });
                                    }
                              
                                    if(rows != undefined && rows != null && rows.length > 0){
                                      self_email.runRegistration([],true,false)
                                    }
                                  })
                                } 
                            }
                        })
                        //email_data.refreshCreateEmailsTable()
                      }
                      // hideLoading()
                      //     Swal.fire({
                      //         icon: 'error',
                      //         title: 'Oops!',
                      //         text: 'Error reason:' + results
                      //     })
                    });

                    pyshell.end((err) => {
                      //if (err) {
                        // Swal.fire({
                        //   icon: "error",
                        //   title: "Oops!",
                        //   text: "Error reason:" + err.message,
                        // });
                        elementErr = document.getElementById(`email-process-${arrayPrep[row]}`)
                        if (typeof(elementErr) != 'undefined' && elementErr != null)
                        {
                          document.getElementById(`email-process-${arrayPrep[row]}`).innerHTML = `<p id="email-process-${arrayPrep[row]}" class="exec-button" style="color:#f62d51;"><i id="exec-icon-${arrayPrep[row]}" class="m-r-10 mdi mdi-close-box" ></i></p>`
                        }

                        child.db.all(`SELECT status FROM EMAILS WHERE id = '${arrayPrep[row]}'`, function(err, rows) {         
                          if (err) {
                              Swal.fire({
                                  icon: 'error',
                                  title: 'Oops..',
                                  text: 'Error reason: ' + err.message
                              })
                          }

                          if(rows == undefined || rows == null){
                            return false
                          }

                          if(rows != undefined && rows != null && rows.length > 0){
                              if(rows[0].status != 'canceled'){
                                child.db.all(`SELECT id_email FROM WAITING WHERE sessions_type = 'REGISTERING' order by created desc`, function (err, rows) {
                                  if (err) {
                                    Swal.fire({
                                      icon: "error",
                                      title: "Oops..",
                                      text: "Error reason: " + err.message,
                                    });
                                  }
                                  
                                  if(rows != undefined && rows != null && rows.length > 0){
                                    self_email.runRegistration([],true,false)
                                  }
                                })
                              }
                          }
                        })
                        //email_data.refreshCreateEmailsTable()
                        // if (err) {
                        //   throw err;
                        // }
                    });
                    elementCheck = document.getElementById('mainCheck')
                    if(elementCheck != undefined || elementCheck != null){
                      elementCheck.checked = false;
                    }
                });
              }
            })
          }, row * 6000);
        }
      })
    })
  },

  killAllRegistrations: function() {
    listOfUnregisteredStatuses = '("'+String(filter_create_vals_dictionary.UNREGISTERED).toLowerCase().replace(/,/g,'","')+'")'
    listOfUnregisteredStatuses.replace("")
    var sqlCanc = `UPDATE EMAILS SET status = 'canceled' WHERE lower(status) in ${listOfUnregisteredStatuses} AND lower(status) != 'unregistered'`;
    child.db.run(sqlCanc, function (err) {}) 
    var sqlWait = `DELETE FROM WAITING WHERE sessions_type = 'REGISTERING'`;
    child.db.run(sqlWait, function (err) {}) 

    child.db.all(
      //`SELECT DISTINCT id,id_window FROM WINDOWS WHERE status_id = '1' and sessions_type = 'REGISTERING'`,
      `SELECT DISTINCT id,id_window FROM WINDOWS WHERE sessions_type = 'REGISTERING' and date(datetime(date_created/ 1000 , 'unixepoch')) >= date('now')-3`,
      function (err, rows) {
        // if (err) {
        //     Swal.fire({
        //         icon: 'error',
        //         title: 'Oops..',
        //         text: 'Error reason: ' + err.message
        //     })
        // }
        var arrayVal = []

        if(rows === undefined || rows.length == 0){
          hideLoading()
          return false
        }

        rows.forEach(function (row) {
          draft = Object.assign(row)
          arrayVal.push(Object.values(draft))
        });

        //windows = row["id_window"];
        backgroundAuto = false;
        modeJSON = true;

        if (arrayVal == null) {
          return false;
        }

        var opt = {
          scriptPath: process.cwd()+LOCATION_UNPACK+'\\_engine\\HG\\',
          pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
          args: [backgroundAuto,modeJSON],
        };

        var pyshell = new child.PythonShell("Kwdw.py", opt);

        pyshell.send(JSON.stringify(arrayVal), { mode: 'json' });

        pyshell.on("message", (results) => {
          // if (results == "Killed") {
          //   let data = ["0", windows];
          //   let sql = `UPDATE WINDOWS SET status_id = ? WHERE id_window = ?`;

          //   child.db.run(sql, data, function (err) {
          //     if (err) {
          //       console.log("ERROR" + err);
          //     }
          //     //UPDATE DONE LOG??
          //   });
          // }
        });

        pyshell.end((err) => {
          if (err) {
            Swal.fire({
              icon: "error",
              title: "Oops!",
              text: "Error reason:" + err.message,
            });
            throw err;
          }
        });
      }
    );
  },
  changeProxyGroup: function(){
      showLoading();
      var data = [...document.querySelectorAll('.subcheckbox:checked')].map(e => e.value);
      data = data.join()  

      arrayPrep = data.split(",")

      if(arrayPrep.length == 0 || arrayPrep == ""){
            hideLoading()
            return false
      }

      child.changeProxy(arrayPrep,'filterProxyGroupImport')

      setTimeout(() => {
        arrayPrep = window.finalEmailListProxyChange
        hideLoading()
        if(arrayPrep.length == 0){
          Swal.fire({
              icon: 'info',
              title: 'Oops..',
              text: 'Not enough proxies. Import new or try another.' 
          })
          email_data.refreshCreateEmailsTable()
          return false 
        }
        email_data.refreshCreateEmailsTable()
        Swal.fire({
          icon: "info",
          title: "Proxy group changed",
          text:"You changed new proxy group on selected emails.",
        });
        
        
      }, 3000)
  }
}
