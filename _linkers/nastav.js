var self_setting = module.exports = {     
    getCountriesOther: function() {
        $("#countries-list").html("<option value=''>Loading...</option>")
        child.db.all("SELECT count(*) as total FROM sqlite_master WHERE name ='COUNTRY' and type='table'", function(err, rows) {  
            if(Object.values(rows)[0].total == 0){
                child.updateCountryList(true)
            }
            else{
                var newArray = ''
            
                    var providers = ''
                    child.db.all("SELECT provider FROM PROVIDERS ORDER BY provider", function(err, rows) {  
                        if (err) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops..',
                                text: 'Error reason: ' + err.message
                            })
                        }

                        if(rows === undefined || rows.length == 0){
                            return false
                        }

                        rows.forEach(function (row) { 
                            providers += `<option value='${row.provider}'>${row.provider}</option>`
                        }) 
                        if(providers.length == 0){ 

                        }else{
                            $("#provider-list").html(providers)
                        }

                        if(document.getElementById('provider-list').value == 'LUCKYSMS'){
                            
                            child.db.all("SELECT a.system,a.name,a.fullname,b.prefix FROM COUNTRY_LUCKY a LEFT JOIN COUNTRY b on a.name = b.name ORDER BY a.fullname", function(err, rows) {  
                                if (err) {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops..',
                                        text: 'Error reason: ' + err.message
                                    })
                                }

                                var systemNumber = 1
                                var i = 0

                                if(rows === undefined || rows.length == 0){
                                    return false
                                }

                                rows.forEach(function (row) { 
                                    i += 1
                                    if(i == 1){
                                        newArray += `<optgroup label="SYSTEM ${systemNumber}">`
                                        systemNumber += 1
                                    }

                                    if(row['system'] == 'system'+systemNumber && row['system'] != 'system1'){
                                        newArray += '</optgroup>'
                                        newArray += `<optgroup label="SYSTEM ${systemNumber}">`
                                        systemNumber += 1
                                    }
                                    
                                    optVal = `<option value="LUCKYSMS_${row['system']}_${row['name']}">${row['fullname']} (${row['name']})</option>`
                                    newArray += optVal
                                    
                                    if(i == rows.length){
                                        newArray += '</optgroup>'
                                    }
                                    //newArray += `<option value='${row.name}'>${row.fullname} (${row.name})</option>`
                                }) 
                                if(newArray.length == 0){
                                    child.db.all("SELECT name,fullname,prefix FROM COUNTRY ORDER BY fullname", function(err, rows) {  
                                        if (err) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops..',
                                                text: 'Error reason: ' + err.message
                                            })
                                        }

                                        if(rows === undefined || rows.length == 0){
                                            return false
                                        }
                                        
                                        rows.forEach(function (row) { 
                                            newArray += `<option value='${row.name}'>${row.fullname} (${row.name})</option>`
                                        }) 
                                        if(newArray.length == 0){
                                            
                                        }else{
                                            $("#countries-list").html(newArray)
                                            if(rows[0].prefix == undefined || rows[0].prefix == null){
                                                document.getElementById('prefix').value = null
                                            }else{
                                                document.getElementById('prefix').value = rows[0].prefix
                                            }
                                            //document.getElementById('prefix').style.display="none"
                                            document.getElementById('updatePrefixBtn').style.display="block"
                                        }
                                    });

                                }else{
                                    $("#countries-list").html(newArray)
                                    if(rows[0].prefix == undefined || rows[0].prefix == null){
                                        document.getElementById('prefix').value = null
                                    }else{
                                        document.getElementById('prefix').value = rows[0].prefix
                                    }
                                    //document.getElementById('prefix').style.display="none"
                                    document.getElementById('updatePrefixBtn').style.display="block"
                                }
                            });

                        }
                        else{
                            child.db.all("SELECT name,fullname,prefix FROM COUNTRY ORDER BY fullname", function(err, rows) {  
                                if (err) {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops..',
                                        text: 'Error reason: ' + err.message
                                    })
                                }

                                if(rows === undefined || rows.length == 0){
                                    return false
                                }
                                
                                rows.forEach(function (row) { 
                                    newArray += `<option value='${row.name}'>${row.fullname} (${row.name})</option>`
                                }) 

                                if(newArray.length == 0){
                                    
                                }else{
                                    $("#countries-list").html(newArray)
                                    if(rows[0].prefix == undefined || rows[0].prefix == null){
                                        document.getElementById('prefix').value = null
                                    }else{
                                        document.getElementById('prefix').value = rows[0].prefix
                                    }
                                    document.getElementById('prefix').style.display="block"
                                    document.getElementById('updatePrefixBtn').style.display="block"
                                }
                            });
                        }
                        
                    });
                    
            }
        });
            
    },

    getCountryValue: function(){
        mainButton = document.getElementById('country')
        sel = document.getElementById('countries-list')
        sel1 = document.getElementById('provider-list')
        
        mainButton.addEventListener("click",function(){
            if(sel1.options[sel1.selectedIndex].value != 'LUCKYSMS'){
                selectVal = sel.options[sel.selectedIndex].value
                child.db.all(`SELECT name,fullname,prefix FROM COUNTRY WHERE name = '${selectVal}' LIMIT 1`, function(err, rows) { 
                    if (err) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops..',
                            text: 'Error reason: ' + err.message
                        })
                    }
                    if(rows[0] == undefined || rows[0].prefix == undefined || rows[0].prefix == null){
                        document.getElementById('prefix').value = null
                    }else{
                        document.getElementById('prefix').value = rows[0].prefix
                    }
                    // if(document.getElementById('provider-list').value != 'LUCKYSMS'){
                    //     document.getElementById('prefix').value = rows[0].prefix
                    // }
                });  
            }
        })

        sel1.addEventListener("change",function(){
            var newArray = ''
            
            if(document.getElementById('provider-list').value == 'LUCKYSMS'){
                child.db.all("SELECT a.system,a.name,a.fullname,b.prefix FROM COUNTRY_LUCKY a LEFT JOIN COUNTRY b on a.name = b.name ORDER BY a.fullname", function(err, rows) {  
                    if (err) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops..',
                            text: 'Error reason: ' + err.message
                        })
                    }

                    var systemNumber = 1
                    var i = 0

                    if(rows === undefined || rows.length == 0){
                        return false
                    }
                    
                    //pocet SYSTEMOV v LUCKY !!!!!!!!!!!!!!!!!!!!!! aktualne nastavene na 4
                    if(rows.length > 0){
                        for (let i = 1; i <= 4; i++) {
                            newArray += `<optgroup label="SYSTEM ${i}">`

                            rows.forEach(function (row) { 
                                if(row['system'] == 'system'+i){
                                    optVal = `<option value="LUCKYSMS_${row['system']}_${row['name']}">${row['fullname']} (${row['name']})</option>`
                                    newArray += optVal
                                }
                            })
                        
                            newArray += '</optgroup>'
                        }
                    }

                    if(newArray.length == 0){
                        
                    }else{
                        $("#countries-list").html(newArray)
                        if(rows[0].prefix == undefined || rows[0].prefix == null){
                            document.getElementById('prefix').value = null
                        }else{
                            document.getElementById('prefix').value = rows[0].prefix
                        }
                        document.getElementById('updatePrefixBtn').style.display="block"
                    }
                });
            }
            else{
                child.db.all("SELECT name,fullname,prefix FROM COUNTRY ORDER BY fullname", function(err, rows) {  
                    if (err) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops..',
                            text: 'Error reason: ' + err.message
                        })
                    }

                    if(rows === undefined || rows.length == 0){
                        return false
                    }
                    
                    rows.forEach(function (row) { 
                        newArray += `<option value='${row.name}'>${row.fullname} (${row.name})</option>`
                    }) 
                    if(newArray.length == 0){
                        
                    }else{
                        $("#countries-list").html(newArray)
                        if(rows[0].prefix == undefined || rows[0].prefix == null){
                            document.getElementById('prefix').value = null
                        }else{
                            document.getElementById('prefix').value = rows[0].prefix
                        }
                        document.getElementById('prefix').style.display="block"
                        document.getElementById('updatePrefixBtn').style.display="block"
                    }
                });
            }
        })

        sel.addEventListener("change",function(){
            selectValDraft = sel.options[sel.selectedIndex].text
            selectVal = selectValDraft.replace(/ *\([^)]*\) */g, "")
            //if(document.getElementById('provider-list').value != 'LUCKYSMS'){
                child.db.all(`SELECT name,fullname,prefix FROM COUNTRY WHERE fullname = '${selectVal}' LIMIT 1`, function(err, rows) { 
                    if (err) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops..',
                            text: 'Error reason: ' + err.message
                        })
                    }
                    if(rows[0] == undefined){
                        document.getElementById('prefix').value = null
                    }else{
                        if(rows[0].prefix == undefined || rows[0].prefix == null){
                            document.getElementById('prefix').value = null
                        }else{
                            document.getElementById('prefix').value = rows[0].prefix
                        }
                    }
                    
                    
                });  
            //}
        })
    },

    updateCountryPrefix: function(){
        let prefixDraft = document.getElementById('prefix').value
        let sel = document.getElementById('countries-list')
        let provider = document.getElementById('provider-list').value

        if(provider != 'LUCKYSMS'){
            var selectVal = sel.options[sel.selectedIndex].value
        }else{
            let selectValDraft = sel.options[sel.selectedIndex].value
            var selectVal = selectValDraft.split('_')[2]
        }
        
        if (/^(?=.*?[1-9])[0-9+()-]+$/.test(prefixDraft)) {
            prefix = prefixDraft
            let data = [prefix,selectVal]
            let sql = `UPDATE COUNTRY SET prefix = ? WHERE name = ?`

            child.db.run(sql,data,function(err){
                if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }

                Swal.fire({
                    icon: 'info',
                    title: 'Country data updated!',
                    text: 'You succesfully change telephone prefix.'
                })

                //console.log(`Row(s) updated: ${this.changes}`)
            })

            //child.db.close()
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Wrong prefix value!',
                text: 'The value you entered is incorrect for the prefix value.'
            })
        }
    },

    updateFarmingSettings: function(defaultVal = false){
        if(defaultVal == true){
            var videosVal = 2
            var minVideos = 120
            var maxVideos = 300
            var newsVal = 2
            var minNews = 120
            var maxNews = 300
            var itemsVal = 3
            var items_category_val = 4
            var searchVal = 4
        }
        else{
            var videosVal = document.getElementById('videosVal').value
            var minVideos = document.getElementById('minVideosVal').value
            var maxVideos = document.getElementById('maxVideosVal').value
            var newsVal = document.getElementById('newsVal').value
            var minNews = document.getElementById('minNewsVal').value
            var maxNews = document.getElementById('maxNewsVal').value
            var itemsVal = document.getElementById('itemsVal').value
            var items_category_val = document.getElementById('items_category_val').value
            var searchVal = document.getElementById('searchVal').value
        }
        if(videosVal =="" || minVideos == ""|| maxVideos == "" || newsVal == "" || minNews == "" || maxNews == "" || itemsVal == "" || items_category_val == "" || searchVal == ""){
            Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'All items all required.'
            })
            return ""
        }
        
        if( items_category_val > 8 ){
            Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'The maximum of shopping categories is 8.'
            })
            return ""
        }

        if(videosVal < 0 || minVideos < 0|| maxVideos < 0 || newsVal < 0 || minNews < 0 || maxNews < 0 || itemsVal < 0 || items_category_val < 0 || searchVal < 0){
            Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'Minimum is 0.'
            })
            return ""
        }

        if(videosVal > 60 || maxVideos > 5000 || newsVal > 60 || maxNews > 5000 || itemsVal > 60 || items_category_val > 60 || searchVal > 60){
            Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'Maximum for second is 5000 and for other numbers 60.'
            })
            return ""
        }

        if(minVideos >= maxVideos || minNews >= maxNews){
            Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'The maximum must be greater than the minimum.'
            })
            return ""
        }
        
        var now = new Date();

        let data = [now.toLocaleDateString("de-DE"), 'FARMING',videosVal,minVideos,maxVideos,newsVal,minNews,maxNews,itemsVal,items_category_val,searchVal]
        let sql = `INSERT OR REPLACE INTO SETTINGS (updated, category, videos_val, min_videos, max_videos, news_val, min_news, max_news, items_val, items_category_val, search_val) values(?,?,?,?,?,?,?,?,?,?,?) `

        child.db.run(sql,data,function(err){
            if (err) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops..',
                    text: 'Error reason: ' + err.message
                })
            }

            document.getElementById('videosVal').value = videosVal
            document.getElementById('minVideosVal').value = minVideos
            document.getElementById('maxVideosVal').value = maxVideos
            document.getElementById('newsVal').value = newsVal
            document.getElementById('minNewsVal').value = minNews
            document.getElementById('maxNewsVal').value = maxNews
            document.getElementById('itemsVal').value = itemsVal
            document.getElementById('items_category_val').value = items_category_val
            document.getElementById('searchVal').value = searchVal
        })

        Swal.fire({
            icon: 'info',
            title: 'Farming data updated!',
            text: 'You succesfully change your farming settings'
        })
        //child.db.close()
    },

    getActualFarmingVal: function(){

        child.db.all(`SELECT * FROM SETTINGS WHERE category = 'FARMING' LIMIT 1`, function(err, rows) { 
            if(rows != undefined && rows != null && rows.length > 0){
                //var lastMod = new Date(rows[0].updated).toLocaleDateString("de-DE")
                if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }
                document.getElementById('videosVal').value = rows[0].videos_val
                document.getElementById('minVideosVal').value = rows[0].min_videos
                document.getElementById('maxVideosVal').value = rows[0].max_videos
                document.getElementById('newsVal').value = rows[0].news_val
                document.getElementById('minNewsVal').value = rows[0].min_news
                document.getElementById('maxNewsVal').value = rows[0].max_news
                document.getElementById('itemsVal').value = rows[0].items_val
                document.getElementById('items_category_val').value = rows[0].items_category_val
                document.getElementById('searchVal').value = rows[0].search_val
                document.getElementById('lastMod').innerHTML = rows[0].updated
            }
        });
    },

    getActualCountryVal: function(){
        child.db.all(`SELECT max(created) as last_created, max(updated) as last_updated FROM COUNTRY`, function(err, rows) { 
            
            if(rows == undefined || rows == null ||  rows[0].last_created == undefined || rows[0].last_created == null ){
                var lastModCreate = null
            }else{
                var lastModCreate = new Date(rows[0].last_created).toLocaleDateString("de-DE")
            }
            
            if(rows == undefined || rows == null ||  rows[0].last_updated == undefined || rows[0].last_updated == null){
                var lastModUpdate = null
            }else{
                var lastModUpdate = new Date(rows[0].last_updated).toLocaleDateString("de-DE")
            }


            if (err) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops..',
                    text: 'Error reason: ' + err.message
                })
            }

            if(rows[0].last_updated == null){
                document.getElementById('lastModCountry').innerHTML = lastModCreate
            }
            else{
                document.getElementById('lastModCountry').innerHTML = lastModUpdate
            }
        });
    },

    updateMaxWindows: function(){
        var newMaxWindows = document.getElementById('max-windows-val').value
        showLoading()
        if(newMaxWindows > 0 && newMaxWindows < 15){
            var now = new Date();

            let data = [now.toLocaleDateString("de-DE"), 'WINDOWS',newMaxWindows]
            let sql = `INSERT OR REPLACE INTO SETTINGS (updated, category, max_windows) values(?,?,?) `

            child.db.run(sql,data,function(err){
                if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }
                hideLoading()
                Swal.fire({
                    icon: 'info',
                    title: 'Max processes updated!',
                    text: 'You have successfully changed the maximum number of running processes at one time.'
                })

                child.db.all(`SELECT max_windows FROM SETTINGS WHERE category = 'WINDOWS' LIMIT 1`, function(err, rows) { 
                    if(rows != undefined && rows != null && rows.length > 0){
                        if (err) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops..',
                                text: 'Error reason: ' + err.message
                            })
                        }
                        var maxWindowsExecutedVal = rows[0].max_windows
                    }
                    else{
                        var maxWindowsExecutedVal = 5
                    }
                    $(".page-wrapper").data("maxwindowval",maxWindowsExecutedVal+1) 
                });
            })
        }
        else{
            hideLoading()
            Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'Number must be greater than 0  and less than 15!'
            })
        }
    },

    updateRecoveryDomain: function(){
        //validaciu na email sem treba doplniť
        var newRecoveryEmail = document.getElementById('maxRecoveryDomain').value
        showLoading()

        if(!/^@([\w\-]+)((\.(\w){2,3})+)$/.test(newRecoveryEmail)){
            hideLoading()
            Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'Recovery domain must be in format for example @domain.com without name before @. If the domain is OK, please check the typos.'
            })
            return false
        }

        if(newRecoveryEmail.length > 0){
            var now = new Date();

            let data = [now.toLocaleDateString("de-DE"), 'RECOVERY',newRecoveryEmail]
            let sql = `INSERT OR REPLACE INTO SETTINGS (updated, category, value_main) values(?,?,?) `

            
            child.db.run(sql,data,function(err){
                if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }
                hideLoading()
                Swal.fire({
                    icon: 'info',
                    title: 'Recovery domain updated!',
                    text: 'You have successfully changed recovery domain.'
                })
            })
        }
    },

    updateMaxEmailNameNum: function(){
        var newMaxEmailNum = document.getElementById('maxMaxEmailNameNum').value
        showLoading()
        if(newMaxEmailNum > 4 && newMaxEmailNum < 12){
            var now = new Date();

            let data = [now.toLocaleDateString("de-DE"), 'NAME_MAX_NUM',newMaxEmailNum]
            let sql = `INSERT OR REPLACE INTO SETTINGS (updated, category, value_main) values(?,?,?) `

            child.db.run(sql,data,function(err){
                if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }
                hideLoading()
                Swal.fire({
                    icon: 'info',
                    title: 'Max number in email name updated!',
                    text: 'You have successfully changed max numbers in email generated in application.'
                })
            })
        }
        else{
            hideLoading()
            Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'Number must be greater than 4 and less than 12!'
            })
        }
    },

    getActualMainSettings: function(){
        child.db.all(`SELECT updated,max_windows FROM SETTINGS WHERE category = 'WINDOWS' LIMIT 1`, function(err, rows) { 
            if(rows != undefined && rows != null && rows.length > 0){
                //var lastMod = new Date(rows[0].updated)
                if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }

                //var lastModFormat =(lastMod.getMonth()+1)  + "." +  lastMod.getDate() + "." + lastMod.getFullYear() 

                document.getElementById('max-windows-val').value = rows[0].max_windows
                document.getElementById('lastModMaxWindows').innerHTML = rows[0].updated
            }
        });
        
        child.db.all(`SELECT updated,value_main FROM SETTINGS WHERE category = 'RECOVERY' LIMIT 1`, function(err, rows) { 
            if(rows != undefined && rows != null && rows.length > 0){
                //var lastMod = new Date(rows[0].updated)
                if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }

                //var lastModFormat =(lastMod.getMonth()+1)  + "." +  lastMod.getDate() + "." + lastMod.getFullYear() 

                document.getElementById('maxRecoveryDomain').value = rows[0].value_main
                document.getElementById('lastModRecoveryDomain').innerHTML = rows[0].updated
            }
        });

        child.db.all(`SELECT updated,value_main FROM SETTINGS WHERE category = 'NAME_MAX_NUM' LIMIT 1`, function(err, rows) { 
            if(rows != undefined && rows != null && rows.length > 0){

                //var lastMod = new Date(rows[0].updated)
                if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }

                //var lastModFormat =(lastMod.getMonth()+1)  + "." +  lastMod.getDate() + "." + lastMod.getFullYear()

                document.getElementById('maxMaxEmailNameNum').value = rows[0].value_main
                document.getElementById('lastModMaxEmailNameNum').innerHTML = rows[0].updated
            }
        });
    },
}
// window.onload = function() {
//     getCountriesOther()
//     getCountryValue()
//     getActualFarmingVal()
//     getActualCountryVal()
// };


document.addEventListener("newPageLoad", function(){
    $("#main").focus();
  });
  