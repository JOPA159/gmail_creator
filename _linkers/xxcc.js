//OLD VERSION IN HTML
// app = require("electron").remote.app
// app.relaunch()
// app.exit()

const { app } = require("electron").remote;
var dbPath = app.getPath('userData')+'\\DBemails.db'
var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(dbPath);

module.exports = {
    pocetZapnuti: function() {
        db.all(`SELECT updated,value_main FROM SETTINGS WHERE category = 'EXEC_RES_NUM' LIMIT 1`, function(err, rows) { 
            if(rows != undefined && rows != null && rows.length > 0){

                if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }

                var now = new Date()
                newExecVal = +rows[0].value_main == 0 ? '1' : +rows[0].value_main + 1 
                if(+newExecVal > 9){
                    newExecVal = 0
                }

                let data = [now.toLocaleDateString("de-DE"), 'EXEC_RES_NUM',newExecVal]
                let sql = `INSERT OR REPLACE INTO SETTINGS (updated, category, value_main) values(?,?,?) `

                if(+newExecVal == 9){
                    app.quit()
                }

                child.db.run(sql,data,function(err){
                    app.relaunch()
                    app.exit()
                })
            }
            else{
                var now = new Date();
                newExecVal = 1
                let data = [now.toLocaleDateString("de-DE"), 'EXEC_RES_NUM',newExecVal]
                let sql = `INSERT OR REPLACE INTO SETTINGS (updated, category, value_main) values(?,?,?) `

                child.db.run(sql,data,function(err){
                    app = require("electron").remote.app
                    app.relaunch()
                    app.exit()
                })
            }
        });
    },
    AppRestart: function(){
        app = require("electron").remote.app
        app.relaunch()
        app.exit()
    }
}