//var child = require('')
const { app,shell } = require("electron").remote;
const url = require("url");
var sqlite3 = require("sqlite3").verbose();
//var dbPath = process.cwd()+LOCATION_UNPACK+'\\_DB\\DBemails.db';
var dbPath = app.getPath('userData')+'\\DBemails.db'
var db = new sqlite3.Database(dbPath);
module.exports = {
    loadScript: function(src) {
        return new Promise(function (resolve, reject) {
            var s;
            s = document.createElement('script');
            s.src = src;
            s.onload = resolve;
            s.onerror = reject;
            document.head.appendChild(s);
        });
    },

    loadNewPage: function(event,param,pageName, datatableJavascriptFile, javascriptFile) {
        $(".sidebar-item a i").removeClass("selected-menu-item-icon")
        $(".sidebar-item a").removeClass("selected-menu-item")
        param.className += ' selected-menu-item'
        param.firstChild.className += ' selected-menu-item-icon'
        
        db.all(`SELECT max_windows FROM SETTINGS WHERE category = 'WINDOWS' LIMIT 1`, function(err, rows) { 
            if(rows != undefined && rows != null && rows.length > 0){
                if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }
                var maxWindowsExecutedVal = rows[0].max_windows
            }
            else{
                var maxWindowsExecutedVal = 5
            }
            $(".page-wrapper").data("maxwindowval",maxWindowsExecutedVal+1) 
        });

        $.ajax({
            url: pageName,
            dataType: "html",
            success: function (data) {
                // if(datatableJavascriptFile != ""){
                //     loadScript("../../../_linkers/datatable/"+datatableJavascriptFile)
                // }

                // if(javascriptFile != ""){
                //     loadScript("../../../_linkers/"+javascriptFile)
                // }
                $(".page-wrapper").fadeOut(function () {
                    $(this).html(data).slideDown();
                    var event = new CustomEvent("newPageLoad", { "detail": "Event for triggering new page loading" });
                    document.dispatchEvent(event);
                });
                
            },
        });
    },
}

// $('.selected-menu-item').on('mousedown', function(e) {
//     console.log("AAAA")
//     if (e.button === 2) {
//       console.log('Disabled');
//       return false;
//     }
// });

// function handleNonLeftClick (e) {
//     console.log("AAAAAA")
//     // e.button will be 1 for the middle mouse button.
//     if (e.button === 1) {
//         // Check if it is a link (a) element; if so, prevent the execution.
//         if (e.target.tagName.toLowerCase() === "a") {
//             e.preventDefault();
//         }
//     }
// }

// window.onload = () => { document.addEventListener("auxclick", handleNonLeftClick); }