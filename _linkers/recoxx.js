var { exit } = require("process");

function sendForm(event) {
    event.preventDefault()

    let recoveryEmail = document.getElementById("recoveryEmail").value;
    console.log(recoveryEmail)

    const {PythonShell} = require("python-shell")
    const path = require("path")

    var opt = {
      scriptPath : path.join(__dirname, '../../../_engine/'),
      args : [recoveryEmail]
    }
    
    var pyshell = new PythonShell('vyt_rec_domem.py', opt);

    pyshell.on('message', results => {
          console.log(results);
          swal(results)
          
    });

    refreshRecoveryEmailTable()
    pyshell.end(err => {
        if (err) {
            hideLoading()    
            Swal.fire({
                icon: 'error',
                title: 'Oops!',
                text: 'Error reason:' + err.message
            })
            throw err
        }  
    });
}

function deleteData(){ 
      var data = [...document.querySelectorAll('.subcheckbox:checked')].map(e => e.value);
      showLoading();
      data = data.join();
      var sql  = "DELETE FROM RECOVERY_DOMAIN WHERE id IN("+data+")"
      
      child.db.run(sql, function(err) {
        if (err) {
            Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'Error reason: ' + err.message
            })
        }
          else{
              console.log("Successfully deleted.");
              refreshRecoveryEmailTable()
              child.db.close();
          }
      });
  
      document.getElementById('mainCheck').checked = false;
  }