function updateRecoveryEmailTable () {
    var result = []
    showLoading()
    child.db.all("SELECT * FROM RECOVERY_DOMAIN", function(err, rows) {  
        if (err) {
            Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'Error reason: ' + err.message
            })
        }

        if(rows === undefined || rows.length == 0){
            hideLoading()
            return false
        }

        rows.forEach(function (row) {  
            draft = Object.assign({"check":row.id},row)
            result.push(Object.values(draft))
        }) 
        recoveryEmailTable = $('#recoveryEmailTable').DataTable( {
            data: result,
            scrollX: true,
            retrieve: true,
            lengthMenu: [ 5, 10, 15, 20],
            columnDefs: [
                {
                    'targets': 0,
                    'searchable': false,
                    'orderable': false,
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox" style="cursor: pointer;" class="subcheckbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';}
                },
                {
                targets: 2, 
                type : "date",
                render: function (data) {
                    var date = new Date(data);
                    return date.toLocaleDateString('de-DE'); 
                }
            }],
            columns: [
                { title: `<input type="checkbox" style="cursor: pointer;" name="mainCheck" id="mainCheck" onclick="checkedAllCheckboxes()">`},
                { title: "Id" },
                { title: "Created"},
                { title: "Recovery email" },
            ]
        } );
        hideLoading()
    });
    
}

function refreshRecoveryEmailTable () {
    var newArray = []
    showLoading()
    $('#recoveryEmailTable').dataTable().fnClearTable();
    
    child.db.all("SELECT * FROM RECOVERY_DOMAIN", function(err, rows) {  
        if (err) {
            Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'Error reason: ' + err.message
            })
        }

        if(rows === undefined || rows.length == 0){
            hideLoading()
            return false
        }

        rows.forEach(function (row) {  
            draft = Object.assign({"check":row.id},row)
            newArray.push(Object.values(draft))
        }) 
        
        if(newArray.length == 0){

        }else{
            $('#recoveryEmailTable').dataTable().fnAddData(newArray);
            $('#recoveryEmailTable').dataTable().fnDraw();
        }
        hideLoading();
    });
    
}


document.addEventListener("newPageLoad", function(){
    updateRecoveryEmailTable()

    var editor;
    editor = new $.fn.dataTable.Editor( {
        //ajax: "../php/staff.php",
        table: "#recoveryEmailTable",
        fields: [ {
                label: "First name:",
                name: "first_name"
            }, {
                label: "Last name:",
                name: "last_name"
            }, {
                label: "Position:",
                name: "position"
            }, {
                label: "Office:",
                name: "office"
            }, {
                label: "Extension:",
                name: "extn"
            }, {
                label: "Start date:",
                name: "start_date",
                type: "datetime"
            }, {
                label: "Salary:",
                name: "salary"
            }
        ]
    } );
    $('#recoveryEmailTable').on( 'click', 'tbody td:not(:first-child)', function (e) {
        editor.inline( this );
    } );

});