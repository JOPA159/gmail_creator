var status = 'registered'

filter_farm_vals_dictionary={ 
    "UNFARMED":["shopping","news","youtube","browsing","processing farming","initializing process","waiting task","unfarmed"], 
    "FARMED":["farmed"], 
    "FAILED":["failed"],
    "CANCELED":["canceled"]
}

var data_farm = module.exports = {
    inicializeFarmingTable: function() {
        var result = []
        showLoading()

            farmingEmailTable = $('#farmingTable').DataTable( {
                ajax: function ( data, callback, settings ) {
                    child.db.all(`
                        SELECT 
                            b.id,
                            b.created,
                            b.email,
                            a.last_farming_date,
                            case when c.id_email is not null then 'WAITING TASK' 
                                when a.status is not null then a.status else 'UNFARMED' end as status,
                            a.total_farming,
                            a.last_farming,
                            b.registered_at
                        FROM EMAILS b
                        LEFT JOIN FARMING a ON b.id = a.email_id
                        LEFT JOIN (
                            SELECT
                                DISTINCT id_email
                            FROM WAITING
                            WHERE 1=1
                                AND sessions_type = 'FARMING'
                        )c on b.id = c.id_email
                        WHERE 1=1
                            AND b.status = '${status}'
                        ORDER BY b.id DESC`, function(err, rows) {  
                    if (err) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops..',
                            text: 'Error reason: ' + err.message
                        })
                    }

                        var out = [];
                        var difference = null
                        databaza = rows
                        searchVal = data.search['value']

                        if(rows == null || rows[0] == undefined){
                            hideLoading()
                            Swal.fire({
                                icon: 'info',
                                title: 'No registered email',
                                text: 'Register some emails first.'
                            })
                            setTimeout( function () {
                                callback( {
                                    draw: data.draw,
                                    data: [],
                                    recordsTotal: 0,
                                    recordsFiltered: 0
                                } );
                            }, 50 );
                            return ""
                        }

                        var keys = Object.keys(rows[0]);

                        function validateDate(date){
                            var regex=new RegExp("([0-9]{4}[-](0[1-9]|1[0-2])[-]([0-2]{1}[0-9]{1}|3[0-1]{1})|([0-2]{1}[0-9]{1}|3[0-1]{1})[-](0[1-9]|1[0-2])[-][0-9]{4})");
                            var dateOk=regex.test(date);
                            if(dateOk){
                                return true
                            }else{
                                return false
                            }
                        }

                        if(databaza.length > 0){
                            data.order.forEach(riadok => {
                                kluc = riadok['column']-1
                                if(riadok['dir'] == 'asc'){
                                    if(validateDate(databaza[0][keys[kluc]])){
                                        databaza.sort(function compare(a,b){
                                            var dateA = new Date(a[keys[kluc]]);
                                            var dateB = new Date(b[keys[kluc]]);
                                            return dateA - dateB;
                                        }) 
                                    }
                                    else if(typeof(databaza[0][keys[kluc]]) === 'number'){
                                        databaza.sort(function(a, b){return a[keys[kluc]]-b[keys[kluc]]});
                                    }
                                    else{
                                        databaza.sort(function(a, b){
                                            var nameA=String(a[keys[kluc]]).toLowerCase(), nameB=String(b[keys[kluc]]).toLowerCase();
                                            if (nameA < nameB) //sort string ascending
                                             return -1;
                                            if (nameA > nameB)
                                             return 1;
                                            return 0; //default return value (no sorting)
                                        });
                                    }
                                }
                                else{
                                    if(validateDate(databaza[0][keys[kluc]])){
                                        databaza.sort(function compare(a,b){
                                            var dateA = new Date(a[keys[kluc]]);
                                            var dateB = new Date(b[keys[kluc]]);
                                            return dateB - dateA;
                                        }) 
                                    }
                                    else if(typeof(databaza[0][keys[kluc]]) === 'number'){
                                        databaza.sort(function(a, b){return b[keys[kluc]]-a[keys[kluc]]});
                                    }
                                    else{
                                        databaza.sort(function(a, b){
                                            var nameA=String(a[keys[kluc]]).toLowerCase(), nameB=String(b[keys[kluc]]).toLowerCase();
                                            if (nameA > nameB) //sort string ascending
                                             return -1;
                                            if (nameA < nameB)
                                             return 1;
                                            return 0; //default return value (no sorting)
                                        });
                                    }
                                }
                            })
                        }

                        var totalArr = [];
                        for (var i = 0; i < rows.length; i++) {
                            totalArr.push(i);
                        }

                        if(searchVal != ''){
                            var searchDatabaza = []
                            for (let i = 0; i < rows.length; i++) {
                                if(JSON.stringify(Object.values(rows[i])).indexOf(searchVal) > -1){
                                    searchDatabaza.push(i)
                                }                             
                            }
                            var difference = totalArr.filter(x => !searchDatabaza.includes(x));

                            difference.forEach(riadok => {
                                delete databaza[riadok]
                            });
                            databaza = databaza.filter(n=>n!==undefined)
                        }else{
                            databaza = rows
                        }

                        if(window.searchFilterStatusFarming != null || window.endDateFarmingVal != null){
                            if(window.searchFilterStatusFarming != null && window.endDateFarmingVal == null){
                                if(window.searchFilterStatusFarming != ""){

                                    if(window.searchFilterStatusFarming.toLowerCase() == 'farmed'){
                                        searchFarmFilterValDict = filter_farm_vals_dictionary.FARMED
                                    }
                                    else if(window.searchFilterStatusFarming.toLowerCase() == 'failed'){
                                        searchFarmFilterValDict = filter_farm_vals_dictionary.FAILED
                                    }
                                    else if(window.searchFilterStatusFarming.toLowerCase() == 'canceled'){
                                        searchFarmFilterValDict = filter_farm_vals_dictionary.CANCELED
                                    }
                                    else if(window.searchFilterStatusFarming.toLowerCase() == 'unfarmed'){
                                        searchFarmFilterValDict = filter_farm_vals_dictionary.UNFARMED
                                    }

                                    var databaza = databaza.filter(function (el) {
                                        return searchFarmFilterValDict.includes(el.status.toLowerCase())
                                    });
                                }
                            }
                            else if(window.searchFilterStatusFarming != null && window.endDateFarmingVal != null){
                                if(window.searchFilterStatusFarming != ""){

                                    if(window.searchFilterStatusFarming.toLowerCase() == 'farmed'){
                                        searchFarmFilterValDict = filter_farm_vals_dictionary.FARMED
                                    }
                                    else if(window.searchFilterStatusFarming.toLowerCase() == 'failed'){
                                        searchFarmFilterValDict = filter_farm_vals_dictionary.FAILED
                                    }
                                    else if(window.searchFilterStatusFarming.toLowerCase() == 'canceled'){
                                        searchFarmFilterValDict = filter_farm_vals_dictionary.CANCELED
                                    }
                                    else if(window.searchFilterStatusFarming.toLowerCase() == 'unfarmed'){
                                        searchFarmFilterValDict = filter_farm_vals_dictionary.UNFARMED
                                    }

                                    var databaza = databaza.filter(function (el) {
                                        return searchFarmFilterValDict.includes(el.status.toLowerCase()) &&
                                                el.created >= window.startDateEmailsVal &&
                                                el.created <= window.endDateEmailsVal+1;
                                    });
                                }
                            }
                            else if(window.searchFilterStatusFarming == null && window.endDateFarmingVal != null){
                                var databaza = databaza.filter(function (el) {
                                    return el.created >= window.startDateFarmingVal &&
                                            el.created <= window.endDateFarmingVal+1;
                                });
                            }
                        }
                        
                        velkost = (data.start+data.length) > databaza.length ? databaza.length : data.start+data.length                

                        for ( var i=data.start, ien=velkost; i<ien ; i++ ) {
                            draft = Object.assign({"check":databaza[i].id},databaza[i],{"ShowWindow":databaza[i].id},{"Executed":databaza[i].id})
                            out.push(Object.values(draft))
                        }                        
                        
                        setTimeout( function () {
                            callback( {
                                draw: data.draw,
                                data: out,
                                recordsTotal: databaza.length,
                                recordsFiltered: databaza.length
                            } );
                        }, 50 );
                    });
                },
                serverSide: true,
                scrollX: true,
                retrieve: true,
                pageLength: window.farmingDatatableRows > 0 ? window.farmingDatatableRows : 10,
                order: [],
                lengthMenu: [ 10, 20, 50, 100, 250],
                columnDefs: [
                    {
                        targets: 'no-sort',
                        orderable: false,
                  } ,
                    {
                        'targets': 0,
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta){
                            return '<input type="checkbox" style="cursor: pointer;" class="subcheckbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';}
                    },
                    {
                        'targets': 9,
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta){
                            return '<p id="getBal_'+$('<div/>').text(data).html()+'" style="cursor: pointer;" onclick="child.maximizeWindow('+"event,"+"'"+$('<div/>').text(data).html()+"',"+"'FARMING'"+')"><i class="m-r-10 mdi mdi-arrange-bring-forward" ></i></p>';}
                    },
                    {
                        'targets': 10,
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta){
                            return '<div id="email-process-'+$('<div/>').text(data).html()+'"><p class="exec-button" style="cursor: pointer;" data-status="'+full[5]+'" onclick="farm.farmFromPlay('+"event,"+"'"+$('<div/>').text(data).html()+"'"+')"><i id="exec-icon-'+$('<div/>').text(data).html()+'" class="m-r-10 mdi mdi-play-circle" ></i></p></div>';}
                    },
                    {
                    targets: 2, 
                    type : "date",
                    render: function (data) {
                                var date = new Date(data);
                                var datestring = date.getDate()  + "." + (date.getMonth()+1) + "." + date.getFullYear() + " " +
                                    date.getHours() + ":" + (date.getMinutes()<10?'0':'') + date.getMinutes();
                                return datestring; 
                        }
                    },
                    {
                        'targets': 4,
                        type : "date",
                        render: function (data){
                            if(data){
                                var date = new Date(data);
                                var datestring = date.getDate()  + "." + (date.getMonth()+1) + "." + date.getFullYear() + " " +
                                    date.getHours() + ":" + (date.getMinutes()<10?'0':'') + date.getMinutes();
                                return datestring; 
                            }
                            else{
                                return $('<div/>').text("").html()
                            }
                        }
                            
                    },
                    {
                        targets: 6, 
                        type : "date",
                        render: function (data) {
                            if(data){
                                return $('<div/>').text(data_farm.formatTime(data)).html()
                            }
                            else{
                                return $('<div/>').text("").html()
                            } 
                            }
                    },
                    {
                        'targets': 7,
                        'render': function (data, type, full, meta){
                            if(data){
                                return $('<div/>').text(data_farm.formatTime(data)).html()
                            }
                            else{
                                return $('<div/>').text("").html()
                            } 
                        }
                            
                    },
                    {
                        'targets': 8,
                        'render': function (data, type, full, meta){
                            if(data){
                                var date = new Date(data);
                            var datestring = date.getDate()  + "." + (date.getMonth()+1) + "." + date.getFullYear() + " " +
                                date.getHours() + ":" + (date.getMinutes()<10?'0':'') + date.getMinutes();
                            return datestring; 
                            }
                            else{
                                return $('<div/>').text("").html()
                            }
                        }
                            
                    }
                ],
                columns: [
                    { title: `<input type="checkbox" style="cursor: pointer;" name="mainCheck" id="mainCheck" onclick="farm_data.checkedAllCheckboxes()">`},
                    { title: "Id" },
                    { title: "Created at"},
                    { title: "Email" },
                    { title: "Last farming date" },
                    { title: "status" },
                    { title: "Total farming time" },
                    { title: "Last farming time" },
                    { title: "Registered at"},
                    { title: "Show window" },
                    { title: "Executed" }
            
                ]
            } );
            hideLoading();
    },

    refreshFarmingTable: function(){
        var newArray = []
        showLoading();
        $('#farmingTable').dataTable().fnClearTable();
        this.inicializeFarmingTable()
        hideLoading();
        // child.db.all(`SELECT 
        //             b.id,
        //             b.created,
        //             b.email,
        //             a.last_farming_date,
        //             case when a.status is not null then a.status else 'UNFARMED' end,
        //             a.total_farming,
        //             a.last_farming,
        //             b.registered_at
        //         FROM EMAILS b
        //         LEFT JOIN FARMING a ON b.id = a.email_id
        //         WHERE 1=1
        //             AND b.status = '${status}'
        //         ORDER BY id DESC`, function(err, rows) {  
        //     if (err) {
        //             Swal.fire({
        //                 icon: 'error',
        //                 title: 'Oops..',
        //                 text: 'Error reason: ' + err.message
        //             })
        //         }
            
        //     if (err) {
        //         Swal.fire({
        //             icon: 'error',
        //             title: 'Oops..',
        //             text: 'Error reason: ' + err.message
        //         })
        //     }
            
        //     if(rows == null || rows == undefined){
        //         hideLoading();
        //         return false
        //     }

        //     rows.forEach(function (row) { 
        //         draft = Object.assign({"check":row.id},row,{"ShowWindow":row.id},{"Executed":row.id})
        //         newArray.push(Object.values(draft))
        //     }) 
        //     if(newArray.length == 0){
                
        //     }else{
        //         $('#farmingTable').dataTable().fnAddData(newArray);
        //         $('#farmingTable').dataTable().fnDraw();
        //     }
        //     hideLoading();
        // });
        
    },

    checkedAllCheckboxes: function(){
        var $that = $('#mainCheck');
        $('.subcheckbox').each(function() {
            this.checked = $that.is(':checked');
        })
    },

    formatTime: function(time) {   
        // Hours, minutes and seconds
        var hrs = ~~(time / 3600);
        var mins = ~~((time % 3600) / 60);
        var secs = ~~time % 60;

        // Output like "1:01" or "4:03:59" or "123:03:59"
        var ret = "";
        if (hrs > 0) {
            ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
        }
        ret += "" + mins + ":" + (secs < 10 ? "0" : "");
        ret += "" + secs;
        return ret;
    },

    datatableRowsGlobalSave: function(elemNameVal){
        elementToChange = document.getElementsByName(elemNameVal)
        if(elementToChange.length>0 && elementToChange[0] != undefined && elementToChange[0] != null){
            elementToChange[0].addEventListener("change", function(){
                window.farmingDatatableRows = this.value
            });
        }
        
    },
}

document.addEventListener("newPageLoad", function(){
    window.startDateFarmingVal = null
    window.endDateFarmingVal = null
    child.getAllProxyGroups("#selectProxyGroupChange")

    if($('#farmingTable').length){
        (function farmingCheckPlay(){
            var procType = 'FARMING'
            var intervalValue = 2000
            var maxSecPlayFailCheck = 60
            var f = ''
            var f = function() {
                if($('#farmingTable').length){
                    //console.log("FARMING "+$('#farmingTable').length)
                    quarantine = []
                    $('[data-status^="FAILED"]').each(function(){
                        id = $(this).parent()[0].attributes['id'].value.replace(/email-process-/, '')
                        $(this)[0].outerHTML = `<p id="email-process-${id}" class="exec-button" style="color:#f62d51;"><i id="exec-icon-${id}" class="m-r-10 mdi mdi-close-box" data-status="FAILED" ></i></p>`
                        quarantine.push(id)
                    })

                    child.db.all(`SELECT id_email, max(status_id) as status_id FROM WINDOWS WHERE sessions_type = 'FARMING' group by id_email`, function(err, rows) {  
                        // if (err) {
                        //     Swal.fire({
                        //         icon: 'error',
                        //         title: 'Oops..',
                        //         text: 'Error reason: ' + err.message
                        //     })
                        // }
                        if(rows === undefined || rows.length == 0){
                            return false
                        }
                        
                        rows.forEach(function (row) { 

                            if(quarantine.includes(row['id_email'])){
                                return false
                            }

                            var parent = document.getElementById(`email-process-${row['id_email']}`); 
                            var allElements = document.getElementsByClassName('mdi-close-box'); 
                            if (parent != undefined && parent.contains(allElements[0])){ 
                                return false
                            }

                            if($(`#email-process-${row['id_email']}.play`).data('sec') == undefined){
                                var sec = ''
                            }
                            else{
                                var sec = $(`#email-process-${row['id_email']}.play`).data('sec')
                            }

                            if(row['status_id'] == 0 && $(`#email-process-${row['id_email']}.play`).length == 1){
                                $(`#email-process-${row['id_email']}.play`).data('sec',sec+=intervalValue/1000)
                                if($(`#email-process-${row['id_email']}.play`).data('sec')>=maxSecPlayFailCheck){
                                    document.getElementById(`email-process-${row['id_email']}`).innerHTML = `<p id="email-process-${row['id_email']}" class="exec-button" style="cursor: pointer;" onclick="farm.farmFromPlay(event,'${row['id_email']}')"><i id="exec-icon-${row['id_email']}" class="m-r-10 mdi mdi-play-circle" ></i></p>`
                                }
                            }

                            if(document.getElementById(`email-process-${row['id_email']}`) != null && row['status_id'] == 1){
                                if($(`#email-process-${row['id_email']}.pause`).length == 0){
                                    document.getElementById(`email-process-${row['id_email']}`).innerHTML = `<p id="email-process-${row['id_email']}" class="exec-button" style="cursor: pointer;" onclick="child.killWindow(event,'${row['id_email']}','FARMING')"><i id="exec-icon-${row['id_email']}" class="m-r-10 mdi mdi-pause-circle" ></i></p>`
                                }
                            }
                            else if(document.getElementById(`email-process-${row['id_email']}`) != null && row['status_id'] == 0){
                                if($(`#email-process-${row['id_email']}.play`).length == 0){
                                    document.getElementById(`email-process-${row['id_email']}`).innerHTML = `<p id="email-process-${row['id_email']}" class="exec-button" style="cursor: pointer;" onclick="farm.farmFromPlay(event,'${row['id_email']}')"><i id="exec-icon-${row['id_email']}" class="m-r-10 mdi mdi-play-circle" ></i></p>`
                                }    
                            }
                        })
                    });
                }
            };
            window.setInterval(f, intervalValue);
        
            f();    
        })();
    }
    $("#filterStatusFarming").change(function () {
        $('#farmingTable').dataTable().fnClearTable();
        window.searchFilterStatusFarming = $(this).val()
        data_farm.inicializeFarmingTable()
        // farmingEmailTable
        //         .columns(5)
        //         .search("(^"+$(this).val()+"$)",true,false)
        //         .draw();
    })

    var startdate;
    var enddate;
    $('#reportrangeFarming').daterangepicker({
       locale: {  "format": "DD.MM.YYYY",
            "separator": " - ",
            "applyLabel": "Apply",
            
            "cancelLabel": "Clear",
            "fromLabel": "From",
            "toLabel": "To",
            "firstDay": 1},

       "opens": "right",
       "startDate":startdate,
       "closeText": "Clear",
    },
    function (start, end, label) {
      /* var s = moment(start.toISOString());
       var e = moment(end.toISOString());
       startdate = s.format("DD-MM-YYYY");
       enddate = e.format("DD-MM-YYYY");*/
       window.startDateFarmingVal = start.format('YYYY-MM-DD')
       window.endDateFarmingVal = end.format('YYYY-MM-DD') 

       //console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
    });
    $('#reportrangeFarming').on('apply.daterangepicker', function (ev, picker) {
        // minDateFilter = moment(picker.startDate.format('MM/DD/YYYY'), 'MM/DD/YYYY');
        // maxDateFilter = moment(picker.endDate.format('MM/DD/YYYY'), 'MM/DD/YYYY');
        minDateFilter = new Date(picker.startDate).getTime();
        maxDateFilter =  new Date(picker.endDate).getTime();
        $('#farmingTable').dataTable().fnDraw();  
     });

     $('#reportrangeFarming').on('cancel.daterangepicker', function (ev, picker) {
        // minDateFilter = moment(picker.startDate.format('MM/DD/YYYY'), 'MM/DD/YYYY');
        // maxDateFilter = moment(picker.endDate.format('MM/DD/YYYY'), 'MM/DD/YYYY');
        minDateFilter = '';
        maxDateFilter =  '';
        $('#farmingTable').dataTable().fnDraw();  
     });
    

    $("#datepicker_from").datepicker({
        showOn: "button",
        buttonImage: process.cwd()+LOCATION+"\\cache-data\\assets\\images\\calendar.gif",
        buttonImageOnly: false,
        "onSelect": function(date) {
        minDateFilter = new Date(date).getTime();
        $('#farmingTable').dataTable().fnDraw();
        }
    }).keyup(function() {
        minDateFilter = new Date(this.value).getTime();
        $('#farmingTable').dataTable().fnDraw();
    });
    
    $("#datepicker_to").datepicker({
        showOn: "button",
        buttonImage: process.cwd()+LOCATION+"\\cache-data\\assets\\images\\calendar.gif",
        buttonImageOnly: false,
        "onSelect": function(date) {
        maxDateFilter = new Date(date).getTime();
        $('#farmingTable').dataTable().fnDraw()
        }
    }).keyup(function() {
        maxDateFilter = new Date(this.value).getTime();
        $('#farmingTable').dataTable().fnDraw()
    });
    $("#datepicker_from").click(function() {
        $(this).datepicker("show");
      });
      $("#datepicker_to").click(function() {
        $(this).datepicker("show");
      }); 
    
    // Date range filter
    minDateFilter = "";
    maxDateFilter = "";
    if($('#farmingTable').length){
    $.fn.dataTableExt.afnFiltering.push(
    function(oSettings, aData, iDataIndex) {
        if(oSettings.sInstance == 'proxyEmailTable'){
            return true;
        }
        if (minDateFilter == ''  ||  maxDateFilter == '' || typeof minDateFilter === 'undefined' || typeof maxDateFilter === 'undefined') {
            return true;
        }
        if(isNaN(aData[8])){
            
            if(!(typeof aData[8] === 'undefined')){
                text = aData[8].replace(/[.]/g, '-');
                aData._date = new Date( text.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3") ).getTime();
            }

            if (minDateFilter && !isNaN(minDateFilter)) {
                if (aData._date < minDateFilter) {
                    return false;
                }
            }
        
            if (maxDateFilter && !isNaN(maxDateFilter)) {
            if (aData._date > maxDateFilter) {
                return false;
            }
            }
        
            return true;
        }    
        return false;
        } 
    )
    }
});