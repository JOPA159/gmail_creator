var data_proxy = module.exports = {
    inicializeProxyTable: function() {
        showLoading()       

            proxyEmailTable = $('#proxyEmailTable').DataTable( {
                ajax: function ( data, callback, settings ) {
                    child.db.all("SELECT * FROM PROXIES ORDER BY id DESC", function(err, rows) {  
                        if (err) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops..',
                                text: 'Error reason: ' + err.message
                            })
                        }

                        var out = [];
                        var difference = null
                        databaza = rows
                        searchVal = data.search['value']
                        
                        if(rows == null || rows[0] == undefined){
                            hideLoading()
                            Swal.fire({
                                icon: 'info',
                                title: 'No proxies',
                                text: 'Add some new proxies.'
                            })
                            setTimeout( function () {
                                callback( {
                                    draw: data.draw,
                                    data: [],
                                    recordsTotal: 0,
                                    recordsFiltered: 0
                                } );
                            }, 50 );
                            return ""
                        }

                        var keys = Object.keys(rows[0]);

                        function validateDate(date){
                            var regex=new RegExp("([0-9]{4}[-](0[1-9]|1[0-2])[-]([0-2]{1}[0-9]{1}|3[0-1]{1})|([0-2]{1}[0-9]{1}|3[0-1]{1})[-](0[1-9]|1[0-2])[-][0-9]{4})");
                            var dateOk=regex.test(date);
                            if(dateOk){
                                return true
                            }else{
                                return false
                            }
                        }

                        if(databaza.length > 0){
                            data.order.forEach(riadok => {
                                kluc = riadok['column']-1
                                if(riadok['dir'] == 'asc'){
                                    if(validateDate(databaza[0][keys[kluc]])){
                                        databaza.sort(function compare(a,b){
                                            var dateA = new Date(a[keys[kluc]]);
                                            var dateB = new Date(b[keys[kluc]]);
                                            return dateA - dateB;
                                        }) 
                                    }
                                    else if(typeof(databaza[0][keys[kluc]]) === 'number'){
                                        databaza.sort(function(a, b){return a[keys[kluc]]-b[keys[kluc]]});
                                    }
                                    else{
                                        databaza.sort(function(a, b){
                                            var nameA=String(a[keys[kluc]]).toLowerCase(), nameB=String(b[keys[kluc]]).toLowerCase();
                                            if (nameA < nameB) //sort string ascending
                                             return -1;
                                            if (nameA > nameB)
                                             return 1;
                                            return 0; //default return value (no sorting)
                                        });
                                    }
                                }
                                else{
                                    if(validateDate(databaza[0][keys[kluc]])){
                                        databaza.sort(function compare(a,b){
                                            var dateA = new Date(a[keys[kluc]]);
                                            var dateB = new Date(b[keys[kluc]]);
                                            return dateB - dateA;
                                        }) 
                                    }
                                    else if(typeof(databaza[0][keys[kluc]]) === 'number'){
                                        databaza.sort(function(a, b){return b[keys[kluc]]-a[keys[kluc]]});
                                    }
                                    else{
                                        databaza.sort(function(a, b){
                                            var nameA=String(a[keys[kluc]]).toLowerCase(), nameB=String(b[keys[kluc]]).toLowerCase();
                                            if (nameA > nameB) //sort string ascending
                                             return -1;
                                            if (nameA < nameB)
                                             return 1;
                                            return 0; //default return value (no sorting)
                                        });
                                    }
                                }
                            })
                        }

                        var totalArr = [];
                        for (var i = 0; i < rows.length; i++) {
                            totalArr.push(i);
                        }

                        if(searchVal != ''){
                            var searchDatabaza = []
                            for (let i = 0; i < rows.length; i++) {
                                if(JSON.stringify(Object.values(rows[i])).indexOf(searchVal) > -1){
                                    searchDatabaza.push(i)
                                }                             
                            }
                            var difference = totalArr.filter(x => !searchDatabaza.includes(x));

                            difference.forEach(riadok => {
                                delete databaza[riadok]
                            });
                            databaza = databaza.filter(n=>n!==undefined)
                        }else{
                            databaza = rows
                        }

                        if(window.searchFilterProxyGroup != null && window.searchFilterProxyGroup != ''){
                            console.log(window.searchFilterProxyGroup)
                            var databaza = databaza.filter(function (el) {
                                return el.proxy_group.toLowerCase() == window.searchFilterProxyGroup.toLowerCase()
                            });
                        }

                        
                        velkost = (data.start+data.length) > databaza.length ? databaza.length : data.start+data.length                
                        
                        for ( var i=data.start, ien=velkost; i<ien ; i++ ) {
                            draft = Object.assign({"check":databaza[i].id},databaza[i])
                            out.push(Object.values(draft))
                        }
                        
                        setTimeout( function () {
                            callback( {
                                draw: data.draw,
                                data: out,
                                recordsTotal: databaza.length,
                                recordsFiltered: databaza.length
                            } );
                        }, 50 );
                    });
                },
                serverSide: true,
                scrollX: true,
                retrieve: true,
                pageLength: window.proxyDatatableRows > 0 ? window.proxyDatatableRows : 10,
                order: [],
                lengthMenu: [ 10, 20, 50, 100, 500],
                columnDefs: [
                    {
                        targets: 'no-sort',
                        orderable: false,
                  } ,
                    {
                        targets: 0,
                        searchable: false,
                        orderable: false,
                        render: function (data, type, full, meta){
                            return '<input type="checkbox" style="cursor: pointer;" class="subcheckbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';}
                    },
                    {
                    targets: 2, 
                    type : "date",
                    render: function (data) {
                        var date = new Date(data);
                        return date.toLocaleDateString('de-DE'); 
                    }
                }],
                columns: [
                    { title: `<input style="cursor: pointer;" type="checkbox" name="mainCheck" id="mainCheck" onclick="proxy_data.checkedAllCheckboxes()">`},
                    { title: "Id" },
                    { title: "Created"},
                    { title: "IP address" },
                    { title: "Port" },
                    { title: "Username" },
                    { title: "Password" },
                    { title: "Id Email" },
                    { title: "Dating of email" },
                    { title: "Proxy group" },
                ]
            } );
            hideLoading()
        
        
    },

    refreshProxyTable: function(){
        var newArray = []
        showLoading();
        $('#proxyEmailTable').dataTable().fnClearTable();
        this.inicializeProxyTable()
        // child.db.all("SELECT * FROM PROXIES ORDER BY id DESC", function(err, rows) {  
        //     if (err) {
        //             Swal.fire({
        //                 icon: 'error',
        //                 title: 'Oops..',
        //                 text: 'Error reason: ' + err.message
        //             })
        //         }
                
        //     rows.forEach(function (row) { 
        //         draft = Object.assign({"check":row.id},row)
        //         newArray.push(Object.values(draft))
        //     }) 
        //     if(newArray.length == 0){
        //         hideLoading();
        //     }else{
        //         $('#proxyEmailTable').dataTable().fnAddData(newArray);
        //         $('#proxyEmailTable').dataTable().fnDraw();
        //         hideLoading();
        //     }
            
        // });
        
    },

    checkedAllCheckboxes: function(){
        var $that = $('#mainCheck');
        $('.subcheckbox').each(function() {
            this.checked = $that.is(':checked');
        })
    },

    datatableRowsGlobalSave: function(elemNameVal){
        elementToChange = document.getElementsByName(elemNameVal)
        if(elementToChange.length>0 && elementToChange[0] != undefined && elementToChange[0] != null){
            elementToChange[0].addEventListener("change", function(){
                window.proxyDatatableRows = this.value
            });
        }
    }

}

document.addEventListener("newPageLoad", function(){
    data_proxy.inicializeProxyTable()
    data_proxy.datatableRowsGlobalSave("proxyEmailTable_length")
    child.getAllProxyGroups("#filterProxyGroup")

    $("#filterProxyGroup").change(function () {
        $('#proxyEmailTable').dataTable().fnClearTable();
        window.searchFilterProxyGroup = $(this).val()
        data_proxy.inicializeProxyTable()
        // createEmailTable
        //     .columns(16)
        //     .search("(^"+$(this).val()+"$)",true,false)
        //     .draw();
    
    })
});

