var data_task = module.exports = {
    inicializeTaskTable: function() {
        var result = []
        showLoading()
            
            taskTable = $('#taskTable').DataTable( {
                ajax: function ( data, callback, settings ) {
                    child.db.all(`
                                SELECT	
                                    id,
                                    date_created,
                                    email,
                                    sessions_type,
                                    upper(status) as status,
                                    id_email
                                FROM (
                                    SELECT
                                        a.*,
                                        row_number() over (partition by email||sessions_type order by sessions_type) rn
                                    FROM (
                                        SELECT 
                                                a.id,
                                                a.date_created,
                                                b.email,
                                                a.sessions_type, 
                                                case when a.sessions_type = 'REGISTERING' then b.status
                                                    when a.sessions_type = 'FARMING' then c.status
                                                    when a.sessions_type = 'FORWARDING' then d.status
                                                    else '' end as status,
                                                a.id_email
                                            FROM WINDOWS a 
                                            LEFT JOIN EMAILS b on a.id_email = b.id
                                            LEFT JOIN FARMING c on a.id_email = c.email_id
                                            LEFT JOIN FORWARDING d on a.id_email = d.email_id
                                            WHERE a.status_id = '1' 
                                        union
                                            SELECT
                                                '',
                                                b.created,
                                                b.email, 
                                                'FARMING' as session,
                                                b.status,
                                                b.id
                                            FROM FARMING a
                                            LEFT JOIN EMAILS b on a.email_id = b.id
                                            WHERE 1=1
                                                and a.status in ('PROCESSING FARMING','INITIALIZING PROCESS')
                                        union
                                            SELECT
                                                '',
                                                b.created,
                                                b.email, 
                                                'FORWARDING' as session,
                                                b.status,
                                                b.id
                                            FROM FORWARDING a
                                            LEFT JOIN EMAILS b on a.email_id = b.id
                                            WHERE 1=1
                                                and a.status in ('PROCESSING FORWARDING','INITIALIZING PROCESS')
                                        union 
                                            SELECT
                                                '',
                                                created,
                                                email,
                                                'REGISTERING' as session,
                                                status,
                                                id
                                            FROM EMAILS
                                            WHERE 1=1
                                                and status in ('registering','initializing process')
                                        ) a 
                                    )
                                    WHERE rn = 1
                                    ORDER BY id
                                `, function(err, rows) {  
                                    if (err) {
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Oops..',
                                            text: 'Error reason: ' + err.message
                                        })
                                    }

                        var out = [];
                        var difference = null
                        databaza = rows
                        searchVal = data.search['value']
                        
                        if(rows == null || rows[0] == undefined){
                            hideLoading()
                            // Swal.fire({
                            //     icon: 'info',
                            //     title: 'No active tasks',
                            //     text: 'Try to start registration, forwarding or farming.'
                            // })
                            setTimeout( function () {
                                callback( {
                                    draw: data.draw,
                                    data: [],
                                    recordsTotal: 0,
                                    recordsFiltered: 0
                                } );
                            }, 50 );
                            return ""
                        }

                        var keys = Object.keys(rows[0]);

                        function validateDate(date){
                            var regex=new RegExp("([0-9]{4}[-](0[1-9]|1[0-2])[-]([0-2]{1}[0-9]{1}|3[0-1]{1})|([0-2]{1}[0-9]{1}|3[0-1]{1})[-](0[1-9]|1[0-2])[-][0-9]{4})");
                            var dateOk=regex.test(date);
                            if(dateOk){
                                return true
                            }else{
                                return false
                            }
                        }

                        if(databaza.length > 0){
                            data.order.forEach(riadok => {
                                kluc = riadok['column']-1
                                if(riadok['dir'] == 'asc'){
                                    if(validateDate(databaza[0][keys[kluc]])){
                                        databaza.sort(function compare(a,b){
                                            var dateA = new Date(a[keys[kluc]]);
                                            var dateB = new Date(b[keys[kluc]]);
                                            return dateA - dateB;
                                        }) 
                                    }
                                    else if(typeof(databaza[0][keys[kluc]]) === 'number'){
                                        databaza.sort(function(a, b){return a[keys[kluc]]-b[keys[kluc]]});
                                    }
                                    else{
                                        databaza.sort(function(a, b){
                                            var nameA=String(a[keys[kluc]]).toLowerCase(), nameB=String(b[keys[kluc]]).toLowerCase();
                                            if (nameA < nameB) //sort string ascending
                                             return -1;
                                            if (nameA > nameB)
                                             return 1;
                                            return 0; //default return value (no sorting)
                                        });
                                    }
                                }
                                else{
                                    if(validateDate(databaza[0][keys[kluc]])){
                                        databaza.sort(function compare(a,b){
                                            var dateA = new Date(a[keys[kluc]]);
                                            var dateB = new Date(b[keys[kluc]]);
                                            return dateB - dateA;
                                        }) 
                                    }
                                    else if(typeof(databaza[0][keys[kluc]]) === 'number'){
                                        databaza.sort(function(a, b){return b[keys[kluc]]-a[keys[kluc]]});
                                    }
                                    else{
                                        databaza.sort(function(a, b){
                                            var nameA=String(a[keys[kluc]]).toLowerCase(), nameB=String(b[keys[kluc]]).toLowerCase();
                                            if (nameA > nameB) //sort string ascending
                                             return -1;
                                            if (nameA < nameB)
                                             return 1;
                                            return 0; //default return value (no sorting)
                                        });
                                    }
                                }
                            })
                        }

                        var totalArr = [];
                        for (var i = 0; i < rows.length; i++) {
                            totalArr.push(i);
                        }

                        if(searchVal != ''){
                            var searchDatabaza = []
                            for (let i = 0; i < rows.length; i++) {
                                if(JSON.stringify(Object.values(rows[i])).indexOf(searchVal) > -1){
                                    searchDatabaza.push(i)
                                }                             
                            }
                            var difference = totalArr.filter(x => !searchDatabaza.includes(x));

                            difference.forEach(riadok => {
                                delete databaza[riadok]
                            });
                            databaza = databaza.filter(n=>n!==undefined)
                        }else{
                            databaza = rows
                        }
                        
                        velkost = (data.start+data.length) > databaza.length ? databaza.length : data.start+data.length                
                        
                        for ( var i=data.start, ien=velkost; i<ien ; i++ ) {
                            draft = Object.assign({"check":databaza[i].id},databaza[i])
                            out.push(Object.values(draft))
                        }
                        
                        setTimeout( function () {
                            callback( {
                                draw: data.draw,
                                data: out,
                                recordsTotal: databaza.length,
                                recordsFiltered: databaza.length
                            } );
                        }, 50 );
                    });
                },
                serverSide: true,
                scrollX: true,
                retrieve: true,
                order: [],
                lengthMenu: [ 10, 20, 50, 100, 500],
                columnDefs: [
                    {
                        targets: 'no-sort',
                        orderable: false,
                  } ,
                    {
                        'targets': 0,
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta){
                            sessionVal = String(full[4])
                            emailVal = String(full[6])
                            return '<input type="checkbox" style="cursor: pointer;" class="subcheckbox" name="id[]" data-session="'+sessionVal+'" data-email="'+emailVal+'" value="' + $('<div/>').text(data).html() + '">';}
                    },
                    {
                    targets: 2, 
                    type : "date",
                    render: function (data) {
                        var date = new Date(data);
                        var datestring = date.getDate()  + "." + (date.getMonth()+1) + "." + date.getFullYear() + " " +
                            date.getHours() + ":" + (date.getMinutes()<10?'0':'') + date.getMinutes();
                        return datestring; 
                    },
                    
                },{
                    'targets': 6,
                    'searchable': false,
                    'orderable': false,
                    'render': function (data, type, full, meta){
                        sessionTypeRender = String("'"+full[4]+"'")
                        return '<p id="getBal_'+$('<div/>').text(data).html()+'" style="cursor: pointer;" onclick="child.maximizeWindow('+"event,"+"'"+$('<div/>').text(data).html()+"',"+sessionTypeRender+')"><i class="m-r-10 mdi mdi-arrange-bring-forward" ></i></p>';}
                }
            
                ],
                columns: [
                    { title: `<input style="cursor: pointer;" type="checkbox" name="mainCheck" id="mainCheck" onclick="task_data.checkedAllCheckboxes()">`},
                    { title: "Process Id" },
                    { title: "Created"},
                    { title: "Email" },
                    { title: "Task type" },
                    { title: "Actual status" },
                    { title: "Show window" },
                ]
            } );
            hideLoading()
        
    },

    refreshTaskTable: function(){
        var newArray = []
        showLoading();
        $('#taskTable').dataTable().fnClearTable();
        this.inicializeTaskTable()

        // child.db.all(`
        //         SELECT 
        //             a.id,
        //             a.date_created,
        //             b.email,
        //             a.sessions_type, 
        //             case when a.sessions_type = 'REGISTERING' then b.status
        //                 when a.sessions_type = 'FARMING' then c.status
        //                 when a.sessions_type = 'FORWARDING' then d.status
        //                 else '' end as status
        //         FROM WINDOWS a 
        //         LEFT JOIN EMAILS b on a.id_email = b.id
        //         LEFT JOIN FARMING c on a.id_email = c.email_id
        //         LEFT JOIN FORWARDING d on a.id_email = d.email_id
        //         WHERE a.status_id = '1' ORDER BY a.id DESC`, function(err, rows) {   
        //     if (err) {
        //             Swal.fire({
        //                 icon: 'error',
        //                 title: 'Oops..',
        //                 text: 'Error reason: ' + err.message
        //             })
        //         }
                
        //     rows.forEach(function (row) { 
        //         draft = Object.assign({"check":row.id},row)
        //         newArray.push(Object.values(draft))
        //     }) 
        //     if(newArray.length == 0){
        //         hideLoading();
        //     }else{
        //         $('#taskTable').dataTable().fnAddData(newArray);
        //         $('#taskTable').dataTable().fnDraw();
        //         hideLoading();
        //     }
            
        // });
        
    },

    checkedAllCheckboxes: function(){
        var $that = $('#mainCheck');
        $('.subcheckbox').each(function() {
            this.checked = $that.is(':checked');
        })
    },
}

document.addEventListener("newPageLoad", function(){
    data_task.inicializeTaskTable()
});
