var data_provider = module.exports = {
    inicializeProviderTable: function() {
        var result = []
        showLoading()
        child.db.exec('CREATE TABLE IF NOT EXISTS PROVIDERS (id INTEGER PRIMARY KEY AUTOINCREMENT, provider TEXT, key TEXT, email_secret TEXT, timeout TEXT, balance TEXT, created_at DATE, updated_at DATE)');
        
        child.db.all("SELECT * FROM PROVIDERS ORDER BY id DESC", function(err, rows) {  
            if (err) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops..',
                    text: 'Error reason: ' + err.message
                })
            }

            rows.forEach(function (row) { 
                draft = Object.assign({"check":row.id},row,{"balanceScore":row.provider},{"editProvider":row.id})
                result.push(Object.values(draft))
            }) 

            providerEmailTable = $('#smsProvidersTable').DataTable( {
                data: result,
                scrollX: true,
                retrieve: true,
                lengthMenu: [ 5, 10, 15, 20],
                columnDefs: [
                    {
                        'targets': 0,
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta){
                            return '<input type="checkbox" style="cursor: pointer;" class="subcheckbox1" id="smsProvCheckbox" onclick="proc_data.onlyOneCheck(this)" name="id[]" value="' + $('<div/>').text(data).html() + '">';}
                    },
                    {
                        'targets': 9,
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta){
                            return '<p id="getBal" style="cursor: pointer;" onclick="proc.getBalanceScore('+"event,"+"'"+$('<div/>').text(data).html()+"'"+')"><i class="m-r-10 mdi mdi-play-circle" ></i></p>';}
                    },
                    {
                        'targets': 10,
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta){
                            return '<p id="editTable" style="cursor: pointer;"><i class="m-r-10 mdi mdi-table-edit" ></i></p>';}
                    },
                    {
                    targets: 7, 
                    type : "date",
                    render: function (data) {
                            var date = new Date(data);
                            return date.toLocaleDateString('de-DE'); 
                        }
                    },
                    {
                    targets: 8, 
                    type : "date",
                    render: function (data) {
                            var date = new Date(data);
                            return date.toLocaleDateString('de-DE'); 
                        }
                    }
                ],
                columns: [
                    //{ title: `<input type="checkbox" style="cursor: pointer;" name="mainCheck" id="mainCheck" onclick="proc_data.checkedAllCheckboxes()">`},
                    null,
                    { title: "Id" },
                    { title: "Provider"},
                    { title: "API key" },
                    { title: "Email/Secret" },
                    { title: "Timeout" },
                    { title: "Balance" },
                    { title: "Created at" },
                    { title: "Updated at" },
                    { title: "Check balance"},
                    { title: "Edit Provider"}
                ]
            } );
            hideLoading()
        });
        
    },

    refreshProviderTable: function(){
        var newArray = []
        showLoading()
        $('#smsProvidersTable').dataTable().fnClearTable();
        
        child.db.all("SELECT * FROM PROVIDERS ORDER BY id DESC", function(err, rows) {  
            if (err) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops..',
                    text: 'Error reason: ' + err.message
                })
            }
            
        // showLoading();
            rows.forEach(function (row) { 
                draft = Object.assign({"check":row.id},row,{"balanceScore":row.provider},{"editProvider":row.id})
                newArray.push(Object.values(draft))
            }) 
            if(newArray.length == 0){
                
            }else{
                $('#smsProvidersTable').dataTable().fnAddData(newArray);
                $('#smsProvidersTable').dataTable().fnDraw();
            }
            hideLoading();
        });
        
    },

    checkedAllCheckboxes: function(){
        var $that = $('#mainCheck');
        $('.subcheckbox').each(function() {
            this.checked = $that.is(':checked');
        })
    },

    onlyOneCheck: function(param){
        $('.subcheckbox').each(function() {
            $(this).not(param).prop('checked', false);
        })
              
    }
}

document.addEventListener("newPageLoad", function(){
    data_provider.inicializeProviderTable ()

    $("#smsProvidersTable").on('mousedown.edit', ".mdi-table-edit", function(e) {
    
        $(this).removeClass().addClass("m-r-10 mdi mdi-send");
        var $row = $(this).closest("tr").off("mousedown");
        var $tds = $row.find("td").not(':first').not(':last');

        $.each($tds, function(i, el) {
          if(i == 2 || i == 3 || i == 4){
                if(i==2){
                    apiKeyValOld = $(this).text()
                }
                else if(i == 3){
                    secretValOld = $(this).text()
                }
                else if(i == 4)
                {
                    timeoutValOld = $(this).text()
                }
                if(i==4){
                    var txt = $(this).text();
                    $(this).html("").append("<input type='number' value=\"" + txt + "\" data-prev=\""+ txt +"\" max='60' min='1'>");
                }else{
                    var txt = $(this).text();
                    $(this).html("").append(`<input type="text" value="${txt}" data-oldval = "${txt}">`);
                }  

          }
        });

        var $firstTdSelect = $row.find("td:first");

        var selectOptions = $("#selectbasic").clone();
        $row.find("td:first").html(selectOptions.show());

      });


      $("#smsProvidersTable").on('mousedown.save', ".mdi-send", function(e) {

        $(this).removeClass().addClass("m-r-10 mdi mdi-table-edit");

        var $row = $(this).closest("tr");
        var $tds = $row.find("td").not(':first').not(':last');

        $.each($tds, function(i, el) {
          if(i==0){
            providerIdVal = $(this).text()
          }

          if(i == 2 || i == 3 || i == 4){
            var txt = $(this).find("input").val();
            if(i == 4 && (txt == "" || txt == undefined || txt == null)){
                txt = 1
            }

            if(txt != undefined){
                if(/'(''|[^'])*'/.test($(this).find("input").val()) ||
                    /\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE){0,1}|INSERT( +INTO){0,1}|MERGE|SELECT|UPDATE|UNION( +ALL){0,1})\b/.test($(this).find("input").val() || (i == 4 && (txt == "" || txt == undefined || txt == null)))

                ){
                    if(i==2){
                        txt = apiKeyValOld 
                    }
                    else if(i==3){
                        txt = secretValOld 
                    }
                    else if(i==4)
                    {
                        txt = timeoutValOld 
                    }
                    $(this).html(txt);
                    providerEmailTable.cell(this).data(txt);
                }
                else{
                    var dateVal = child.dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss");
                    if(i==2){
                        var sqlProx = `UPDATE PROVIDERS SET key = '${txt}' ,updated_at = '${dateVal}' WHERE id = ${providerIdVal}`
                    }
                    else if(i == 3){
                        var sqlProx = `UPDATE PROVIDERS SET email_secret = '${txt}', updated_at = '${dateVal}' WHERE id = ${providerIdVal}`
                    }
                    else if(i == 4)
                    {
                        var sqlProx = `UPDATE PROVIDERS SET timeout = ${txt}, updated_at = '${dateVal}' WHERE id = ${providerIdVal}`
                    } 

                    child.db.run(sqlProx, function (err) {
                        if (err) {
                            Swal.fire({
                            icon: 'error',
                            title: 'Something went wrong while updating providers..',
                            text: 'Error reason: ' + err.message
                            })

                            if(i==2){
                                txt = apiKeyValOld 
                            }
                            else if(i == 3){
                                txt = secretValOld 
                            }
                            else if(i == 4)
                            {
                                txt = timeoutValOld 
                            }
        
                            $(this).html(txt);
                            providerEmailTable.cell(this).data(txt);

                        } 
                    })
                }
            }  
          }

        var txt = $(this).find("input").val();

        $(this).html(txt);
        providerEmailTable.cell(this).data(txt);

        });
        var $firstTdSelect = $row.find("td:first");
        var selectValue = $firstTdSelect.find("select").val();
        $row.find("td:first").html('<input type="checkbox" style="cursor: pointer;" class="subcheckbox" name="id[]" value="' + providerIdVal + '">');
        
        providerEmailTable.cell($firstTdSelect).data(selectValue);
        
      });

    //   //for editing input purposes.
    //   $("#smsProvidersTable").on('mousedown', "input", function(e) {
    //     e.stopPropagation();
    //   });

    //   //for editing input purposes.
    //   $("#smsProvidersTable").on('mousedown', "select", function(e) {
    //     e.stopPropagation();
    //   });

});