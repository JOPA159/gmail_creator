//var debounce = require('debounce');
const { exists } = require('original-fs');
filter_create_vals_dictionary={ 
    "UNREGISTERED":["waiting task","initializing process","waiting_for_sms","registering","finishing_registration","filling_form","unregistered"], 
    "REGISTERED":["registered"], 
    "FAILED":["failed_initializing", "failed_register","failed_country_is_not_support","failed_email_is_already_use","failed_problem_with_number","country_is_not_support"],
    "CANCELED":["canceled"]
}

var proxy = []
var emailData = []
var sql = "SELECT * FROM PROXIES "
child.db.all(sql,function(err, rows) { 
    if(rows != undefined && rows != null && rows.length > 0){
        if(rows === undefined || rows.length == 0){
            return false
        }
        rows.forEach(function (row) { 
            proxy.push(Object.values(row))
        }) 
    } else {
        proxy.push([])
    }   
}) 

child.db.all(`SELECT 
                a.id,
                a.created,
                a.email||'@gmail.com' as email,
                a.password,
                a.firstName,
                a.lastName,
                a.day,
                a.month,
                a.year,
                a.gender,
                a.telephoneNumber,
                a.country,
                a.smsProviderId,
                a.gmailCode,
                a.proxyId,
                a.status,
                a.scoreV3,
                a.scoreV2,
                a.recoveryId,
                a.registered_at,
                '' as showwindow,
                '' as executed,
                b.hostname||':'||b.port||':'||b.username||':'||b.password as proxy,
                b.proxy_group
                --b.port,
                --b.username,
                --b.password as password1
                FROM EMAILS a 
                LEFT JOIN PROXIES b on a.proxyId = b.id`, function(err, rows) {  
    if(rows != undefined && rows != null && rows.length > 0){
        if(rows === undefined || rows.length == 0){
            return false
        }
        rows.forEach(function (row) { 
            emailData.push(Object.values(row))
        }) 
        window.emailData = emailData
    } else {
        emailData.push([])
    }   
})



var buttonCommonCsv = {
    exportOptions: {
        format: {
            body: function ( data, row, column, node ) {
                if(/<\/?[a-z][\s\S]*>/i.test(data) == false){
                    if(column === 2){
                        data = data+'@gmail.com'   
                        return data
                    }else {   
                        return data;
                    }
                
                }
            }
        }
    },
    customize: function(csv){
        // var header = [
        //     'IP Proxy',
        //     'PORT Proxy',
        //     'LOGIN Proxy',
        //     'PASSWORD Proxy',
        // ];
        finalExportData = []
        if(window.searchFilterStatusEmails == null){
            finalExportData = window.emailData
        }else{
            if(window.searchFilterStatusEmails.toLowerCase() == 'registered'){
                searchFilterValDict = filter_create_vals_dictionary.REGISTERED
            }
            else if(window.searchFilterStatusEmails.toLowerCase() == 'failed'){
                searchFilterValDict = filter_create_vals_dictionary.FAILED
            }
            else if(window.searchFilterStatusEmails.toLowerCase() == 'canceled'){
                searchFilterValDict = filter_create_vals_dictionary.CANCELED
            }
            else if(window.searchFilterStatusEmails.toLowerCase() == 'unregistered'){
                searchFilterValDict = filter_create_vals_dictionary.UNREGISTERED
            }

            window.emailData.forEach(element => {
                if(searchFilterValDict.includes(element[15])){
                    finalExportData.push(element)
                }
            });
        }
        // var header = [
        //         'Proxy',
        //     ];
        // data.header = data.header.concat(header)
        // data.body = []

        // data.body = finalExportData
        var split_csv = csv.split("\n");
        //split_csv[0] = split_csv[0] + ',"Proxy"'
        split_csv[0] = 'Id,"Created at","Email","Password","First name","Last name","Day","Month","Year","Gender","Phone","Country","SMS Provider","Gmail code","Proxy ID","Status","Recaptcha score V3","Recaptcha one click","Recovery email","Registered at","Show","window","Executed","Proxy","Proxy group"'

        $.each(finalExportData, function (index, csv_row) {
            // var csv_cell_array = csv_row.split('","');
            // // //Remove replace the two quotes which are left at the beginning and the end (first and last cell)
            // csv_cell_array[0] = csv_cell_array[0].replace(/"/g, '');
            // csv_cell_array[split_csv.slice(1).length -1] = csv_cell_array[split_csv.slice(1).length -1].replace(/"/g, '');

            // var exportColummn = false;
            // finalExportData.forEach(element => {
            //     if(parseInt(element[0]) == csv_cell_array[0] ){
            //         exportColummn = true
            //         csv_cell_array.push(element[element.length - 1]);
            //     }
                
            // });
            // //RANDOM EXAMPLE : Make some test, special cutomizing depending of the value of the cell (if cell 5 is equal to a certain value, give a value to row 6)
            // if (csv_cell_array[5].toLowerCase().trim() == "a certain value") {
            //         csv_cell_array[6] = "2";
            // }
            // else if (csv_cell_array[5].toLowerCase() == "another value") {
            //         csv_cell_array[6] = "5";
            // }
            // //Else, define an empty 6th cell
            // else {
            //         csv_cell_array[6] = "";
            // }

            //RANDOM EXAMPLE : Empty the 5th cell and set the 7th to true
            // csv_cell_array[5] = "";
            // csv_cell_array[7] = "true";

            // //Join the table on the quotes and comma; add back the quotes at the beginning and end
            // if(exportColummn){
            //     csv_cell_array_quotes = '"' + csv_cell_array.join('","') + '"';

            //     // //Insert the new row into the rows array at the previous index (index +1 because the header was sliced)
            //     split_csv[index + 1] = csv_cell_array_quotes;
            // }

            csv_cell_array_quotes = '"' + finalExportData[index].join('","') + '"';

                // //Insert the new row into the rows array at the previous index (index +1 because the header was sliced)
            split_csv[index + 1] = csv_cell_array_quotes;
    });

        csv = split_csv.join("\n");
        //console.log(csv)
        return csv

        // $(data.body).each(function (i) {
        //     var id = $(this)[1]
        //     proxy.forEach(function (idecko) {

        //         if(Number(idecko[6]) == data.body[i][0]){
        //             data.body[i].push(idecko[2])
        //             data.body[i].push(idecko[3])
        //             data.body[i].push(idecko[4])
        //             data.body[i].push(idecko[5])
        //         }
        //     }) 
        // }); 
    }
}


var buttonCommon = {
    exportOptions: {
        format: {
            body: function ( data, row, column, node ) {
                // if(/<\/?[a-z][\s\S]*>/i.test(data) == false){
                //     if(column === 2){
                //         data = data+'@gmail.com'   
                //         return data
                //     }else {   
                //         return data;
                //     }
                
                // }
            }
        }
    },
    customizeData: function(data){
        // var header = [
        //     'IP Proxy',
        //     'PORT Proxy',
        //     'LOGIN Proxy',
        //     'PASSWORD Proxy',
        // ];
        finalExportData = []
        if(window.searchFilterStatusEmails == null){
            finalExportData = window.emailData
        }else{
            if(window.searchFilterStatusEmails.toLowerCase() == 'registered'){
                searchFilterValDict = filter_create_vals_dictionary.REGISTERED
            }
            else if(window.searchFilterStatusEmails.toLowerCase() == 'failed'){
                searchFilterValDict = filter_create_vals_dictionary.FAILED
            }
            else if(window.searchFilterStatusEmails.toLowerCase() == 'canceled'){
                searchFilterValDict = filter_create_vals_dictionary.CANCELED
            }
            else if(window.searchFilterStatusEmails.toLowerCase() == 'unregistered'){
                searchFilterValDict = filter_create_vals_dictionary.UNREGISTERED
            }

            window.emailData.forEach(element => {
                if(searchFilterValDict.includes(element[15])){
                    finalExportData.push(element)
                }
            });
        }

        var header = [
                'Proxy',
                'Proxy_group',
            ];
        data.header = data.header.concat(header)
        data.body = []

        data.body = finalExportData

        // $(data.body).each(function (i) {
        //     var id = $(this)[1]
        //     proxy.forEach(function (idecko) {

        //         if(Number(idecko[6]) == data.body[i][0]){
        //             data.body[i].push(idecko[2])
        //             data.body[i].push(idecko[3])
        //             data.body[i].push(idecko[4])
        //             data.body[i].push(idecko[5])
        //         }
        //     }) 
        // }); 
    }
}

var data_email = module.exports = {
    openExampExcel: function() {
            const {shell} = require("electron").remote
            var fileNameExcelImp = 'Import_example.xlsx'
            child.shell.openItem(process.cwd()+LOCATION_UNPACK+"\\_images\\"+ fileNameExcelImp);
    },

    inicializeCreateEmailsTable: function() {
        showLoading()

            createEmailTable = $('#smsCreateEmailsTable').DataTable( {
                scrollY: '250px',
                dom: 'Blfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommonCsv, {
                        text: 'CSV Export<span id="countdown"></span>',
                        title: null,
                        extend: 'csvHtml5',
                        attr: {id: 'csvButtonExport'},
                        exportOptions: {
                            //columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25] 
                            columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22] 
                        }
                    } ),
                    $.extend( true, {}, buttonCommon, {
                        text: 'Excel Export<span id="countdown"></span>',
                        title: null,
                        extend: 'excelHtml5',
                        attr: {id: 'excelButtonExport'},
                        exportOptions: {
                            //columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25] 
                            columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22] 
                        }
                    } ),
                    {
                        text: 'Import Excel',
                        action: async function () {
                            const { value: file } = await Swal.fire({
                                title: 'Import new emails',
                                html: 'Only Excel files with formats .xlsx or .xls <button class="dt-button" onclick="email_data.openExampExcel()">Import EXAMPLE</button>',
                                input: 'file',
                                inputAttributes: {
                                //MIME FORMATS  
                                //'accept': ".csv,application/vnd.ms-excel,.xlt,application/vnd.ms-excel,.xla,application/vnd.ms-excel,.xlsx,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.xltx,application/vnd.openxmlformats-officedocument.spreadsheetml.template,.xlsm,application/vnd.ms-excel.sheet.macroEnabled.12,.xltm,application/vnd.ms-excel.template.macroEnabled.12,.xlam,application/vnd.ms-excel.addin.macroEnabled.12,.xlsb,application/vnd.ms-excel.sheet.binary.macroEnabled.12",
                                'accept': ".xlsx,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.xls,application/vnd.ms-excel",
                                'aria-label': 'Import excel with new emails'
                                }
                            })
                            if (file) {
                                const reader = new FileReader()
                                reader.onload = (e) => {
                                    Swal.fire({
                                        title: "Loading...",
                                        html: 'Progress: <span id="actual-import-progress-val">0</span> from <span id="total-import-progress-val">?</span><br><span id="message-proggress"> </span>',
                                        allowOutsideClick: false,
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        willOpen: () => {
                                            Swal.showLoading();
                                        }
                                    });
                                    var inserted_values_emails = []
                                    var inserted_values_proxies = []
                                    var registered_required_columns = [2,3,15,22]
                                    var unregistered_required_columns = [2,3,4,5,6,7,8,9,11,12,15,22]
                                    var contents = email_import.processExcel(e.target.result);
                                    var result = Object.keys(contents).map((key) => [Number(key), contents[key]]);
                                    output_draft = result[0][1]
                                    output = output_draft.filter(x => x.length != 0 )

                                    if(output.length == 1){
                                        hideLoading()
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Not enough rows required',
                                            text: 'Make sure your file has a header or at least one line with the data you want to insert.'
                                        })
                                    }
                                
                                    var jsonArr = []
                                    $("#total-import-progress-val").text(output.length-1)

                                    if(output.length-1 > 2000){
                                        hideLoading()
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Too much rows',
                                            text: 'You can import a maximum of 2000 rows at a time.'
                                        })
                                        return false
                                    }

                                    for (let row = 1; row < output.length; row++) {
                                        //$("#actual-import-progress-val").text(row)
                                        //NADPIS A HEADER
                                        if(row == 0 || output[row] == 0){
                                            continue
                                        }

                                        output_detail = output[row]
                                        
                                        proxyInput = output_detail[22].split(':')
                                        
                                        var missing_columns_output = ''
                                        
                                        let required_columns_check = registered_required_columns.filter(x => (output_detail[x]==undefined || output_detail[x]==null || output_detail[x]=='' || output_detail[x]== ' '));
                                        
                                        if(required_columns_check.length){
                                            for (let index = 0; index < required_columns_check.length; index++) {
                                                if(index+1 == required_columns_check.length){
                                                    missing_columns_output += output[0][required_columns_check[index]]
                                                }
                                                else{
                                                    missing_columns_output += output[0][required_columns_check[index]] + ","
                                                }
                                            }
                                            hideLoading()
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Not enough rows required',
                                                text: `Missing required value in row-${row+1} at column(s): ${missing_columns_output}.`
                                            })
                                            return false
                                        }
                                        if(output_detail[15] == 'registered'){

                                            actual_date = child.dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss,sss").slice(0, -1)

                                            created_at = output_detail[1] == undefined ? actual_date : output_detail[1]
                                            registered_at = output_detail[19] == undefined ? actual_date : output_detail[19]
                                            
                                            if(proxyInput[2] != undefined && proxyInput[3] != undefined){
                                                jsonArr.push({
                                                    created: created_at,
                                                    email: output_detail[2],
                                                    email_password: output_detail[3],
                                                    phone: output_detail[10],
                                                    country: output_detail[11],
                                                    smsProviderId: output_detail[12],
                                                    status: output_detail[15],
                                                    recoveryId: output_detail[18],
                                                    registered_at: registered_at,
                                                    ipaddress: proxyInput[0],
                                                    port: proxyInput[1],
                                                    username: proxyInput[2],
                                                    password: proxyInput[3],
                                                    proxyGroup: output_detail[23]

                                                });
                                            }
                                            else{
                                                jsonArr.push({
                                                    created: created_at,
                                                    email: output_detail[2],
                                                    email_password: output_detail[3],
                                                    phone: output_detail[10],
                                                    country: output_detail[11],
                                                    smsProviderId: output_detail[12],
                                                    status: output_detail[15],
                                                    recoveryId: output_detail[18],
                                                    registered_at: registered_at,
                                                    ipaddress: proxyInput[0],
                                                    port: proxyInput[1],
                                                    username: null,
                                                    password: null,
                                                    proxyGroup: output_detail[23]
                                                });
                                            }
                                            
                                        }
                                        else if(output_detail[15] == 'unregistered'){
                                            actual_date = child.dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss,sss").slice(0, -1)

                                            created_at = output_detail[1] == undefined ? actual_date : output_detail[1]
                                            registered_at = output_detail[19] == undefined ? actual_date : output_detail[19]

                                            if(proxyInput[2] != undefined && proxyInput[3] != undefined){
                                                jsonArr.push({
                                                    created: created_at,
                                                    email: output_detail[2],
                                                    email_password: output_detail[3],
                                                    firstName: output_detail[4],
                                                    lastName: output_detail[5],
                                                    day: output_detail[6],
                                                    month: output_detail[7],
                                                    year: output_detail[8],
                                                    gender: output_detail[9],
                                                    country: output_detail[11],
                                                    smsProviderId: output_detail[12],
                                                    status: output_detail[15],
                                                    recoveryId: output_detail[18],
                                                    ipaddress: proxyInput[0],
                                                    port: proxyInput[1],
                                                    username: proxyInput[2],
                                                    password: proxyInput[3],
                                                    proxyGroup: output_detail[23]

                                                });
                                            }
                                            else{
                                                jsonArr.push({
                                                    created: created_at,
                                                    email: output_detail[2],
                                                    email_password: output_detail[3],
                                                    firstName: output_detail[4],
                                                    lastName: output_detail[5],
                                                    day: output_detail[6],
                                                    month: output_detail[7],
                                                    year: output_detail[8],
                                                    gender: output_detail[9],
                                                    country: output_detail[11],
                                                    smsProviderId: output_detail[12],
                                                    status: output_detail[15],
                                                    recoveryId: output_detail[18],
                                                    ipaddress: proxyInput[0],
                                                    port: proxyInput[1],
                                                    username: null,
                                                    password: null,
                                                    proxyGroup: output_detail[23]
                                                });
                                            }
                                        }
                                        else{
                                            hideLoading()
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Not enough rows required',
                                                text: `Incorrect value in STATUS column at row:${row+1}. Only values are accepted: registered, unregistered`
                                            })
                                            return false
                                        }                                   
                                    }  
                                    //ZAPIS DO DB
                                    var opt = {
                                        scriptPath : process.cwd()+LOCATION_UNPACK+'\\_engine\\PXY\\',
                                        pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
                                        args : [jsonArr]
                                    }

                                    var pyshell = new child.PythonShell('ipmpxy.py', opt);
                                    
                                    pyshell.send(JSON.stringify(jsonArr), { mode: 'json' });
                                
                                    pyshell.on('message', results => {
                                        if(results.includes('PROGRESSNUM')){
                                            $("#actual-import-progress-val").text(results.split(':')[1])
                                        }
                                        
                                        if(results != 'IMPORT_DONE' && results.includes('PROGRESSNUM') == false){
                                            $("#message-proggress").text(results)

                                            setTimeout(function () {
                                                hideLoading()

                                                data_email.refreshCreateEmailsTable()
                                                return true
                                            }, 3000);
                                        }

                                        
                                        if(results == 'IMPORT_DONE'){
                                            setTimeout(function () {
                                                hideLoading()

                                                data_email.refreshCreateEmailsTable()
                                                return true
                                            }, 3000);
                                            
                                        }                                    
                                    });  
                                    pyshell.end(err => {
                                        if (err) {
                                            hideLoading()  
                                            
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops!',
                                                text: 'Error reason:' + err.message
                                            })

                                            setTimeout(function () {
                                                hideLoading()

                                                data_email.refreshCreateEmailsTable()
                                                return true
                                            }, 3000);
                                        }  
                                        return false
                                    });
                                    //END DB
                                }
                                reader.readAsBinaryString(file)
                            }
                        }
                    },
                    {
                        text: 'Import CSV',
                        action: async function () {
                            const { value: file } = await Swal.fire({
                                title: 'Import new emails',
                                html: 'Only csv files <button class="dt-button" onclick="email_data.openExampExcel()">Import EXAMPLE</button>',
                                input: 'file',
                                inputAttributes: {
                                //MIME FORMATS  
                                //'accept': ".csv,application/vnd.ms-excel,.xlt,application/vnd.ms-excel,.xla,application/vnd.ms-excel,.xlsx,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.xltx,application/vnd.openxmlformats-officedocument.spreadsheetml.template,.xlsm,application/vnd.ms-excel.sheet.macroEnabled.12,.xltm,application/vnd.ms-excel.template.macroEnabled.12,.xlam,application/vnd.ms-excel.addin.macroEnabled.12,.xlsb,application/vnd.ms-excel.sheet.binary.macroEnabled.12",
                                'accept': ".csv",
                                'aria-label': 'Import csv with new emails'
                                }
                            })
                            if (file) {
                                const reader = new FileReader()
                                reader.onload = (e) => {
                                    Swal.fire({
                                        title: "Loading...",
                                        html: 'Progress: <span id="actual-import-progress-val">0</span> from <span id="total-import-progress-val">?</span><br><span id="message-proggress"> </span>',
                                        allowOutsideClick: false,
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        willOpen: () => {
                                            Swal.showLoading();
                                        }
                                    });
                                    var inserted_values_emails = []
                                    var inserted_values_proxies = []
                                    var registered_required_columns = [2,3,15,22]
                                    var unregistered_required_columns = [2,3,4,5,6,7,8,9,11,12,15,22]
                                    var contents = email_import.processExcel(e.target.result);
                                    var result = Object.keys(contents).map((key) => [Number(key), contents[key]]);
                                    output_draft = result[0][1]
                                    output = output_draft.filter(x => x.length != 0 )

                                    if(output.length == 1){
                                        hideLoading()
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Not enough rows required',
                                            text: 'Make sure your file has a header or at least one line with the data you want to insert.'
                                        })
                                    }
                                
                                    var jsonArr = []
                                    $("#total-import-progress-val").text(output.length-1)

                                    if(output.length-1 > 2000){
                                        hideLoading()
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Too much rows',
                                            text: 'You can import a maximum of 2000 rows at a time.'
                                        })
                                        return false
                                    }
                                    var lengthArray = output
                                    // var usingSplit = output[1][0].split(',');
                                    // var usingSplit = []
                                    // for (let row1 = 1; row1 < lengthArray.length; row1++) {
                                    //     usingSplit[row1] = output[row1].replace(/['"]+/g, '')
                                    // }
                                    // console.log(usingSplit)
                                    // usingSplit[1] = usingSplit[1].replace(/['"]+/g, '')
                                    // output = usingSplit
                                    for (let row = 1; row < lengthArray.length; row++) {
                                        //$("#actual-import-progress-val").text(row)
                                        //NADPIS A HEADER
                                        if(row == 0 || output[row] == 0){
                                            continue
                                        }
                                        if(!Number.isInteger(output_detail[0])){
                                            output_detail = output_detail[0].split(',')   
                                            for (let row1 = 0; row1 < output_detail.length; row1++) {
                                                output_detail[row1] = output_detail[row1].replace(/['"]+/g, '')
                                            }
                                        }
                                        
                                        proxyInput = output_detail[22].split(':')
                                        
                                        var missing_columns_output = ''
                                        
                                        let required_columns_check = registered_required_columns.filter(x => (output_detail[x]==undefined || output_detail[x]==null || output_detail[x]=='' || output_detail[x]== ' '));
                                        
                                        if(required_columns_check.length){
                                            for (let index = 0; index < required_columns_check.length; index++) {
                                                if(index+1 == required_columns_check.length){
                                                    missing_columns_output += output[0][required_columns_check[index]]
                                                }
                                                else{
                                                    missing_columns_output += output[0][required_columns_check[index]] + ","
                                                }
                                            }
                                            hideLoading()
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Not enough rows required',
                                                text: `Missing required value in row-${row+1} at column(s): ${missing_columns_output}.`
                                            })
                                            return false
                                        }
                                        if(output_detail[15] == "registered"){

                                            actual_date = child.dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss,sss").slice(0, -1)

                                            created_at = output_detail[1] == undefined ? actual_date : output_detail[1]
                                            registered_at = output_detail[19] == undefined ? actual_date : output_detail[19]
                                            
                                            if(proxyInput[2] != undefined && proxyInput[3] != undefined){
                                                jsonArr.push({
                                                    created: created_at,
                                                    email: output_detail[2],
                                                    email_password: output_detail[3],
                                                    phone: output_detail[10],
                                                    country: output_detail[11],
                                                    smsProviderId: output_detail[12],
                                                    status: output_detail[15],
                                                    recoveryId: output_detail[18],
                                                    registered_at: registered_at,
                                                    ipaddress: proxyInput[0],
                                                    port: proxyInput[1],
                                                    username: proxyInput[2],
                                                    password: proxyInput[3],
                                                    proxyGroup: output_detail[23]    
                                                });
                                            }
                                            else{
                                                jsonArr.push({
                                                    created: created_at,
                                                    email: output_detail[2],
                                                    email_password: output_detail[3],
                                                    phone: output_detail[10],
                                                    country: output_detail[11],
                                                    smsProviderId: output_detail[12],
                                                    status: output_detail[15],
                                                    recoveryId: output_detail[18],
                                                    registered_at: registered_at,
                                                    ipaddress: proxyInput[0],
                                                    port: proxyInput[1],
                                                    username: null,
                                                    password: null,
                                                    proxyGroup: output_detail[23]
                                                });
                                            }
                                            
                                        }
                                        else if(output_detail[15] == "unregistered"){
                                            actual_date = child.dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss,sss").slice(0, -1)

                                            created_at = output_detail[1] == undefined ? actual_date : output_detail[1]
                                            registered_at = output_detail[19] == undefined ? actual_date : output_detail[19]

                                            if(proxyInput[2] != undefined && proxyInput[3] != undefined){
                                                jsonArr.push({
                                                    created: created_at,
                                                    email: output_detail[2],
                                                    email_password: output_detail[3],
                                                    firstName: output_detail[4],
                                                    lastName: output_detail[5],
                                                    day: output_detail[6],
                                                    month: output_detail[7],
                                                    year: output_detail[8],
                                                    gender: output_detail[9],
                                                    country: output_detail[11],
                                                    smsProviderId: output_detail[12],
                                                    status: output_detail[15],
                                                    recoveryId: output_detail[18],
                                                    ipaddress: proxyInput[0],
                                                    port: proxyInput[1],
                                                    username: proxyInput[2],
                                                    password: proxyInput[3],
                                                    proxyGroup: output_detail[23]

                                                });
                                            }
                                            else{
                                                jsonArr.push({
                                                    created: created_at,
                                                    email: output_detail[2],
                                                    email_password: output_detail[3],
                                                    firstName: output_detail[4],
                                                    lastName: output_detail[5],
                                                    day: output_detail[6],
                                                    month: output_detail[7],
                                                    year: output_detail[8],
                                                    gender: output_detail[9],
                                                    country: output_detail[11],
                                                    smsProviderId: output_detail[12],
                                                    status: output_detail[15],
                                                    recoveryId: output_detail[18],
                                                    ipaddress: proxyInput[0],
                                                    port: proxyInput[1],
                                                    username: null,
                                                    password: null,
                                                    proxyGroup: output_detail[23]
                                                });
                                            }
                                        }
                                        else{
                                            hideLoading()
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Not enough rows required',
                                                text: `Incorrect value in STATUS column at row:${row+1}. Only values are accepted: registered, unregistered`
                                            })
                                            return false
                                        }                                   
                                    }  
                                    //ZAPIS DO DB
                                    var opt = {
                                        scriptPath : process.cwd()+LOCATION_UNPACK+'\\_engine\\PXY\\',
                                        pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
                                        args : [jsonArr]
                                    }

                                    var pyshell = new child.PythonShell('ipmpxy.py', opt);
                                    
                                    pyshell.send(JSON.stringify(jsonArr), { mode: 'json' });
                                
                                    pyshell.on('message', results => {
                                        if(results.includes('PROGRESSNUM')){
                                            $("#actual-import-progress-val").text(results.split(':')[1])
                                        }
                                        
                                        if(results != 'IMPORT_DONE' && results.includes('PROGRESSNUM') == false){
                                            $("#message-proggress").text(results)

                                            setTimeout(function () {
                                                hideLoading()

                                                data_email.refreshCreateEmailsTable()
                                                return true
                                            }, 3000);
                                        }

                                        
                                        if(results == 'IMPORT_DONE'){
                                            setTimeout(function () {
                                                hideLoading()

                                                data_email.refreshCreateEmailsTable()
                                                return true
                                            }, 3000);
                                            
                                        }                                    
                                    });  
                                    pyshell.end(err => {
                                        if (err) {
                                            hideLoading()    
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops!',
                                                text: 'Error reason:' + err.message
                                            })
                                            
                                            setTimeout(function () {
                                                hideLoading()

                                                data_email.refreshCreateEmailsTable()
                                                return true
                                            }, 3000);
                                        }  
                                        return false
                                    });
                                    //END DB
                                }
                                reader.readAsBinaryString(file)
                            }
                        }
                    },
                ],
                ajax: function ( data, callback, settings ) {
                    child.db.all(`
                    SELECT 
                        a.id,
                        a.created,
                        a.email,
                        a.password,
                        a.firstName,
                        a.lastName,
                        a.day,
                        a.month,
                        a.year,
                        a.gender,
                        a.telephoneNumber,
                        a.country,
                        a.smsProviderId,
                        a.gmailCode,
                        a.proxyId,
                        case when b.id_email is null then a.status else 'WAITING TASK' end as status,
                        a.scoreV3,
                        a.scoreV2,
                        a.recoveryId,
                        a.registered_at
                    FROM EMAILS a 
                    LEFT JOIN (
                        SELECT
                            DISTINCT id_email
                        FROM WAITING
                        WHERE 1=1
                            AND sessions_type = 'REGISTERING'
                    )b on a.id = b.id_email
                    ORDER BY a.id DESC
                    `, function(err, rows) {  
                        if (err) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops..',
                                text: 'Error reason: ' + err.message
                            })
                        }

                        var out = [];
                        var difference = null
                        databaza = rows
                        searchVal = data.search['value']

                        if(rows == null || rows[0] == undefined){
                            hideLoading()
                            Swal.fire({
                                icon: 'info',
                                title: 'No emails',
                                text: 'Add some new tasks or registered emails.'
                            })
                            setTimeout( function () {
                                callback( {
                                    draw: data.draw,
                                    data: [],
                                    recordsTotal: 0,
                                    recordsFiltered: 0
                                } );
                            }, 50 );
                            return ""
                        }

                        var keys = Object.keys(rows[0]);

                        function validateDate(date){
                            var regex=new RegExp("([0-9]{4}[-](0[1-9]|1[0-2])[-]([0-2]{1}[0-9]{1}|3[0-1]{1})|([0-2]{1}[0-9]{1}|3[0-1]{1})[-](0[1-9]|1[0-2])[-][0-9]{4})");
                            var dateOk=regex.test(date);
                            if(dateOk){
                                return true
                            }else{
                                return false
                            }
                        }

                        if(databaza.length > 0){
                            data.order.forEach(riadok => {
                                kluc = riadok['column']-1
                                if(riadok['dir'] == 'asc'){
                                    if(validateDate(databaza[0][keys[kluc]])){
                                        databaza.sort(function compare(a,b){
                                            var dateA = new Date(a[keys[kluc]]);
                                            var dateB = new Date(b[keys[kluc]]);
                                            return dateA - dateB;
                                        }) 
                                    }
                                    else if(typeof(databaza[0][keys[kluc]]) === 'number'){
                                        databaza.sort(function(a, b){return a[keys[kluc]]-b[keys[kluc]]});
                                    }
                                    else{
                                        databaza.sort(function(a, b){
                                            var nameA=String(a[keys[kluc]]).toLowerCase(), nameB=String(b[keys[kluc]]).toLowerCase();
                                            if (nameA < nameB) //sort string ascending
                                             return -1;
                                            if (nameA > nameB)
                                             return 1;
                                            return 0; //default return value (no sorting)
                                        });
                                    }
                                }
                                else{
                                    if(validateDate(databaza[0][keys[kluc]])){
                                        databaza.sort(function compare(a,b){
                                            var dateA = new Date(a[keys[kluc]]);
                                            var dateB = new Date(b[keys[kluc]]);
                                            return dateB - dateA;
                                        }) 
                                    }
                                    else if(typeof(databaza[0][keys[kluc]]) === 'number'){
                                        databaza.sort(function(a, b){return b[keys[kluc]]-a[keys[kluc]]});
                                    }
                                    else{
                                        databaza.sort(function(a, b){
                                            var nameA=String(a[keys[kluc]]).toLowerCase(), nameB=String(b[keys[kluc]]).toLowerCase();
                                            if (nameA > nameB) //sort string ascending
                                             return -1;
                                            if (nameA < nameB)
                                             return 1;
                                            return 0; //default return value (no sorting)
                                        });
                                    }
                                }
                            })
                        }

                        var totalArr = [];
                        for (var i = 0; i < rows.length; i++) {
                            totalArr.push(i);
                        }

                        if(searchVal != ''){
                            var searchDatabaza = []
                            for (let i = 0; i < rows.length; i++) {
                                if(JSON.stringify(Object.values(rows[i])).indexOf(searchVal) > -1){
                                    searchDatabaza.push(i)
                                }                             
                            }
                            var difference = totalArr.filter(x => !searchDatabaza.includes(x));

                            difference.forEach(riadok => {
                                delete databaza[riadok]
                            });
                            databaza = databaza.filter(n=>n!==undefined)
                        }else{
                            databaza = rows
                        }

                        if(window.searchFilterStatusEmails != null || window.endDateEmailsVal != null){
                            if(window.searchFilterStatusEmails != null && window.endDateEmailsVal == null){
                                if(window.searchFilterStatusEmails != ""){

                                    if(window.searchFilterStatusEmails.toLowerCase() == 'registered'){
                                        searchFilterValDict = filter_create_vals_dictionary.REGISTERED
                                    }
                                    else if(window.searchFilterStatusEmails.toLowerCase() == 'failed'){
                                        searchFilterValDict = filter_create_vals_dictionary.FAILED
                                    }
                                    else if(window.searchFilterStatusEmails.toLowerCase() == 'canceled'){
                                        searchFilterValDict = filter_create_vals_dictionary.CANCELED
                                    }
                                    else if(window.searchFilterStatusEmails.toLowerCase() == 'unregistered'){
                                        searchFilterValDict = filter_create_vals_dictionary.UNREGISTERED
                                    }

                                    var databaza = databaza.filter(function (el) {
                                        return searchFilterValDict.includes(el.status.toLowerCase())
                                    });
                                }
                            }
                            else if(window.searchFilterStatusEmails != null && window.endDateEmailsVal != null){
                                if(window.searchFilterStatusEmails != ""){

                                    if(window.searchFilterStatusEmails.toLowerCase() == 'registered'){
                                        searchFilterValDict = filter_create_vals_dictionary.REGISTERED
                                    }
                                    else if(window.searchFilterStatusEmails.toLowerCase() == 'failed'){
                                        searchFilterValDict = filter_create_vals_dictionary.FAILED
                                    }
                                    else if(window.searchFilterStatusEmails.toLowerCase() == 'canceled'){
                                        searchFilterValDict = filter_create_vals_dictionary.CANCELED
                                    }
                                    else if(window.searchFilterStatusEmails.toLowerCase() == 'unregistered'){
                                        searchFilterValDict = filter_create_vals_dictionary.UNREGISTERED
                                    }

                                    var databaza = databaza.filter(function (el) {
                                        return searchFilterValDict.includes(el.status.toLowerCase()) &&
                                                el.created >= window.startDateEmailsVal &&
                                                el.created <= window.endDateEmailsVal+1;
                                    });
                                }
                            }
                            else if(window.searchFilterStatusEmails == null && window.endDateEmailsVal != null){
                                var databaza = databaza.filter(function (el) {  
                                    return el.created >= window.startDateEmailsVal &&
                                            el.created <= window.endDateEmailsVal+1;
                                });
                            }
                        }
                        
                        velkost = (data.start+data.length) > databaza.length ? databaza.length : data.start+data.length                
                        
                        for ( var i=data.start, ien=velkost; i<ien ; i++ ) {
                            draft = Object.assign({"check":databaza[i].id},databaza[i],{"ShowWindow":databaza[i].id},{"Executed":databaza[i].id})
                            out.push(Object.values(draft))
                        }                        
                        
                        setTimeout( function () {
                            callback( {
                                draw: data.draw,
                                data: out,
                                recordsTotal: databaza.length,
                                recordsFiltered: databaza.length
                            } );
                        }, 50 );
                    });
                },
                serverSide: true,
                scrollX: true,
                retrieve: true,
                pageLength: window.createEmailDatatableRows > 0 ? window.createEmailDatatableRows : 10,
                order: [],
                lengthMenu: [ 10, 20, 50, 100, 250],
                columnDefs: [
                    {
                        targets: 'no-sort',
                        orderable: false,
                    } ,
                    {
                        "targets": [ 2,7,8,9,10,14 ],
                        "visible": false
                    },
                    {
                        'targets': 0,
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta){
                            return '<input type="checkbox" style="cursor: pointer;" class="subcheckbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';}
                    },
                    {
                        'targets': 21,
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta){
                            return '<p id="getBal_'+$('<div/>').text(data).html()+'" style="cursor: pointer;" onclick="child.maximizeWindow(event, '+$('<div/>').text(data).html()+')"><i class="m-r-10 mdi mdi-arrange-bring-forward" ></i></p>';}
                    },
                    {
                        'targets': 22,
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta){
                            return '<div id="email-process-'+$('<div/>').text(data).html()+'"><p class="exec-button" style="cursor: pointer;" data-status="'+full[16]+'" onclick="email.registerFromPlay('+"event,"+"'"+$('<div/>').text(data).html()+"'"+')"><i id="exec-icon-'+$('<div/>').text(data).html()+'" class="m-r-10 mdi mdi-play-circle" ></i></p></div>';}
                    },
                    {
                    targets: 2, 
                    type : "date",
                    render: function (data) {
                            var date = new Date(data);
                            var datestring = date.getDate()  + "." + (date.getMonth()+1) + "." + date.getFullYear() + " " +
                                date.getHours() + ":" + (date.getMinutes()<10?'0':'') + date.getMinutes();
                            return datestring; 
                        }
                    },
                    {
                        'targets': 20,
                        type : "date",
                        render: function (data){
                            if(data){
                                var date = new Date(data);
                                var datestring = date.getDate()  + "." + (date.getMonth()+1) + "." + date.getFullYear() + " " +
                                    date.getHours() + ":" + (date.getMinutes()<10?'0':'') + date.getMinutes();
                                return $('<div/>').text(datestring).html()
                            }
                            else{
                                return $('<div/>').text("").html()
                            }
                        }
                            
                    }
                ],
                columns: [
                    { title: `<input type="checkbox" style="cursor: pointer;" name="mainCheck" id="mainCheck" onclick="email_data.checkedAllCheckboxes()">`},
                    { title: "Id" },
                    { title: "Created at"},
                    { title: "Email" },
                    { title: "Password" },
                    { title: "First name" },
                    { title: "Last name" },
                    { title: "Day" },
                    { title: "Month" },
                    { title: "Year"},
                    { title: "Gender" },
                    { title: "Phone" },
                    { title: "Country" },
                    { title: "SMS Provider" },
                    { title: "Gmail code" },
                    { title: "Proxy ID" },
                    { title: "Status" },
                    { title: "Recaptcha score V3" },
                    { title: "Recaptcha one click" },
                    { title: "Recovery email" },
                    { title: "Registered at"},
                    { title: "Show window" },
                    { title: "Executed" }         
                ]
            } );
            hideLoading();

            createEmailTable.on('buttons-processing', function ( e, indicator ) {
                if ( indicator ) {
                    createEmailTable.buttons().disable();
                }
                else {
                    createEmailTable.buttons().enable();
                }
            } );
    },

    refreshCreateEmailsTable: function(){
        var newArray = []
        showLoading();
        
        $('#smsCreateEmailsTable').dataTable().fnClearTable();
        this.inicializeCreateEmailsTable ()
        var emailData = []
        child.db.all(`SELECT 
                        a.id,
                        a.created,
                        a.email||'@gmail.com' as email,
                        a.password,
                        a.firstName,
                        a.lastName,
                        a.day,
                        a.month,
                        a.year,
                        a.gender,
                        a.telephoneNumber,
                        a.country,
                        a.smsProviderId,
                        a.gmailCode,
                        a.proxyId,
                        a.status,
                        a.scoreV3,
                        a.scoreV2,
                        a.recoveryId,
                        a.registered_at,
                        '' as showwindow,
                        '' as executed,
                        b.hostname||':'||b.port||':'||b.username||':'||b.password as proxy,
                        b.proxy_group
                        --b.port,
                        --b.username,
                        --b.password as password1
                    FROM EMAILS a 
                    LEFT JOIN PROXIES b on a.proxyId = b.id`, function(err, rows) {  
            if(rows != undefined && rows != null && rows.length > 0){
                if(rows === undefined || rows.length == 0){
                    return false
                }
                rows.forEach(function (row) { 
                    emailData.push(Object.values(row))
                }) 
                window.emailData = emailData
            } else {
                emailData.push([])
            }   
        })
        // child.db.all("SELECT * FROM EMAILS ORDER BY id DESC", function(err, rows) {  
        //     if (err) {
        //             Swal.fire({
        //                 icon: 'error',
        //                 title: 'Oops..',
        //                 text: 'Error reason: ' + err.message
        //             })
        //         }

        //     rows.forEach(function (row) { 
        //         draft = Object.assign({"check":row.id},row,{"ShowWindow":row.id},{"Executed":row.id})
        //         newArray.push(Object.values(draft))
        //     }) 
        //     if(newArray.length == 0){

        //     }else{
        //         $('#smsCreateEmailsTable').dataTable().fnAddData(newArray);
        //         $('#smsCreateEmailsTable').dataTable().fnDraw();
        //     } 
        //     hideLoading();
        // });
        
    },

    checkedAllCheckboxes: function(){
        var $that = $('#mainCheck');
        $('.subcheckbox').each(function() {
            this.checked = $that.is(':checked');
        })
    },

    showProviders: function(){
        var newArray = []
        child.db.all("SELECT distinct provider FROM PROVIDERS ORDER BY id DESC", function(err, rows) {  
            if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }

            rows.forEach(function (row) { 
                newArray.push(Object.values(row))
            }) 
            if(newArray.length == 0){
                
            }else{
                var i;
                for (i = 0; i < newArray.length; i++) {
                    var option = document.createElement("option");
                    option.text = newArray[i][0];
                    option.value = newArray[i][0];
                    var select = document.getElementById("providers-list");
                    if(select != null){
                        select.appendChild(option);
                    }
                }

            }
            
        });

    },

    getCountriesLucky: function() {
        $("#countries-list").html("<option value=''>Loading...</option>")
        child.db.all("SELECT name,fullname,system FROM COUNTRY_LUCKY ORDER BY system,fullname", function(err, rows) {  
            if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }
            var res = ''    
            var systemNumber = 1
            var i = 0

            if(rows === undefined || rows.length == 0){
                return false
            }

            //rows = JSON.parse(results)
            rows.forEach(function (row) { 
                    
                i += 1
                if(i == 1){
                    res += `<optgroup label="SYSTEM ${systemNumber}">`
                    res += `<option value='LUCKYSMS_system${systemNumber}_RAND'>RANDOM..</option>`
                    systemNumber += 1
                }
                if(row['system'] == 'system'+systemNumber && row['system'] != 'system1'){
                    res += '</optgroup>'
                    res += `<optgroup label="SYSTEM ${systemNumber}">`
                    res += `<option value='LUCKYSMS_system${systemNumber}_RAND'>RANDOM..</option>`
                    systemNumber += 1
                }
                
                optVal = `<option value="LUCKYSMS_${row['system']}_${row['name']}">${row['fullname']} (${row['name']})</option>`
                res += optVal
                
                if(i == rows.length){
                    res += '</optgroup>'
                }
                
            })    
            $("#countries-list").html(res)
        });
    },

    getCountriesOther: function(providerValInput = "") {
        $("#countries-list").html("<option value=''>Loading...</option>")
        var newArray = `<option value='RAND'>RANDOM..</option>`

        // POZOR HODNOTY SA MENIA AJ V PYTHONE KTORY GENERUJE TASKY
        var smspvaActiveCountries = "'RU','UA','DE','KZ','HT','RO','AR','BA','BR','KH','CM','CA','CL','CY','DO','EG','EE','FI','FR','GH','IN','ID','IQ','IE','IL','KE','KG','LA','LV','LT','MY','MX','MD','MA','NL','NZ','NG','PK','PH','PL','PT','ZA','ES','SE','UK','US','VN'"
        var getsmscodeActiveCountries = "'CN','US','MY','PH','KH','MN','VN','ID','BR','ZA','UK','HK','MO','MA'"
        var pvacodesActiveCountries = "'AF','AG'"
        
        if(providerValInput == "SMSPVA"){
            finalSqlCountries = `SELECT name,fullname FROM COUNTRY WHERE name in (${smspvaActiveCountries}) ORDER BY fullname`
        }else if(providerValInput == "GETSMSCODE"){
            finalSqlCountries = `SELECT name,fullname FROM COUNTRY WHERE name in (${getsmscodeActiveCountries}) ORDER BY fullname`
        }else if(providerValInput == "PVACODES"){
            //finalSqlCountries = `SELECT name,fullname FROM COUNTRY WHERE name in (${pvacodesActiveCountries}) ORDER BY fullname`
            finalSqlCountries = `SELECT name,fullname FROM COUNTRY ORDER BY fullname`
        }else{
            finalSqlCountries = `SELECT name,fullname FROM COUNTRY ORDER BY fullname`
        }

        child.db.all(finalSqlCountries, function(err, rows) {  
            if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }

            if(rows === undefined || rows.length == 0){
                return false
            }

            rows.forEach(function (row) { 
                newArray += `<option value='${row.name}'>${row.fullname} (${row.name})</option>`
            }) 
            if(newArray.length == 0){
                
            }else{
                $("#countries-list").html(newArray)
            }
        });    
    },

    getRecovery: function() {
        child.db.all(`SELECT 
                    max(case when category = 'RECOVERY' then value_main else "" end) as recovery_domain, 
                    max(case when category = 'NAME_MAX_NUM' then value_main else "" end) as max_num_name 
                FROM SETTINGS `, function(err, rows) {  
            if (err) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops..',
                    text: 'Error reason: ' + err.message
                })
            }

            $("#recovery-email").val(rows[0].recovery_domain)
            $("#recovery-email").attr("data-maxnums",rows[0].max_num_name)

            if(rows[0].recovery_domain == undefined || rows[0].recovery_domain == null){
                $("#actualRecoveryDomain").text("No recovery domain")
            }
            else{
                $("#actualRecoveryDomain").text(rows[0].recovery_domain)
            }
        });      
    },

    hideColumnGroup: function() {
    $("a.toggle-vis").on("click", function (e) {
        e.preventDefault();

        var columnsData = []

        if ($(this).attr("data-desc") == "Registration-data"){
            columnsData = [5,6,7,8,9,10]
        }
        else if($(this).attr("data-desc") == "SMS-data"){
            columnsData = [11,12,13,14]
        }
        else if($(this).attr("data-desc") == "ALL"){
            columnsData = [5,6,7,8,9,10]
            var column = createEmailTable.columns(columnsData);
            columnsData1 = [11,12,13,14]
            var column1 = createEmailTable.columns(columnsData1);
            if(column.visible()[0] == false && column1.visible()[0] == true){
                column.visible(!column.visible()[0])
            }
            else if(column.visible()[0] == true && column1.visible()[0] == false){
                column1.visible(!column1.visible()[0])
            }
            else if(column.visible()[0] == true && column1.visible()[0] == true ){
                column.visible(!column.visible()[0]);
                column1.visible(!column1.visible()[0]);
            }else{
                column.visible(!column.visible()[0]);
                column1.visible(!column1.visible()[0]);
            }
            return true
        }
        
        var column = createEmailTable.columns(columnsData);
        column.visible(!column.visible()[0]);
    });
    },
    getFreeProxyDataVal: function(){
        child.db.all(
        //`SELECT DISTINCT id,id_window FROM WINDOWS WHERE status_id = '1' and sessions_type = 'REGISTERING'`,
        `SELECT COUNT(*) as TOTAL FROM PROXIES WHERE (id_email is null or id_email = '' or id_email = ' ') `,
        function (err, rows) {
            document.getElementById("UnusedProxyDataVal").innerHTML = rows[0].TOTAL
        })
    },
    datatableRowsGlobalSave: function(elemNameVal){
        elementToChange = document.getElementsByName(elemNameVal)
        if(elementToChange.length>0 && elementToChange[0] != undefined && elementToChange[0] != null){
            elementToChange[0].addEventListener("change", function(){
                window.createEmailDatatableRows = this.value
            });
        }
        
    }
}

document.addEventListener("newPageLoad", function(){
    window.startDateEmailsVal = null
    window.endDateEmailsVal = null
    
    if($('#smsCreateEmailsTable').length){  

        data_email.inicializeCreateEmailsTable()
        data_email.showProviders()
        data_email.getRecovery()
        data_email.hideColumnGroup()
        data_email.getFreeProxyDataVal()
        data_email.datatableRowsGlobalSave('smsCreateEmailsTable_length')
        child.getAllProxyGroups("#selectProxyGroup")
        child.getAllProxyGroups('#filterProxyGroupImport')

        $("#providers-list").change(function () {
            var val = $(this).val();
            if (val == "LUCKYSMS") {
                data_email.getCountriesLucky();
            } else if (val == "SMSPVA") {
                data_email.getCountriesOther("SMSPVA")
            } else if (val == "PVACODES") {
                data_email.getCountriesOther("PVACODES")
            } else if (val == "GETSMSCODE") {
                data_email.getCountriesOther("GETSMSCODE")
            } else if (val == "item0") {
                $("#countries-list").html("<option value='country0'>Please select provider first..</option>")
            }
        });
        
        (function emailCheckPlay(){
            var procType = 'REGISTERING'
            var intervalValue = 2000
            var maxSecPlayFailCheck = 60
            var f = ''
            var f = function() {
                if($('#smsCreateEmailsTable').length){
                    quarantine = []
                    $('[data-status^="failed"]').each(function(){
                        id = $(this).parent()[0].attributes['id'].value.replace(/email-process-/, '')
                        $(this)[0].outerHTML = `<p id="email-process-${id}" class="exec-button" data-status="failed-register" style="color:#f62d51;"><i id="exec-icon-${id}" class="m-r-10 mdi mdi-close-box" ></i></p>`
                        quarantine.push(id)
                    })
                    
                    $('[data-status^="registered"]').each(function(){
                        id = $(this).parent()[0].attributes['id'].value.replace(/email-process-/, '')
                        $(this)[0].outerHTML = `<p id="email-process-${id}" class="exec-button" data-status="registered" style="color:#4eb354;"><i id="exec-icon-${id}" class="m-r-10 mdi mdi-checkbox-marked-circle" ></i></p>`
                        quarantine.push(id)
                    })
                    
                    child.db.all(`SELECT id_email, max(status_id) as status_id FROM WINDOWS WHERE sessions_type = 'REGISTERING' group by id_email`, function(err, rows) {  
                        // if (err) {
                        //     Swal.fire({
                        //         icon: 'error',
                        //         title: 'Oops..',
                        //         text: 'Error reason: ' + err.message
                        //     })
                        // }
                        if(rows === undefined || rows.length == 0){
                            return false
                        }

                        rows.forEach(function (row) { 
                            
                            if(quarantine.includes(row['id_email'])){
                                return false
                            }
                            
                            var parent = document.getElementById(`email-process-${row['id_email']}`); 
                            var allElements = document.getElementsByClassName('mdi-close-box'); 
                            if (parent != undefined && parent.contains(allElements[0])){ 
                                return false
                            }

                            if($(`#email-process-${row['id_email']}.play`).data('sec') == undefined){
                                var sec = ''
                            }
                            else{
                                var sec = $(`#email-process-${row['id_email']}.play`).data('sec')
                            }
                            
                            if(row['status_id'] == 0 && $(`#email-process-${row['id_email']}.play`).length == 1){
                                $(`#email-process-${row['id_email']}.play`).data('sec',sec+=intervalValue/1000)
                                if($(`#email-process-${row['id_email']}.play`).data('sec')>=maxSecPlayFailCheck){
                                    document.getElementById(`email-process-${row['id_email']}`).innerHTML = `<p id="email-process-${row['id_email']}" class="exec-button" style="cursor: pointer;" onclick="email.registerFromPlay(event,'${row['id_email']}')"><i id="exec-icon-${row['id_email']}" class="m-r-10 mdi mdi-play-circle" ></i></p>`
                                }
                            }

                            if(document.getElementById(`email-process-${row['id_email']}`) != null && row['status_id'] == 1){
                                if($(`#email-process-${row['id_email']}.pause`).length == 0){
                                    document.getElementById(`email-process-${row['id_email']}`).innerHTML = `<p id="email-process-${row['id_email']}" class="exec-button" style="cursor: pointer;" onclick="child.killWindow(event,'${row['id_email']}','REGISTERING')"><i id="exec-icon-${row['id_email']}" class="m-r-10 mdi mdi-pause-circle" ></i></p>`
                                }
                            }
                            else if(document.getElementById(`email-process-${row['id_email']}`) != null && row['status_id'] == 0){
                                if($(`#email-process-${row['id_email']}.play`).length == 0){
                                    document.getElementById(`email-process-${row['id_email']}`).innerHTML = `<p id="email-process-${row['id_email']}" class="exec-button" style="cursor: pointer;" onclick="email.registerFromPlay(event,'${row['id_email']}')"><i id="exec-icon-${row['id_email']}" class="m-r-10 mdi mdi-play-circle" ></i></p>`
                                }    
                            }
                        })
                    });
                }
                
            };
            window.setInterval(f, intervalValue);
            
            f();
        })();
    }

    // Date range filter
    minDateFilter = "";
    maxDateFilter = "";
    if($('#smsCreateEmailsTable').length){
    $.fn.dataTableExt.afnFiltering.push(
    function(oSettings, aData, iDataIndex) {
        if (minDateFilter == ''  ||  maxDateFilter == '' || typeof minDateFilter === 'undefined' || typeof maxDateFilter === 'undefined') {
            return true;
        }
        if(isNaN(aData[20])){
            // if(typeof minDateFilter === 'undefined'){

            // }
            if(!(typeof aData[20] === 'undefined')){
                text = aData[20].replace(/[.]/g, '-');
                aData._date = new Date( text.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3") ).getTime();
            }
            
    
            if (minDateFilter && !isNaN(minDateFilter)) {
                if (aData._date < minDateFilter) {
                    return false;
                }
            }
        
            if (maxDateFilter && !isNaN(maxDateFilter)) {
            if (aData._date > maxDateFilter) {
                return false;
            }
            }
        
            return true;
        }    
        return false;
        } 
    )
    }
    
    $("#filterStatus").change(function () {
            $('#smsCreateEmailsTable').dataTable().fnClearTable();
            window.searchFilterStatusEmails = $(this).val()
            data_email.inicializeCreateEmailsTable()
            // createEmailTable
            //     .columns(16)
            //     .search("(^"+$(this).val()+"$)",true,false)
            //     .draw();
        
    })
    var startdate;
    var enddate;
    $('#reportrangeEmails').daterangepicker({
       locale: {  "format": "DD.MM.YYYY",
            "separator": " - ",
            "applyLabel": "Apply",
            
            "cancelLabel": "Clear",
            "fromLabel": "From",
            "toLabel": "To",
            "firstDay": 1},

       "opens": "right",
       "startDate":startdate,
       "closeText": "Clear",
    },
    function (start, end, label) {
      /* var s = moment(start.toISOString());
       var e = moment(end.toISOString());
       startdate = s.format("DD-MM-YYYY");
       enddate = e.format("DD-MM-YYYY");*/
       window.startDateEmailsVal = start.format('YYYY-MM-DD')
       window.endDateEmailsVal = end.format('YYYY-MM-DD')

    	 //console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
    });
    $('#reportrangeEmails').on('apply.daterangepicker', function (ev, picker) {
        // minDateFilter = moment(picker.startDate.format('MM/DD/YYYY'), 'MM/DD/YYYY');
        // maxDateFilter = moment(picker.endDate.format('MM/DD/YYYY'), 'MM/DD/YYYY');
        minDateFilter = new Date(picker.startDate).getTime();
        maxDateFilter =  new Date(picker.endDate).getTime();
        $('#smsCreateEmailsTable').dataTable().fnDraw();  
     });

     $('#reportrangeEmails').on('cancel.daterangepicker', function (ev, picker) {
        // minDateFilter = moment(picker.startDate.format('MM/DD/YYYY'), 'MM/DD/YYYY');
        // maxDateFilter = moment(picker.endDate.format('MM/DD/YYYY'), 'MM/DD/YYYY');
        minDateFilter = '';
        maxDateFilter =  '';
        $('#smsCreateEmailsTable').dataTable().fnDraw();  
     });

    $("#datepicker_from").datepicker({
        showOn: "button",
        buttonImage: process.cwd()+LOCATION+"\\cache-data\\assets\\images\\calendar.gif",
        buttonImageOnly: false,
        "onSelect": function(date) {
        minDateFilter = new Date(date).getTime();
        $('#smsCreateEmailsTable').dataTable().fnDraw();    
        }
    }).keyup(function() {
        minDateFilter = new Date(this.value).getTime();
        $('#smsCreateEmailsTable').dataTable().fnDraw();
    });
    
    $("#datepicker_to").datepicker({
        showOn: "button",
        buttonImage: process.cwd()+LOCATION+"\\cache-data\\assets\\images\\calendar.gif",
        buttonImageOnly: false,
        "onSelect": function(date) {
        maxDateFilter = new Date(date).getTime();
        $('#smsCreateEmailsTable').dataTable().fnDraw()
        }
    }).keyup(function() {
        maxDateFilter = new Date(this.value).getTime();
        $('#smsCreateEmailsTable').dataTable().fnDraw()
    });

    $("#datepicker_from").click(function() {
        $(this).datepicker("show");
      });
      $("#datepicker_to").click(function() {
        $(this).datepicker("show");
      });  
});