//Custom functions JAVASCRIPT PROXY
//var path = require("path")
var self_task = module.exports = {
    killAllWaitTasks: function(){ 
        Swal.fire({
            title: `Are you sure you want to DELETE ALL WAITING TASKS?`,
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: `Yes, Stop it!`,
        }).then((result) => {
            if (result.isConfirmed) {
                showLoading()
                sql = `DELETE FROM WAITING`,
                child.db.run(sql, function (err) {
                    if (err) {
                        hideLoading()
                        Swal.fire({
                          icon: 'error',
                          title: 'Oops..',
                          text: 'Error reason: ' + err.message
                        })
                    } 
                    hideLoading()
                    waittask_data.refreshWaitTaskTable()
                  });
                  hideLoading()
                document.getElementById('mainCheck').checked = false;
                Array.from(document.querySelectorAll('.subcheckbox:checked')).forEach(e => e.checked = false)  
            }
        })
    },

    killSelectedWaitTasks: function(){ 
        var data = [...document.querySelectorAll('.subcheckbox:checked')].map(e => e.value);

        if(data == ''){
            return false
        }

        showLoading()
        
        data = data.join()    

        arrayPrep = data.split(",")
        conditionVal = '('+data+')'
        var arrayVal = []
        
        var sql = `DELETE FROM WAITING WHERE id in ${conditionVal}`;
    
        child.db.run(sql, function (err) {
        if (err) {
            hideLoading()
            Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'Error reason: ' + err.message
            })
        } 
        hideLoading()
        waittask_data.refreshWaitTaskTable()
        });
        hideLoading()
        document.getElementById('mainCheck').checked = false;
        Array.from(document.querySelectorAll('.subcheckbox:checked')).forEach(e => e.checked = false)  
    },
}