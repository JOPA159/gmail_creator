//Custom functions JAVASCRIPT PROXY
//var path = require("path")
var self_proxy = module.exports = {
    generateProxiesOutput: function(stopVal=false){
        //var e = document.getElementById("inputGroupSelect01")
        var separatorValue = ':'
        var txt = document.getElementById("proxyTextareaId")
        var inpt = document.getElementById("proxiesImport")

        var existingGroup = document.getElementById("filterProxyGroupImport").value
        var newGroup = document.getElementById("newProxyGroup").value

        if(document.getElementById("new-group").checked){
            draftProxyGroup = newGroup
        }else if(document.getElementById("existing-group").checked){
            draftProxyGroup = existingGroup
        }else{
            draftProxyGroup = 'DEFAULT GROUP'
        }

        finalProxyGroup = String(draftProxyGroup).toUpperCase()

        if(finalProxyGroup.length == 0){
            return false
        }

        if(finalProxyGroup.length > 25){
            Swal.fire({
                icon: 'error',
                title: 'MAX GROUP LENGTH IS 25',
                text: 'Your proxy group name is too long..'
            })
            return false
        }

        if((inpt.value != "" || txt.value != "")){
            
            //document.getElementById("Insert").style.display="block"

            if(document.getElementById("textarea").style.display == "none"){

                var file = inpt.files[0];

                var reader = new FileReader();

                reader.readAsText(file)

                reader.onload = function(progressEvent){
                    // Entire file
                    //console.log(this.result);
                    //console.log("fffff")
                    // By lines
                    var lines = this.result.split('\n')
                    var firstLine = lines[0].split(separatorValue)
                    
                    /*document.getElementById("input1").value = firstLine[0] 
                    document.getElementById("input2").value = firstLine[1]
                    document.getElementById("input3").value = firstLine[2]
                    document.getElementById("input4").value = firstLine[3]*/

                    //document.getElementById("outputcheck").style.display="block"

                    if(stopVal == true){return}
                    var jsonArr = []
                    
                    for(var line = 0; line < lines.length; line++){
                        if(line > 2000){   
                            Swal.fire({
                                icon: 'error',
                                title: 'MAX 2000 ROWS',
                                text: 'Your file is too long..'
                            })
                            return false
                        }
                        var firstLineLoop = lines[line].split(separatorValue)
                        if(firstLineLoop.length == 4){
                            jsonArr.push({
                                ipaddress: firstLineLoop[0],
                                port: firstLineLoop[1],
                                username: firstLineLoop[2],
                                password: firstLineLoop[3],
                                group: finalProxyGroup
                            });
                        }
                        else if(firstLineLoop.length == 2){
                            jsonArr.push({
                                ipaddress: firstLineLoop[0],
                                port: firstLineLoop[1],
                                username: null,
                                password: null,
                                group: finalProxyGroup
                            });
                        }
                    
                    };  
                            
                    self_proxy.postProxy(jsonArr) 
                };  

                //console.log(reader.readAsText(file));


            }
            else if(document.getElementById("inputfile").style.display == "none"){
                
                var lines = document.getElementById('proxyTextareaId').value.split('\n')
                var firstLine = lines[0].split(separatorValue)
                console.log(lines[0])

                /*document.getElementById("input1").value = firstLine[0] 
                document.getElementById("input2").value = firstLine[1]
                document.getElementById("input3").value = firstLine[2]
                document.getElementById("input4").value = firstLine[3]*/

                //document.getElementById("outputcheck").style.display="block"

                if(stopVal == true){return}

                var jsonArr = []

                for(var line = 0; line < lines.length; line++){
                    if(line > 2000){   
                        Swal.fire({
                            icon: 'error',
                            title: 'MAX 2000 ROWS',
                            text: 'Your file is too long..'
                        })
                        return false
                    }
                    var firstLineLoop = lines[line].split(separatorValue)
                    if(firstLineLoop.length == 4){
                        jsonArr.push({
                            ipaddress: firstLineLoop[0],
                            port: firstLineLoop[1],
                            username: firstLineLoop[2],
                            password: firstLineLoop[3],
                            group: finalProxyGroup
                        });
                    }
                    else if(firstLineLoop.length == 2){
                        jsonArr.push({
                            ipaddress: firstLineLoop[0],
                            port: firstLineLoop[1],
                            username: null,
                            password: null,
                            group: finalProxyGroup
                        });
                    }
                    //console.log(line);
                };
                
                self_proxy.postProxy(jsonArr)
            }

            
            //document.getElementById("proxiesImport").value = ""
            
        }
        
    },

    readTextFile: function(file)
    {
        var rawFile = new XMLHttpRequest();
        rawFile.open("GET", file, false);
        rawFile.onreadystatechange = function ()
        {
            if(rawFile.readyState === 4)
            {
                if(rawFile.status === 200 || rawFile.status == 0)
                {
                    var allText = rawFile.responseText;
                    alert(allText);
                }
            }
        }
        rawFile.send(null);
    },

    postProxy: function(output) {
        child.Slaninka()
        var opt = {
            scriptPath : process.cwd()+LOCATION_UNPACK+'\\_engine\\PXY\\',
            pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
            args : [output]
        }

        var pyshell = new child.PythonShell('pxxy.py', opt);

        pyshell.send(JSON.stringify(output), { mode: 'json' });

        pyshell.on('message', results => {
            //output = JSON.parse(results)
            window.close() 
        });

        pyshell.end(err => {
            if (err) {
                hideLoading()    
                Swal.fire({
                    icon: 'error',
                    title: 'Oops!',
                    text: 'Error reason:' + err.message
                })
                throw err
            }    
        });
    },

    deleteData: function(){ 
        var data = [...document.querySelectorAll('.subcheckbox:checked')].map(e => e.value);
        if(data == ''){
            return false
        }

        Swal.fire({
            title: "Are you sure?",
            text: "If the proxy has an email assigned, it will also be deleted!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!",
          }).then((result) => {
            if (result.isConfirmed) {
                showLoading();
                data = data.join();
                var sql  = "DELETE FROM PROXIES WHERE id IN("+data+")"
                
                child.db.run(sql, function(err) {
                    if (err) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops..',
                            text: 'Error reason: ' + err.message
                        })
                    }
                    else{
                        child.db.run("DELETE FROM EMAILS WHERE proxyId IN ("+data+")", function(err, rows) {  
                            if (err) {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops..',
                                        text: 'Error reason: ' + err.message
                                    })
                                }
                                    document.getElementById('mainCheck').checked = false;
                                    Array.from(document.querySelectorAll('.subcheckbox:checked')).forEach(e => e.checked = false) 
                                    proxy_data.refreshProxyTable() 
                                    //proxy_data.inicializeProxyTable()
                        })
                    }
                });
            }
        })
    },
    deleteDataUnused: function(){ 
        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!",
          }).then((result) => {
            if (result.isConfirmed) {
                showLoading();
                var sql  = "DELETE FROM PROXIES WHERE id_email is null"
                
                child.db.run(sql, function(err) {
                    if (err) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops..',
                            text: 'Error reason: ' + err.message
                        })
                    }
                    proxy_data.refreshProxyTable() 
                });
            }
        })
    },
    changeProxyGroup: function(){
        var data = [...document.querySelectorAll('.subcheckbox:checked')].map(e => e.value);
        data = data.join()  

        arrayPrep = data.split(",")

        if(arrayPrep.length == 0 || arrayPrep == ""){
              return false
        }

        var existingGroup = document.getElementById("filterProxyGroupImport").value
        var newGroup = document.getElementById("newProxyGroup").value

        if(document.getElementById("new-group").checked){
            draftProxyGroup = newGroup
        }else if(document.getElementById("existing-group").checked){
            draftProxyGroup = existingGroup
        }else{
            draftProxyGroup = 'DEFAULT GROUP'
        }

        finalProxyGroup = String(draftProxyGroup).toUpperCase()

        if(finalProxyGroup.length == 0){
            return false
        }

        if(finalProxyGroup.length > 25){
            Swal.fire({
                icon: 'error',
                title: 'MAX GROUP LENGTH IS 25',
                text: 'Your proxy group name is too long..'
            })
            return false
        }

        arrayPrep.forEach(function(email){
            child.db.exec(`UPDATE PROXIES SET proxy_group = '${finalProxyGroup}' where id = ${email}`)
        })

        proxy_data.refreshProxyTable() 

        Swal.fire({
            icon: "info",
            title: "Proxy group changed.",
            text:"You have successfully changed the proxy group on the selected proxies.",
        });
    }
}