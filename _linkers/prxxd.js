//Check ich providers already exists in table
var self_provider = module.exports = {
    sendFormMainProvider: function(event){
        showLoading();
        child.Slaninka()
        var newArray = []
        child.db.all("SELECT distinct provider FROM PROVIDERS", function(err, rows) {    
            rows.forEach(function (row) { 
                newArray.push(Object.values(row)[0])
            }) 

            if (err) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops..',
                    text: 'Error reason: ' + err.message
                })
            }
            else{
                if(newArray.includes(document.getElementById("provider-list").value)){   
                    Swal.fire({
                        icon: 'error',
                        title: 'Provider exists in table',
                        text: 'Duplicite provider. Delete existing provider from the table first.'
                    })
                    return false
                }
                else{
                    self_provider.sendForm(event)
                }
            } 
            
        });
    },

    newProvider: function(){
        var jsonArr = []

        let providerValue= document.getElementById("provider-list").value;
        let keyValue= document.getElementById("key").value;
        let secretValue= document.getElementById("secret").value;
        let timeoutValue= document.getElementById("timeout").value;

        var objResult = new Object();
        objResult.provider = providerValue
        objResult.key = keyValue
        objResult.secret = secretValue
        objResult.timeout = timeoutValue

        jsonArr.push(objResult)
        //var jsonString= JSON.stringify(objResult);

        var opt = {
            scriptPath : process.cwd()+LOCATION_UNPACK+'\\_engine\\PSK\\',
            pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
            args : [jsonArr]
        }
        
        var pyshell = new child.PythonShell('poskyt.py', opt);

        pyshell.send(JSON.stringify(jsonArr), { mode: 'json' });
        
        pyshell.on('message', results => {
                JSON.parse(results);
                proc_data.refreshProviderTable()
                
        });
        

        document.getElementById("key").value = '';
        document.getElementById("secret").value = '';
        document.getElementById("timeout").value = '1';
        document.getElementById("provider-list").value = 'LUCKYSMS';

        
        pyshell.end(err => {
            if (err) {
                hideLoading()    
                Swal.fire({
                    icon: 'error',
                    title: 'Oops!',
                    text: 'Error reason:' + err.message
                })
                throw err
            }  
        });
    },

    sendForm: function(event) {
    event.preventDefault()

    if(document.getElementById("key").value.length == 0){
        Swal.fire({
            icon: 'error',
            title: 'API key must be filled',
            text: 'No value!'
        })

        return false
    }   

    if(document.getElementById("provider-list").value == 'GETSMSCODE' && document.getElementById("secret").value.length == 0){
        Swal.fire({
            icon: 'error',
            title: 'GETSMSCODE also needs a filled email/secret',
            text: 'No value!'
        })

        return false
    }   

    if(document.getElementById("provider-list").value == 'LUCKYSMS'){
        child.updateLucky()
    }
    
    child.db.all("SELECT count(*) as total FROM sqlite_master WHERE name ='COUNTRY' and type='table'", function(err, rows) {  
        if (err) {
            Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'Error reason: ' + err.message
            })
        }

        if(Object.values(rows)[0].total == 0){
            child.updateCountryList(event)
        }
    });

    self_provider.newProvider()
    
},

    deleteData: function(){     
    var data = [...document.querySelectorAll('.subcheckbox1:checked')].map(e => e.value);

    if(data == ''){
        return false
    }
    showLoading();
    data = data.join();
    
    if(data == ''){
        hideLoading()
        return false
    }
    
    for (let row = 0; row < data.length; row++) {
        if(data[row] != ','){
            child.db.all(`SELECT id as provider_id, PROVIDER FROM PROVIDERS WHERE id = '${data[row]}'`, function(err, rows) {  
                if (err) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops..',
                            text: 'Error reason: ' + err.message
                        })
                    }

                if(rows === undefined || rows.length == 0){
                    hideLoading()
                    return false
                }
                var windowArray = [] 
                rows.forEach(function(row){

                    child.db.all(`SELECT id FROM EMAILS WHERE smsProviderId = '${row['provider']}' and status != 'registered'`, function(err, rows) {  
                        if (err) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops..',
                                    text: 'Error reason: ' + err.message
                                })
                            }
                        Swal.fire({
                            title: `Are you sure you want to delete ${row['provider']}?`,
                            text: "You won't be able to revert this!",
                            icon: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#3085d6",
                            cancelButtonColor: "#d33",
                            confirmButtonText: `Yes, delete ${row['provider']}!`,
                        }).then((result) => {
                            if (result.isConfirmed) {
                                if(rows === undefined || rows.length == 0){
                                    providerIdDelete = row['provider_id']
                                    //DELETE PROVIDER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    var sql  = "DELETE FROM PROVIDERS WHERE id =(?)"
                                    
                                    child.db.run(sql,providerIdDelete, function(err) {
                                        if (err) {
                                                Swal.fire({
                                                    icon: 'error',
                                                    title: 'Oops..',
                                                    text: 'Error reason: ' + err.message
                                                })
                                            }
                                        proc_data.refreshProviderTable()
                                    });
                                    Swal.fire("Deleted!", "Your file has been deleted.", "success");
                                }
                                else{
                                    Swal.fire({
                                        title: `Do you want to delete ALL EMAILS(and running processes) with this provider??`,
                                        text: "You won't be able to revert this!",
                                        icon: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#3085d6",
                                        cancelButtonColor: "#d33",
                                        confirmButtonText: `Yes, delete ALL EMAILS!`,
                                    }).then((result) => {
                                        if (result.isConfirmed) {
                                            if(rows === undefined || rows.length == 0){
                                                hideLoading()
                                                return false
                                            }
                                            rows.forEach(function (row) { 
                                                var sql  = `DELETE FROM EMAILS WHERE id = '${row['id']}'`
    
                                                child.db.run(sql, function(err) {
                                                    if (err) {
                                                            Swal.fire({
                                                                icon: 'error',
                                                                title: 'Oops..',
                                                                text: 'Error reason: ' + err.message
                                                            })
                                                        }
                                                    var emailIdVal = row['id']
                                                    child.db.all(`SELECT id FROM PROXIES WHERE id_email = '${row['id']}'`, function(err, rows) {  
                                                        if (err) {
                                                                Swal.fire({
                                                                    icon: 'error',
                                                                    title: 'Oops..',
                                                                    text: 'Error reason: ' + err.message
                                                                })
                                                            }
                                        
                                                        if(rows === undefined || rows.length == 0){
                                                            hideLoading()
                                                            return false
                                                        }

                                                        rows.forEach(function(row){
                                                            let data = [row['id']];
                                                            let sql = `DELETE FROM PROXIES WHERE id = ? `;
                                                
                                                            child.db.run(sql, data, function (err) {
                                                                if (err) {
                                                                    console.log("ERROR" + err);
                                                                }
                                                                var arrayVal = []
                                                                //KILL ALL WINDOWS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!§§§
                                                                child.db.all(`SELECT id, id_window FROM WINDOWS WHERE id_email = '${emailIdVal}'`, function(err, rows) {  
                                                                    if (err) {
                                                                            Swal.fire({
                                                                                icon: 'error',
                                                                                title: 'Oops..',
                                                                                text: 'Error reason: ' + err.message
                                                                            })
                                                                        }
                                                    
                                                                    if(rows === undefined || rows.length == 0){
                                                                        hideLoading()
                                                                        return false
                                                                    }
            
                                                                    rows.forEach(function (row) {
                                                                        draft = Object.assign(row)
                                                                        arrayVal.push(Object.values(draft))
                                                                      });

                                                                      if (arrayVal == null) {
                                                                        return false;
                                                                      }
                                                                
                                                                      backgroundAuto = false;
                                                                      modeJSON = true;
                                                                
                                                                      var opt = {
                                                                        scriptPath: process.cwd()+LOCATION_UNPACK+'\\_engine\\HG\\',
                                                                        pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
                                                                        args: [backgroundAuto, modeJSON],
                                                                      };
                                                                
                                                                      var pyshell = new child.PythonShell("Kwdw.py", opt);
                                                                
                                                                      pyshell.send(JSON.stringify(arrayVal), { mode: 'json' });
                                                                      
                                                                      pyshell.on("message", (results) => {
                                                                      });
                                                                
                                                                      pyshell.end((err) => {
                                                                        if (err) {
                                                                          hideLoading();
                                                                          Swal.fire({
                                                                            icon: "error",
                                                                            title: "Oops!",
                                                                            text: "Error reason:" + err.message,
                                                                          });
                                                                          throw err;
                                                                      }
                                                                    });
                                                                    
                                                                })

                                                            });
                                                        })
                                                    })
                                                });
                                            })

                                            var sql  = "DELETE FROM PROVIDERS WHERE id IN ("+row['provider_id']+")"
    
                                            child.db.run(sql, function(err) {
                                                if (err) {
                                                        Swal.fire({
                                                            icon: 'error',
                                                            title: 'Oops..',
                                                            text: 'Error reason: ' + err.message
                                                        })
                                                    }
                                                proc_data.refreshProviderTable()
                                            });
                                            Swal.fire("Deleted!", "Your file has been deleted.", "success");
                                        }
                                    });
                                }  
                            }
                        });                   
                    });
                    
                })
            });
        } 
    }
    hideLoading();
    document.getElementById('mainCheck').checked = false;
    //child.db.close()
},

    getBalanceScore: function(event,providerId){
    event.preventDefault()
    showLoading()

    elementBalance = document.getElementsByClassName('mdi-send')
    if(elementBalance != undefined || elementBalance != null){
        if(elementBalance.length > 0){
            //console.log(elementBalance)
            hideLoading()
            Swal.fire({
                icon: 'info',
                title: 'Oops..',
                text: 'It looks like you are trying to update your provider. Please complete the update first so that you can check your balance.' 
            })
            return false
        }
    }

    var opt = {
      scriptPath : process.cwd()+LOCATION_UNPACK+'\\_engine\\win_prov\\win_cr\\',
      pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
      //args : [providerId]
    }

    if(providerId == "LUCKYSMS"){
        var pyshell = new child.PythonShell('l_s_c.py', opt);
    }
    else if(providerId == "GETSMSCODE"){
        var pyshell = new child.PythonShell('gsmc_c.py', opt); 
    }
    else if(providerId == "PVACODES"){
        var pyshell = new child.PythonShell('pv_c.py', opt);
    }
    else if(providerId == "SMSPVA"){
        var pyshell = new child.PythonShell('spv_c.py', opt);
    }
    else{
        Swal.fire({
            icon: 'error',
            title: 'Unknown provider',
            text: 'This provider is not supported for getting balance.'
        })

        return false
    }

    pyshell.on('message', results => {
        proc_data.refreshProviderTable()
    });

    
    pyshell.end(err => {
        if (err) {
            hideLoading()    
            Swal.fire({
                icon: 'error',
                title: 'Could not retrieve current balance.',
                text: 'Error reason:' + err.message
                //text: 'Error reason: You will probably have incorrect credentials or the provider has an error on their part.'
            })
            //throw err
        }  
    });
}
}