var crypto = require('crypto');
var Moment = require('moment-timezone');
const algorithm = 'aes-256-cfb';
var Buffer = require('buffer/').Buffer


function encryptText(keyStr, text) {
    const hash = crypto.createHash('sha256');
    hash.update(keyStr);
    const keyBytes = hash.digest();
  
    const iv = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv(algorithm, keyBytes, iv);
    let enc = [iv, cipher.update(text, 'utf8')];
    enc.push(cipher.final());
  
    ttt = []
  
    enc[0].forEach(element => {
      ttt.push(element)
    });
  
    enc[1].forEach(element => {
      ttt.push(element)
    });
  
    enc[2].forEach(element => {
      ttt.push(element)
    });
  
    return Buffer.from(ttt).toString('base64');
  }

var self_forward = module.exports = {
    killAllForwardings: function(){ 
        listOfUnforwardedStatuses = '("'+String(filter_forward_vals_dictionary.UNFORWARDED).toLowerCase().replace(/,/g,'","')+'")'
        listOfUnforwardedStatuses.replace("")
        var sqlCanc = `UPDATE FORWARDING SET status = 'CANCELED' WHERE lower(status) in ${listOfUnforwardedStatuses}`;
        child.db.run(sqlCanc, function (err) {}) 
        var sqlWait = `DELETE FROM WAITING WHERE sessions_type = 'FORWARDING'`;
        child.db.run(sqlWait, function (err) {}) 

        child.db.all(
            //`SELECT DISTINCT id,id_window FROM WINDOWS WHERE status_id = '1' and sessions_type = 'REGISTERING'`,
            `SELECT DISTINCT id,id_window FROM WINDOWS WHERE sessions_type = 'FORWARDING' and date(datetime(date_created/ 1000 , 'unixepoch')) >= date('now')-3`,
            function (err, rows) {
            // if (err) {
            //     Swal.fire({
            //         icon: 'error',
            //         title: 'Oops..',
            //         text: 'Error reason: ' + err.message
            //     })
            // }
            var arrayVal = []
            
            if(rows === undefined || rows.length == 0){
                hideLoading()
                return false
            }

            rows.forEach(function (row) {
                draft = Object.assign(row)
                arrayVal.push(Object.values(draft))
            });
            //windows = row["id_window"];
            backgroundAuto = false;
            modeJSON = true;
        
            if (arrayVal == null) {
                return false;
            }
        
            var opt = {
                scriptPath: process.cwd()+LOCATION_UNPACK+'\\_engine\\HG\\',
                pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
                args: [backgroundAuto,modeJSON],
            };
        
            var pyshell = new child.PythonShell("Kwdw.py", opt);
        
            pyshell.send(JSON.stringify(arrayVal), { mode: 'json' });
        
            pyshell.on("message", (results) => {
                // if (results == "Killed") {
                //   let data = ["0", windows];
                //   let sql = `UPDATE WINDOWS SET status_id = ? WHERE id_window = ?`;
        
                //   child.db.run(sql, data, function (err) {
                //     if (err) {
                //       console.log("ERROR" + err);
                //     }
                //     //UPDATE DONE LOG??
                //   });
                // }
            });
        
            pyshell.end((err) => {
                if (err) {
                Swal.fire({
                    icon: "error",
                    title: "Oops!",
                    text: "Error reason:" + err.message,
                });
                throw err;
                }
            });
            }
        );
        document.getElementById('mainCheck').checked = false;
        Array.from(document.querySelectorAll('.subcheckbox:checked')).forEach(e => e.checked = false)  
    },

    killSelectedForwardings: function(){     
        var session_type = 'FORWARDING'
        var newArray = []
        showLoading()

        var data = [...document.querySelectorAll('.subcheckbox:checked')].map(e => e.value);
        data = data.join()    
        
        if(data == ''){
            hideLoading()
            return false
        }

        arrayPrep = data.split(",")
        conditionVal = '('+data+')'

        listOfUnforwardedStatuses = '("'+String(filter_forward_vals_dictionary.UNFORWARDED).toLowerCase().replace(/,/g,'","')+'")'
        listOfUnforwardedStatuses.replace("")
        var sqlCanc = `UPDATE FORWARDING SET status = 'CANCELED' WHERE lower(status) in ${listOfUnforwardedStatuses} AND email_id in ${conditionVal} AND status != 'unforwarded'`;
        child.db.run(sqlCanc, function (err) {}) 
        var sqlWait = `DELETE FROM WAITING WHERE sessions_type = 'FORWARDING' and id_email in ${conditionVal}`;
        child.db.run(sqlWait, function (err) {}) 

        var arrayVal = []
        child.db.all(`SELECT id,id_window FROM WINDOWS WHERE sessions_type = '${session_type}' AND id_email in ${conditionVal} ORDER BY id DESC`, function(err, rows) {  
            if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }
    
            if(rows === undefined || rows.length == 0){
                hideLoading()
                return false
            }
        
            rows.forEach(function (row) {
                draft = Object.assign(row)
                arrayVal.push(Object.values(draft))
                });
        
                if (arrayVal == null) {
                return false;
                }
        
                backgroundAuto = false;
                modeJSON = true;
        
                var opt = {
                scriptPath: process.cwd()+LOCATION_UNPACK+'\\_engine\\HG\\',
                pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
                args: [backgroundAuto, modeJSON],
                };
        
                var pyshell = new child.PythonShell("Kwdw.py", opt);
        
                pyshell.send(JSON.stringify(arrayVal), { mode: 'json' });
                
                pyshell.on("message", (results) => {
                    hideLoading();
                });
        
                pyshell.end((err) => {
                if (err) {
                    hideLoading();
                    Swal.fire({
                    icon: "error",
                    title: "Oops!",
                    text: "Error reason:" + err.message,
                    });
                    throw err;
                }
            });
            
        });
        document.getElementById('mainCheck').checked = false;
        Array.from(document.querySelectorAll('.subcheckbox:checked')).forEach(e => e.checked = false)  
    },

    forwardEmails: function(automate=false){ 
        child.countdownStartDebounce("forwardEmailStart")
        child.Slaninka()
        if(automate==true){
            self_forward.runForwarding([],"","",true,false, null)
            return false
        }
        var newArray = []
        if($("#forwardEmail").val() != '' && $("#forwardEmail").val() != undefined && $("#forwardEmail").val() != null){
            var forwardEmail = $("#forwardEmail").val().toLowerCase()
        }else{
            var forwardEmail = ''
        }

        var forwardCustomProxy = null
        if (document.getElementById('specificPRoxy').checked){
            forwardCustomProxy =  $("#forwardProxyCustom").val().trim()
            forwardCustomProxyChecker = forwardCustomProxy.split(":")
            if(forwardCustomProxyChecker.length != 4 && forwardCustomProxyChecker.length != 2){
                Swal.fire({
                    icon: "error",
                    title: "Oops!",
                    text: "You are trying to use an embedded proxy with a bad or unsupported format. Please check the recommended format."
                });
                return false
            }
        }

        var forwardPass = $("#forwardPassEmail").val()
        var arrayPrep = []
        var data = ''


        if(forwardEmail == ''){
            Swal.fire({
                icon: 'info',
                title: 'Oops!',
                text: 'First enter forward email address to which the emails will be sent'
            })
            // document.getElementById(`email-process-${emailIdCheck}`).innerHTML = `<p id="email-process-${emailIdCheck}" class="exec-button" style="cursor: pointer;" onclick="forward.forwardFromPlay(event,'${emailIdCheck}')"><i id="exec-icon-${emailIdCheck}" class="m-r-10 mdi mdi-play-circle" ></i></p>`
            return false
        }
        else if(forwardPass == ''){
            Swal.fire({
                icon: 'info',
                title: 'Oops!',
                text: 'First enter password for forward email'
            })
            //document.getElementById(`email-process-${emailIdCheck}`).innerHTML = `<p id="email-process-${emailIdCheck}" class="exec-button" style="cursor: pointer;" onclick="forward.forwardFromPlay(event,'${emailIdCheck}')"><i id="exec-icon-${emailIdCheck}" class="m-r-10 mdi mdi-play-circle" ></i></p>`
            return false
        }

        if(/^([A-Za-z0-9_\-\.])+\@([gmail|GMAIL])+\.(com)$/.test(forwardEmail) == false){
            Swal.fire({
                icon: 'error',
                title: 'ONLY GMAIL',
                text: 'Please enter valid GMAIL address'
            })
            //document.getElementById(`email-process-${emailIdCheck}`).innerHTML = `<p id="email-process-${emailIdCheck}" class="exec-button" style="cursor: pointer;" onclick="forward.forwardFromPlay(event,'${emailIdCheck}')"><i id="exec-icon-${emailIdCheck}" class="m-r-10 mdi mdi-play-circle" ></i></p>`
            return false
        }

        var data = [...document.querySelectorAll('.subcheckbox:checked')].map(e => e.value);
        data = data.join()    

        if(data == ""){
            child.db.all(`SELECT id FROM EMAILS WHERE id NOT IN (select email_id from FORWARDING) and status = 'registered'`, function(err, rows) { 
                if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }
                var data = [...rows].map(e => e.id);
                data = data.join() 
                arrayPrep = data.split(",")

                if(arrayPrep.length == 0){
                    Swal.fire({
                        icon: 'info',
                        title: 'Oops!',
                        text: 'No free emails for registrating'
                    })
                    return ""
                }
                
                self_forward.runForwarding(arrayPrep,forwardEmail,forwardPass,false,false, forwardCustomProxy)
            });
        }
        else{
            arrayPrep = data.split(",")
            if(arrayPrep.length == 0){
                Swal.fire({
                    icon: 'info',
                    title: 'Oops!',
                    text: 'No free emails for registrating'
                })
                return ""
            }
            self_forward.runForwarding(arrayPrep,forwardEmail,forwardPass,false,true,forwardCustomProxy)
        }
        document.getElementById('mainCheck').checked = false;
        Array.from(document.querySelectorAll('.subcheckbox:checked')).forEach(e => e.checked = false)    
    },

    runForwarding: function(arrayPrep,forwardEmail,forwardPass,automate,userEvent, forwardCustomProxy){
        var aktualCas = Moment().tz('Europe/Vienna').add(3, 'minutes').unix()
        var zamokkk = 'jopaIsKing/'+aktualCas
        var uvidimeVyskusmae = encryptText('SecretKey', zamokkk);
        child.db.all(`
        SELECT	
            count(*) as total
        FROM (
            SELECT 
                id_email,
                sessions_type
            FROM WINDOWS 
            WHERE 1=1
                and status_id = '1'
            union
            SELECT
                email_id,
                'FARMING' as session
            FROM FARMING 
            WHERE 1=1
                and status in ('PROCESSING FARMING','INITIALIZING PROCESS')
            union
            SELECT
                email_id,
                'FORWARDING' as session
            FROM FORWARDING
            WHERE 1=1
                and status in ('PROCESSING FORWARDING','INITIALIZING PROCESS')
            union 
            SELECT
                id,
                'REGISTERING' as session
            FROM EMAILS
            WHERE 1=1
                and status in ('registering','initializing process')
        )
        `, function(err, rows) { 
            if(rows != undefined && rows != null && rows.length > 0){
                if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }
                var actualWindowsExecVal = rows[0].total
            }
            else{
                var actualWindowsExecVal = 5
            }
            
            // useSameProxyVal = $('#use-proxy-forward:checked').length ? 1 : 0

            //Zmena proxy zaciatok
            proxyGroupVal = document.getElementById('selectProxyGroupChange').value

            if(automate == false && proxyGroupVal != ''){
            child.changeProxy(arrayPrep,'selectProxyGroupChange')
            }

            if(proxyGroupVal != '' && automate == false){
            setTimeout(() => {
                arrayPrep = window.finalEmailListProxyChange

                if(arrayPrep.length == 0){
                    Swal.fire({
                        icon: 'info',
                        title: 'Oops..',
                        text: 'Not enough proxies. Import new or try another.' 
                    })
                    forward_data.refreshForwardingTable()
                    return false 
                }
            }, 3000)
            }
            //Zmena proxy koniec

            if(arrayPrep.length == 0){
                if(automate == false){
                    Swal.fire({
                        icon: 'info',
                        title: 'Oops..',
                        text: 'You have no emails for forwarding.' 
                    })
                    forward_data.refreshForwardingTable()
                }
            }

            if(actualWindowsExecVal > $(".page-wrapper").data("maxwindowval")){
                if($('#maxWindowsSwalError').length){
                    return false
                }
                if(automate == false){
                  Swal.fire({
                    icon: 'error',
                    title: 'Maximum number of processes reached',
                    html: `You have reached the maximum number of processes running at one time. The remaining processes have been added to the waiting tasks.
                          If you want a different maximum number of processes, you can change it in the settings or click here: <a id="maxWindowsSwalError" onclick="loader.loadNewPage(this,'nastav.html','','nastav');"href="javascript:void(0)">Settings</a>`, 
                  })
                  
                  for (let rowX = 0; rowX < arrayPrep.length; rowX++) {
                    child.db.all(`SELECT id FROM WAITING WHERE id_email = ${arrayPrep[rowX]} and sessions_type = 'FORWARDING'`, function(err, rows) { 
                        if (err) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops..',
                                text: 'Error reason: ' + err.message
                            })
                        }
                      
                        if(rows == undefined || rows == null || rows.length == 0){
                          var now = new Date()
                          let data_window = [now, arrayPrep[rowX] ,"FORWARDING",forwardEmail,forwardPass,forwardCustomProxy]
                          let sql_window = `INSERT INTO WAITING (created, id_email, sessions_type, forward_email, forward_email_password, forward_proxy) values(?,?,?,?,?,?) `
                          child.db.run(sql_window,data_window,function(err){
                            //if (err) {
                              // console.log(err.message)
                            //}
                            //farm_data.refreshFORWARDINGTable()
                          })
                        }
      
                      
                    })
                  }
        
                  return 
                }
                else{
                  return
                }
            }

            child.db.all(`SELECT id_email FROM WAITING WHERE sessions_type = 'FORWARDING' order by created`, function (err, rows) {
                if (err) {
                  Swal.fire({
                    icon: "error",
                    title: "Oops..",
                    text: "Error reason: " + err.message,
                  });
                }
                
                if(!userEvent){
                    if(rows != undefined && rows != null && rows.length > 0){
                    //arrayPrep = [...rows,...arrayPrep]
                    var dataWait = [...rows].map((e) => e.id_email);
                    dataWait = dataWait.join();
                    arrayPrepWait = dataWait.split(",");
                    arrayPrep = child.uniq_fast(arrayPrepWait.concat(arrayPrep))
                    }
                }
      
                if(arrayPrep.length == 0){
                    Swal.fire({
                        icon: 'info',
                        title: 'Oops..',
                        text: 'You have no emails for forwarding.' 
                    })
                }

                    for (let row = 0; row < arrayPrep.length; row++) {
                        
                        //if(automate==true && row >= $(".page-wrapper").data("maxwindowval")){ return false }
                        if(arrayPrep[row].length == 0){ 
                            Swal.fire({
                                icon: 'info',
                                title: 'Oops..',
                                text: "You have no emails for forwarding. Let's see if we have any registering tasks.."
                            })
                            email.runRegistration([],true,false)
                            return false 
                        }

                        if(actualWindowsExecVal+1 > $(".page-wrapper").data("maxwindowval") || row+1 >= $(".page-wrapper").data("maxwindowval")){
                            if($('#maxWindowsSwalError').length == 0 && automate == false){                      
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Maximum number of processes reached',
                                    html: `You have reached the maximum number of processes running at one time. The remaining processes have been added to the waiting tasks. 
                                        If you want a different maximum number of processes, you can change it in the settings or click here: <a id="maxWindowsSwalError" onclick="loader.loadNewPage(this,'nastav.html','','nastav');"href="javascript:void(0)">Settings</a>`, 
                                })
                            }

                            for (let rowX = row; rowX < arrayPrep.length; rowX++) {
                                if(rowX > 101){
                                    return 
                                }
                                
                                child.db.all(`SELECT id FROM WAITING WHERE id_email = ${arrayPrep[rowX]} and sessions_type = 'FORWARDING'`, function(err, rows) { 
                                if (err) {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops..',
                                        text: 'Error reason: ' + err.message
                                    })
                                }
                                if(rows == undefined || rows == null || rows.length == 0){
                                    var now = new Date();
                                    
                                    let data_window = [now, arrayPrep[rowX] ,"FORWARDING",forwardEmail,forwardPass, forwardCustomProxy]
                                    let sql_window = `INSERT INTO WAITING (created, id_email, sessions_type, forward_email, forward_email_password,forward_proxy) values(?,?,?,?,?,?) `
                                    
                                    child.db.run(sql_window,data_window,function(err){
                                        //if (err) {
                                        // console.log(err.message)
                                        //}
                                        //forward_data.refreshForwardingTable()
                                    })
                                }
                                })
                                
                            }
                            return false
                        }
            
                        actualWindowsExecVal += 1

                        setTimeout(function timer() {
                            var localProxy = $('#local-proxy:checked').length
                            child.db.all(`SELECT id from WINDOWS WHERE sessions_type in ('FARMING','FORWARDING') and id_email = '${arrayPrep[row]}' and status_id = '1'
                                        union all
                                        SELECT email_id from FORWARDING where status in ('PROCESSING FORWARDING','INITIALIZING PROCESS') and email_id = '${arrayPrep[row]}'`, function(err, rows) { 
                                if(rows == undefined || rows == null || rows.length == 0){
                                    if (err) {
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Oops..',
                                            text: 'Error reason: ' + err.message
                                        })
                                    }

                                    jelen = '0ced6612-79b9-41f9-bb2c-d75b64aae37a'
                                    
                                    if(automate == true){
                                        
                                        child.db.all(`SELECT forward_email, forward_email_password, forward_proxy FROM WAITING WHERE id_email = ${arrayPrep[row]} and sessions_type = 'FORWARDING'`, function(err, rows) { 
                                            if (err) {
                                                Swal.fire({
                                                    icon: 'error',
                                                    title: 'Oops..',
                                                    text: 'Error reason: ' + err.message
                                                })
                                            }
                                        
                                            if(forwardEmail == '' && forwardPass == ''){
                                                forwardEmail = rows[0].forward_email
                                                forwardPass = rows[0].forward_email_password
                                                forward_proxy = rows[0].forward_proxy
                                            }
                                            let sqlWait = `DELETE FROM WAITING WHERE id_email = ${arrayPrep[row]}`;
                                            child.db.run(sqlWait, function (err) {
                                                if (err) {
                                                // Swal.fire({
                                                //   icon: 'error',
                                                //   title: 'Oops..',
                                                //   text: 'Error reason: ' + err.message
                                                // })
                                                }
                                            })
                                            var opt = {
                                                scriptPath : process.cwd()+LOCATION_UNPACK+'\\_engine\\FXW\\',
                                                pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
                                                args : [arrayPrep[row],forwardEmail,forwardPass,jelen,uvidimeVyskusmae, forward_proxy,localProxy]
                                            }
                                            child.taskLogger(arrayPrep[row],"FORWARDING")
                                            var pyshell = new child.PythonShell('efwx.py', opt);

                                            pyshell.on('message', results => {
                                                if(results == 'Error_log'){
                                                    elementErr = document.getElementById(`email-process-${arrayPrep[row]}`)
                                                    if(elementErr != undefined || elementErr != null){
                                                        elementErr.innerHTML = `<p id="email-process-${arrayPrep[row]}" class="exec-button" style="color:#f62d51;"><i id="exec-icon-${arrayPrep[row]}" class="m-r-10 mdi mdi-close-box" ></i></p>`
                                                    }
                                                }else{
                                                    child.db.all(`SELECT status FROM FORWARDING WHERE email_id = '${arrayPrep[row]}'`, function(err, rows) {         
                                                        if (err) {
                                                            Swal.fire({
                                                                icon: 'error',
                                                                title: 'Oops..',
                                                                text: 'Error reason: ' + err.message
                                                            })
                                                        }
                        
                                                        if(rows == undefined || rows == null){
                                                          return false
                                                        }
                        
                                                        if(rows != undefined && rows != null && rows.length > 0){
                                                            if(rows[0].status != 'CANCELED'){
                                                              child.db.all(`SELECT id_email FROM WAITING WHERE sessions_type = 'FORWARDING' order by created`, function (err, rows) {
                                                                if (err) {
                                                                  Swal.fire({
                                                                    icon: "error",
                                                                    title: "Oops..",
                                                                    text: "Error reason: " + err.message,
                                                                  });
                                                                }
                                                          
                                                                if(rows != undefined && rows != null && rows.length > 0){
                                                                  self_forward.forwardEmails(true)
                                                                }
                                                              })
                                                            } 
                                                        }
                                                      })
                                                    //forward_data.refreshForwardingTable()
                                                }
                                                
                                            });
        
                                            pyshell.end(err => {
                                                elementErr = document.getElementById(`email-process-${arrayPrep[row]}`)
                                                if(elementErr != undefined || elementErr != null){
                                                    elementErr.innerHTML = `<p id="email-process-${arrayPrep[row]}" class="exec-button" style="color:#f62d51;"><i id="exec-icon-${arrayPrep[row]}" class="m-r-10 mdi mdi-close-box" ></i></p>`
                                                }
                                                
                                                child.db.all(`SELECT status FROM FORWARDING WHERE email_id = '${arrayPrep[row]}'`, function(err, rows) {         
                                                    if (err) {
                                                        Swal.fire({
                                                            icon: 'error',
                                                            title: 'Oops..',
                                                            text: 'Error reason: ' + err.message
                                                        })
                                                    }

                                                    if(rows != undefined && rows != null && rows.length > 0){
                                                        if(rows[0].status != 'CANCELED'){
                                                          child.db.all(`SELECT id_email FROM WAITING WHERE sessions_type = 'FORWARDING' order by created`, function (err, rows) {
                                                            if (err) {
                                                              Swal.fire({
                                                                icon: "error",
                                                                title: "Oops..",
                                                                text: "Error reason: " + err.message,
                                                              });
                                                            }
                                                            
                                                            if(rows != undefined && rows != null && rows.length > 0){
                                                              self_forward.forwardEmails(true)
                                                            }
                                                          })
                                                        }
                                                    }
                                                  })
                    
                                                // if (err) {
                                                // throw err;
                                                // }
                                            });
                                        })
                                    }
                                    else{
                                        
                                        var opt = {
                                            scriptPath : process.cwd()+LOCATION_UNPACK+'\\_engine\\FXW\\',
                                            pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
                                            args : [arrayPrep[row],forwardEmail,forwardPass,jelen,uvidimeVyskusmae,forwardCustomProxy]
                                        }

                                        var pyshell = new child.PythonShell('efwx.py', opt);
                                        let sqlWait = `DELETE FROM WAITING WHERE id_email = ${arrayPrep[row]}`;
                                        child.db.run(sqlWait, function (err) {
                                            if (err) {
                                            // Swal.fire({
                                            //   icon: 'error',
                                            //   title: 'Oops..',
                                            //   text: 'Error reason: ' + err.message
                                            // })
                                            }
                                        })
                                        pyshell.on('message', results => {
                                            if(results == 'Error_log'){
                                                elementErr = document.getElementById(`email-process-${arrayPrep[row]}`)
                                                if(elementErr != undefined || elementErr != null){
                                                    elementErr.innerHTML = `<p id="email-process-${arrayPrep[row]}" class="exec-button" style="color:#f62d51;"><i id="exec-icon-${arrayPrep[row]}" class="m-r-10 mdi mdi-close-box" ></i></p>`
                                                }
                                            }else{
                                                child.db.all(`SELECT status FROM FORWARDING WHERE email_id = '${arrayPrep[row]}'`, function(err, rows) {         
                                                    if (err) {
                                                        Swal.fire({
                                                            icon: 'error',
                                                            title: 'Oops..',
                                                            text: 'Error reason: ' + err.message
                                                        })
                                                    }
                    
                                                    if(rows == undefined || rows == null){
                                                        return false
                                                    }
                    
                                                    if(rows != undefined && rows != null && rows.length > 0){
                                                        if(rows[0].status != 'CANCELED'){
                                                            child.db.all(`SELECT id_email FROM WAITING WHERE sessions_type = 'FORWARDING' order by created`, function (err, rows) {
                                                            if (err) {
                                                                Swal.fire({
                                                                    icon: "error",
                                                                    title: "Oops..",
                                                                    text: "Error reason: " + err.message,
                                                                });
                                                            }
                                                        
                                                            if(rows != undefined && rows != null && rows.length > 0){
                                                                self_forward.forwardEmails(true)
                                                            }
                                                            })
                                                        } 
                                                    }
                                                    })
                                                //forward_data.refreshForwardingTable()
                                            }
                                            
                                        });
    
                                        pyshell.end(err => {
                                            elementErr = document.getElementById(`email-process-${arrayPrep[row]}`)
                                            if(elementErr != undefined || elementErr != null){
                                                elementErr.innerHTML = `<p id="email-process-${arrayPrep[row]}" class="exec-button" style="color:#f62d51;"><i id="exec-icon-${arrayPrep[row]}" class="m-r-10 mdi mdi-close-box" ></i></p>`
                                            }
                                            
                                            child.db.all(`SELECT status FROM FORWARDING WHERE email_id = '${arrayPrep[row]}'`, function(err, rows) {         
                                                if (err) {
                                                    Swal.fire({
                                                        icon: 'error',
                                                        title: 'Oops..',
                                                        text: 'Error reason: ' + err.message
                                                    })
                                                }
                        
                                                if(rows != undefined && rows != null && rows.length > 0){
                                                    if(rows[0].status != 'CANCELED'){
                                                        child.db.all(`SELECT id_email FROM WAITING WHERE sessions_type = 'FORWARDING' order by created `, function (err, rows) {
                                                        if (err) {
                                                            Swal.fire({
                                                            icon: "error",
                                                            title: "Oops..",
                                                            text: "Error reason: " + err.message,
                                                            });
                                                        }
                                                        
                                                        if(rows != undefined && rows != null && rows.length > 0){
                                                            self_forward.forwardEmails(true)
                                                        }
                                                        })
                                                    }
                                                }
                                                })
                
                                            if (err) {
                                            throw err;
                                            }
                                        });
                                    }
                                    
                                    elementCheck = document.getElementById('mainCheck')
                                    if(elementCheck != undefined || elementCheck != null){
                                        elementCheck.checked = false;
                                    }
                                }
                            })
                        }, row * 6000);
                    }
                })
        })
    },

    showPassword: function() {
        var x = document.getElementById("forwardPassEmail");
        if (x.type === "password") {
        x.type = "text";
        } else {
        x.type = "password";
        }
    },

    forwardFromPlay: function(event, emailId) {
        event.preventDefault();

        if(window.disTimeVal > 0){
            return false
        }

        if(document.querySelectorAll('.loading-play-logo').length >= $(".page-wrapper").data("maxwindowval")){
            Swal.fire({
              icon: "error",
              title: "Too much processes",
              text: "You must wait for the running processes to complete",
            });
            return false
        }
        
        document.getElementById(
        `email-process-${emailId}`
        ).innerHTML = `<img src="${process.cwd()+LOCATION}/cache-data/assets/images/loading.gif" id="email-process-${emailId}" class="play loading-play-logo" data-sec="0" style="height:13px"/ >`;
        
        setTimeout(function () {
            return true
        }, 2000);    

        //console.log($('img[id^="email-process-"].play').length)
        var clearExistsForward= null
            
        var clearExistsForward = setInterval(function () {
        var maxVal = 0;
        $('img[id^="email-process-"].play').each(function () {
            if (
            $(this).attr("id") != `email-process-${emailId}` &&
            maxVal < $(this).data("sec")
            ) {
            maxVal = $(this).data("sec");
            }
        });
    
        if (
            maxVal <= $(`img[id^="email-process-${emailId}"].play`).data("sec")
        ) { 

            var forwardEmail = $("#forwardEmail").val().toLowerCase()
            var forwardPass = $("#forwardPassEmail").val()

            if(forwardEmail == ''){
                Swal.fire({
                    icon: 'info',
                    title: 'Oops!',
                    text: 'First enter forward email address to which the emails will be sent'
                })
                document.getElementById(`email-process-${emailId}`).innerHTML = `<p id="email-process-${emailId}" class="exec-button" style="cursor: pointer;" onclick="forward.forwardFromPlay(event,'${emailId}')"><i id="exec-icon-${emailId}" class="m-r-10 mdi mdi-play-circle" ></i></p>`
                clearInterval(clearExistsForward);
                return false
            }
            else if(forwardPass == ''){
                Swal.fire({
                    icon: 'info',
                    title: 'Oops!',
                    text: 'First enter password for forward email'
                })
                document.getElementById(`email-process-${emailId}`).innerHTML = `<p id="email-process-${emailId}" class="exec-button" style="cursor: pointer;" onclick="forward.forwardFromPlay(event,'${emailId}')"><i id="exec-icon-${emailId}" class="m-r-10 mdi mdi-play-circle" ></i></p>`
                clearInterval(clearExistsForward);
                return false
            }

            if(/^([A-Za-z0-9_\-\.])+\@([gmail|GMAIL])+\.(com)$/.test(forwardEmail) == false){
                Swal.fire({
                    icon: 'error',
                    title: 'ONLY GMAIL',
                    text: 'Please enter valid GMAIL address'
                })
                document.getElementById(`email-process-${emailId}`).innerHTML = `<p id="email-process-${emailId}" class="exec-button" style="cursor: pointer;" onclick="forward.forwardFromPlay(event,'${emailId}')"><i id="exec-icon-${emailId}" class="m-r-10 mdi mdi-play-circle" ></i></p>`
                clearInterval(clearExistsForward);
                return false
            }  

            $(`:checkbox[value=${emailId}]`).prop("checked", "true");

            self_forward.forwardEmails(event);
            clearInterval(clearExistsForward);

            setTimeout(function () {
                return true
            }, 4000);
            
        }
        }, 2000);
    },

    resetAllForwardings: function(){ 
        var sql = `DELETE FROM FORWARDING WHERE status = 'FAILED'`;
        
        child.db.run(sql, function (err) {
          if (err) {
              hideLoading()
              Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'Error reason: ' + err.message
              })
          } 
          hideLoading()
          forward_data.refreshForwardingTable()
        });
        hideLoading()
        document.getElementById('mainCheck').checked = false;
        Array.from(document.querySelectorAll('.subcheckbox:checked')).forEach(e => e.checked = false)  
    },
    
    resetSelectedForwardings: function(){ 
         
        var session_type = 'FORWARDING'
        var newArray = []
        showLoading()
    
        var data = [...document.querySelectorAll('.subcheckbox:checked')].map(e => e.value);
        data = data.join()    
        
        if(data == ''){
          hideLoading()
          return false
        }
    
        arrayPrep = data.split(",")
        conditionVal = '('+data+')'
        
        var sql = `DELETE FROM FORWARDING WHERE status = 'FAILED' and email_id in ${conditionVal}`;
        
        child.db.run(sql, function (err) {
          if (err) {
              hideLoading()
              Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'Error reason: ' + err.message
              })
          } 
          hideLoading()
          forward_data.refreshForwardingTable()
        });
        hideLoading()
        document.getElementById('mainCheck').checked = false;
        Array.from(document.querySelectorAll('.subcheckbox:checked')).forEach(e => e.checked = false)  
    }
}