//Custom functions JAVASCRIPT PROXY
//var path = require("path")
var self_task = module.exports = {
    killAllTasks: function(){ 
        Swal.fire({
            title: `Are you sure you want to DELETE ALL ACTIVE TASKS?`,
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: `Yes, Stop it!`,
        }).then((result) => {
            if (result.isConfirmed) {
                showLoading()

                let sqlWaitAll = `DELETE FROM WAITING`
          
                child.db.run(sqlWaitAll, function (err) {
                    if (err) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops..',
                            text: 'Error reason: ' + err.message
                        })
                    }
                })

                let sqlEmailProcess = `UPDATE EMAILS SET status = 'unregistered' WHERE status in ('registering','initializing process')`
          
                child.db.run(sqlEmailProcess, function (err) {
                    if (err) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops..',
                            text: 'Error reason: ' + err.message
                        })
                    }
                })

                let sqlFarmProcess = `DELETE FROM FARMING WHERE last_farming_date IS NULL AND status in ('PROCESSING FARMING','INITIALIZING PROCESS')`
          
                child.db.run(sqlFarmProcess, function (err) {
                    if (err) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops..',
                            text: 'Error reason: ' + err.message
                        })
                    }
                })
                
                let sqlForwardProcess = `DELETE FROM FORWARDING WHERE forwarding_date IS NULL AND status in ('PROCESSING FARMING','INITIALIZING PROCESS')`
          
                child.db.run(sqlForwardProcess, function (err) {
                    if (err) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops..',
                            text: 'Error reason: ' + err.message
                        })
                    }
                })

                child.db.all(`SELECT DISTINCT 
                                id_email,
                                sessions_type 
                              FROM WINDOWS 
                              WHERE status_id = 1`,
                    function (err, rows) {
                        if (err) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops..',
                                text: 'Error reason: ' + err.message
                            })
                        }

                        rows.forEach(function (row) {
                            var sqlCanc = ''
                            if(row.sessions_type == 'REGISTERING'){
                                    var sqlCanc = `UPDATE EMAILS SET status = 'canceled' WHERE id = ${row.id_email}`;

                                    child.db.run(sqlCanc, function (err) {
                                        if (err) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops..',
                                                text: 'Error reason: ' + err.message
                                            })
                                        }
                                    })     
                            }else if(row.sessions_type == 'FARMING'){
                                child.db.all(`SELECT DISTINCT 
                                    email_id
                                FROM FARMING
                                WHERE email_id = ${row.id_email}`,
                                function (err, rows) {
                                    if (err) {
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Oops..',
                                            text: 'Error reason: ' + err.message
                                        })
                                    }

                                    if(rows != undefined && rows != null && rows.length != 0 ){
                                        var sqlCanc = `UPDATE FARMING SET status = 'CANCELED' WHERE email_id = ${row.id_email}`;
                                    }else{
                                        var sqlCanc = `INSERT INTO FARMING (email_id,status) values(${row.id_email},'CANCELED')`;
                                    }
                                    
                                    child.db.run(sqlCanc, function (err) {
                                        if (err) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops..',
                                                text: 'Error reason: ' + err.message
                                            })
                                        }
                                    })     
                                })

                            }else if(row.sessions_type == 'FORWARDING'){
                                child.db.all(`SELECT DISTINCT 
                                        email_id
                                    FROM FORWARDING
                                    WHERE email_id = ${row.id_email}`,
                                    function (err, rows) {
                                        if (err) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops..',
                                                text: 'Error reason: ' + err.message
                                            })
                                        }

                                        if(rows != undefined && rows != null && rows.length != 0 ){
                                            var sqlCanc = `UPDATE FORWARDING SET status = 'CANCELED' WHERE email_id = ${row.id_email}`;
                                        }else{
                                            var sqlCanc = `INSERT INTO FORWARDING (email_id,status) values(${row.id_email},'CANCELED')`;
                                        }
                                        
                                        child.db.run(sqlCanc, function (err) {
                                            if (err) {
                                                Swal.fire({
                                                    icon: 'error',
                                                    title: 'Oops..',
                                                    text: 'Error reason: ' + err.message
                                                })
                                            }
                                        })     
                                    })    
                            }                      
                            
                        })
                })

                child.db.all(`SELECT DISTINCT id,id_window FROM WINDOWS WHERE date(datetime(date_created/ 1000 , 'unixepoch')) >= date('now')-3`,
                    function (err, rows) {
                    if (err) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops..',
                            text: 'Error reason: ' + err.message
                        })
                    }
                    var arrayVal = []

                    if(rows === undefined || rows.length == 0){
                        hideLoading()
                        return false
                    }

                    rows.forEach(function (row) {
                        draft = Object.assign(row)
                        arrayVal.push(Object.values(draft))
                    });
                    //windows = row["id_window"];
                    backgroundAuto = false;
                    modeJSON = true;
                
                    if (arrayVal == null) {
                        hideLoading();
                        return false;
                    }
                
                    var opt = {
                        scriptPath: process.cwd()+LOCATION_UNPACK+'\\_engine\\HG\\',
                        pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
                        args: [backgroundAuto,modeJSON],
                    };
                
                    var pyshell = new child.PythonShell("Kwdw.py", opt);
                
                    pyshell.send(JSON.stringify(arrayVal), { mode: 'json' });

                    pyshell.on("message", (results) => {
                        task_data.refreshTaskTable()
                        hideLoading();
                    });
                
                    pyshell.end((err) => {
                        
                        if (err) {
                        task_data.refreshTaskTable()
                        hideLoading();
                        Swal.fire({
                            icon: "error",
                            title: "Oops!",
                            text: "Error reason:" + err.message,
                        });
                        throw err;
                        }
                    });
                    }
                );
                document.getElementById('mainCheck').checked = false;
                Array.from(document.querySelectorAll('.subcheckbox:checked')).forEach(e => e.checked = false)  
            }
        })
    },

    killSelectedTasks: function(){ 
        var data = [...document.querySelectorAll('.subcheckbox:checked')].filter(e => e.value != '' && e.value != null && e.value != undefined).map(e => e.value);

        checked_options = [...document.querySelectorAll('.subcheckbox:checked')].map(e => [e.value,e.dataset.session,e.dataset.email]);

        if(data == '' && checked_options == ''){
            return false
        }

        showLoading()
        
        data = data.join()    

        arrayPrep = data.split(",")
        conditionVal = '('+data+')'
        var arrayVal = []

        for (let row = 0; row < checked_options.length; row++) {
            if(checked_options[row][0] == '' || checked_options[row][0] == null || checked_options[row][0] == undefined){
                
                if(checked_options[row][1] == 'REGISTERING'){
                    var sqlCanc = `UPDATE EMAILS SET status = 'canceled' WHERE id = ${checked_options[row][2]}`;
                }else if(checked_options[row][1] == 'FORWARDING'){
                    var sqlCanc = `UPDATE FORWARDING SET status = 'CANCELED' WHERE email_id = ${checked_options[row][2]}`;
                }else if(checked_options[row][1] == 'FARMING'){
                    var sqlCanc = `UPDATE FARMING SET status = 'CANCELED' WHERE email_id = ${checked_options[row][2]}`;
                }
                
                child.db.run(sqlCanc, function (err) {
                    if (err) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops..',
                            text: 'Error reason: ' + err.message
                        })
                    }
                })   
            }
        }

        child.db.all(`SELECT id,id_window,id_email,sessions_type FROM WINDOWS WHERE id in ${conditionVal} ORDER BY id DESC`, function(err, rows) {  
            if (err) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops..',
                    text: 'Error reason: ' + err.message
                })
            }

            if(rows === undefined || rows.length == 0){
                hideLoading()
                return false
            }
            
            rows.forEach(function (row) {
                draft = Object.assign(row)
                arrayVal.push(Object.values(draft))

                var sqlCanc = ''

                if(row.sessions_type == 'REGISTERING'){
                    var sqlCanc = `UPDATE EMAILS SET status = 'canceled' WHERE id = ${row['id_email']}`;

                    child.db.run(sqlCanc, function (err) {
                        if (err) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops..',
                                text: 'Error reason: ' + err.message
                            })
                        }
                    })
                }else if(row.sessions_type == 'FARMING'){
                    child.db.all(`SELECT DISTINCT 
                                email_id
                            FROM FARMING
                            WHERE email_id = ${row['id_email']}`,
                            function (err, rows) {
                                if (err) {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops..',
                                        text: 'Error reason: ' + err.message
                                    })
                                }

                                if(rows != undefined && rows != null && rows.length != 0 ){
                                    var sqlCanc = `UPDATE FARMING SET status = 'CANCELED' WHERE email_id = ${row['id_email']}`;
                                }else{
                                    var sqlCanc = `INSERT INTO FARMING (email_id,status) values(${row['id_email']},'CANCELED')`;
                                }
                                
                                child.db.run(sqlCanc, function (err) {
                                    if (err) {
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Oops..',
                                            text: 'Error reason: ' + err.message
                                        })
                                    }
                                })     
                            })
                }else if(row.sessions_type == 'FORWARDING'){
                    child.db.all(`SELECT DISTINCT 
                            email_id
                        FROM FORWARDING
                        WHERE email_id = ${row['id_email']}`,
                        function (err, rows) {
                            if (err) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops..',
                                    text: 'Error reason: ' + err.message
                                })
                            }

                            if(rows != undefined && rows != null && rows.length != 0 ){
                                var sqlCanc = `UPDATE FORWARDING SET status = 'CANCELED' WHERE email_id = ${row['id_email']}`;
                            }else{
                                var sqlCanc = `INSERT INTO FORWARDING (email_id,status) values(${row['id_email']},'CANCELED')`;
                            }
                            
                            child.db.run(sqlCanc, function (err) {
                                if (err) {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops..',
                                        text: 'Error reason: ' + err.message
                                    })
                                }
                            })     
                        })   
                }
                
                
            });
            
            if (arrayVal == null) {
                hideLoading()
                return false;
            }
            
            backgroundAuto = false;
            modeJSON = true;

            var opt = {
            scriptPath: process.cwd()+LOCATION_UNPACK+'\\_engine\\HG\\',
            pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
            args: [backgroundAuto, modeJSON],
            };
    
            var pyshell = new child.PythonShell("Kwdw.py", opt);
    
            pyshell.send(JSON.stringify(arrayVal), { mode: 'json' });
            
            pyshell.on("message", (results) => {
                task_data.refreshTaskTable()
                hideLoading();
            });
    
            pyshell.end((err) => {
            if (err) {
                task_data.refreshTaskTable()
                hideLoading();
                Swal.fire({
                icon: "error",
                title: "Oops!",
                text: "Error reason:" + err.message,
                });
                throw err;
            }
            });
            
        });
        document.getElementById('mainCheck').checked = false;
        Array.from(document.querySelectorAll('.subcheckbox:checked')).forEach(e => e.checked = false)  
        task_data.refreshTaskTable()
    },
}