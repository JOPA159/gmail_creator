var self_statistic = module.exports = {
    getStatisticData: function(){
        child.db.all(`SELECT max_windows FROM SETTINGS WHERE category = 'WINDOWS' LIMIT 1`, function(err, rows) { 
            if(rows[0].max_windows != undefined || rows[0].max_windows == 0){
                max_windows = rows[0].max_windows   
            }else{
                max_windows = 0
            }

            child.db.all(`SELECT count(*) as total_actual FROM WINDOWS WHERE status_id = '1'`, function(err, rows) { 
                if(rows[0].total_actual != undefined){
                    actual_windows = rows[0].total_actual
                }else{
                    actual_windows = 0
                }

                execution_capacity_ratio = actual_windows/max_windows
                
                $("#window-execution-val-act").text((execution_capacity_ratio*100).toFixed(0)+"%")
                $("#window-execution-val-act-graph").removeClass()
                $("#window-execution-val-act-graph").addClass("c100 red p"+(execution_capacity_ratio*100).toFixed(0))
            })
        })

        child.db.all(`SELECT 
                    count(id) as all_emails,
                    count(case when status = 'registered' then id end) as registered_emails,
                    count(case when status = 'unregistered' then id end) as unregistered,
                    count(case when status != 'registered' or status is null or status != 'register_failed' then id end) as totalemails,
                    count(case when status like 'failed%' then id end) as register_failed
                FROM EMAILS`, 
            function(err, rows) { 
                if(rows[0].registered_emails == 0 || rows[0].all_emails == 0){
                    registered_emails_ratio =  0
                }else{
                    registered_emails_ratio = rows[0].registered_emails/rows[0].all_emails
                }            
                
                $("#email-registered-val-act").text((registered_emails_ratio*100).toFixed(0)+"%")
                $("#email-registered-val-act-graph").removeClass()
                $("#email-registered-val-act-graph").addClass("c100 green p"+(registered_emails_ratio*100).toFixed(0))

                $("#registered-total-val").text(rows[0].registered_emails+" Registered")
                $("#failed-total-val").text(rows[0].register_failed+" Failed")
                $("#totalEmails-total-val").text(rows[0].totalemails+" Total emails")
                $("#unregistered-total-val").text(rows[0].unregistered+" Unregistered")

                child.db.all(`SELECT 
                            count(*) as all_forwarded_emails
                        FROM FORWARDING
                        where status = 'FORWARDED'`, 
                function(err, rows) { 
                    $("#forwarded-total-val").text(rows[0].all_forwarded_emails+" Forwarded")
                })

                child.db.all(`SELECT 
                            count(*) as all_farmed_emails
                        FROM FARMING
                        where status = 'FARMED'`, 
                function(err, rows) { 
                    $("#farmed-total-val").text(rows[0].all_farmed_emails+" Farmed")
                })
        })

        child.db.all(`SELECT 
                    count(case when sessions_type = 'REGISTERING' then id end) as actual_running_registrations,
                    count(case when sessions_type = 'FORWARDING' then id end) as actual_running_forwarding,
                    count(case when sessions_type = 'FARMING' then id end) as actual_running_farmings
                FROM WINDOWS
                WHERE 1=1
                    and status_id = '1'`, 
            function(err, rows) { 
                if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }
  
                if(rows === undefined || rows.length == 0){
                    return false
                }
                
                $("#email-processes-val").text(rows[0].actual_running_registrations+" Registrations")
                $("#forwarding-processes-val").text(rows[0].actual_running_forwarding+" Forwardings")
                $("#farming-processes-val").text(rows[0].actual_running_farmings+" Farmings")
        })

        // child.db.all(`SELECT * FROM SETTINGS WHERE category = 'WINDOWS' LIMIT 1`, function(err, rows) { 
        //     if(rows.length > 0){
        //         var lastMod = new Date(rows[0].updated).toLocaleDateString("de-DE")
        //         if (err) {
        //             Swal.fire({
        //                 icon: 'error',
        //                 title: 'Oops..',
        //                 text: 'Error reason: ' + err.message
        //             })
        //         }
        //         document.getElementById('videosVal').value = rows[0].videos_val
        //         document.getElementById('minVideosVal').value = rows[0].min_videos
        //         document.getElementById('maxVideosVal').value = rows[0].max_videos
        //         document.getElementById('newsVal').value = rows[0].news_val
        //         document.getElementById('minNewsVal').value = rows[0].min_news
        //         document.getElementById('maxNewsVal').value = rows[0].max_news
        //         document.getElementById('itemsVal').value = rows[0].items_val
        //         document.getElementById('items_category_val').value = rows[0].items_category_val
        //         document.getElementById('searchVal').value = rows[0].search_val
        //         document.getElementById('lastMod').innerHTML = lastMod
        //     }
        // });
    },
}

document.addEventListener("newPageLoad", function(){
    self_statistic.getStatisticData()
})