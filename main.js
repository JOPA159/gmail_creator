// Modules to control application life and create native browser window
PRD_MD = 0
BLD_MD = 0

const {app, BrowserWindow, shell, ipcMain} = require('electron')
//global.sharedObj = {appName: "Koala Bot system",PROD_MODE: PRD_MD}
var path = require("path");
var { PythonShell } = require("python-shell");
const { autoUpdater } = require('electron-updater');
const Swal = require('sweetalert2')
const log = require('electron-log');
const checkInternetConnected = require('check-internet-connected');
var sqlite3 = require("sqlite3").verbose();
const {machineId, machineIdSync} = require('node-machine-id');
var dateFormat = require('dateformat');
//const JavaScriptObfuscator = require('javascript-obfuscator');
// var xlsx = require('xlsx')
// console.log(XLSX)
let machine_id = machineIdSync({original: true})
const NodeRSA = require('node-rsa');
var Moment = require('moment-timezone');

let zahada = machine_id

function loggerMesage(type,message = '',procType = '',identifier = ''){
  var dateVal = dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss,sss").slice(0, -1);
  var separator = '#~#! '
  var lineseparator = '#~~#'

  var e = new Error();
  var regex = /[^/]*$/
  var match = regex.exec(e.stack.split("\n")[2]);
  var locationVal = match[0].split(':')
  var locationValFinal = {
    filepath: locationVal[0],
    line: locationVal[1],
    column: locationVal[2]
  };

  if(type == 'error'){
    fs.appendFile(app.getPath('userData')+"\\error.log",dateVal + separator+ '[' +identifier + ']' + separator + 'ERROR' + separator + 'ON LINE:' + locationValFinal['column'] + separator + message + separator + 'FILE:{' + 'Not defined FILE' + '}' + lineseparator + '\n',function(err){

    })
  }
  else if(type == 'process'){
    procLogFile.once('open', function(fd) {
      procLogFile.write(dateVal + separator+ '[' +identifier + ']' + separator + 'PROCESS' + separator + procType + separator + 'Message:' + message + lineseparator + '\n');
      procLogFile.end();
    });
  }
}

process.on('uncaughtException', function (err) {
  loggerMesage('error',err.message,'SYSTEM PROCESS','SYSTEM ERROR')
  //app.quit()
})

const OrolSukromie = 'MIICXQIBAAKBgQCWQ0hZMvlS2RLx0eMdSe1oWk63+5qnA9f89bJyRR50SNrk3TuI0DMHXIyKcqw4VWZq5sNxuwWGt6h5CqW4Ia+KE0qPqR37nAv2FK1rwZppLW8Pr7MwnGJ08BhcnXWAUHlnpruVZPMd7WxS01cfvCU9EA7WqKlLuZN5IdsiBmn4+QIDAQABAoGACNv+sAOpX5Ec5NUTJu7s6HIqof5bp+06FtgFpWcd3I1PGp9Qm0hrOcvjHWaVcNzYJQVmEJaORNsfO8O88ZZK2ynyWULYc2/Pp5M6frzz4orB7tpWxmhp805XvVOJGdhzkU497dzkQK6Vf+zT4EWvOmvoM27T/ENJRLLNpeO9luECQQDbTnlUcQJ/29AJDV/AJYnjzrOcaZllyrkW4PqFYi5fKOS7AzUFpgmjmP1yWnRyYI6GlDSanu2puKv1vv/CwfNPAkEAr2d4Clv+ZJiadX3+yrDMS/ri8pVVi19OHWMhvpstzGJqa4GDKrBal9vpoWXOKoe47gJcr7w/5iWJD1VUhjVdNwJBAKmNwXaMrP8Hho8JHQdkvng3flwmtKejlXhf1rC7WNjPuA/yidLlLTyj8wevcapghrD78rO8vkji82hbFEiSRTcCQG79rCnx+FGzdtmt9pJPuZbClOUntc/RaN6TogC4tQxFok2Q0G0s1saNM5Mblo+2/kAtYnUZS93YFpiLSEb8cZUCQQClR51CrqxOY25q3rHTiFbTeBnd774DnWDN1j0omOCk+eUiDHsjNXv7IT4J/yafG4k3Bfl1ErkCugklc/QBEpDC'
//autoUpdater.autoDownload = false;
autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = 'info';

// ipcMain.on("msg",(event,data)=>{
//   console.log(data)
//   event.sender.send("reply","FUNGUJE TOOOOOOOO")
// })

autoUpdater.on('update-available', (event,data) => {
  mainWindow.webContents.send('update_available');
  //autoUpdater.downloadUpdate();
});

autoUpdater.on('update-downloaded', (event,data) => {
  mainWindow.webContents.send('update_downloaded');
});

ipcMain.on('restart_app', (event,data) => {
  autoUpdater.quitAndInstall();
});

// ipcMain.on('svina_check', (event,data) => {
//     svina_data = data

//     var now = new Date();
      
//     let data_svin = [now.toLocaleDateString("de-DE"), svina_data]
//     let sql_svin = `INSERT OR REPLACE INTO OROL (created, vrabec) values(?,?)`

//     db.run(sql_svin,data_svin,function(err){
//       if (err) {
//         // console.log(err.message)
//       }
//     })
//     //sem bude treba dorobit funkcionalitu na pracu s licenciou
// });

if(BLD_MD){
  PATH_BUILD_ASAR = '\\resources\\app.asar'
  PATH_BUILD_UNPACK = '\\resources\\app.asar.unpacked'
  global.sharedObj = {LOCATION: PATH_BUILD_ASAR, LOCATION_UNPACK: PATH_BUILD_UNPACK,appName: "Koala Bot system",PROD_MODE: PRD_MD}
}
else{
  PATH_BUILD_ASAR = ''
  PATH_BUILD_UNPACK = ''
  global.sharedObj = {LOCATION: PATH_BUILD_ASAR, LOCATION_UNPACK: PATH_BUILD_UNPACK,appName: "Koala Bot system",PROD_MODE: PRD_MD}
}

//var dbPath = process.cwd()+PATH_BUILD_UNPACK+'\\_DB\\DBemails.db';
var dbPath = app.getPath('userData')+'\\DBemails.db'
var db = new sqlite3.Database(dbPath);

function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

var fs = require('fs');
var dir = app.getPath('userData')+'\\tmp';

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow () {
  // Create the browser window.

  mainWindow = new BrowserWindow({
    width: 1200,
    height: 800,
    minWidth: 960,
    minHeight: 680,
    show: false,
    frame: false,
    icon: process.cwd()+PATH_BUILD_ASAR+'\\_images\\logo-app.png',
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
      disableBlinkFeatures: "Auxclick"
      //devTools: false
    }
  })
  mainWindow.setBackgroundColor('#842448')
  //mainWindow.webContents.openDevTools()

  // and load the cache-i.html of the app.
  //mainWindow.loadFile('cache-i.html')
  //mainWindow.loadFile('./cache-data/dtt/ltr/cache-i.html')
  // Open the DevTools.
  //mainWindow.webContents.openDevTools()
  
  mainWindow.on('show', ()=>{
      setTimeout(() => {
        mainWindow.setOpacity(1);
  }, 200);
  })

  mainWindow.on('hide', () => {
    mainWindow.setOpacity(0);
  });

  mainWindow.once('ready-to-show',() => {
      mainWindow.show()
  })

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    //mainWindow = null
    //ArchivaciaPrehliadac()
    NulovanieStatusov()
    let sqlProx = `DELETE FROM WAITING`;
    db.run(sqlProx, function (err) {})

     db.all(
      //`SELECT DISTINCT id,id_window FROM WINDOWS WHERE status_id = '1' and sessions_type = 'REGISTERING'`,
      `SELECT DISTINCT id,id_window,date_created FROM WINDOWS `,
      function (err, rows) {
        if (err) {
          app.quit()
        }
        var arrayVal = []
        
        if(rows === undefined || rows.length == 0){
          return false
        }

        rows.forEach(function (row) {
          draft = Object.assign(row)
          arrayVal.push(Object.values(draft))

          var timestampSevenDays = new Date().getTime() - (7 * 24 * 60 * 60 * 1000)
          var windowDate = new Date(row['date_created']).getTime()

          if(windowDate < timestampSevenDays){
            let sqlProx = `DELETE FROM WINDOWS WHERE id = ${row['id']}`;
             db.run(sqlProx, function (err) {
              if (err) {
                // Swal.fire({
                //   icon: 'error',
                //   title: 'Oops..',
                //   text: 'Error reason: ' + err.message
                // })
              }
            })
          }
          
        });
        //windows = row["id_window"];
        backgroundAuto = false;
        modeJSON = true;
  
        if (arrayVal == null) {
          return false;
        }
  
        var opt = {
          scriptPath: process.cwd()+PATH_BUILD_UNPACK+'\\_engine\\HG\\',
          pythonPath: process.cwd()+PATH_BUILD_UNPACK+'\\org_sys\\Scripts\\python.exe', 
          args: [backgroundAuto,modeJSON],
        };
  
        var pyshell = new PythonShell("Kwdw.py", opt);
  
        pyshell.send(JSON.stringify(arrayVal), { mode: 'json' });
  
        pyshell.on("message", (results) => {
            if(results == 'KILL_OK'){
                var opt = {
                  scriptPath: process.cwd()+PATH_BUILD_UNPACK+'\\_engine\\HG\\',
                  pythonPath: process.cwd()+PATH_BUILD_UNPACK+'\\org_sys\\Scripts\\python.exe', 
                };
          
                var pyshell = new PythonShell("CTMP.py", opt);
          
                pyshell.on("message", (results) => {
                    if(results == 'CLEARED_TMP'){
                        mainWindow = null
                    }
                });
          
                pyshell.end((err) => {
                  mainWindow = null
                });
            }
        });
  
        pyshell.end((err) => {
          mainWindow = null
        });
      }
    );

  })
}

function NulovanieStatusov(){
  db.all(`
      with t0 as (
        SELECT	
          id as id_email,
          created,
          email,
          session as sessions_type,
          upper(status) as status
        FROM (
          SELECT
            a.*,
            row_number() over (partition by email||session order by session) rn
          FROM (
              SELECT
                '',
                b.created,
                b.email, 
                'FARMING' as session,
                b.status,
                b.id
              FROM FARMING a
              LEFT JOIN EMAILS b on a.email_id = b.id
              WHERE 1=1
                and a.status in ('PROCESSING FARMING','INITIALIZING PROCESS')
            union
              SELECT
                '',
                b.created,
                b.email, 
                'FORWARDING' as session,
                b.status,
                b.id
              FROM FORWARDING a
              LEFT JOIN EMAILS b on a.email_id = b.id
              WHERE 1=1
                and a.status in ('PROCESSING FORWARDING','INITIALIZING PROCESS')
            union 
              SELECT
                '',
                created,
                email,
                'REGISTERING' as session,
                status,
                id
              FROM EMAILS
              WHERE 1=1
                and status in ('registering','initializing process')
            ) a 
          )
          WHERE rn = 1
          ORDER BY id
      ), t1 as (
          select 
            a.*,
            b.created as created_session,
            c.date_created
          from t0 a
          left join LOGS b on a.id_email = b.id_email		
                  and a.sessions_type = b.sessions_type
          left join (
                  SELECT	
                    id_email,
                    sessions_type,
                    max(date_created) as date_created
                  FROM WINDOWS
                  WHERE status_id = 1
                  GROUP BY
                    id_email,
                    sessions_type
                )c on a.id_email = c.id_email		
                and a.sessions_type = c.sessions_type
      )
      SELECT	
        a.id_email,
        a.sessions_type
      from t1 a
      WHERE 1=1
        and datetime(a.created_session/1000, 'unixepoch','localtime') < datetime(datetime(),'-3 minutes','localtime')
        and a.date_created is null
      `, function (err,rows) {
        // if (err) {
        //     Swal.fire({
        //         icon: 'error',
        //         title: 'Oops..',
        //         text: 'Error reason: ' + err.message
        //     })
        // }

        if(rows === undefined || rows.length == 0){
          return false
        }
        
        rows.forEach(function (row) {
            if(row.sessions_type == 'REGISTERING'){
              let sqlReg = `UPDATE EMAILS SET status = 'unregistered' WHERE id = ${row.id_email}`;

              db.run(sqlReg, function (err) {

              })
            } 
            else if(row.sessions_type == 'FORWARDING'){
              let sqlForward = `UPDATE FORWARDING SET status = 'FAILED' WHERE email_id = ${row.id_email}`;

              db.run(sqlForward, function (err) {

              })
            }
            else if(row.sessions_type == 'FARMING'){
              let sqlFarm = `UPDATE FARMING SET status = 'FAILED' WHERE email_id = ${row.id_email}`;

              db.run(sqlFarm, function (err) {

              })
            }
        })
      });
}


// function ArchivaciaPrehliadac(){
//   var fs = require('fs')
//   var zipFolder = require('zip-folder')
//   var parentfoldNamePath = app.getPath('userData')+'\\Google\\'
//   var foldNamePath = app.getPath('userData')+'\\Google\\Chrome\\'

//   if (!fs.existsSync(parentfoldNamePath)){
//     fs.mkdirSync(parentfoldNamePath);
//   }

//   if (!fs.existsSync(foldNamePath)){
//     fs.mkdirSync(foldNamePath);
//   }

//   fs.readdir(foldNamePath, (err, files) => {
//     files.forEach(file => {
//       if(fs.existsSync(foldNamePath+file)){
//         if(fs.lstatSync(foldNamePath+file).isDirectory()){
//           if (fs.existsSync(foldNamePath+file+'.zip')) {
//             //fs.unlinkSync(foldNamePath+file+'.zip')
//             // zipFolder(foldNamePath+file, foldNamePath+file+'.zip', function(err) {
//             //   if(err) {
//             //   } else {
//             //       fs.rmdirSync(foldNamePath+file)
//             //   }
//             // });
//           }else{
//             zipFolder(foldNamePath+file, foldNamePath+file+'.zip', function(err) {
//               if(err) {
//               } else {
//                   fs.rmdirSync(foldNamePath+file)
//               }
//             });
//           }
//         }
//       }
//     });
//   });
// }

function updateLucky() {
  db.all("SELECT count(*) as total,count(distinct case when system in ('system1','system3','system4') then system end) as system_count FROM COUNTRY_LUCKY", function(err, rows) {  

    if(Object.values(rows)[0].total == 0 || Object.values(rows)[0].system_count < 3){
      var opt = {
        scriptPath: process.cwd()+PATH_BUILD_UNPACK+'\\_engine\\win_prov\\Country\\',
        pythonPath: process.cwd()+PATH_BUILD_UNPACK+'\\org_sys\\Scripts\\python.exe', 
        //args : [recoveryEmail]
      };

      var pyshell = new PythonShell(`l_s_cntr.py`, opt);

      pyshell.on("message", (results) => {
      });

      pyshell.end((err) => {
        if (err) {
          Swal.fire({
            icon: "error",
            title: "Oops!",
            text: "Error reason:" + err.message,
          });
          db.all(`SELECT id FROM COUNTRY_LUCKY`, function(err, rows) {  
            if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }

            if(rows === undefined || rows.length == 0){
                var sql  = `DELETE FROM PROVIDERS WHERE id = 'LUCKYSMS'`
          
                db.run(sql, function(err) {
                    if (err) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops..',
                                text: 'Error reason: ' + err.message
                            })
                        }
                })
                return false
            }
          })

          throw err;
        }
        return "";
      });
    }  
  })
  
}

function updateCountry(){
  db.all("SELECT count(*) as total FROM COUNTRY", function(err, rows) {  

    if(Object.values(rows)[0].total == 0 || Object.values(rows)[0].total <= 240){
      var opt = {
        scriptPath: process.cwd()+PATH_BUILD_UNPACK+'\\_engine\\win_prov\\Country\\',
        pythonPath: process.cwd()+PATH_BUILD_UNPACK+'\\org_sys\\Scripts\\python.exe', 
      };

      var pyshell = new PythonShell("gl_cn.py", opt);

      pyshell.on("message", (results) => {
        //console.log(results)
      });

      pyshell.end((err) => {
        if (err) {
          Swal.fire({
            icon: "error",
            title: "Oops!",
            text: "Error reason:" + err.message,
          });
          db.all(`SELECT id FROM COUNTRY`, function(err, rows) {  
            if (err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops..',
                        text: 'Error reason: ' + err.message
                    })
                }

            if(rows === undefined || rows.length == 0){
                var sql  = `DELETE FROM PROVIDERS WHERE id != 'LUCKYSMS'`
          
                db.run(sql, function(err) {
                    if (err) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops..',
                                text: 'Error reason: ' + err.message
                            })
                        }
                })
                return false
            }
          })

          throw err;
        }
      });
    }
  })
}

function migrationCheck(){
  db.all(`SELECT COUNT(*) AS COLUMN_CHECK FROM pragma_table_info('PROXIES') WHERE name='proxy_group'`, function(err, rows) {  
    if(rows[0]['COLUMN_CHECK']==0){
      db.exec('ALTER TABLE PROXIES ADD COLUMN proxy_group TEXT')
    } 
    db.exec("UPDATE PROXIES SET proxy_group = 'DEFAULT GROUP' where proxy_group is null")
  })
}

function tableExistCheckColumn(){
  db.all(`SELECT COUNT(*) AS COLUMN_CHECK_FW FROM pragma_table_info('FORWARDING') WHERE name='forward_proxy'`, function(err, rows) {  
    if(rows[0]['COLUMN_CHECK_FW']==0){
      db.exec('ALTER table FORWARDING add column forward_proxy text');
    } 
  })

  db.all(`SELECT COUNT(*) AS COLUMN_CHECK_W FROM pragma_table_info('WAITING') WHERE name='forward_proxy'`, function(err, rows) {  
    if(rows[0]['COLUMN_CHECK_W']==0){
      db.exec('ALTER table WAITING add column forward_proxy text');
    } 
  })
}

function tableExistCheck(){
     db.exec('CREATE TABLE IF NOT EXISTS EMAILS (id INTEGER PRIMARY KEY AUTOINCREMENT,created date, email text, password text, firstName text, lastName text, day text, month text, year text, gender text, telephoneNumber text, country text, smsProviderId integer,  gmailCode text, proxyId integer,status text,scoreV3 text, scoreV2 boolean, recoveryId integer,registered_at date  )');
     db.exec('CREATE TABLE IF NOT EXISTS FARMING (id INTEGER PRIMARY KEY AUTOINCREMENT,email_id integer, last_farming_date date, total_farming integer, last_farming integer,status text )');
     db.exec('CREATE TABLE IF NOT EXISTS FORWARDING (id INTEGER PRIMARY KEY AUTOINCREMENT,forwarding_date date,email_id integer, forward_email text, status text )');
     db.exec('CREATE TABLE IF NOT EXISTS PROVIDERS (id INTEGER PRIMARY KEY AUTOINCREMENT, provider TEXT, key TEXT, email_secret TEXT, timeout TEXT, balance TEXT, created_at DATE, updated_at DATE)');
     db.exec('CREATE TABLE IF NOT EXISTS PROXIES (id INTEGER PRIMARY KEY AUTOINCREMENT, date_created date, hostname text, port text, username text, password text, id_email text, date_email date, proxy_group text )');
     db.exec('CREATE TABLE IF NOT EXISTS RECOVERY_DOMAIN (id INTEGER PRIMARY KEY AUTOINCREMENT, created date, recovery_domain text )');
     db.exec('CREATE TABLE IF NOT EXISTS WINDOWS (id INTEGER PRIMARY KEY AUTOINCREMENT, date_created date, id_window text, id_email text, sessions_type text, status_id text )');
     db.exec('CREATE TABLE IF NOT EXISTS SETTINGS (id INTEGER PRIMARY KEY AUTOINCREMENT,updated date,category text UNIQUE, videos_val integer,min_videos integer,max_videos integer,news_val integer,min_news integer,max_news integer,items_val integer,items_category_val integer,search_val integer,max_windows integer,value_main text )');
     db.exec('CREATE TABLE IF NOT EXISTS COUNTRY_LUCKY (id INTEGER PRIMARY KEY AUTOINCREMENT,created date, updated date, system text, name text, fullname text )');
     db.exec('CREATE TABLE IF NOT EXISTS COUNTRY (id INTEGER PRIMARY KEY AUTOINCREMENT,created date, updated date, name text, fullname text, prefix text )');
     db.exec('CREATE TABLE IF NOT EXISTS WAITING (id INTEGER PRIMARY KEY AUTOINCREMENT,created date, id_email text, sessions_type text, forward_email text, forward_email_password text)');
     db.exec('CREATE TABLE IF NOT EXISTS LOGS (id INTEGER PRIMARY KEY AUTOINCREMENT,created date, id_email text, sessions_type text )');
    }

function defaultSettings(){
   db.all("SELECT count(*) as total FROM SETTINGS WHERE category ='FARMING'", function(err, rows) {  
    if (err) {
        app.quit()
    }

    if(Object.values(rows)[0].total == 0){
      
      var videosVal = 2
      var minVideos = 120
      var maxVideos = 300
      var newsVal = 2
      var minNews = 120
      var maxNews = 300
      var itemsVal = 3
      var items_category_val = 4
      var searchVal = 4

      var now = new Date();

      let data = [now.toLocaleDateString("de-DE"), 'FARMING',videosVal,minVideos,maxVideos,newsVal,minNews,maxNews,itemsVal,items_category_val,searchVal]
      let sql = `INSERT OR REPLACE INTO SETTINGS (updated, category, videos_val, min_videos, max_videos, news_val, min_news, max_news, items_val, items_category_val, search_val) values(?,?,?,?,?,?,?,?,?,?,?) `
      
       db.run(sql,data,function(err){
        if (err) {
          // console.log(err.message)
        }   
      })
    }
  });  
  
   db.all(`SELECT 
            count(case when category ='WINDOWS' then id end) as total_windows, 
            count(case when category ='RECOVERY' then id end) as total_recovery,
            count(case when category ='NAME_MAX_NUM' then id end) as total_recovery
          FROM SETTINGS`, function(err, rows) {  
    if (err) {
        app.quit()
    }

    if(Object.values(rows)[0].total_windows == 0){

      var now = new Date();
      
      let data_window = [now.toLocaleDateString("de-DE"), "WINDOWS",5]
      let sql_window = `INSERT OR REPLACE INTO SETTINGS (updated,category, max_windows) values(?,?,?) `

       db.run(sql_window,data_window,function(err){
        if (err) {
          // console.log(err.message)
        }
      })
    }

    if(Object.values(rows)[0].total_recovery == 0){

      var now = new Date();
      
      let data_window = [now.toLocaleDateString("de-DE"), "RECOVERY",""]
      let sql_window = `INSERT OR REPLACE INTO SETTINGS (updated,category, value_main) values(?,?,?) `

       db.run(sql_window,data_window,function(err){
        if (err) {
          // console.log(err.message)
        }
      })
    }

    if(Object.values(rows)[0].total_recovery == 0){

      var now = new Date();
      
      let data_window = [now.toLocaleDateString("de-DE"), "NAME_MAX_NUM","4"]
      let sql_window = `INSERT OR REPLACE INTO SETTINGS (updated,category, value_main) values(?,?,?) `

       db.run(sql_window,data_window,function(err){
        if (err) {
          // console.log(err.message)
        }
      })
    }

  }); 
}

// autoUpdater.on('update-downloaded', () => {
//     autoUpdater.quitAndInstall()
// });

// autoUpdater.on('update-available', (ev, info) => {
//     //Console.log("SSSSSSS")
// });

// let loading

// autoUpdater.on('download-progress', (progressObj) => {
//   let log_message = "Download speed: " + progressObj.bytesPerSecond;
//   log_message = log_message + ' - Downloaded ' + progressObj.percent + '%';
//   log_message = log_message + ' (' + progressObj.transferred + "/" + progressObj.total + ')';
//   //sendStatusToWindow(log_message);
// })

//function sendStatusToWindow(text) {
  //log.info(text);
  //loading.webContents.send('message', text);
//}

const config = {
  timeout: 5000, //timeout connecting to each try (default 5000)
  retries: 5,//number of retries to do before failing (default 5)
  domain: 'google.com'//the domain to check DNS record of
}

function ExecutingApp(){
  checkInternetConnected(config)
    .then(() => {
      loading.loadFile('./cache-data/dtt/ltr/en-l.html')
      createWindow()
      tableExistCheck()
      migrationCheck()
      tableExistCheckColumn()
      defaultSettings()
      updateCountry()
      updateLucky()
      mainWindow.webContents.once('dom-ready', () => {
        mainWindow.show()

        if(!loading.isDestroyed()){
          loading.hide()
          loading.close()
        }
        
        autoUpdater.checkForUpdatesAndNotify();
      })
      // long loading html

      mainWindow.loadFile('./cache-data/dtt/ltr/cache-i.html')
      
    }).catch((err) => {
      loading.loadFile('./cache-data/dtt/ltr/e-l-n-c.html')
      setTimeout(function(){
        app.quit()
      },6000)
  });
}

function treatData(input) {
  return new Promise((resolve, reject) => {
    var treated = OrolAth(input)
    //console.log(treated)
    resolve(treated)
    if(treated){
      OrolINS(input)
      FinOKLIC()
      vrabecWindow.close()
    }
    else{
      OrolEE()
      return false
    }  
  })
}

ipcMain.on('svina_check', async (event, rawdata) => {
  //console.log(rawdata) // prints my raw data
  //var dataTreated = await treatData(rawdata)
  //console.log(dataTreated) // prints Undefined
  OrolAth(rawdata)

})

function OrolAth(cennaHodnota){
  //OrolINS(cennaHodnota)
  //FinOKLIC()
  //vrabecWindow.close()
  checkInternetConnected(config)
    .then(() => {
      let zahada = machine_id
      const OrolVerejnost = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCWQ0hZMvlS2RLx0eMdSe1oWk63+5qnA9f89bJyRR50SNrk3TuI0DMHXIyKcqw4VWZq5sNxuwWGt6h5CqW4Ia+KE0qPqR37nAv2FK1rwZppLW8Pr7MwnGJ08BhcnXWAUHlnpruVZPMd7WxS01cfvCU9EA7WqKlLuZN5IdsiBmn4+QIDAQAB'

      const SukromnyBuldozer = new NodeRSA('-----BEGIN RSA PRIVATE KEY-----'+OrolSukromie+'-----END RSA PRIVATE KEY-----');
      const VerejnyBuldozer = new NodeRSA('-----BEGIN PUBLIC KEY-----'+OrolVerejnost+'-----END PUBLIC KEY-----')
      const vedVies = VerejnyBuldozer.encrypt(zahada, 'base64');

      var axios = require('axios');
      const https = require('https');
      const agent = new https.Agent({
        rejectUnauthorized: false
      });
      var FormData = require('form-data');
      var data = new FormData();
      data.append('data', vedVies);

      var config1 = {
        method: 'post',
        httpsAgent: agent,
        url: 'https://koala-system.com/api/licence-verify',
        headers: { 
          'Authorization': cennaHodnota, 
          ...data.getHeaders()
        },
        data : data
      };

      axios(config1)
      .then(function (response) {
        var res = response.data.data
        var statusResponse = response.status
        var clearMessage = SukromnyBuldozer.decrypt(res, 'utf8');
        
        var words = clearMessage.split('&');
        let unix_timestamp = words[1]

        var skuskacasu = Moment.unix(unix_timestamp).format()
        var skuskacasu2 = Moment().tz('Europe/Vienna').format()
        
        if(statusResponse == 200){
          if(words[0] == 'traktor'){
            OrolEE()
          } else if(words[0] == 'kombajn'){
            if(skuskacasu > skuskacasu2){
              OrolINS(cennaHodnota)
              FinOKLIC()
              vrabecWindow.close()
            } else {
              OrolEE()
            }
          }
          //console.log(statusResponse);
        } else {
          OrolEE()
        }
        
      })
      .catch(function (error) {
        OrolEE()
      });
    }).catch((err) => {
      NoConnectionWindow()
      setTimeout(function(){
        app.quit()
      },6000)
  });
}

function OrolINS(poklad){
  var now = new Date();

  let data = [now.toLocaleDateString("de-DE"), poklad]
  let sql = `INSERT OR REPLACE INTO OROL (created, vrabec) values(?,?) `

  db.run(sql,data,function(err){
      if (err) {
          Swal.fire({
              icon: 'error',
              title: 'Oops..',
              text: 'Error reason: ' + err.message
          })
      }
  })
}

function OrolEE(){
  vrabecWindow2 = new BrowserWindow({show: false, frame: false,icon: process.cwd()+PATH_BUILD_ASAR+'\\_images\\logo-app.png',modal:true,width: 800,height: 650}) //650 x 65 orig
  vrabecWindow.close()
  vrabecWindow2.setBackgroundColor('#842448')
  vrabecWindow2.setResizable(false)
  vrabecWindow2.loadFile('./cache-data/dtt/ltr/ee.html')
  vrabecWindow2.show()
}

function OrolApply(){
  vrabecWindow = new BrowserWindow({show: false, frame: false,icon: process.cwd()+PATH_BUILD_ASAR+'\\_images\\logo-app.png',modal:true,width: 800,height: 650}) //650 x 65 orig
  vrabecWindow.setBackgroundColor('#842448')
  vrabecWindow.setResizable(false)
  vrabecWindow.loadFile('./cache-data/dtt/ltr/orol.html')
  vrabecWindow.show()
}

function NoConnectionWindow(){
  if(vrabecWindow){
    vrabecWindow.hide()
  }else if(vrabecWindow2){
    vrabecWindow2.hide()
  }

  loading = new BrowserWindow({show: false, frame: false,icon: process.cwd()+PATH_BUILD_ASAR+'\\_images\\logo-app.png',modal:true})
  loading.setBackgroundColor('#842448')
  loading.setResizable(false)
  loading.loadFile('./cache-data/dtt/ltr/e-l-n-c.html')
  loading.show()
}

function OrolCheck(){
  let svina_data = null
  let data = null
  db.exec('CREATE TABLE IF NOT EXISTS OROL (id INTEGER PRIMARY KEY AUTOINCREMENT, created date, vrabec text )');
  db.all("SELECT * FROM OROL order by created desc LIMIT 1", function(err, rows) {  
    if (err) {
      app.quit()
    }

    if(Object.values(rows)[0] == null || Object.values(rows)[0] == undefined){
      OrolApply()
    }
    else{
      if(OrolAth(rows[0].vrabec)){
        FinOKLIC()
        return true 
      }
      else{
        checkInternetConnected(config)
        .then(() => {
            OrolApply()

            var sql  = `DELETE FROM OROL`
            db.run(sql, function(err) {
                if (err) {
                  app.quit()
                }
            })

            var sql2 = `DELETE FROM SQLITE_SEQUENCE WHERE name='OROL'`
            db.run(sql2, function(err) {
                if (err) {
                  app.quit()
                }
            })
        }).catch((err) => {
            NoConnectionWindow()
            setTimeout(function(){
              app.quit()
            },6000)
        });
      }
      }
  });
}

function FinOKLIC(){
  loading = new BrowserWindow({show: false, frame: false,icon: process.cwd()+PATH_BUILD_ASAR+'\\_images\\logo-app.png',modal:true})
  loading.setBackgroundColor('#842448')
  loading.setResizable(false)
  //autoUpdater.checkForUpdatesAndNotify();
  
  loading.once('show', () => {
      ExecutingApp()
  })

  // loading.on('closed', function () {
  //   app.quit()
  // })

  loading.loadFile('./cache-data/dtt/ltr/en-l.html')
  loading.show()
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
//app.on('ready', createWindow)

const gotTheLock = app.requestSingleInstanceLock()

if (!gotTheLock) {
  app.quit()
} else {
  app.on('second-instance', (event, commandLine, workingDirectory) => {
    // Someone tried to run a second instance, we should focus our window.
    if (mainWindow) {
      if (mainWindow.isMinimized()) mainWindow.restore()
      mainWindow.focus()
    }
  })
  app.on('ready', () => { 
    OrolCheck()
  })
}

if (!fs.existsSync(dir)){
  fs.mkdirSync(dir);
}

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  app.quit()

})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
