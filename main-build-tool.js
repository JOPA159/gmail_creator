'use strict';
const {app,BrowserWindow, shell} = require('electron')
global.sharedObj = {mode: process.argv[2]}


let mainWindow
function createWindow () {
  // Create the browser window.

  mainWindow = new BrowserWindow({
    width: 1200,
    height: 800,
    minWidth: 960,
    minHeight: 640,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
    }
  })
  
  //mainWindow.webContents.openDevTools()

  // and load the cache-i.html of the app.
  //mainWindow.loadFile('cache-i.html')

  mainWindow.loadFile(process.cwd()+'\\build_tools\\js-build.html')
  // Open the DevTools.
  mainWindow.webContents.openDevTools()
  
  mainWindow.on('show', ()=>{
      setTimeout(() => {
        mainWindow.setOpacity(1);
  }, 200);
  })

  mainWindow.on('hide', () => {
    mainWindow.setOpacity(0);
  });

  mainWindow.once('ready-to-show',() => {
      mainWindow.show()
  })

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
//app.on('ready', createWindow)

app.on('ready', () => {
    createWindow()
    // mainWindow.webContents.once('dom-ready', () => {
    //   mainWindow.show()
    // })
    // mainWindow.loadFile('./cache-i.html')
})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  app.quit()

})
