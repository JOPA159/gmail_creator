
const customTitlebar = require('custom-electron-titlebar')
const { Menu, MenuItem } = require('electron').remote
var appVersion = require("electron").remote.app.getVersion();

const Swal = require('sweetalert2')
require( 'datatables.net-dt' )();
require( 'datatables.net-buttons-dt' )();
require( 'datatables.net-buttons/js/buttons.html5.js' )();

let MyTitleBar = new customTitlebar.Titlebar({
    backgroundColor: customTitlebar.Color.fromHex('#414755')
    //icon: './icon.svg'
})

require('dns').resolve('www.google.com', function(err) {
    if (err) {
        MyTitleBar.updateTitle('KOALA BOT system - NO INTERNET CONNECTION!')
    } else {
        MyTitleBar.updateTitle('KOALA BOT system '+appVersion)
    }
});

const menu = new Menu();
//menu.append(new MenuItem({}))
MyTitleBar.updateMenu(menu)
  