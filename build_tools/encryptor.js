//Treba to dať na začiatok cache-i.html hneď pod render.js do body elementu
/* <script>
        var enc = require(process.cwd()+'\\_linkers\\encryptor.js')
        enc.deleteFiles()
        enc.encrypt()
        //enc.encryptFiles(['cx-xe','fxxm','fxwx','home','ixm-ex','logs','nacitat','prxxd','paaex','nastav'])
        //enc.pythonObf()
</script> */
const ObfuscateSuffix = '_O_DRAFT'
module.exports = {
    obfuscateFilesDefended: function(fileArr=[]){
        const fs = require('fs');
        var exec = require('child_process').exec, OfbExec;

        for (let file = 0; file < fileArr.length; file++) {
            origFileObf = process.cwd()+'\\_linkers\\'+fileArr[file]+'.js'
            finalFileObf = process.cwd()+'\\_linkers\\'+fileArr[file]+'.js'

            OfbExec = exec(`javascript-obfuscator ${origFileObf} --output ${finalFileObf} --compact true --self-defending true --control-flow-flattening true --dead-code-injection true --string-array true --string-array-index-shift true --string-array-encoding rc4 --numbers-to-expressions true --identifier-names-generator hexadecimal --split-strings true --split-strings-chunk-length 5 --string-array-wrappers-count 5 --string-array-wrappers-chained-calls true --string-array-wrappers-parameters-max-count 5 --string-array-wrappers-type function --string-array-threshold 1 --transform-object-keys true`,
                function (error, stdout, stderr) {
                    console.log('stdout: ' + stdout);
                    console.log('stderr: ' + stderr);
                    if (error !== null) {
                        console.log('exec error: ' + error);
                    }
            });   
        }
    },

    obfuscateFiles: function(fileArr=[]){
        const fs = require('fs');
        var exec = require('child_process').exec, OfbExec;

        for (let file = 0; file < fileArr.length; file++) {
            origFileObf = process.cwd()+'\\_linkers\\'+fileArr[file]+'.js'
            finalFileObf = process.cwd()+'\\_linkers\\'+fileArr[file]+ObfuscateSuffix+'.js'

            OfbExec = exec(`javascript-obfuscator ${origFileObf} --output ${finalFileObf} --compact true --self-defending false --control-flow-flattening true --dead-code-injection true --string-array true --string-array-index-shift true --string-array-encoding rc4 --numbers-to-expressions true --identifier-names-generator hexadecimal --split-strings true --split-strings-chunk-length 5 --string-array-wrappers-count 5 --string-array-wrappers-chained-calls true --string-array-wrappers-parameters-max-count 5 --string-array-wrappers-type function --string-array-threshold 1 --transform-object-keys true`,
                function (error, stdout, stderr) {
                    console.log('stdout: ' + stdout);
                    console.log('stderr: ' + stderr);
                    if (error !== null) {
                        console.log('exec error: ' + error);
                    }
            });   
        }
    },

    obfuscate: function(){
        const fs = require('fs');
        const path = require('path')
        var exec = require('child_process').exec, OfbExec;

        fs.readdir(process.cwd()+'\\_linkers\\', (err, files) => {
            if (err) console.log(err);
            fileLength = files.length
            for (const file of files) {
                if(path.extname(file) == ''){
                    var folderName = file;
                    fs.readdir(process.cwd()+'\\_linkers\\'+folderName, (err, files) => {
                        files.forEach(file => {
                            var fileName = path.parse(file).name
                            origFileObf = process.cwd()+'\\_linkers\\'+folderName+'\\'+fileName+'.js'
                            finalFileObf = process.cwd()+'\\_linkers\\'+folderName+'\\'+fileName+ObfuscateSuffix+'.js'

                            OfbExec = exec(`javascript-obfuscator ${origFileObf} --output ${finalFileObf} --compact true --self-defending false --control-flow-flattening true --dead-code-injection true --string-array true --string-array-index-shift true --string-array-encoding rc4 --numbers-to-expressions true --identifier-names-generator hexadecimal --split-strings true --split-strings-chunk-length 5 --string-array-wrappers-count 5 --string-array-wrappers-chained-calls true --string-array-wrappers-parameters-max-count 5 --string-array-wrappers-type function --string-array-threshold 1 --transform-object-keys true`,
                                function (error, stdout, stderr) {
                                    console.log('stdout: ' + stdout);
                                    console.log('stderr: ' + stderr);
                                    if (error !== null) {
                                        console.log('exec error: ' + error);
                                    }
                            });  
                        });
                    });
                }

                if(path.extname(file) == '.js'){
                    var fileName = path.parse(file).name
                    origFileObf = process.cwd()+'\\_linkers\\'+fileName+'.js'
                    finalFileObf = process.cwd()+'\\_linkers\\'+fileName+ObfuscateSuffix+'.js'

                    OfbExec = exec(`javascript-obfuscator ${origFileObf} --output ${finalFileObf} --compact true 
                    --self-defending false --control-flow-flattening true --dead-code-injection true --string-array true --string-array-index-shift true --string-array-encoding rc4 
                    --numbers-to-expressions true --identifier-names-generator 'hexadecimal' --split-strings true --split-strings-chunk-length 5 --string-array-wrappers-count 5 --string-array-wrappers-chained-calls true --string-array-wrappers-parameters-max-count 5 --string-array-wrappers-type 'function' --string-array-threshold 1 --transform-object-keys true`,
                        function (error, stdout, stderr) {
                            console.log('stdout: ' + stdout);
                            console.log('stderr: ' + stderr);
                            if (error !== null) {
                                console.log('exec error: ' + error);
                            }
                    });  
                }

                if (!--fileLength){
                    console.log("OBFUSCATION WAS SUCCESSFULLY COMPLETED.")
                }
            }
            console.log("WAIT FOR OBFUSCATING!")
        });
    },

    deleteObfuscatedFiles: function(){
        const fs = require('fs');
        const path = require('path')
        fs.readdir(process.cwd()+'\\_linkers\\', (err, files) => {
            if (err) console.log(err);
            for (const file of files) {
                if(path.extname(file) == ''){
                    var folderName = file;
                    fs.readdir(process.cwd()+'\\_linkers\\'+folderName, (err, files) => {
                        files.forEach(file => {
                            if(path.basename(file).indexOf(ObfuscateSuffix) > -1){
                                fs.unlink(path.join(process.cwd()+'\\_linkers\\'+folderName, file), err => {
                                    if (err) console.log(err);
                                });
                            }
                        });
                    });
                }

                if(path.basename(file).indexOf(ObfuscateSuffix) > -1){
                    fs.unlink(path.join(process.cwd()+'\\_linkers\\', file), err => {
                        if (err) console.log(err);
                    });
                }
            }
        });

        fs.readdir(process.cwd()+'\\_routers\\', (err, files) => {
            files.forEach(file => {
                if(path.basename(file).indexOf(ObfuscateSuffix) > -1){
                    fs.unlink(path.join(process.cwd()+'\\_routers\\', file), err => {
                        if (err) console.log(err);
                    });
                }
            });
        });

        fs.readdir(process.cwd()+'\\', (err, files) => {
            files.forEach(file => {
                if(path.basename(file).indexOf(ObfuscateSuffix) > -1){
                    fs.unlink(path.join(process.cwd()+'\\', file), err => {
                        if (err) console.log(err);
                    });
                }
            });
        });
    },

    //Only file names without .js
    encryptFiles: function(fileArr=[]){
        const bytenode = require('bytenode');
        const fs = require('fs');
        const v8 = require('v8');
        v8.setFlagsFromString('--no-lazy');

        for (let file = 0; file < fileArr.length; file++) {
            if (!fs.existsSync(process.cwd()+'\\_linkers\\'+fileArr[file]+'.jsc')) {

                bytenode.compileFile(process.cwd()+'\\_linkers\\'+fileArr[file]+ObfuscateSuffix+'.js', process.cwd()+'\\_linkers\\'+fileArr[file].replace(ObfuscateSuffix,'')+'.jsc');
            }
            else{
                console.log(fileArr[file]+'.jsc - is already exists.')
            }        
        }
    },

    encrypt: function(){
        const bytenode = require('bytenode');
        const fs = require('fs');
        const v8 = require('v8');
        const path = require('path')

        v8.setFlagsFromString('--no-lazy');

        fs.readdir(process.cwd()+'\\_linkers\\', (err, files) => {
            if (err) console.log(err);
            for (const file of files) {
                if(path.extname(file) == ''){
                    var folderName = file;
                    fs.readdir(process.cwd()+'\\_linkers\\'+folderName, (err, files) => {
                        files.forEach(file => {
                            var fileName = path.parse(file).name
                            if (!fs.existsSync(process.cwd()+'\\_linkers\\'+folderName+'\\'+fileName+'.jsc') && path.basename(fileName).indexOf(ObfuscateSuffix) > -1) {
                                bytenode.compileFile(process.cwd()+'\\_linkers\\'+folderName+'\\'+fileName+'.js', process.cwd()+'\\_linkers\\'+folderName+'\\'+fileName.replace(ObfuscateSuffix,'')+'.jsc');
                            }
                            else{
                                console.log(fileName+'.jsc - is already exists.')
                            }  
                        });
                    });
                }

                if(path.extname(file) == '.js'){
                    var fileName = path.parse(file).name
                    if (!fs.existsSync(process.cwd()+'\\_linkers\\'+fileName+'.jsc') && path.basename(fileName).indexOf(ObfuscateSuffix) > -1) {
                        bytenode.compileFile(process.cwd()+'\\_linkers\\'+fileName+'.js', process.cwd()+'\\_linkers\\'+fileName.replace(ObfuscateSuffix,'')+'.jsc');
                    }
                    else{
                        console.log(fileName+'.jsc - is already exists.')
                    }  
                }
            }
        });
    },

    deleteFiles: function(ext = 'jsc'){
        const fs = require('fs');
        const path = require('path')
        fs.readdir(process.cwd()+'\\_linkers\\', (err, files) => {
            if (err) console.log(err);
            for (const file of files) {
                if(path.extname(file) == ''){
                    var folderName = file;
                    fs.readdir(process.cwd()+'\\_linkers\\'+folderName, (err, files) => {
                        files.forEach(file => {
                            if(path.extname(file) == '.'+ext){
                                if(path.basename(file).indexOf('renderer') == -1 && path.basename(file).indexOf('mainRUNNER') == -1){
                                    fs.unlink(path.join(process.cwd()+'\\_linkers\\'+folderName, file), err => {
                                        if (err) console.log(err);
                                    });
                                } 
                            }
                        });
                    });
                }

                if(path.extname(file) == '.'+ext){
                    if(path.basename(file).indexOf('renderer') == -1 && path.basename(file).indexOf('mainRUNNER') == -1){
                        fs.unlink(path.join(process.cwd()+'\\_linkers\\', file), err => {
                            if (err) console.log(err);
                        });
                    }
                }
            }
        });

        fs.readdir(process.cwd()+'\\_routers\\', (err, files) => {
            files.forEach(file => {
                if(path.extname(file) == '.'+ext){
                    if(path.basename(file).indexOf('renderer') == -1 && path.basename(file).indexOf('mainRUNNER') == -1){
                        fs.unlink(path.join(process.cwd()+'\\_routers\\', file), err => {
                            if (err) console.log(err);
                        });
                    }
                }
            });
        });

        fs.readdir(process.cwd()+'\\', (err, files) => {
            files.forEach(file => {
                if(path.extname(file) == '.'+ext){
                    if(path.basename(file).indexOf('renderer') == -1 && path.basename(file).indexOf('mainRUNNER') == -1){
                        fs.unlink(path.join(process.cwd()+'\\', file), err => {
                            if (err) console.log(err);
                        });
                    }
                }
            });
        });
    },

    pythonObf: function(){
        var child_process = require('child_process');
        child_process.execSync('pyarmor obfuscate '+process.cwd()+'\\_engine\\',{stdio:[0,1,2]});
    },
}