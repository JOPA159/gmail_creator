const { app, BrowserWindow, ipc, dialog, remote, shell } = require("electron").remote;
const ipcRenderer = require("electron");
const NodeRSA = require('node-rsa');
const parentWindow = require("electron").remote.getCurrentWindow();
const appName = require("electron").remote.getGlobal("sharedObj").appName;
const PROD_MODE = require("electron").remote.getGlobal("sharedObj").PROD_MODE;
const path = require("path");
const url = require("url");
const { PythonShell } = require("python-shell");
var sqlite3 = require("sqlite3").verbose();
//var dbPath = process.cwd()+LOCATION_UNPACK+'\\_DB\\DBemails.db';
var dbPath = app.getPath('userData')+'\\DBemails.db'
var db = new sqlite3.Database(dbPath);
var fs = require('fs');
var dateFormat = require('dateformat');
var errLogFile = fs.createWriteStream(app.getPath('userData')+"\\error.log",{flags: 'a'});
var procLogFile = fs.createWriteStream(app.getPath('userData')+"\\process.log",{flags: 'a'});
var XLSX = require('xlsx');
var importFresh = require('import-fresh');
const {machineId, machineIdSync} = require('node-machine-id');
let machine_id = machineIdSync({original: true})
var Moment = require('moment-timezone');
const checkInternetConnected = require('check-internet-connected');
//LOGGER EXAMPLE:
//loggerMesage(type = 'error',message = 'Takto sa bude zapisovat error',identifier = 'marek@test.sk')
//loggerMesage(type = 'process',message = 'takto sa bude zapisovat proces',procType = 'FORWARDING',identifier = '')
var seno = machine_id
db.all(`SELECT max_windows FROM SETTINGS WHERE category = 'WINDOWS' LIMIT 1`, function(err, rows) { 
  if(rows != undefined && rows != null && rows.length > 0){
      if (err) {
          Swal.fire({
              icon: 'error',
              title: 'Oops..',
              text: 'Error reason: ' + err.message
          })
      }
      var maxWindowsExecutedVal = rows[0].max_windows
  }
  else{
      var maxWindowsExecutedVal = 5
  }
  $(".page-wrapper").data("maxwindowval",maxWindowsExecutedVal+1) 
});
const lakatosSukr = 'MIICXQIBAAKBgQCWQ0hZMvlS2RLx0eMdSe1oWk63+5qnA9f89bJyRR50SNrk3TuI0DMHXIyKcqw4VWZq5sNxuwWGt6h5CqW4Ia+KE0qPqR37nAv2FK1rwZppLW8Pr7MwnGJ08BhcnXWAUHlnpruVZPMd7WxS01cfvCU9EA7WqKlLuZN5IdsiBmn4+QIDAQABAoGACNv+sAOpX5Ec5NUTJu7s6HIqof5bp+06FtgFpWcd3I1PGp9Qm0hrOcvjHWaVcNzYJQVmEJaORNsfO8O88ZZK2ynyWULYc2/Pp5M6frzz4orB7tpWxmhp805XvVOJGdhzkU497dzkQK6Vf+zT4EWvOmvoM27T/ENJRLLNpeO9luECQQDbTnlUcQJ/29AJDV/AJYnjzrOcaZllyrkW4PqFYi5fKOS7AzUFpgmjmP1yWnRyYI6GlDSanu2puKv1vv/CwfNPAkEAr2d4Clv+ZJiadX3+yrDMS/ri8pVVi19OHWMhvpstzGJqa4GDKrBal9vpoWXOKoe47gJcr7w/5iWJD1VUhjVdNwJBAKmNwXaMrP8Hho8JHQdkvng3flwmtKejlXhf1rC7WNjPuA/yidLlLTyj8wevcapghrD78rO8vkji82hbFEiSRTcCQG79rCnx+FGzdtmt9pJPuZbClOUntc/RaN6TogC4tQxFok2Q0G0s1saNM5Mblo+2/kAtYnUZS93YFpiLSEb8cZUCQQClR51CrqxOY25q3rHTiFbTeBnd774DnWDN1j0omOCk+eUiDHsjNXv7IT4J/yafG4k3Bfl1ErkCugklc/QBEpDC'

process.on('uncaughtException', function (err) {
  self.loggerMesage('error',err.message,'SYSTEM PROCESS','SYSTEM ERROR')
  //app.quit()
})

var self = module.exports = {
  PROD_MODE,app, BrowserWindow, ipc, dialog, remote, shell,appName,path,url,PythonShell,
  sqlite3,dbPath,db,fs,dateFormat,errLogFile,procLogFile,XLSX,importFresh,
  
  loggerMesage: function(type,message = '',procType = '',identifier = ''){
    var dateVal = dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss,sss").slice(0, -1);
    var separator = '#~#! '
    var lineseparator = '#~~#'

    var e = new Error();
    var regex = /[^/]*$/
    var match = regex.exec(e.stack.split("\n")[2]);
    var locationVal = match[0].split(':')
    var locationValFinal = {
      filepath: locationVal[0],
      line: locationVal[1],
      column: locationVal[2]
    };

    if(type == 'error'){
      fs.appendFile(app.getPath('userData')+"\\error.log",dateVal + separator+ '[' +identifier + ']' + separator + 'ERROR' + separator + 'ON LINE:' + locationValFinal['column'] + separator + message + separator + 'FILE:{' + 'Not defined FILE' + '}' + lineseparator + '\n',function(err){

      })
    }
    else if(type == 'process'){
      procLogFile.once('open', function(fd) {
        procLogFile.write(dateVal + separator+ '[' +identifier + ']' + separator + 'PROCESS' + separator + procType + separator + 'Message:' + message + lineseparator + '\n');
        procLogFile.end();
      });
    }
  },

  getNewWindow: function(
    name,
    frameValue = true,
    widthValue = 1200,
    heightValue = 800,
    idWindow
  ) {
    var childWindow = new BrowserWindow({
      width: widthValue,
      height: heightValue,
      minWidth: 500,
      minHeight: 350,
      icon: process.cwd() + "/_images/logo-app.png",
      parent: parentWindow,
      modal: true,
      frame: frameValue,
      webPreferences: {
        nodeIntegration: true,
        enableRemoteModule: true,
        disableBlinkFeatures: "Auxclick"
      },
    });

    childWindow.setBackgroundColor('#842448')
    childWindow.loadFile(process.cwd()+LOCATION+"\\cache-data\\dtt\\ltr\\" + name + ".html");


    childWindow.on("show", () => {
      setTimeout(() => {
        childWindow.setOpacity(1);
      }, 200);
    });

    childWindow.on("hide", () => {
      childWindow.setOpacity(0);
    });

    childWindow.once("ready-to-show", () => {
      childWindow.show();
    });

    // Emitted when the window is closed.
    childWindow.on("closed", function () {
      // Dereference the window object, usually you would store windows
      // in an array if your app supports multi windows, this is the time
      // when you should delete the corresponding element.

      if (idWindow == 12345) {
        showLoading();
        proxy_data.refreshProxyTable() 
        //proxy_data.inicializeProxyTable()
      }

      childWindow = null;
    });
  },

  getScore: function() {
    dialog.showErrorBox("My message", "hi.");
  },

  // showLoading: function(){
  //   Swal.fire({
  //     title: "Loading...",
  //     allowOutsideClick: false,
  //     onBeforeOpen: () => {
  //       Swal.showLoading();
  //     },
  //   });
  // },

  // hideLoading: function() {
  //   Swal.close();
  // },

  updateCountryList: function(setting = false) {
    //event.preventDefault()
    db.all("SELECT count(*) as total FROM PROVIDERS",function (err, rows) {
      if (err) {
        return console.error(err.message);
      }

      if(rows[0].total == 0){
        Swal.fire({
          icon: "info",
          title: "Add a provider first.",
          text:"In case you want to update countries, you must first have at least one provider added.",
        });
        return false
      }

      showLoading();
      if (document.getElementById("provider-list").value != "LUCKYSMS") {
        var opt = {
          scriptPath: process.cwd()+LOCATION_UNPACK+'\\_engine\\win_prov\\Country\\',
          pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
        };

        var pyshell = new PythonShell("gl_cn.py", opt);

        pyshell.on("message", (results) => {
          hideLoading();
          if (setting == true) {
            var newArray = "";
            $("#countries-list").html("");
            db.all(
              "SELECT name,fullname,prefix FROM COUNTRY ORDER BY fullname",
              function (err, rows) {
                if (err) {
                  return console.error(err.message);
                }

                if(rows === undefined || rows.length == 0){
                  hideLoading()
                  return false
                }

                rows.forEach(function (row) {
                  newArray += `<option value='${row.name}'>${row.fullname} (${row.name})</option>`;
                });
                if (newArray.length == 0) {
                } else {
                  if (
                    document.getElementById("provider-list").value == "LUCKYSMS"
                  ) {
                    document.getElementById("prefix").style.display = "none";
                    document.getElementById("updatePrefixBtn").style.display =
                      "none";
                  }
                  $("#countries-list").html(newArray);
                  document.getElementById("prefix").value = rows[0].prefix;
                  document.getElementById("prefix").style.display = "block";
                  document.getElementById("updatePrefixBtn").style.display =
                    "block";

                  Swal.fire({
                    icon: "info",
                    title: "Country data updated!",
                    text:
                      "You succesfully get actual country data and reset your changes.",
                  });
                }
                //location.reload()
              }
            );
          }
        });

        pyshell.end((err) => {
          if (err) {
            hideLoading();
            Swal.fire({
              icon: "error",
              title: "Oops!",
              text: "Error reason:" + err.message,
            });
            db.all(`SELECT id FROM COUNTRY`, function(err, rows) {  
              if (err) {
                      Swal.fire({
                          icon: 'error',
                          title: 'Oops..',
                          text: 'Error reason: ' + err.message
                      })
                  }
      
              if(rows === undefined || rows.length == 0){
                  var sql  = `DELETE FROM PROVIDERS WHERE id != 'LUCKYSMS'`
            
                  db.run(sql, function(err) {
                      if (err) {
                              Swal.fire({
                                  icon: 'error',
                                  title: 'Oops..',
                                  text: 'Error reason: ' + err.message
                              })
                          }
                  })
                  return false
              }
            })

            throw err;
          }
        });
      } else {
        self.updateLucky();
        var newArray = "";
        db.all(
          "SELECT a.system,a.name,a.fullname,b.prefix FROM COUNTRY_LUCKY a LEFT JOIN COUNTRY b on a.name = b.name ORDER BY a.fullname",
          function (err, rows) {
            if (err) {
              Swal.fire({
                icon: "error",
                title: "Oops..",
                text: "Error reason: " + err.message,
              });
            }
            var systemNumber = 1;
            var i = 0;

            if(rows === undefined || rows.length == 0){
              return false
            }
            rows.forEach(function (row) {
              i += 1;
              if (i == 1) {
                newArray += `<optgroup label="SYSTEM ${systemNumber}">`;
                systemNumber += 1;
              }

              if (
                row["system"] == "system" + systemNumber &&
                row["system"] != "system1"
              ) {
                newArray += "</optgroup>";
                newArray += `<optgroup label="SYSTEM ${systemNumber}">`;
                systemNumber += 1;
              }

              optVal = `<option value="LUCKYSMS_${row["system"]}_${row["name"]}">${row["fullname"]} (${row["name"]})</option>`;
              newArray += optVal;

              if (i == rows.length) {
                newArray += "</optgroup>";
              }
              //newArray += `<option value='${row.name}'>${row.fullname} (${row.name})</option>`
            });
            if (newArray.length == 0) {
            } else {
              $("#countries-list").html(newArray);
              if(rows[0].prefix == undefined || rows[0].prefix == null){
                document.getElementById('prefix').value = null
            }else{
                document.getElementById('prefix').value = rows[0].prefix
            }
            document.getElementById('updatePrefixBtn').style.display="block"
              Swal.fire({
                icon: "info",
                title: "LUCKYSMS countries updated!",
                text:
                  "You succesfully get actual country data and reset your changes.",
              });
            }
          }
        );
      }
    })
  },

  updateLucky: function() {
    var opt = {
      scriptPath: process.cwd()+LOCATION_UNPACK+'\\_engine\\win_prov\\Country\\',
      pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
      //args : [recoveryEmail]
    };

    var pyshell = new PythonShell(`l_s_cntr.py`, opt);

    pyshell.on("message", (results) => {
      hideLoading();
    });

    pyshell.end((err) => {
      if (err) {
        hideLoading();
        Swal.fire({
          icon: "error",
          title: "Oops!",
          text: "Error reason:" + err.message,
        });
        db.all(`SELECT id FROM COUNTRY_LUCKY`, function(err, rows) {  
          if (err) {
                  Swal.fire({
                      icon: 'error',
                      title: 'Oops..',
                      text: 'Error reason: ' + err.message
                  })
              }

          if(rows === undefined || rows.length == 0){
              var sql  = `DELETE FROM PROVIDERS WHERE id = 'LUCKYSMS'`
        
              db.run(sql, function(err) {
                  if (err) {
                          Swal.fire({
                              icon: 'error',
                              title: 'Oops..',
                              text: 'Error reason: ' + err.message
                          })
                      }
              })
              return false
          }
        })

        throw err;
      }
      return "";
    });
  },

  killWindow: function(event, windowsParam, sessionType) {
    event.preventDefault();

    document.getElementById(
      `email-process-${windowsParam}`
    ).innerHTML = `<img src="${process.cwd()+LOCATION}/cache-data/assets/images/loading.gif" id="email-process-${windowsParam}" class="pause loading-play-logo" style="height:13px"/ >`;

    backgroundAuto = false

    if (windowsParam == null) {
      return false
    }
    var arrayVal = []

    if(sessionType == 'REGISTERING'){
      listOfUnregisteredStatuses = '("'+String(filter_create_vals_dictionary.UNREGISTERED).toLowerCase().replace(/,/g,'","')+'")'
      listOfUnregisteredStatuses.replace("")
      var sqlCanc = `UPDATE EMAILS SET status = 'canceled' WHERE lower(status) in ${listOfUnregisteredStatuses} AND id = ${windowsParam} AND lower(status) != 'unregistered'`;
      child.db.run(sqlCanc, function (err) {}) 
      var sqlWait = `DELETE FROM WAITING WHERE sessions_type = 'REGISTERING' and id_email = ${windowsParam}`;
      child.db.run(sqlWait, function (err) {}) 
    }else if (sessionType == 'FARMING'){
      listOfUnfarmedStatuses = '("'+String(filter_farm_vals_dictionary.UNFARMED).toLowerCase().replace(/,/g,'","')+'")'
      listOfUnfarmedStatuses.replace("")
      var sqlCanc = `UPDATE FARMING SET status = 'CANCELED' WHERE lower(status) in ${listOfUnfarmedStatuses} AND email_id = ${windowsParam}`;
      child.db.run(sqlCanc, function (err) {}) 
      var sqlWait = `DELETE FROM WAITING WHERE sessions_type = 'FARMING' and id_email = ${windowsParam}`;
      child.db.run(sqlWait, function (err) {}) 
    }else if (sessionType == 'FORWARDING'){
      listOfUnforwardedStatuses = '("'+String(filter_forward_vals_dictionary.UNFORWARDED).toLowerCase().replace(/,/g,'","')+'")'
      listOfUnforwardedStatuses.replace("")
      var sqlCanc = `UPDATE FORWARDING SET status = 'CANCELED' WHERE lower(status) in ${listOfUnforwardedStatuses} AND email_id = ${windowsParam}`;
      child.db.run(sqlCanc, function (err) {}) 
      var sqlWait = `DELETE FROM WAITING WHERE sessions_type = 'FORWARDING' and id_email = ${windowsParam}`;
      child.db.run(sqlWait, function (err) {}) 
    }

    db.all(
      `SELECT DISTINCT id,id_window FROM WINDOWS WHERE status_id = '1' and id_email = '${windowsParam}' and sessions_type = '${sessionType}'`,
      function (err, rows) {
        if(rows === undefined || rows.length == 0){
          return false
        }

        rows.forEach(function (row) {
          draft = Object.assign(row)
          arrayVal.push(Object.values(draft))
        });
        //console.log(arrayVal)
        if (arrayVal == null) {
          return false;
        }

        backgroundAuto = false;
        modeJSON = true;

        var opt = {
          scriptPath: process.cwd()+LOCATION_UNPACK+'\\_engine\\HG\\',
          pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
          args: [backgroundAuto, modeJSON],
        };

        var pyshell = new PythonShell("Kwdw.py", opt);

        pyshell.send(JSON.stringify(arrayVal), { mode: 'json' });
        
        pyshell.on("message", (results) => {
          //console.log(results)
        });

        pyshell.end((err) => {
          if (err) {
            hideLoading();
            Swal.fire({
              icon: "error",
              title: "Oops!",
              text: "Error reason:" + err.message,
            });
            throw err;
          }
        });
      }
    );
  },

  Slaninka: function(){
    let {app} = require('electron').remote
    db.all("SELECT * FROM OROL order by created desc LIMIT 1", function(err, rows) {  
      if (err) {
        app.quit()
      }
  
      if(Object.values(rows)[0] == null || Object.values(rows)[0] == undefined){
        app.quit()
      }
      
      else{
        var querystring = require('querystring');
        var request = require('request');
        
        let zahada = seno
        
        const lakatosVer = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCWQ0hZMvlS2RLx0eMdSe1oWk63+5qnA9f89bJyRR50SNrk3TuI0DMHXIyKcqw4VWZq5sNxuwWGt6h5CqW4Ia+KE0qPqR37nAv2FK1rwZppLW8Pr7MwnGJ08BhcnXWAUHlnpruVZPMd7WxS01cfvCU9EA7WqKlLuZN5IdsiBmn4+QIDAQAB'
        
        const SukromnyBuldozer = new NodeRSA('-----BEGIN RSA PRIVATE KEY-----'+lakatosSukr+'-----END RSA PRIVATE KEY-----');
        const VerejnyBuldozer = new NodeRSA('-----BEGIN PUBLIC KEY-----'+lakatosVer+'-----END PUBLIC KEY-----')
        const vedVies = VerejnyBuldozer.encrypt(zahada, 'base64');

        console.log('test')
        var request = require('request');
        var options = {
          'method': 'POST',
          'url': 'https://koala-system.com/api/licence-verify',
          'headers': {
            'Authorization': rows[0].vrabec
          },
          formData: {
            'data': vedVies
          }
        };
        request(options, function (err, res, body) {
          if(res == undefined || res.statusCode == undefined || res == null || res.statusCode == null){
            app.quit()
          }else{
            var statusResponse = res.statusCode
            if(statusResponse == 200){
              var output = JSON.parse(body)
              var resData = output.data
              var statusResponse = res.statusCode
              var clearMessage = SukromnyBuldozer.decrypt(resData, 'utf8');
              var words = clearMessage.split('&');
              let unix_timestamp = words[1]
              var skuskacasu = Moment.unix(unix_timestamp).format()
              var skuskacasu2 = Moment().tz('Europe/Vienna').format()
              if(words[0] == 'traktor'){
                app.quit()
              } else if(words[0] == 'kombajn'){
                if(skuskacasu > skuskacasu2){

                } else {
                  app.quit()
                }
              }
              else{
                app.quit()
              }
            }else{
              app.quit()
            }
          }
        });

       
      
        // request({
        //     headers: {
        //       'Authorization': rows[0].vrabec,
        //       'Content-Length': contentLength,
        //       'Content-Type': 'application/x-www-form-urlencoded'
        //     },
        //     uri: 'https://koala-system.com/api/licence-verify',
        //     body: formData,
        //     method: 'POST'
        //   }, function (err, res, body) {
        //     if(res == undefined || res.statusCode == undefined || res == null || res.statusCode == null){
        //       app.quit()
        //     }else{
        //       var statusResponse = res.statusCode
        //       if(statusResponse == 200){
        //         var output = JSON.parse(body)
        //         var resData = output.data
        //         var statusResponse = res.statusCode
        //         var clearMessage = SukromnyBuldozer.decrypt(resData, 'utf8');
        //         var words = clearMessage.split('&');
        //         let unix_timestamp = words[1]
        //         var skuskacasu = Moment.unix(unix_timestamp).format()
        //         var skuskacasu2 = Moment().tz('Europe/Vienna').format()
        //         if(words[0] == 'traktor'){
        //           app.quit()
        //         } else if(words[0] == 'kombajn'){
        //           if(skuskacasu > skuskacasu2){

        //           } else {
        //             app.quit()
        //           }
        //         }
        //         else{
        //           app.quit()
        //         }
        //       }else{
        //         app.quit()
        //       }
        //     }
        //   });
      }
    })
  },
  maximizeWindow: function(event, windowsParam, sessionType) {
    var getBal = 'getBal_'+windowsParam
    document.getElementById(getBal).style.pointerEvents = 'none';
    db.all(
      `SELECT DISTINCT id,id_window FROM WINDOWS WHERE id_email = '${windowsParam}' and sessions_type = '${sessionType}' and status_id = '1' and id_email not in (
        SELECT
        email_id
        FROM FARMING 
        WHERE 1=1
        and status in ('INITIALIZING PROCESS')
        union
        SELECT
        email_id
        FROM FORWARDING
        WHERE 1=1
        and status in ('INITIALIZING PROCESS')
        union 
        SELECT
        id
        FROM EMAILS
        WHERE 1=1
        and status in ('initializing process')
      )`,
      function (err, rows) {
        //console.log(sessionType)
        if(rows === undefined || rows.length == 0){
            return false
        }

        rows.forEach(function (row) {
          windows = row["id_window"];

          if (windows == null) {
            return false;
          }
          

          var opt = {
            scriptPath: process.cwd()+LOCATION_UNPACK+'\\_engine\\HG\\',
            pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
            args: [windows],
          };
          var pyshell = new PythonShell("Mzrx.py", opt);

          pyshell.on("message", (results) => {
            //console.log(results);
          });

          pyshell.end((err) => {
            if (err) {
              hideLoading();
              Swal.fire({
                icon: "error",
                title: "Oops!",
                text: "Error reason:" + err.message,
              });
              throw err;
            }
          });
        });
          setTimeout(function(){
            document.getElementById(getBal).style.pointerEvents = 'auto'; 
          },2000);
      }
    );
  },

  addForWaiting: function(){
    return false
  },

  countdownStartDebounce: function(elementName){
    self.debounceGlobalCounter(5)

    // elemButt = document.getElementById(elementName)
    // if(elemButt != undefined || elemButt != null){
    //   elemButt.disabled = true
    // }
    
    // elemNodes = document.querySelectorAll('[id=countdown]')
    // //elemCount = document.getElementById("countdown")
    // elemList = [...elemNodes]

    // elemList.forEach(elemCount =>{
    //   if(elemCount != undefined || elemCount != null){
    //     elemCount.textContent = " (5)"
    //   }

    //   var disabledChecker = setInterval(function(){
    //       elemButt = document.getElementById(elementName)
    //       if(elemButt == undefined || elemButt == null){
    //         self.debounceGlobalCounter(timeleft)
    //         timeleft = 0
    //         clearInterval(downloadTimer)
    //         clearTimeout(disCount)
    //         clearInterval(disabledChecker)
    //       }else{
    //         if(elemButt.disabled == false){
    //           self.debounceGlobalCounter(timeleft)
    //           timeleft = 0
    //           clearInterval(downloadTimer)
    //           clearTimeout(disCount)
    //           clearInterval(disabledChecker)
    //           elemCount.textContent = ""
    //         }
    //       }
          
    //   }, 50);

    //   var timeleft = 5;
    //   var downloadTimer = setInterval(function(){
    //     timeleft -= 1
    //     if(timeleft <= 0){
    //       clearInterval(downloadTimer);
    //       clearInterval(disabledChecker)
    //       elemCount.textContent = "";
    //     } else {
    //       elemCount.textContent = " ("+timeleft+")";
    //     }
    //   }, 1000);

    //   var disCount = setTimeout(function() {
    //       elemButt.disabled = false;
    //   }, 5000);
    // })
  },

  debounceGlobalCounter: function(remainingTimeoutVal){
    if(window.disTimeVal > 0){
      return false
    }
    window.disTimeVal = remainingTimeoutVal
    elemNodes = document.querySelectorAll('[id=countdown]')

    var globalDebouncerSetter = setInterval(function(){
      if(window.disTimeVal > 0){
        elemNodes = document.querySelectorAll('[id=countdown]')
        
        elemNodes.forEach(elemCountItem =>{
          elemCountItem.closest('button').disabled = true
          elemCountItem.textContent = " ("+window.disTimeVal+")"
        })
      }else{
        elemNodes = document.querySelectorAll('[id=countdown]')
        elemNodes.forEach(elemCountItem =>{
          elemCountItem.closest('button').disabled = false
          elemCountItem.textContent = ""
        })
        //console.log(elemNodes)
        clearInterval(globalDebouncerSetter)
      }
    },50)

    var globalDebounceInterval = setInterval(function(){
      window.disTimeVal -= 1
      if(window.disTimeVal == 0){
        clearInterval(globalDebounceInterval)
      } 
    },1000)
  },

  uniq_fast: function(a) {
    var seen = {};
    var out = [];
    var len = a.length;
    var j = 0;
    for(var i = 0; i < len; i++) {
         var item = a[i];
         if(seen[item] !== 1) {
               seen[item] = 1;
               out[j++] = item;
         }
    }
    return out;
  },

  handleNonLeftClick: function(e) {
    // e.button will be 1 for the middle mouse button.
    if (e.button === 1 || e.ctrlKey || e.shiftKey) {
        // Check if it is a link (a) element; if so, prevent the execution.
        if (e.target.tagName.toLowerCase() === "a") {
            e.preventDefault();
        }
    }
  },

  taskLogger: function(idEmail,sessionsType){
    if(idEmail == null || idEmail == '' || sessionsType == null || sessionsType == '' ){
      return false
    }

    var now = new Date()

    let data_window = [now, idEmail,sessionsType]
    let sql_window = `INSERT INTO LOGS (created, id_email, sessions_type) values(?,?,?) `

    child.db.run(sql_window,data_window,function(err){
      //if (err) {
        // console.log(err.message)
      //}
      //email_data.refreshCreateEmailsTable()
    })
  },
  getAllProxyGroups: function(filterName){

    if(filterName == '#filterProxyGroupImport'){
        newArray = ''
    }else if(filterName == '#selectProxyGroup'){
        var newArray = "<option value=''>All proxy groups:</option>"
    }else if(filterName == '#selectProxyGroupChange'){
        var newArray = "<option value=''>Use another proxy group:</option>"
    }
    else{
        var newArray = "<option value=''>Filter by proxy group:</option>"
    }
    
    child.db.all("SELECT distinct proxy_group FROM PROXIES order by proxy_group", function(err, rows) {  
        if (err) {
            Swal.fire({
                icon: 'error',
                title: 'Oops..',
                text: 'Error reason: ' + err.message
            })
        }

        if(rows === undefined || rows.length == 0){
            $(filterName).html(newArray)
            return false
        }
        
        rows.forEach(function (row) {           
          if(filterName == '#filterProxyGroup' && window.searchFilterProxyGroup == row.proxy_group ){
            newArray += `<option value='${row.proxy_group}' selected>${row.proxy_group}</option>`
          }else{
            newArray += `<option value='${row.proxy_group}'>${row.proxy_group}</option>`
          }
        }) 

        if(newArray.length == 0){
            $(filterName).html(newArray)
        }else{
            $(filterName).html(newArray)
        }
    });
  },
  changeProxy: function(idEmail, proxyGroup){
    window.finalEmailListProxyChange = null
    proxyGroupVal = document.getElementById(proxyGroup).value

    child.db.all(`SELECT id from PROXIES where proxy_group = '${proxyGroupVal}' and (id_email is null or id_email = '' or id_email = ' ') order by id`, function(err, rows) {  
      if (err) {
          Swal.fire({
              icon: 'error',
              title: 'Oops..',
              text: 'Error reason: ' + err.message
          })
      }

      finalEmailList = []
      if(rows === undefined || rows.length == 0){
        Swal.fire({
          icon: "error",
          title: "Oops!",
          text: "No free proxies!",
        });
        window.finalEmailListProxyChange = finalEmailList
      }

      proxyList = []
      rows.forEach(function (row) { 
        proxyList.push(row.id)
      })

      var output = idEmail.map(function(obj,index){
        var myobj = {};
        myobj[obj] = proxyList[index];
        return myobj
      });

      output.forEach(function(email){

        if(Object.values(email)[0] == undefined){
          Swal.fire({
            icon: "error",
            title: "Oops!",
            text: "Not enough proxies for all emails. Import new or try another.",
          });
          window.finalEmailListProxyChange = finalEmailList
        }

        if(Object.values(email)[0] != undefined){
          finalEmailList.push(Object.keys(email)[0])
          child.db.exec(`UPDATE EMAILS SET proxyId = ${Object.values(email)[0]} where id = ${Object.keys(email)[0]}`)
          child.db.exec(`UPDATE PROXIES SET id_email = null where id_email = ${Object.keys(email)[0]}`)
          child.db.exec(`UPDATE PROXIES SET id_email = ${Object.keys(email)[0]} where id = ${Object.values(email)[0]}`)
        }
      })
      window.finalEmailListProxyChange = finalEmailList
    })
  }
}

const config = {
  timeout: 5000, //timeout connecting to each try (default 5000)
  retries: 5,//number of retries to do before failing (default 5)
  domain: 'google.com'//the domain to check DNS record of
}  

var sledVal = 0

document.addEventListener("newPageLoad", function(){
  window.searchFilterStatusEmails = null
  window.searchFilterStatusFarming = null
  window.searchFilterStatusForwarding = null
  window.finalEmailListProxyChange = null

  if(sledVal == 0 || Moment(Moment().tz('Europe/Vienna').valueOf()).diff(Moment(sledVal),'minutes') >= 5 ){
    sledVal = Moment().tz('Europe/Vienna').valueOf()
      checkInternetConnected(config)
      .then(() => {
        self.Slaninka()
      }).catch((err) => {
      setTimeout(function(){
        self.Slaninka()
      },60000)
    });
  }
  
})

document.addEventListener("auxclick", self.handleNonLeftClick);

$(document).on('click', 'a[href^="http"]', function(event) {
  event.preventDefault();
  shell.openExternal(this.href);
});

window.onload = function () {

  var textarea = document.getElementById("proxyTextareaId");

  if(typeof(textarea) != 'undefined' && textarea != null){
      var limit = 2000; // <---max no of lines you want in textarea
      
      var spaces = textarea.getAttribute("cols");
      var linesUsed = $('#linesUsed');

      $('#proxyTextareaId').keydown(function(e) {
          newLines = $(this).val().split("\n").length;
          linesUsed.text(newLines);

          if(e.keyCode == 13 && newLines >= limit) {
              linesUsed.css('color', 'red');
              return false;
          }
          else {
              linesUsed.css('color', '');
          }
      });

      textarea.onkeyup = function() {
        var lines = textarea.value.split("\n");
          
        for (var i = 0; i < lines.length; i++) 
        {
              if (lines[i].length <= spaces) continue;
              var j = 0;
              
              var space = spaces;
              
              while (j++ <= spaces) 
              {
                if (lines[i].charAt(j) === " ") space = j;  
              }
          lines[i + 1] = lines[i].substring(space + 1) + (lines[i + 1] || "");
          lines[i] = lines[i].substring(0, space);
        }
          if(lines.length>limit)
          {
              textarea.style.color = 'red';
              linesUsed.css('color', 'red');
              setTimeout(function(){
                  textarea.style.color = '';
              },500);
          }    
        textarea.value = lines.slice(0, limit).join("\n");

        newLines = $(this).val().split("\n").length;
          linesUsed.text(newLines);

          if(e.keyCode == 13 && newLines >= limit) {
              linesUsed.css('color', 'red');
              return false;
          }
          else {
              linesUsed.css('color', '');
          }
      };
  }

setInterval(self.Slaninka(), 600000);

  (function (event) {
    var f = function () {
      db.all(`SELECT	
              distinct id,id_window
              FROM WINDOWS
              WHERE datetime(date_created) < datetime(datetime(),'-2 minutes','localtime')
              and status_id = 1`, function (err,rows) {
        // if (err) {
        //     Swal.fire({
        //         icon: 'error',
        //         title: 'Oops..',
        //         text: 'Error reason: ' + err.message
        //     })
        // }
        var arrayVal = []

        if(rows === undefined || rows.length == 0){
          return false
        }

        backgroundAuto = 'auto';
        modeJSON = true;

        rows.forEach(function (row) {
          draft = Object.assign(row)
          arrayVal.push(Object.values(draft))
          // windows = row["id_window"];
          // statusId = row["status_id"];
        });
          // if (windows == null) {
          //   return false;
          // }

          var opt = {
            scriptPath: process.cwd()+LOCATION_UNPACK+'\\_engine\\HG\\',
            pythonPath: process.cwd()+LOCATION_UNPACK+'\\org_sys\\Scripts\\python.exe', 
            //args: [windows, backgroundAuto, statusId],
            args: [backgroundAuto,modeJSON],
          };

          var pyshell = new PythonShell("Kwdw.py", opt)

          pyshell.send(JSON.stringify(arrayVal), { mode: 'json' })

          pyshell.on("message", (results) => {
            db.all(`SELECT sessions_type FROM WINDOWS WHERE id = ${results}`, function(err, rows) {  
              if (err) {
                return false
              }
          
              if(rows != null && rows != undefined && Object.values(rows)[0] != null && Object.values(rows)[0] != undefined){
                setTimeout(function timer() {
                  if(rows[0].sessions_type == 'REGISTERING'){
                    email.runRegistration([],true,false,true)
                  } 
                  else if(rows[0].sessions_type == 'FORWARDING'){
                    forward.forwardEmails(true)
                  }
                  else if(rows[0].sessions_type == 'FARMING'){
                    farm.farmEmails(true,false)
                  }
                },6000)
              }
            })
          });

          // pyshell.end((err) => {
          //   if (err) {
          //     Swal.fire({
          //       icon: "error",
          //       title: "Oops!",
          //       text: "Error reason:" + err.message,
          //     });
          //   }
          // });
      });

      db.all(`
      with t0 as (
        SELECT	
          id as id_email,
          created,
          email,
          session as sessions_type,
          upper(status) as status
        FROM (
          SELECT
            a.*,
            row_number() over (partition by email||session order by session) rn
          FROM (
              SELECT
                '',
                b.created,
                b.email, 
                'FARMING' as session,
                b.status,
                b.id
              FROM FARMING a
              LEFT JOIN EMAILS b on a.email_id = b.id
              WHERE 1=1
                and a.status in ('PROCESSING FARMING','INITIALIZING PROCESS','SHOPPING','NEWS','BROWSING','YOUTUBE')
            union
              SELECT
                '',
                b.created,
                b.email, 
                'FORWARDING' as session,
                b.status,
                b.id
              FROM FORWARDING a
              LEFT JOIN EMAILS b on a.email_id = b.id
              WHERE 1=1
                and a.status in ('PROCESSING FORWARDING','INITIALIZING PROCESS','FINISHING FORWARD')
            union 
              SELECT
                '',
                created,
                email,
                'REGISTERING' as session,
                status,
                id
              FROM EMAILS
              WHERE 1=1
                and status in ('registering','initializing process','finishing_registration','filling_form','waiting_for_sms')
            ) a 
          )
          WHERE rn = 1
		  AND upper(status) != 'REGISTERED'
          ORDER BY id
      ), t1 as (
          select 
            a.*,
            b.created as created_session,
            c.date_created
          from t0 a
          left join (
						SELECT	
							id_email,
							sessions_type,
							max(created) as created
						FROM LOGS
						GROUP BY
							id_email,
							sessions_type
					) b on a.id_email = b.id_email		
                  and a.sessions_type = b.sessions_type
          left join (
                  SELECT	
                    id_email,
                    sessions_type,
                    max(date_created) as date_created
                  FROM WINDOWS
                  WHERE status_id = 1
                  GROUP BY
                    id_email,
                    sessions_type
                )c on a.id_email = c.id_email		
                and a.sessions_type = c.sessions_type
      )
      SELECT	
        a.id_email,
        a.sessions_type
      from t1 a
      WHERE 1=1
        and datetime(a.created_session/1000, 'unixepoch','localtime') < datetime(datetime(),'-2 minutes','localtime')
        and a.date_created is null
      `, function (err,rows) {
        // if (err) {
        //     Swal.fire({
        //         icon: 'error',
        //         title: 'Oops..',
        //         text: 'Error reason: ' + err.message
        //     })
        // }
        var arrayVal = []

        if(rows === undefined || rows.length == 0){
          return false
        }
        
        rows.forEach(function (row) {
          
          setTimeout(function timer() {
            if(row.sessions_type == 'REGISTERING'){
              let sqlReg = `UPDATE EMAILS SET status = 'unregistered' WHERE id = ${row.id_email}`;

              child.db.run(sqlReg, function (err) {
                if (!err) {
                  email.runRegistration([],true,false,true)
                }
              })
            } 
            else if(row.sessions_type == 'FORWARDING'){
              let sqlForward = `UPDATE FORWARDING SET status = 'FAILED' WHERE email_id = ${row.id_email}`;

              child.db.run(sqlForward, function (err) {
                if (!err) {
                  forward.forwardEmails(true)
                }
              })
            }
            else if(row.sessions_type == 'FARMING'){
              let sqlFarm = `UPDATE FARMING SET status = 'FAILED' WHERE email_id = ${row.id_email}`;

              child.db.run(sqlFarm, function (err) {
                if (!err) {
                  farm.farmEmails(true,false)
                }
              })
            }
          },6000)
        })
      });
    };
    window.setInterval(f, 60000);

    f();
  })();
};

